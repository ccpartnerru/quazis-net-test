<?php
namespace console\controllers;

use common\models\User;
use \yii\helpers\BaseConsole;
use yii\helpers\Console;

class UsersController extends DefaultController
{
    public function actionCreate()
    {
        do {
            $errorMessage = '';
            $username = BaseConsole::input("Username: ");

            if (User::findByUsername($username) instanceof User) {
                $errorMessage = 'User with this "username" already exists.';
                $this->displayError($errorMessage);
            }

        } while (!empty($errorMessage));

        do {
            $errorMessage = '';
            $email = BaseConsole::input("Email: ");

            if (User::findByEmail($email) instanceof User) {
                $errorMessage = 'User with this "email" already exists.';
                $this->displayError($errorMessage);
            }

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $errorMessage = $email . ' is not a valid email address.';
                $this->displayError($errorMessage);
            }

        } while (!empty($errorMessage));

        do {
            $errorMessage = '';
            $password = BaseConsole::input("Password: ");

            if (empty($password)) {
                $errorMessage = 'Password cannot be empty.';
                $this->displayError($errorMessage);
            }

        } while (!empty($errorMessage));

        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();

        if ($user->save()) {
            $this->displaySuccess('User created successfully!');

            $this->displayInfo('Username: ' . $username);
            $this->displayInfo('Email: ' . $email);
            $this->displayInfo('Password: ' . $password);
        } else {
            $this->displayError('Error creating user!');
        }
    }
}