<?php
namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

class DefaultController extends Controller
{
    protected function displayError($message)
    {
        return $this->stdout($message . PHP_EOL, Console::BG_RED);
    }

    protected function displaySuccess($message)
    {
        return $this->stdout($message . PHP_EOL, Console::BG_GREEN);
    }

    protected function displayInfo($message)
    {
        return $this->stdout($message . PHP_EOL);
    }
}