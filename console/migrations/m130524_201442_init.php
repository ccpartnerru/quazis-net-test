<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'init_db_bp.sql'));
    }

    public function safeDown()
    {
        echo "This migration cannot be reverted.\n";

        return false;
    }
}
