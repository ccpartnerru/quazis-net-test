<?php

use yii\db\Migration;

/**
 * Class m190115_144138_add_safe_columns_for_stat_click_table
 */
class m190115_144138_add_safe_columns_for_stat_click_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('stat_click', 'is_safe', $this->tinyInteger(4)->null()->after('bot_reason'));
        $this->addColumn('stat_click', 'not_safe_reason', $this->tinyInteger(4)->null()->after('is_safe'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190115_144138_add_safe_columns_for_stat_click_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190115_144138_add_safe_columns_for_stat_click_table cannot be reverted.\n";

        return false;
    }
    */
}
