/*
Navicat MySQL Data Transfer

Source Server         : localhost OpenServer
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : quazis_net

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2019-01-11 18:16:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for link
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source_partner_id` int(11) NOT NULL,
  `campaign` varchar(255) NOT NULL,
  `adgroup` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `keyword` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`key`),
  KEY `source_partner_id` (`source_partner_id`,`campaign`,`adgroup`,`category`,`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for page_zone_feed_custom_settings
-- ----------------------------
DROP TABLE IF EXISTS `page_zone_feed_custom_settings`;
CREATE TABLE `page_zone_feed_custom_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_zone_rule_group_id` int(11) NOT NULL,
  `order_number` int(11) NOT NULL,
  `feed_partner_id` int(11) NOT NULL,
  `items_count` int(11) DEFAULT NULL,
  `keyword_replacement` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_zone_rule_group_id` (`page_zone_rule_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for page_zone_rule_group
-- ----------------------------
DROP TABLE IF EXISTS `page_zone_rule_group`;
CREATE TABLE `page_zone_rule_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_section_name` varchar(255) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `page_zone_name` varchar(255) NOT NULL,
  `source_partner_id` int(11) NOT NULL,
  `inbound_keyword` varchar(255) DEFAULT NULL,
  `feed_sort_id` int(11) NOT NULL,
  `items_count` int(11) NOT NULL,
  `keyword_replacement` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_section_name` (`page_section_name`,`page_name`,`page_zone_name`,`source_partner_id`,`inbound_keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for page_zone_rule_weekday_time
-- ----------------------------
DROP TABLE IF EXISTS `page_zone_rule_weekday_time`;
CREATE TABLE `page_zone_rule_weekday_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_zone_rule_group_id` int(11) NOT NULL,
  `week_day` tinyint(11) NOT NULL,
  `hour` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_zone_rule_group_id` (`page_zone_rule_group_id`,`week_day`,`hour`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for search_keyword
-- ----------------------------
DROP TABLE IF EXISTS `search_keyword`;
CREATE TABLE `search_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for search_keyword_related
-- ----------------------------
DROP TABLE IF EXISTS `search_keyword_related`;
CREATE TABLE `search_keyword_related` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_keyword_id` int(11) NOT NULL,
  `keyword_related` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `search_keyword_id` (`search_keyword_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for stat_click
-- ----------------------------
DROP TABLE IF EXISTS `stat_click`;
CREATE TABLE `stat_click` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `market_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `request_uri` varchar(1024) DEFAULT NULL,
  `request_refer` varchar(1024) DEFAULT NULL,
  `keyword` varchar(255) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `link_key` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `is_bot` tinyint(4) DEFAULT NULL,
  `bot_reason` tinyint(4) DEFAULT NULL,
  `inbound_stat_click_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
