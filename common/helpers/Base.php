<?php

namespace common\helpers;

class Base {

    public static function array_get($array, $key, $default = null)
    {
        if( ! is_array($array) && ! is_iterable($array) ) {
            throw new \Exception(__FUNCTION__ . "(). The 1st argument should be array.");
        }

        if( isset($array[$key]) ) return $array[$key];

        $key = explode('.', $key);

        $last = [];

        while(count($key) > 1)
        {
            $last[] = array_pop($key);

            $index = implode('.', $key);

            if( isset($array[$index]) )
            {
                return self::array_get($array[$index], implode('.', $last), $default);
            }
        }

        return $default;
    }

    /**
     * @param float $rating
     * @return float
     */
    public static function roundHalfRating($rating) {
        $rating = round($rating, 1);

        $wholePart = intval($rating);
        $fraction = $rating - intval($rating);

        if ($fraction >= 0.25 && $fraction <= 0.75) {
            $fraction = 0.5;
        } elseif ($fraction < 0.25) {
            $fraction = 0;
        } else {
            $wholePart += 1;
            $fraction = 0;
        }

        return $wholePart + $fraction;
    }

    public static function ratingHalfFormat($rating) {
        $rating = self::roundHalfRating($rating);

        if (is_int($rating)) {
            return $rating;
        }

        return number_format($rating, 1);
    }

}