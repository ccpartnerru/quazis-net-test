<?php

namespace common\helpers;

use Yii;
use common\models\Market;

class SiteSettings
{
    const DEFAULT_SITE_MARKET_ID = Market::MARKET_ID_US;

    public static function getSiteMarketId()
    {
        if (!empty(Yii::$app->params['site_market_id'])) {
            return Yii::$app->params['site_market_id'];
        }

        return self::DEFAULT_SITE_MARKET_ID;
    }
}