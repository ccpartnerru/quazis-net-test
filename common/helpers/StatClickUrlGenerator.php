<?php

namespace common\helpers;

use Yii;

class StatClickUrlGenerator {
    const DEFAULT_REQUEST_URL_PARAM = 'request_url';
    const DEFAULT_REQUEST_REFER_PARAM = 'request_refer';
    const DEFAULT_REQUEST_FEED_PARTNER_ID_PARAM = 'feed_parner_id';

    /**
     * @return string
     */
    public static function getRequestUrlParam()
    {
        return self::DEFAULT_REQUEST_URL_PARAM;
    }

    /**
     * @return string
     */
    public static function getRequestReferParam()
    {
        return self::DEFAULT_REQUEST_REFER_PARAM;
    }

    /**
     * @return string
     */
    public static function getRequestFeedPartnerIdParam()
    {
        return self::DEFAULT_REQUEST_FEED_PARTNER_ID_PARAM;
    }

    /**
     * @return string
     */
    public static function generateInboundClickUrl()
    {
        $requestUrlParam = self::getRequestUrlParam();
        $inboundClickUrl = \yii\helpers\Url::toRoute([
            'stat-click/inbound',
            $requestUrlParam => base64_encode(\common\helpers\Url::getCurrentUrl())
        ], true);

        return $inboundClickUrl;
    }

    /**
     * @param string $clickUrl
     * @param int $feedPartnerId
     * @return string
     */
    public static function generateOutboundClickUrl($clickUrl = '', $feedPartnerId = 0)
    {
        $route = [];
        $route[] = 'stat-click/outbound';

        $requestUrlParam = self::getRequestUrlParam();
        $route[$requestUrlParam] = base64_encode(\common\helpers\Url::getCurrentUrl());

        if ($clickUrl) {
            $requestReferParam = self::getRequestReferParam();
            $route[$requestReferParam] = base64_encode($clickUrl);
        }

        if ($feedPartnerId) {
            $requestFeedPartnerIdParam = self::getRequestFeedPartnerIdParam();
            $route[$requestFeedPartnerIdParam] = $feedPartnerId;
        }

        $inboundClickUrl = \yii\helpers\Url::toRoute($route, true);

        return $inboundClickUrl;
    }

}