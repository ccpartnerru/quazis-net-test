<?php

namespace common\helpers;

use common\components\yii\helpers\UrlFrontend;
use Yii;

class Url {
    const DEFAULT_KEYWORD_PARAM         = 'q';
    const DEFAULT_KEYWORD               = 'refrigerator';
    const DEFAULT_LINK_KEY_PARAM        = 'link_key';
    const DEFAULT_CATEGORY_NAME_PARAM   = 'cat';
    const DEFAULT_MIN_PRICE_PARAM       = 'min_price';
    const DEFAULT_MAX_PRICE_PARAM       = 'max_price';
    const DEFAULT_SORT_PARAM            = 'sort';
    const DEFAULT_PAGE_NUMBER_PARAM     = 'p';
    const DEFAULT_ITEM_TITLE_PARAM      = 'it';

    /**
     * @return string
     */
    public static function getItemTitleParam()
    {
        return self::DEFAULT_ITEM_TITLE_PARAM;
    }

    /**
     * @return string
     */
    public static function getKeywordParam()
    {
        return self::DEFAULT_KEYWORD_PARAM;
    }

    /**
     * @return string
     */
    public static function getPageNumberParam()
    {
        return self::DEFAULT_PAGE_NUMBER_PARAM;
    }

    /**
     * @return string
     */
    public static function getCategoryNameParam()
    {
        return self::DEFAULT_CATEGORY_NAME_PARAM;
    }

    /**
     * @return string
     */
    public static function getMinPriceParam()
    {
        return self::DEFAULT_MIN_PRICE_PARAM;
    }

    /**
     * @return string
     */
    public static function getMaxPriceParam()
    {
        return self::DEFAULT_MAX_PRICE_PARAM;
    }

    /**
     * @return string
     */
    public static function getSortParam()
    {
        return self::DEFAULT_SORT_PARAM;
    }

    /**
     * @return string
     */
    public static function getDefaultKeyword()
    {
        return self::DEFAULT_KEYWORD;
    }

    /**
     * @return string
     */
    public static function getLinkKeyParam()
    {
        return self::DEFAULT_LINK_KEY_PARAM;
    }

    /**
     * @param boolean $useDefaultKeyword - if keyword in url empty, return default keyword
     * @return string
     */
    public static function getKeyword($useDefaultKeyword = true)
    {
        $keywordParam = self::getKeywordParam();
        $keyword = Yii::$app->request->get($keywordParam);

        if (empty($keyword) && $useDefaultKeyword) {
            $keyword = self::getDefaultKeyword();
        }

        return $keyword;
    }

    /**
     * @return null|string
     */
    public static function getItemTitle()
    {
        $itemTitleParam = self::getItemTitleParam();
        return Yii::$app->request->get($itemTitleParam);
    }

    /**
     * @param bool $useDefaultPageNumber
     * @return int|null
     */
    public static function getPageNumber($useDefaultPageNumber = true)
    {
        $param = self::getPageNumberParam();
        $pageNumber = Yii::$app->request->get($param);

        if (empty($pageNumber) && $useDefaultPageNumber) {
            $pageNumber = 1;
        }

        return $pageNumber;
    }

    /**
     * @return null|float
     */
    public static function getMinPrice()
    {
        $param = self::getMinPriceParam();
        $value = Yii::$app->request->get($param);

        return is_null($value) ? null : floatval($value);
    }

    /**
     * @return null|float
     */
    public static function getMaxPrice()
    {
        $param = self::getMaxPriceParam();
        $value = Yii::$app->request->get($param);

        return is_null($value) ? null : floatval($value);
    }

    /**
     * @return null|string
     */
    public static function getSort()
    {
        $param = self::getSortParam();
        $value = Yii::$app->request->get($param);

        return is_null($value) ? null : $value;
    }

    /**
     * @return string|null
     */
    public static function getCategoryName()
    {
        $param = self::getCategoryNameParam();
        $value = Yii::$app->request->get($param);

        return $value;
    }

    /**
     * @return string|null
     */
    public static function getLinkKey()
    {
        $paramName = self::getLinkKeyParam();

        return Yii::$app->request->get($paramName);
    }

    /**
     * @param $url
     * @return null|string
     */
    public static function getLinkKeyFromUrl($url)
    {
        $urlArray = parse_url($url);

        if (isset($urlArray['query'])) {
            parse_str($urlArray['query'], $query);

            $paramName = self::getLinkKeyParam();

            if (isset($query[$paramName])) {
                return $query[$paramName];
            }
        }

        return null;
    }

    /**
     * @param $url
     * @return null|string
     */
    public static function getSourceClickIdFromUrl($url)
    {
        $urlArray = parse_url($url);

        if (isset($urlArray['query'])) {
            parse_str($urlArray['query'], $query);

            $sourceClickIdParamNames = [
                'gclid',
                'msclkid',
                'auclid'
            ];

            if ($sourceClickIdParamNames && count($sourceClickIdParamNames)) {
                foreach ($sourceClickIdParamNames as $sourceClickIdParamName) {
                    if (isset($query[$sourceClickIdParamName])) {
                        return $query[$sourceClickIdParamName];
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param $url
     * @return null|string
     */
    public static function getKeywordFromUrl($url)
    {
        $urlArray = parse_url($url);

        if (isset($urlArray['query'])) {
            parse_str($urlArray['query'], $query);

            $keywordParam = self::getKeywordParam();

            if (isset($query[$keywordParam])) {
                return urldecode($query[$keywordParam]);
            }
        }

        return null;
    }

    /**
     * @return bool|string|null
     */
    public static function getRequestUrl()
    {
        $requestUrlParam = StatClickUrlGenerator::getRequestUrlParam();
        $requestUrl = Yii::$app->request->get($requestUrlParam);

        return $requestUrl ? base64_decode($requestUrl) : null;
    }

    /**
     * @return bool|string|null
     */
    public static function getRequestRefer()
    {
        $requestReferParam = StatClickUrlGenerator::getRequestReferParam();
        $requestRefer = Yii::$app->request->get($requestReferParam);

        return $requestRefer ? base64_decode($requestRefer) : null;
    }

    /**
     * @return int|null
     */
    public static function getRequestFeedPartnerId()
    {
        $requestFeedPartnerIdParam = StatClickUrlGenerator::getRequestFeedPartnerIdParam();
        $requestFeedPartnerId = Yii::$app->request->get($requestFeedPartnerIdParam);

        return intval($requestFeedPartnerId) > 0 ? intval($requestFeedPartnerId) : null;
    }

    /**
     * @return string
     */
    public static function getCurrentUrl()
    {
        return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    }

    /**
     * @param array $params - array of variable parameters, where key is query param name, value - is query param value
     * @param bool|string $url
     * @return string
     */
    public static function modifyUrlParams($params, $url = false){
        if ($url === false){
            $url = self::getCurrentUrl();
        }

        $url_array = parse_url($url);

        parse_str($url_array['query'], $query_array);
        foreach ($params as $key => $value) {
            $query_array[$key] = $value;
        }

        return $url_array['scheme'].'://'.$url_array['host'].$url_array['path'].'?'.http_build_query($query_array);
    }

    /**
     * @param string $keyword
     * @return string
     */
    public static function generateShoppingSearchUrl($keyword)
    {
        /** @var yii\web\UrlManager $urlManagerFrontend */
        $urlManagerFrontend = Yii::$app->urlManagerFrontend;

        return $urlManagerFrontend->createAbsoluteUrl([
            'shopping/search',
            self::getKeywordParam() => $keyword
        ], true);
    }

    /**
     * @param string $feedShortName
     * @param string $itemId
     * @param string|null $keyword
     * @return string
     */
    public static function generateShoppingDetailUrl($feedShortName, $itemId, $keyword = null)
    {
        /** @var yii\web\UrlManager $urlManagerFrontend */
        $urlManagerFrontend = Yii::$app->urlManagerFrontend;

        $keywordParam = self::getKeywordParam();

        return $urlManagerFrontend->createAbsoluteUrl([
            'shopping/detail',
            'feedPartnerShortName' => $feedShortName,
            'itemId' => $itemId,
            $keywordParam => is_null($keyword) ? self::getKeyword() : $keyword
        ]);
    }

    /**
     * @param string $categoryName
     * @return string
     */
    public static function generateShoppingCategoryUrl($categoryName)
    {
        /** @var yii\web\UrlManager $urlManagerFrontend */
        $urlManagerFrontend = Yii::$app->urlManagerFrontend;

        return $urlManagerFrontend->createAbsoluteUrl([
            'shopping/category',
            self::getCategoryNameParam() => $categoryName,
        ]);
    }
}