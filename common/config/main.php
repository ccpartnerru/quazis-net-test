<?php

$params = [
    'site_market_id' => \common\models\Market::MARKET_ID_US
];

$params = array_merge(
    $params,
    require __DIR__ . '/../../frontend/config/page_sections.php'
);

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\redis\Cache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'user' => [
            'class' => 'common\components\User',
            'identityClass' => 'common\models\User',
        ],
        'session' => [
            'class' => 'common\components\Session',
        ],
    ],
    'controllerMap' => [
        'migration' => [
            'class' => 'bizley\migration\controllers\MigrationController',
        ],
    ],
    'params' => $params
];
