<?php


namespace common\controllers;

use Yii;
use common\components\User;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package common\controllers
 *
 * @property User $user
 */
class DefaultController extends Controller
{
    /**
     * @return User
     */
    public function getUser()
    {
        return Yii::$app->user;
    }
}