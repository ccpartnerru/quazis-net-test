<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PageZoneSettings;

/**
 * PageZoneSettingsSearch represents the model behind the search form of `common\models\PageZoneSettings`.
 */
class PageZoneSettingsSearch extends PageZoneSettings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'source_partner_id', 'items_number'], 'integer'],
            [['page_section_name', 'page_name', 'page_zone_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageZoneSettings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'source_partner_id' => $this->source_partner_id,
            'items_number' => $this->items_number,
        ]);

        $query->andFilterWhere(['like', 'page_section_name', $this->page_section_name])
            ->andFilterWhere(['like', 'page_name', $this->page_name])
            ->andFilterWhere(['like', 'page_zone_name', $this->page_zone_name]);

        return $dataProvider;
    }
}
