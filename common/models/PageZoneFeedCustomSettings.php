<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_zone_feed_custom_settings".
 *
 * @property int $id
 * @property int $page_zone_rule_group_id
 * @property int $order_number
 * @property int $feed_partner_id
 * @property int $items_count
 * @property string $keyword_replacement
 */
class PageZoneFeedCustomSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_zone_feed_custom_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_zone_rule_group_id', 'order_number', 'feed_partner_id'], 'required'],
            [['page_zone_rule_group_id', 'order_number', 'feed_partner_id', 'items_count'], 'integer'],
            [['keyword_replacement'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_zone_rule_group_id' => 'Page Zone Rule Group ID',
            'order_number' => 'Order Number',
            'feed_partner_id' => 'Feed Partner ID',
            'items_count' => 'Items Count',
            'keyword_replacement' => 'Keyword Replacement',
        ];
    }
}
