<?php

namespace common\models;

use Yii;
use yii\base\Model;

class PageZone extends aPageZone
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /** @var  int */
    public $items_count;

    /** @var  Page */
    public $page;

    /** @var boolean */
    public $is_feed_settings_editable;

    /** @var  PageZoneFeedSort */
    public $page_zone_feed_sort;

    /** @var  PageZoneFeedSettings[] */
    public $page_zone_feed_settings;

    /**
     * @param Page $page
     * @return PageZone[]
     */
    public static function getAllPageZoneByPage($page) {
        $zones = [];

        if ($page->page_config && isset($page->page_config['zones']) && count($page->page_config['zones'])) {
            $zonesConfig = $page->page_config['zones'];

            foreach ($zonesConfig as $zoneConfig) {
                $zoneName = $zoneConfig['name'];
                $zone = self::getByPageAndPageZoneName($page, $zoneName);
                if ($zone) {
                    $zones[] = $zone;
                }
            }
        }

        return $zones;
    }

    /**
     * @param Page $page
     * @param string $pageZoneName
     * @return PageZone|null
     */
    public static function getByPageAndPageZoneName($page, $pageZoneName)
    {
        $zoneConfig = self::getPageZoneConfigFromPageConfigByName($page->page_config, $pageZoneName);

        if ($zoneConfig) {
            $model = new self();
            $model->name        = $zoneConfig['name'];
            $model->description = isset($zoneConfig['description']) ? $zoneConfig['description'] : '';
            $model->items_count = $zoneConfig['items_count'];
            $model->page        = $page;
            if (isset($zoneConfig['is_feed_settings_editable']) && is_bool($zoneConfig['is_feed_settings_editable'])) {
                $model->is_feed_settings_editable = $zoneConfig['is_feed_settings_editable'];
            } else {
                $model->is_feed_settings_editable = false;
            }

            if (isset($zoneConfig['feed_sort_id']) && is_int($zoneConfig['feed_sort_id'])) {
                $page_zone_feed_sort = PageZoneFeedSort::getById($zoneConfig['feed_sort_id']);
                if ($page_zone_feed_sort) {
                    $model->page_zone_feed_sort = $page_zone_feed_sort;
                }
            }
            // use SEQUENCE sort by default
            if (!$model->page_zone_feed_sort) {
                $model->page_zone_feed_sort = PageZoneFeedSort::getDefaultSort();
            }

            if (isset($zoneConfig['feed_settings']) && is_array($zoneConfig['feed_settings'])) {
                $feedSettings = $zoneConfig['feed_settings'];
                foreach ($feedSettings as $feedSettingsItem) {
                    if (!isset($feedSettingsItem['feed_id'])) continue;
                    $feedId = $feedSettingsItem['feed_id'];
                    $feed = Feed::getById($feedId);
                    $itemsCount = isset($feedSettingsItem['items_count']) ? intval($feedSettingsItem['items_count']) : $model->items_count;

                    $pageZoneFeedSettings = PageZoneFeedSettings::createModel($feed, $itemsCount, '', $model);

                    $model->page_zone_feed_settings[] = $pageZoneFeedSettings;
                }
            }

            return $model;
        }

        return null;
    }

    /**
     * @param string $pageConfig
     * @param string $pageZoneName
     * @return null|array
     */
    protected function getPageZoneConfigFromPageConfigByName($pageConfig, $pageZoneName)
    {
        $zonesConfig = ($pageConfig && isset($pageConfig['zones'])) ? $pageConfig['zones'] : null;
        if ($zonesConfig && count($zonesConfig) > 0) {
            foreach ($zonesConfig as $zoneConfig) {
                if (isset($zoneConfig['name']) && $zoneConfig['name'] == $pageZoneName) {
                    return $zoneConfig;
                }
            }
        }

        return null;
    }

    /**
     * @param string $pageSectionName
     * @param string $pageName
     * @param string $pageZoneName
     * @return PageZone|null
     */
    public static function getByPageSectionNameAndPageNameAndPageZoneName($pageSectionName, $pageName, $pageZoneName)
    {
        $page = Page::getPageByPageSectionNameAndPageName($pageSectionName, $pageName);
        return $page->getPageZoneByName($pageZoneName);
    }
}
