<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Partner extends Model
{
    const PARTNER_ID_YAHOO          = 1;
    const PARTNER_ID_EBAY           = 2;
    const PARTNER_ID_ALIEXPRESS     = 3;
    const PARTNER_ID_WALMART        = 4;
    const PARTNER_ID_ADWORDS_SEARCH = 5;

    /** @var  int */
    public $id;

    /** @var  string */
    public $name;

    /** @var  string */
    public $short_name;

    /**
     * @return array
     */
    protected static function config() {
        return [
            self::PARTNER_ID_YAHOO => [
                'name' => 'Yahoo',
                'short_name' => 'yh'
            ],
            self::PARTNER_ID_EBAY => [
                'name' => 'eBay',
                'short_name' => 'ebay'
            ],
            self::PARTNER_ID_ALIEXPRESS => [
                'name' => 'Aliexpress',
                'short_name' => 'axs'
            ],
            self::PARTNER_ID_WALMART => [
                'name' => 'Walmart',
                'short_name' => 'wm'
            ],
            self::PARTNER_ID_ADWORDS_SEARCH => [
                'name' => 'Adwords Search',
                'short_name' => 'adw'
            ],
        ];
    }

    /**
     * @return int[]
     */
    public static function getAllIds()
    {
        return array_keys(self::config());
    }

    /**
     * @return Partner[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::getAllIds() as $id) {
            $model = static::getById($id);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @return Partner[]
     */
    public static function getAllSortByName()
    {
        $models = self::getAll();

        uasort($models, function(Partner $itemA, Partner $itemB) {
            return strcmp($itemA->name, $itemB->name);
        });

        return $models;
    }

    /**
     * @param int $id
     * @return Partner|null
     */
    public static function getById($id)
    {
        $modelConfig = self::getConfigById($id);

        if ($modelConfig) {
            $className = get_called_class();
            if (empty($className) || !class_exists($className) || $className instanceof Partner) {
                $className = 'self';
            }

            $model = new $className();
            $model->id          = $id;
            $model->name        = $modelConfig['name'];
            $model->short_name  = $modelConfig['short_name'];
            return $model;
        }

        return null;
    }

    /**
     * @param string $shortName
     * @return Partner|null
     */
    public static function getByShortName($shortName)
    {
        $modelConfig = self::getConfigByShortName($shortName);

        if ($modelConfig) {
            $id = $modelConfig['id'];
            return self::getById($id);
        }

        return null;
    }

    /**
     * @param $id
     * @return Partner|null
     */
    public static function findOne($id)
    {
        return self::getById($id);
    }

    /**
     * @param int $id
     * @return array|null
     */
    protected static function getConfigById($id)
    {
        if (!in_array($id, self::getAllIds())) {
            return null;
        }

        $config = self::config();
        if (!isset($config[$id])) {
            return null;
        }

        $modelConfig = $config[$id];
        $modelConfig['id'] = $id;

        return $modelConfig;
    }

    /**
     * @param string $shortName
     * @return array|null
     */
    protected static function getConfigByShortName($shortName)
    {
        $ids = self::getAllIds();
        foreach ($ids as $id) {
            $model = self::getById($id);
            if ($shortName == $model->short_name) {
                return self::getConfigById($model->id);
            }

        }

        return null;
    }
}
