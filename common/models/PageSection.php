<?php

namespace common\models;

use common\helpers\Url;
use Yii;
use yii\base\Model;

/**
 * Class PageSection
 * @package common\models
 *
 * @property Page[] $pages
 */
class PageSection extends Model
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /** @var array  */
    public $section_config = [];

    /**
     * @param array $modelConfig
     * @return PageSection|null
     */
    protected static function createModelByModelConfig($modelConfig)
    {
        if ($modelConfig) {
            $model = new self();
            $model->name           = $modelConfig['name'];
            $model->description    = isset($modelConfig['description']) ? $modelConfig['description'] : '';
            $model->section_config = $modelConfig;

            return $model;
        }

        return null;
    }

    /**
     * @return Page[]
     */
    protected function getPages() {
        static $_pages;

        if (is_null($_pages)) {
            $_pages = Page::getAllPageByPageSection($this);
        }

        return $_pages;
    }

    /**
     * @return PageSection[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::config() as $configPageSection) {
            $model = self::createModelByModelConfig($configPageSection);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @return array
     */
    protected static function config()
    {
        return Yii::$app->params['sections'];
    }

    /**
     * @param string $name
     * @return array|null
     */
    public static function getConfigPageSectionByName($name)
    {
        $config = self::config();

        foreach ($config as $id => $configPageSection) {
            if (isset($configPageSection['name']) && $configPageSection['name'] == $name) {
                return $configPageSection;
            }
        }

        return null;
    }

    /**
     * @param string $name
     * @return PageSection|null
     */
    public static function getPageSectionByName($name)
    {
        $configPage = self::getConfigPageSectionByName($name);

        return self::createModelByModelConfig($configPage);

    }

    /**
     * @param string $pageName
     * @return Page|null
     */
    public function getPageByName($pageName)
    {
        return Page::getPageByPageSectionAndPageName($this, $pageName);
    }
}
