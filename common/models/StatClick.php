<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "stat_click".
 *
 * @property int $id
 * @property int $type
 * @property int $partner_id
 * @property int $market_id
 * @property int $device_id
 * @property string $created_at
 * @property string $request_uri
 * @property string $request_refer
 * @property string $keyword
 * @property string $user_ip
 * @property string $user_agent
 * @property string $link_key
 * @property string $session_id
 * @property int $is_bot
 * @property int $bot_reason
 * @property int $is_safe
 * @property int $not_safe_reason
 * @property int $inbound_stat_click_id
 */
class StatClick extends \yii\db\ActiveRecord
{
    const STAT_CLICK_TYPE_INBOUND  = 0;
    const STAT_CLICK_TYPE_OUTBOUND = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stat_click';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'partner_id', 'market_id', 'device_id', 'is_bot', 'bot_reason', 'inbound_stat_click_id', 'is_safe', 'not_safe_reason'], 'integer'],
            [['created_at'], 'safe'],
            [['keyword', 'user_ip', 'user_agent', 'link_key', 'session_id'], 'string', 'max' => 255],
            [['request_uri', 'request_refer'], 'string', 'max' => 1024]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'partner_id' => 'Partner ID',
            'market_id' => 'Market ID',
            'device_id' => 'Device ID',
            'created_at' => 'Created At',
            'request_uri' => 'Request Uri',
            'request_refer' => 'Request Refer',
            'keyword' => 'Keyword',
            'user_ip' => 'User Ip',
            'user_agent' => 'User Agent',
            'link_key' => 'Link Key',
            'session_id' => 'Session ID',
            'is_bot' => 'Is Bot',
            'bot_reason' => 'Bot Reason',
            'inbound_stat_click_id' => 'Inbound Stat Click ID',
            'is_safe' => 'Is safe',
            'not_safe_reason' => 'Not Safe Reason',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
