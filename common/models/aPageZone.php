<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

abstract class aPageZone extends Model
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /** @var  int */
    public $items_count;

    /** @var  Page */
    public $page;

    /** @var boolean */
    public $is_feed_settings_editable;

    /** @var  PageZoneFeedSort */
    public $page_zone_feed_sort;

    /** @var  PageZoneFeedSettings[] */
    public $page_zone_feed_settings;

    public static function getByPageAndPageZoneName($page, $pageZoneName) {
        throw new Exception('Override this method');
    }
}
