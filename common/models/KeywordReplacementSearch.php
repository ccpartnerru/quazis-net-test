<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\KeywordReplacement;

/**
 * KeywordReplacementSearch represents the model behind the search form of `common\models\KeywordReplacement`.
 */
class KeywordReplacementSearch extends KeywordReplacement
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'feed_partner_id', 'source_partner_id', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['keyword', 'replacement'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KeywordReplacement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'feed_partner_id' => $this->feed_partner_id,
            'source_partner_id' => $this->source_partner_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'keyword', $this->keyword])
            ->andFilterWhere(['like', 'replacement', $this->replacement]);

        return $dataProvider;
    }
}
