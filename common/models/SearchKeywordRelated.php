<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "search_keyword_related".
 *
 * @property int $id
 * @property int $search_keyword_id
 * @property string $keyword_related
 */
class SearchKeywordRelated extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'search_keyword_related';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['search_keyword_id', 'keyword_related'], 'required'],
            [['search_keyword_id'], 'integer'],
            [['keyword_related'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'search_keyword_id' => 'Search Keyword ID',
            'keyword_related' => 'Keyword Related',
        ];
    }
}
