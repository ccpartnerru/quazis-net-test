<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_zone_rule_weekday_time".
 *
 * @property int $id
 * @property int $page_zone_rule_group_id
 * @property string $week_day
 * @property string $hour
 */
class PageZoneRuleWeekdayTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_zone_rule_weekday_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_zone_rule_group_id', 'week_day', 'hour'], 'required'],
            [['page_zone_rule_group_id', 'week_day', 'hour'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_zone_rule_group_id' => 'Page Zone Rule Group ID',
            'week_day' => 'Week Day',
            'hour' => 'Hour',
        ];
    }

    /**
     * @param [][] $weekDayTimeData
     * @return array
     */
    public static function getActiveHoursWeekDay($weekDayTimeData)
    {
        $activeWeekDayHours = [];

        foreach ($weekDayTimeData as $day => $hours) {
            foreach ($hours as $hour => $isHourEnabled) {
                if (intval($isHourEnabled) == 0) continue;

                $activeWeekDayHours[$day][] = $hour;
            }
        }

        return $activeWeekDayHours;
    }
}
