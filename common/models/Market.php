<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Market extends Model
{
    const MARKET_ID_US = 1;
    const MARKET_ID_UK = 2;
    const MARKET_ID_FR = 3;
    const MARKET_ID_DE = 4;
    const MARKET_ID_CA = 5;
    const MARKET_ID_AU = 6;
    const MARKET_ID_ES = 7;
    const MARKET_ID_NL = 8;
    const MARKET_ID_BR = 9;
    const MARKET_ID_RU = 10;
    const MARKET_ID_MX = 11;
    const MARKET_ID_JP = 12;
    const MARKET_ID_AE = 13;
    const MARKET_ID_IT = 14;
    const MARKET_ID_DK = 15;
    const MARKET_ID_SE = 16;
    const MARKET_ID_NO = 17;
    const MARKET_ID_BE = 18;
    const MARKET_ID_IE = 19;

    /** @var  int */
    public $id;

    /** @var  string */
    public $name;

    /** @var  string */
    public $currency;

    /** @var  string */
    public $region;

    /**
     * @return array
     */
    protected static function config() {
        return [
            self::MARKET_ID_US => [
                'name' => 'US',
                'currency' => 'USD',
                'region' => 'US'
            ],
            self::MARKET_ID_UK => [
                'name' => 'UK',
                'currency' => 'GBP',
                'region' => 'EU'
            ],
            self::MARKET_ID_FR => [
                'name' => 'FR',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_DE => [
                'name' => 'DE',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_CA => [
                'name' => 'CA',
                'currency' => 'CAD',
                'region' => 'CA'
            ],
            self::MARKET_ID_AU => [
                'name' => 'AU',
                'currency' => 'AUD',
                'region' => 'AU'
            ],
            self::MARKET_ID_ES => [
                'name' => 'ES',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_NL => [
                'name' => 'NL',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_BR => [
                'name' => 'BR',
                'currency' => 'BRL',
                'region' => 'BR'
            ],
            self::MARKET_ID_RU => [
                'name' => 'RU',
                'currency' => 'RUB',
                'region' => 'RU'
            ],
            self::MARKET_ID_MX => [
                'name' => 'MX',
                'currency' => 'MXN',
                'region' => 'MX'
            ],
            self::MARKET_ID_JP => [
                'name' => 'JP',
                'currency' => 'JPY',
                'region' => 'JP'
            ],
            self::MARKET_ID_AE => [
                'name' => 'AE',
                'currency' => 'AED',
                'region' => 'AE'
            ],
            self::MARKET_ID_IT => [
                'name' => 'IT',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_DK => [
                'name' => 'DK',
                'currency' => 'DKK',
                'region' => 'EU'
            ],
            self::MARKET_ID_SE => [
                'name' => 'SE',
                'currency' => 'SEK',
                'region' => 'EU'
            ],
            self::MARKET_ID_NO => [
                'name' => 'NO',
                'currency' => 'NOK',
                'region' => 'EU'
            ],
            self::MARKET_ID_BE => [
                'name' => 'BE',
                'currency' => 'EUR',
                'region' => 'EU'
            ],
            self::MARKET_ID_IE => [
                'name' => 'IE',
                'currency' => 'EUR',
                'region' => 'EU'
            ]
        ];
    }

    /**
     * @return int[]
     */
    public static function getAllIds()
    {
        return array_keys(self::config());
    }

    /**
     * @return Market[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::getAllIds() as $id) {
            $model = static::getById($id);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param int $id
     * @return Market|null
     */
    public static function getById($id)
    {
        $modelConfig = self::getConfigById($id);

        if ($modelConfig) {
            $model = new self();
            $model->id = $id;
            $model->name        = $modelConfig['name'];
            $model->currency    = $modelConfig['currency'];
            $model->region      = $modelConfig['region'];

            return $model;
        }

        return null;
    }

    /**
     * @param string $name
     * @return Market|null
     */
    public static function getByName($name)
    {
        $modelConfig = self::getConfigByName($name);

        if ($modelConfig) {
            $id = $modelConfig['id'];
            return self::getById($id);
        }

        return null;
    }

    /**
     * @param $id
     * @return Market|null
     */
    public static function findOne($id)
    {
        return self::getById($id);
    }

    /**
     * @param int $id
     * @return array|null
     */
    protected static function getConfigById($id)
    {
        $config = self::config();
        if (isset($config[$id])) {
            $modelConfig = $config[$id];
            $modelConfig['id'] = $id;

            return $modelConfig;
        }

        return null;
    }

    /**
     * @param string $name
     * @return array|null
     */
    protected static function getConfigByName($name)
    {
        $ids = self::getAllIds();
        foreach ($ids as $id) {
            $model = self::getById($id);
            if ($name == $model->name) {
                return self::getConfigById($model->id);
            }

        }

        return null;
    }
}