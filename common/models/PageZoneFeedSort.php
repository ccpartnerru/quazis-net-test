<?php

namespace common\models;

use common\libraries\feedContentManager\entities\Item;
use Yii;
use yii\base\Model;

class PageZoneFeedSort extends Model
{
    const TYPE_ID_SEQUENCE   = 1;
    const TYPE_ID_ONE_BY_ONE = 2;

    /** @var  int */
    public $id;

    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /**
     * @return array
     */
    protected function config()
    {
        return [
            self::TYPE_ID_SEQUENCE => [
                'name' => 'in sequence',
                'description' => 'Every feed goes after another'
            ],
            self::TYPE_ID_ONE_BY_ONE => [
                'name' => 'one by one',
                'description' => 'Take on 1 item from each feed. Then we repeat this action until the items at the feeds end.'
            ]
        ];
    }

    /**
     * @return int[]
     */
    public static function getAllIds()
    {
        return array_keys(self::config());
    }

    /**
     * @return PageZoneFeedSort[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::getAllIds() as $id) {
            $model = static::getById($id);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param int $id
     * @return PageZoneFeedSort|null
     */
    public static function getById($id)
    {
        $modelConfig = self::getConfigById($id);

        if ($modelConfig) {
            $model = new self();
            $model->id          = $id;
            $model->name        = $modelConfig['name'];
            $model->description = $modelConfig['description'];

            return $model;
        }

        return null;
    }

    /**
     * @param $id
     * @return PageZoneFeedSort|null
     */
    public static function findOne($id)
    {
        return self::getById($id);
    }

    /**
     * @param int $id
     * @return array|null
     */
    protected static function getConfigById($id)
    {
        $config = self::config();
        if (isset($config[$id])) {
            return $config[$id];
        }

        return null;
    }

    /**
     * @param Item[][] $resultsItems
     * @return Item[]
     */
    public function mixItems($resultsItems) {
        return self::mixItemsBySortId($this->id, $resultsItems);
    }

    /**
     * Mix one or more resultItems arrays
     * @param int $sortId
     * @param Item[][] $resultsItems
     * @return Item[]
     */
    public static function mixItemsBySortId($sortId, $resultsItems)
    {
        switch ($sortId) {
            case self::TYPE_ID_SEQUENCE:
                return self::mixItemsSequence($resultsItems);
            case self::TYPE_ID_ONE_BY_ONE:
                return self::mixItemsOneByOne($resultsItems);
            default:
                return self::mixItemsSequence($resultsItems);
        }
    }

    /**
     * @param Item[][] $resultsItems
     * @return Item[]
     */
    private static function mixItemsOneByOne($resultsItems)
    {
        $mixResultItems = [];

        if (count($resultsItems)) {
            while (!self::isAllSubArraysEmpty($resultsItems)) {
                foreach ($resultsItems as &$resultItems) {
                    $resultItem = array_shift($resultItems);

                    if (!is_null($resultItem)) {
                        $mixResultItems[] = $resultItem;
                    }
                }
            }
        }

        return $mixResultItems;
    }

    private static function mixItemsSequence($resultsItems)
    {
        $mixResultItems = [];

        if (count($resultsItems)) {
            foreach ($resultsItems as $resultItems) {
                $mixResultItems = array_merge($mixResultItems, $resultItems);
            }
        }

        return $mixResultItems;
    }

    /**
     * @param [][] $arrays
     * @return bool
     */
    private static function isAllSubArraysEmpty($arrays)
    {
        foreach ($arrays as $array) {
            if (is_array($array) && count($array) > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return PageZoneFeedSort|null
     */
    public static function getDefaultSort()
    {
        return self::getById(self::TYPE_ID_SEQUENCE);
    }
}
