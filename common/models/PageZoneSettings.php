<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_zone_settings".
 *
 * @property int $id
 * @property string $page_section_name
 * @property string $page_name
 * @property string $page_zone_name
 * @property int $source_partner_id
 * @property int $items_number
 */
class PageZoneSettings extends \yii\db\ActiveRecord
{
    const SOURCE_PARTNER_ID_ALL = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_zone_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_section_name', 'page_name', 'page_zone_name', 'items_number'], 'required'],
            [['source_partner_id', 'items_number'], 'integer'],
            [['page_section_name', 'page_name', 'page_zone_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_section_name' => 'Page Section Name',
            'page_name' => 'Page Name',
            'page_zone_name' => 'Page Zone Name',
            'source_partner_id' => 'Source Partner ID',
            'items_number' => 'Items Number',
        ];
    }

    /**
     * @param string $pageSectionName
     * @param string $pageName
     * @param string $pageZoneName
     * @param int $sourcePartnerId
     * @return array|null|PageZoneSettings
     */
    public static function getBySectionAandPAgeAndZoneAndSourcePartnerId($pageSectionName, $pageName, $pageZoneName, $sourcePartnerId)
    {
        return self::find()
            ->where(['=', 'page_section_name', $pageSectionName])
            ->andWhere(['=', 'page_name', $pageName])
            ->andWhere(['=', 'page_zone_name', $pageZoneName])
            ->andWhere(['=', 'source_partner_id', $sourcePartnerId])
            ->one();
    }

    /**
     * @param string $pageSectionName
     * @param string $pageName
     * @param string $pageZoneName
     * @param int $sourcePartnerId
     * @return array|PageZoneSettings|null
     */
    public static function getPageZoneSettings($pageSectionName, $pageName, $pageZoneName, $sourcePartnerId)
    {
        $model = self::getBySectionAandPAgeAndZoneAndSourcePartnerId($pageSectionName, $pageName, $pageZoneName, $sourcePartnerId);

        if (!$model) {
            $model = self::getBySectionAandPAgeAndZoneAndSourcePartnerId($pageSectionName, $pageName, $pageZoneName, self::SOURCE_PARTNER_ID_ALL);
        }

        return $model;
    }
}
