<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "search_keyword".
 *
 * @property int $id
 * @property string $keyword
 * @property SearchKeywordRelated[] $relatedKeywords
 */
class SearchKeyword extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'search_keyword';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keyword'], 'required'],
            [['keyword'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Keyword',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedKeywords()
    {
        $cache = \Yii::$app->cache;
        $key = md5('relatedKeywords_'.mb_strtolower($this->keyword));
        if ($cache && $cache->exists($key)) {
            return unserialize($cache->get($key));
        }

        $activeQuery = $this->hasMany(SearchKeywordRelated::class, ['search_keyword_id' => 'id']);
        if ($cache) {
            $value = serialize($activeQuery);
            $duration = 86400; //24 * 3600
            $cache->set($key, $value, $duration);
        }

        return $activeQuery;
    }
}
