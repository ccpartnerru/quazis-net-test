<?php

namespace common\models;

use common\helpers\Url;
use Yii;
use yii\base\Model;

class PageZoneActive extends aPageZone
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /** @var  int */
    public $items_count;

    /** @var  Page */
    public $page;

    /** @var boolean */
    public $is_feed_settings_editable;

    /** @var  PageZoneFeedSort */
    public $page_zone_feed_sort;

    /** @var PageZone */
    public $pageZone;

    /** @var  PageZoneFeedSettings[] */
    public $page_zone_feed_settings;

    /**
     * @param Page $page
     * @param string $pageZoneName
     * @return PageZoneActive
     */
    public static function getByPageAndPageZoneName($page, $pageZoneName)
    {
        $model = new self();
        $pageZone = PageZone::getByPageAndPageZoneName($page, $pageZoneName);
        $model->pageZone = $pageZone;

        /** @var \common\components\User $user */
        $user = Yii::$app->user;

        //setup default settings from config
        $model->name                        = $pageZone->name;
        $model->description                 = $pageZone->description;
        $model->items_count                 = $pageZone->items_count;
        $model->page                        = $pageZone->page;
        $model->is_feed_settings_editable   = $pageZone->is_feed_settings_editable;
        $model->page_zone_feed_sort         = $pageZone->page_zone_feed_sort;
        $model->page_zone_feed_settings     = $pageZone->page_zone_feed_settings;

        //get zone settings by partner source id
        $pageSectionName = $model->pageZone->page->page_section->name;
        $pageName = $model->pageZone->page->name;
        $pageZoneName = $model->pageZone->name;
        $sourcePartnerId = $user->getSourcePartnerId() ? $user->getSourcePartnerId() : 0;
        $inboundKeyword = Url::getKeyword(false);
        $weekday = date('N') - 1; //0 - Monday
        $hour = date('G');

        /** @var PageZoneRuleGroup $pageZoneRuleGroup */
        $pageZoneRuleGroup = PageZoneRuleGroup::find()
            ->from(['pzrg' => PageZoneRuleGroup::tableName()])
            ->leftJoin(['pzrwt' => PageZoneRuleWeekdayTime::tableName()], 'pzrwt.page_zone_rule_group_id = pzrg.id')
            ->where(['=', 'pzrg.page_section_name', $pageSectionName])
            ->andWhere(['=', 'pzrg.page_name', $pageName])
            ->andWhere(['=', 'pzrg.page_zone_name', $pageZoneName])
            ->andWhere(['IN', 'pzrg.source_partner_id', [$sourcePartnerId, 0]])
            ->andWhere(['=', 'pzrg.inbound_keyword', $inboundKeyword])
            ->andWhere(['=', 'pzrwt.week_day', $weekday])
            ->andWhere(['=', 'pzrwt.hour', $hour])
            ->groupBy('pzrg.id')
            ->orderBy('pzrg.source_partner_id DESC')
            ->one();

        if ($pageZoneRuleGroup) {
            $model->items_count = $pageZoneRuleGroup->items_count;
            $model->page_zone_feed_sort = PageZoneFeedSort::getById($pageZoneRuleGroup->feed_sort_id);

            $feedCustomSettings = $pageZoneRuleGroup->feedCustomSettings;
            $model->page_zone_feed_settings = [];
            foreach ($feedCustomSettings as $feedCustomSettingsRow) {
                $feedPartnerId = $feedCustomSettingsRow->feed_partner_id;
                $feed = Feed::getById($feedPartnerId);
                $keywordReplacement = '';
                if (!empty($feedCustomSettingsRow->keyword_replacement)) {
                    $keywordReplacement = $feedCustomSettingsRow->keyword_replacement;
                } elseif (!empty($pageZoneRuleGroup->keyword_replacement)) {
                    $keywordReplacement = $pageZoneRuleGroup->keyword_replacement;
                }

                $itemsCount = $model->items_count;
                if ($feedCustomSettingsRow->items_count) {
                    $itemsCount = $feedCustomSettingsRow->items_count;
                }
                $model->page_zone_feed_settings[] = PageZoneFeedSettings::createModel($feed, $itemsCount, $keywordReplacement, $model->pageZone);
            }
        }

        return $model;
    }
}
