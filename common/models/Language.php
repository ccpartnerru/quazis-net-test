<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Language extends Model
{
    const LANGUAGE_CODE_EN_US = 'en-US';
    const LANGUAGE_CODE_RU_RU = 'ru-RU';

    /** @var  string */
    public $code;

    /**
     * @return array
     */
    protected static function config() {
        return [
            self::LANGUAGE_CODE_EN_US,
            self::LANGUAGE_CODE_RU_RU,
        ];
    }

    /**
     * @return string[]
     */
    public static function getAllCodes()
    {
        return self::config();
    }

    /**
     * @return Language[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::getAllCodes() as $code) {
            $model = static::getByCode($code);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param string $code
     * @return Language|null
     */
    public static function getByCode($code)
    {
        $allCodes = self::getAllCodes();

        if (in_array($code, $allCodes)) {
            $model       = new self();
            $model->code = $code;
            return $model;
        }

        return null;
    }
}
