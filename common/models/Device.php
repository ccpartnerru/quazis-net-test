<?php

namespace common\models;

use Yii;
use yii\base\Model;

class Device extends Model
{
    const DEVICE_ID_UNKNOWN = 0;
    const DEVICE_ID_DESKTOP = 1;
    const DEVICE_ID_PHONE   = 2;
    const DEVICE_ID_TABLET  = 3;
    const DEVICE_ID_OTHER   = 4;

    /** @var  int */
    public $id;

    /** @var  string */
    public $name;

    /**
     * @return array
     */
    protected static function config() {
        return [
            self::DEVICE_ID_UNKNOWN => [
                'name' => 'Unknown'
            ],
            self::DEVICE_ID_DESKTOP => [
                'name' => 'Desktop'
            ],
            self::DEVICE_ID_PHONE => [
                'name' => 'Phone'
            ],
            self::DEVICE_ID_TABLET => [
                'name' => 'Tablet'
            ],
            self::DEVICE_ID_OTHER => [
                'name' => 'Other'
            ],
        ];
    }

    /**
     * @return int[]
     */
    public static function getAllIds()
    {
        return array_keys(self::config());
    }

    /**
     * @return Device[]
     */
    public static function getAll()
    {
        $models = [];
        foreach (static::getAllIds() as $id) {
            $model = static::getById($id);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @param int $id
     * @return Device|null
     */
    public static function getById($id)
    {
        $modelConfig = self::getConfigById($id);

        if ($modelConfig) {
            $model = new self();
            $model->id      = $id;
            $model->name    = $modelConfig['name'];
            return $model;
        }

        return null;
    }

    /**
     * @param $id
     * @return Device|null
     */
    public static function findOne($id)
    {
        return self::getById($id);
    }

    /**
     * @param int $id
     * @return array|null
     */
    protected static function getConfigById($id)
    {
        $config = self::config();
        if (isset($config[$id])) {
            return $config[$id];
        }

        return null;
    }
}
