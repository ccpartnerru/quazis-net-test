<?php

namespace common\models;

use Yii;
use yii\base\Model;

class PageZoneFeedSettings extends Model
{
    /** @var  Feed */
    public $feed;

    /** @var  int */
    public $items_count;

    /** @var  string */
    public $keyword_replacement;

    /** @var  PageZone */
    public $page_zone;

    /**
     * @param Feed $feed
     * @param int $items_count
     * @param string $keyword_replacement
     * @param PageZone $page_zone
     *
     * @return PageZoneFeedSettings
     */
    public static function createModel($feed, $items_count, $keyword_replacement, $page_zone)
    {
        $model = new self();
        $model->feed = $feed;
        $model->items_count = $items_count;
        $model->keyword_replacement = $keyword_replacement;
        $model->page_zone = $page_zone;

        return $model;
    }
}
