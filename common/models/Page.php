<?php

namespace common\models;

use common\helpers\Url;
use Yii;
use yii\base\Model;

/**
 * Class Page
 * @package common\models
 *
 * @property PageZone[] $zones
 */
class Page extends Model
{
    /** @var  string */
    public $name;

    /** @var  string */
    public $description;

    /** @var array  */
    public $page_config = [];

    /** @var  PageSection */
    public $page_section;

    /** @var PageZone[] */
    private $_zones;

    /**
     * @param array $modelConfig
     * @return Page|null
     */
    protected static function createModelByModelConfig($modelConfig)
    {
        if ($modelConfig) {
            $model = new self();
            $model->name        = $modelConfig['name'];
            $model->description = $modelConfig['description'];
            $model->page_config = $modelConfig;

            return $model;
        }

        return null;
    }

    /**
     * @return PageZone[]
     */
    protected function getZones()
    {
        if (empty($this->_zones)) {
            $this->_zones = PageZone::getAllPageZoneByPage($this);
        }

        return $this->_zones;
    }

    /**
     * @param PageSection $pageSection
     * @return Page[]
     */
    public static function getAllPageByPageSection($pageSection) {
        $pages = [];

        if ($pageSection->section_config && isset($pageSection->section_config['pages']) && count($pageSection->section_config['pages'])) {
            $pagesConfig = $pageSection->section_config['pages'];

            foreach ($pagesConfig as $pageConfig) {
                $pageName = $pageConfig['name'];
                $page = self::getPageByPageSectionAndPageName($pageSection, $pageName);
                if ($page) {
                    $pages[] = $page;
                }
            }
        }

        return $pages;
    }

    /**
     * @param PageSection $pageSection
     * @param string $pageName
     * @return Page|null
     */
    public static function getPageByPageSectionAndPageName($pageSection, $pageName)
    {
        $pageConfig = self::getPageConfigFromPageSectionConfigByName($pageSection->section_config, $pageName);

        if ($pageConfig) {
            $model = new self();
            $model->name         = $pageConfig['name'];
            $model->description  = isset($pageConfig['description']) ? $pageConfig['description'] : '';
            $model->page_config = $pageConfig;
            $model->page_section = $pageSection;

            return $model;
        }

        return null;
    }

    /**
     * @param string $pageSectionConfig
     * @param string $pageName
     * @return null|array
     */
    protected function getPageConfigFromPageSectionConfigByName($pageSectionConfig, $pageName)
    {
        $pagesConfig = ($pageSectionConfig && isset($pageSectionConfig['pages'])) ? $pageSectionConfig['pages'] : null;

        if ($pagesConfig && count($pagesConfig) > 0) {
            foreach ($pagesConfig as $pageConfig) {
                if (isset($pageConfig['name']) && $pageConfig['name'] == $pageName) {
                    return $pageConfig;
                }
            }
        }

        return null;
    }

    /**
     * @param string $pageZoneName
     * @return PageZone|null
     */
    public function getPageZoneByName($pageZoneName)
    {
        return PageZone::getByPageAndPageZoneName($this, $pageZoneName);
    }

    /**
     * @param string $pageZoneName
     * @return PageZoneActive|null
     */
    public function getPageZoneActiveByName($pageZoneName)
    {
        return PageZoneActive::getByPageAndPageZoneName($this, $pageZoneName);
    }

    /**
     * @param string $pageSectionName
     * @param string $pageName
     * @return null|Page
     */
    public static function getPageByPageSectionNameAndPageName($pageSectionName, $pageName) {
        $pageSection = PageSection::getPageSectionByName($pageSectionName);

        return self::getPageByPageSectionAndPageName($pageSection, $pageName);
    }
}
