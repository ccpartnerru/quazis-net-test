<?php


namespace common\components\yii\helpers;

use Yii;
use yii\web\UrlManager;

class UrlFrontend extends \yii\helpers\Url
{
    /**
     * @return \yii\web\UrlManager
     */
    public static function getUrlManager()
    {
        if (!static::$urlManager) {
            /** @var UrlManager $urlManagerFrontend */
            $urlManagerFrontend = Yii::$app->urlManagerFrontend;

            static::$urlManager = $urlManagerFrontend;
        }

        return static::$urlManager;

    }
}