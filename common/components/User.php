<?php

namespace common\components;

use common\libraries\botDetector\BotDetector;
use common\libraries\safeDetector\SafeDetector;
use Yii;
use common\models\Device;
use common\models\Market;

/**
 * Class User
 * @package common\components
 */
class User extends \yii\web\User
{
    /** @var Session  */
    private $session;

    /** @var  string */
    private $ip;

    /** @var  array */
    private $geoIpData;

    /** @var  Market|null */
    private $market;

    /** @var  Device|null */
    private $device = null;

    /** @var  string */
    private $userAgent;

    /** @var  int|null */
    private $botReason = null;

    /** @var  int|null */
    private $notSafeReason = null;

    /** @var null|string */
    private $sourceClickId = null;

    /** @var  string */
    private $city = null;

    /** @var  string */
    private $region = null;

    /**
     * @return string
     */
    public function getIp()
    {
        if (empty($this->ip)) {
            $ip = $_SERVER['REMOTE_ADDR'];

            if ($ip == '127.0.0.1') {
                //$ip = '91.210.253.194'; //Yoshkar-Ola
                $ip = '68.235.38.41'; // USA
                //$ip = '185.17.149.204'; //UK
            }

            $this->ip = $ip;
        }

        return $this->ip;
    }

    /**
     * @return array
     */
    protected function getGeoIpData()
    {
        if (empty($this->geoIpData)) {
            $ip = $this->getIp();
            $this->geoIpData = geoip_record_by_name($ip);
        }

        return $this->geoIpData;
    }

    /**
     * @return string|null
     */
    public function getCity()
    {
        if (empty($this->city)) {
            $geoIpData = $this->getGeoIpData();
            if (isset($geoIpData['city'])) {
                $this->city = $geoIpData['city'];
            }
        }

        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getRegion()
    {
        if (empty($this->region)) {
            $geoIpData = $this->getGeoIpData();
            if (isset($geoIpData['region'])) {
                $this->region = $geoIpData['region'];
            }
        }

        return $this->region;
    }

    /**
     * @return Market|null
     */
    public function getMarket()
    {
        if (empty($this->market)) {
            $geoIpData = $this->getGeoIpData();
            $countryCode = mb_strtoupper($geoIpData['country_code']);

            if ($countryCode == 'GB') $countryCode = 'UK';

            $this->market = Market::getByName($countryCode);
        }

        return $this->market;
    }

    /**
     * @return Device|null
     */
    public function getDevice()
    {
        if (is_null($this->device)) {
            $mobileDetect = new \Detection\MobileDetect();
            if ($mobileDetect->isTablet()) {
                $device = Device::getById(Device::DEVICE_ID_TABLET);
            } elseif ($mobileDetect->isMobile()) {
                $device = Device::getById(Device::DEVICE_ID_PHONE);
            } else {
                $device = Device::getById(Device::DEVICE_ID_DESKTOP);
            }

            $this->device = $device;
        }

        return $this->device;
    }

    /**
     * @return bool
     */
    public function isBot()
    {
        $botReason = $this->getBotReason();
        if ($botReason == BotDetector::BOT_REASON_NON_BOT) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return int|null
     */
    public function getBotReason()
    {
        if (is_null($this->botReason)) {
            $this->botReason = $this->getSession()->getBotReason();
        }

        if (is_null($this->botReason)) {
            $botDetector = new BotDetector();
            $botDetector->checkUser($this);

            $this->botReason = $botDetector->botReason;
            $this->getSession()->setBotReason($this->botReason);
        }

        return $this->botReason;
    }

    /**
     * @return bool
     */
    public function isSafe()
    {
        $notSafeReason = $this->getBotReason();
        if ($notSafeReason == SafeDetector::SAFE_REASON_SAFE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int|null
     */
    public function getNotSafeReason()
    {
        if (is_null($this->notSafeReason)) {
            $this->notSafeReason = $this->getSession()->getNotSafeReason();
        }

        if (is_null($this->notSafeReason)) {
            $safeDetector = new SafeDetector();
            $safeDetector->checkUser($this);

            $this->notSafeReason = $safeDetector->notSafeReason;
            $this->getSession()->setNotSafeReason($this->notSafeReason);
        }

        return $this->notSafeReason;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        if (empty($this->userAgent)) {
            $this->userAgent = $_SERVER['HTTP_USER_AGENT'];
        }

        return $this->userAgent;
    }

    /**
     * @return Session|mixed|\yii\web\Session
     */
    public function getSession()
    {
        if (empty($this->session)) {
            $this->session = Yii::$app->session;
        }

        return $this->session;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->getSession()->getId();
    }

    /**
     * @return int|null
     */
    public function getInboundStatClickId()
    {
        return $this->getSession()->getInboundStatClickId();
    }

    /**
     * @param int $value
     */
    public function setInboundStatClickId($value)
    {
        $this->getSession()->setInboundStatClickId($value);
    }

    /**
     * @return string
     */
    public function getLinkKey()
    {
        return $this->getSession()->getLinkKey();
    }

    /**
     * @param string $value
     */
    public function setLinkKey($value)
    {
        $this->getSession()->setLinkKey($value);
    }

    /**
     * @return int
     */
    public function getSourcePartnerId()
    {
        return $this->getSession()->getSourcePartnerId();
    }

    /**
     * @param int $value
     */
    public function setSourcePartnerId($value)
    {
        $this->getSession()->setSourcePartnerId($value);
    }

    /**
     * @param int $value
     */
    public function setBotReason($value)
    {
        $this->getSession()->setBotReason($value);
    }

    /**
     * @param int $value
     */
    public function setNotSafeReason($value)
    {
        $this->getSession()->setNotSafeReason($value);
    }

    /**
     * @return string|null
     */
    public function getSourceClickId()
    {
        return $this->getSession()->getSourceClickId();
    }

    /**
     * @param string|null $value
     */
    public function setSourceClickId($value)
    {
        $this->getSession()->setSourceClickId($value);
    }
}