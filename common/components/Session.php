<?php

namespace common\components;

use Yii;

class Session extends \yii\web\Session
{
    /**
     * @return int|null
     */
    public function getInboundStatClickId()
    {
        return $this->get('inboundStatClickId');
    }

    /**
     * @param int $inboundStatClickId
     */
    public function setInboundStatClickId($inboundStatClickId)
    {
        $this->set('inboundStatClickId', $inboundStatClickId);
    }

    /**
     * @return string
     */
    public function getLinkKey()
    {
        return $this->get('linkKey');
    }

    /**
     * @param string $linkKey
     */
    public function setLinkKey($linkKey)
    {
        $this->set('linkKey', $linkKey);
    }

    /**
     * @return int
     */
    public function getSourcePartnerId()
    {
        return $this->get('sourcePartnerId');
    }

    /**
     * @param int $sourcePartnerId
     */
    public function setSourcePartnerId($sourcePartnerId)
    {
        $this->set('sourcePartnerId', $sourcePartnerId);
    }

    /**
     * @return int
     */
    public function getBotReason()
    {
        return $this->get('botReason');
    }

    /**
     * @param int $botReason
     */
    public function setBotReason($botReason)
    {
        return $this->set('botReason', $botReason);
    }

    /**
     * @return string|null
     */
    public function getSourceClickId()
    {
        return $this->get('sourceClickId');
    }

    /**
     * @param string|null $sourceClickId
     */
    public function setSourceClickId($sourceClickId)
    {
        $this->set('sourceClickId', $sourceClickId);
    }

    /**
     * @return int
     */
    public function getNotSafeReason()
    {
        return $this->get('notSafeReason');
    }

    /**
     * @param int $notSafeReason
     */
    public function setNotSafeReason($notSafeReason)
    {
        return $this->set('notSafeReason', $notSafeReason);
    }

}