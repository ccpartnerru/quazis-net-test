<?php


namespace common\libraries\feedContentManager;


class ContentManagerRequest
{
    /** @var  ContentManagerSubRequest[] */
    protected $requests;

    /**
     * @return ContentManagerSubRequest[]
     */
    public function getSubRequests()
    {
        return $this->subRequests;
    }

    /**
     * @param ContentManagerSubRequest[] $subRequests
     */
    public function setSubRequests($subRequests)
    {
        $this->subRequests = $subRequests;
    }

    /**
     * @param ContentManagerSubRequest $subRequest
     */
    public function addSubRequests($subRequest)
    {
        $this->subRequests[] = $subRequest;
    }
}