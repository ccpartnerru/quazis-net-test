<?php


namespace common\libraries\feedContentManager;


class ContentManagerResponse
{
    /** @var  ContentManagerSubResponse[] */
    protected $subResponses;

    /**
     * @return ContentManagerSubResponse[]
     */
    public function getSubResponses()
    {
        return $this->subResponses;
    }

    /**
     * @param ContentManagerSubResponse[] $subResponses
     */
    public function setSubResponses($subResponses)
    {
        $this->subResponses = $subResponses;
    }

    /**
     * @param ContentManagerSubResponse $subResponse
     * @param string $subResponseId
     */
    public function addSubResponse($subResponse, $subResponseId)
    {
        $this->subResponses[$subResponseId] = $subResponse;
    }

    /**
     * @param string $subRequestId
     * @return ContentManagerSubResponse|null
     */
    public function getSubResponseBySubRequestId($subRequestId)
    {
        if (isset($this->subResponses[$subRequestId])) {
            return $this->subResponses[$subRequestId];
        }

        return null;
    }
}