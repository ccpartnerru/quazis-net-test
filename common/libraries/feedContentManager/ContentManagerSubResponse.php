<?php


namespace common\libraries\feedContentManager;


use common\libraries\feedContentManager\services\feed\Result;

class ContentManagerSubResponse
{
    /** @var  Result */
    protected $result;

    /**
     * @return Result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}