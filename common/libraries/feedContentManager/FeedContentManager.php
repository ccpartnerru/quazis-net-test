<?php


namespace common\libraries\feedContentManager;


use common\libraries\feedContentManager\feed\FeedRequest;
use common\libraries\feedContentManager\feed\FeedResponse;
use common\libraries\feedContentManager\services\feed\Service;
use common\libraries\feedContentManager\services\feed\ServiceManager;
use common\models\Feed;

class FeedContentManager
{
    /** @var FeedRequest[]  */
    protected $requests = [];

    /** @var FeedResponse[]  */
    protected $responses = [];

    /** @var  ServiceManager */
    protected $serviceManager;

    /**
     * @return FeedRequest[]
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * @param FeedRequest[] $requests
     */
    public function setRequests($requests)
    {
        $this->requests = $requests;
    }

    /**
     * @param FeedRequest $request
     * @throws \Exception
     */
    public function addRequest($request)
    {
        $id = $request->getRequestId();

        if (empty($id)) {
            throw new \Exception('Request id empty!');
        }

        if (isset($this->requests[$id])) {
            throw new \Exception('Request with this id already exist!');
        }

        $this->requests[$id] = $request;
    }

    /**
     * @return FeedResponse[]
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @param FeedResponse[] $responses
     */
    public function setResponses($responses)
    {
        $this->responses = $responses;
    }

    /**
     * @param FeedResponse $response
     * @throws \Exception
     */
    public function addResponse($response)
    {
        $requestId = null;
        if ($response->getResult() && $response->getResult()->getRequest() && !empty($response->getResult()->getRequest()->getRequestId())) {
            $requestId = $response->getResult()->getRequest()->getRequestId();
        }

        if (!$requestId) {
            throw new \Exception('Response with empty request id!');
        }

        if (isset($this->responses[$requestId])) {
            throw new \Exception('Response with this id already exist!');
        }

        $this->responses[$requestId] = $response;
    }

    /**
     * @return ServiceManager
     */
    public function getServiceManager()
    {
        if (!$this->serviceManager) {
            $this->serviceManager = new ServiceManager();
        }

        return $this->serviceManager;
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager($serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function exec()
    {
        $serviceManager = $this->getServiceManager();

        $requests = $this->getRequests();
        if (count($requests)) {
            foreach ($requests as $request) {
                $feed = $request->getFeed();
                if ($feed) {
                    $service = $this->getFeedService($feed);
                    $service->setRequest($request);
                    $serviceManager->addService($service);
                }
            }
        }

        $results = $serviceManager->getResults();
        foreach ($results as $result) {
            $response = new FeedResponse();
            $response->setResult($result);

            $this->addResponse($response);
        }
    }

    /**
     * @param Feed $feed
     * @return Service
     * @throws \Exception
     */
    protected function getFeedService($feed)
    {
        $namespacePrefix = __NAMESPACE__;

        $feedServiceFolderName = $this->getFeedServiceFolderName($feed);

        $serviceClassName = $namespacePrefix . '\\services\\' . $feedServiceFolderName . '\\Service';
        if (!class_exists($serviceClassName)) {
            throw new \Exception('Service Class ' . $feedServiceFolderName . ' doesn\'t exist!');
        }

        $clientClassName = $namespacePrefix . '\\services\\' . $feedServiceFolderName . '\\Client';
        if (!class_exists($clientClassName)) {
            throw new \Exception('Client Class ' . $feedServiceFolderName . ' doesn\'t exist!');
        }

        $extractorClassName = $namespacePrefix . '\\services\\' . $feedServiceFolderName . '\\Extractor';
        if (!class_exists($extractorClassName)) {
            throw new \Exception('Extractor Class ' . $feedServiceFolderName . ' doesn\'t exist!');
        }

        return new $serviceClassName(new $clientClassName(), new $extractorClassName());
    }


    protected function getFeedServiceFolderName($feed)
    {
        $feedName = $feed->name;

        $feedServiceFolderName = explode(' ', $feedName);
        array_walk($feedServiceFolderName, function (&$item, $key) {
            $item = strtolower($item);
            if ($key != 0 ) {
                $item = ucfirst($item);
            }
        });

        $feedServiceFolderName = implode('', $feedServiceFolderName);

        return $feedServiceFolderName;
    }

    /**
     * @param FeedRequest $request
     * @return FeedResponse|null
     */
    public function getResponseByRequest($request)
    {
        return $this->getResponseByRequestId($request->getRequestId());
    }

    /**
     * @param $requestId
     * @return FeedResponse|null
     */
    public function getResponseByRequestId($requestId)
    {
        if (isset($this->responses[$requestId])) {
            return $this->responses[$requestId];
        }

        return null;
    }
}