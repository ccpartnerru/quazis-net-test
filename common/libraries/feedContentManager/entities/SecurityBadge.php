<?php
namespace common\libraries\feedContentManager\entities;


class SecurityBadge
{
	protected $provider;

	protected $badgeType;

	/**
	 * @return mixed
	 */
	public function getProvider()
	{
		return $this->provider;
	}

	/**
	 * @param mixed $provider
	 */
	public function setProvider( $provider )
	{
		$this->provider = $provider;
	}

	/**
	 * @return mixed
	 */
	public function getBadgeType()
	{
		return $this->badgeType;
	}

	/**
	 * @param mixed $badgeType
	 */
	public function setBadgeType( $badgeType )
	{
		$this->badgeType = $badgeType;
	}

}