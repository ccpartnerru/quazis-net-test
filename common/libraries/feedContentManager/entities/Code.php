<?php
namespace common\libraries\feedContentManager\entities;

class Code
{
    const SKU  = 'sku';
    const UPC  = 'upc';
    const EAN  = 'ean';
    const SIPP = 'sipp';

    public $type;

    public $value;

    public function __construct($type = null, $value = null)
    {
        if( ! is_null($type) )
        {
            $this->setType($type);
        }

        if( ! is_null($value) )
        {
            $this->setValue($value);
        }
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}
