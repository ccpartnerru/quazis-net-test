<?php
namespace common\libraries\feedContentManager\entities;


class Review
{

    /**
     * @var  string
     */
    protected $title;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string
     */
    protected $pros;

    /**
     * @var string
     */
    protected $cons;

    /**
     * @var Rating
     */
    protected $rating;

    /**
     * @var string
     */
    protected $timestamp;

    /**
     * @var User
     */
    protected $author;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $trackUrl;

    /**
     * @var string
     */
    protected $feed;

	/**
     * @Serializer\Type("bool")
     * 
	 * @var boolean
	 */
	protected $isPaid;

	/**
     * @Serializer\Type("bool")
     * 
	 * @var boolean
	 */
	protected $isExact;

	/**
	 * @var string
	 */
	protected $sourceText;

	/**
	 * @var integer
	 */
	protected $K;

	/**
	 * @var string
	 */
	protected $namespace;

    /**
     * @return string
     */
    public function getPros()
    {
        return $this->pros;
    }

    /**
     * @param string $pros
     */
    public function setPros($pros)
    {
        $this->pros = $pros;
    }

    /**
     * @return string
     */
    public function getCons()
    {
        return $this->cons;
    }

    /**
     * @param string $cons
     */
    public function setCons($cons)
    {
        $this->cons = $cons;
    }

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

	/**
	 * @return bool
	 */
	public function getIsPaid()
	{
		return $this->isPaid;
	}

	/**
	 * @param bool $isPaid
	 */
	public function setIsPaid( $isPaid )
	{
		$this->isPaid = $isPaid;
	}

	/**
	 * @return bool
	 */
	public function getIsExact()
	{
		return $this->isExact;
	}

	/**
	 * @param bool $isExact
	 */
	public function setIsExact( $isExact )
	{
		$this->isExact = $isExact;
	}

	/**
	 * @return string
	 */
	public function getSourceText()
	{
		return $this->sourceText;
	}

	/**
	 * @param string $sourceText
	 */
	public function setSourceText( $sourceText )
	{
		$this->sourceText = $sourceText;
	}

	/**
	 * @return int
	 */
	public function getK()
	{
		return $this->K;
	}

	/**
	 * @param int $K
	 */
	public function setK( $K )
	{
		$this->K = $K;
	}

	/**
	 * @return string
	 */
	public function getNamespace()
	{
		return $this->namespace;
	}

	/**
	 * @param string $namespace
	 */
	public function setNamespace( $namespace )
	{
		$this->namespace = $namespace;
	}
}