<?php
namespace common\libraries\feedContentManager\entities;


class Annotation
{
    /**
     * @var string
     */
    protected $phrase;

    /**
     * @return string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @param string $phrase
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;
    }



}