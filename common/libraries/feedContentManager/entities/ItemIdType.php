<?php

namespace common\libraries\feedContentManager\entities;


class ItemIdType
{
    const TYPE_PRODUCT_ID = 'product';
    const TYPE_OFFER_ID   = 'offer';
}