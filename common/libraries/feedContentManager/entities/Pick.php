<?php
namespace common\libraries\feedContentManager\entities;


class Pick
{
    /**
     * @var string
     */
    protected $feed;

    /**
     * @var string
     */
    protected $status;

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}