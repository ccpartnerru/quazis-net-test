<?php

namespace common\libraries\feedContentManager\entities;

class Ad
{
    public $image;

    public $title;

    public $favicon;

    public $description;

    public $displayUrl;

    public $url;

    public $trackUrl;

    /**
     * @Serialize\Type("bool")
     */
    public $officialSite;

    public $rating;

    /**
     * @Serialize\XmlList(entry = "siteLink")
     */
    public $siteLinks;

    public $siteLinkLayout;

    public $feed;

    /**
     * @Serialize\Type("bool")
     */
    public $isPaid;

    /**
     * @Serialize\XmlList(entry = "rating")
     * 
     * @var Rating[]
     */
    protected $ratingList;

    /**
     * @var Phone|null
     */
    protected $phone;

    /**
     * @var Business|null
     */
    protected $business;

    /**
     * @Serialize\XmlList(entry = "annotation")
     * 
     * @var Annotation[]
     */
    protected $annotations;

	/**
     * @Serialize\XmlList(entry = "callout")
     * 
	 * @var Callout[]
	 */
	protected $callouts;

	/**
	 * @var Review|null
	 */
	protected $review;

	/**
	 * @var Ad4thLineAnnotation|null
	 */
	protected $ad4thLineAnnotation;

	/**
	 * @var TopAd|null
	 */
	protected $topAd;

	/**
	 * @var SecurityBadge|null
	 */
	protected $securityBadge;

	/**
	 * @var Brand|null
	 */
	protected $brand;

    /**
     * @return Annotation[]
     */
    public function getAnnotations()
    {
        return $this->annotations;
    }

	/**
	 * @param Annotation[] $annotations
	 * @return Ad
	 */
	public function setAnnotations($annotations)
	{
		$this->annotations = $annotations;
		return $this;
	}

    /**
     * @param Annotation $annotation
     */
    public function addAnnotation($annotation)
    {
        if( empty($this->annotations) ){
            $this->annotations = [];
        }

        $this->annotations[] = $annotation;
    }

    /**
     * @return Business|null
     */
    public function getBusiness()
    {
        return $this->business;
    }

	/**
	 * @param Business $business
	 * @return Ad
	 */
    public function setBusiness($business)
    {
        $this->business = $business;
        return $this;
    }

    /**
     * @return Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

	/**
	 * @param Phone $phone
	 * @return Ad
	 */
    public function setPhone($phone)
    {
        $this->phone = $phone;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

	/**
	 * @param mixed $url
	 * @return Ad
	 */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

	/**
	 * @param mixed $trackUrl
	 * @return Ad
	 */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getOfficialSite()
    {
        return $this->officialSite;
    }

    /**
     * @param mixed $officialSite
     */
    public function setOfficialSite($officialSite)
    {
        $this->officialSite = $officialSite;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

	/**
	 * @param mixed $rating
	 * @return Ad
	 */
    public function setRating($rating)
    {
        $this->rating = $rating;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteLinks()
    {
        return $this->siteLinks;
    }

	/**
	 * @param mixed $siteLinks
	 * @return Ad
	 */
    public function setSiteLinks($siteLinks)
    {
        $this->siteLinks = $siteLinks;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getSiteLinkLayout()
    {
        return $this->siteLinkLayout;
    }

	/**
	 * @param mixed $siteLinkLayout
	 * @return Ad
	 */
    public function setSiteLinkLayout($siteLinkLayout)
    {
        $this->siteLinkLayout = $siteLinkLayout;
	    return $this;
    }

    /**
     * @return mixed
     */
    public function getFeed()
    {
        return $this->feed;
    }

	/**
	 * @param mixed $feed
	 * @return Ad
	 */
    public function setFeed($feed)
    {
        $this->feed = $feed;
	    return $this;
    }

	/**
	 * @param boolean $isPaid
	 * @return Ad
	 */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = boolval($isPaid);
	    return $this;
    }

    public function getIsPaid()
    {
        return $this->isPaid;
    }

	/**
	 * @param string $title
	 * @return Ad
	 */
    public function setTitle($title)
    {
        $this->title = $title;
	    return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

	/**
	 * @param string $description
	 * @return $this
	 */
    public function setDescription($description)
    {
        $this->description = $description;
	    return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

	/**
	 * @param $favicon
	 * @return Ad
	 */
    public function setFavicon($favicon)
    {
        $this->favicon = new Favicon($favicon);
	    return $this;
    }

    public function getFavicon()
    {
        return $this->favicon;
    }

    /**
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

	/**
	 * @param mixed $image
	 * @return Ad
	 */
    public function setImage($image)
    {
        $this->image = new Image($image);
        return $this;
    }

	/**
	 * @param string $displayUrl
	 * @return Ad
	 */
	public function setDisplayUrl($displayUrl)
	{
		$this->displayUrl = $displayUrl;
		return $this;
	}

	/**
	 * @return string
	 */
    public function getDisplayUrl()
    {
        return $this->displayUrl;
    }

    /**
     * @return Rating[]
     */
    public function getRatingList()
    {
        return $this->ratingList;
    }

    /**
     * @param Rating[] $ratingList
     */
    public function setRatingList($ratingList)
    {
        $this->ratingList = $ratingList;
    }

	/**
	 * @return Callout[]
	 */
	public function getCallouts()
	{
		return $this->callouts;
	}

	/**
	 * @param Callout[] $callouts
	 */
	public function setCallouts( array $callouts )
	{
		$this->callouts = $callouts;
	}

	/**
	 * @return Review|null
	 */
	public function getReview()
	{
		return $this->review;
	}

	/**
	 * @param Review|null $review
	 */
	public function setReview( $review )
	{
		$this->review = $review;
	}

	/**
	 * @return Ad4thLineAnnotation|null
	 */
	public function getAd4thLineAnnotation()
	{
		return $this->ad4thLineAnnotation;
	}

	/**
	 * @param Ad4thLineAnnotation|null $ad4thLineAnnotation
	 */
	public function setAd4thLineAnnotation( $ad4thLineAnnotation )
	{
		$this->ad4thLineAnnotation = $ad4thLineAnnotation;
	}

	/**
	 * @return TopAd|null
	 */
	public function getTopAd()
	{
		return $this->topAd;
	}

	/**
	 * @param TopAd|null $topAd
	 */
	public function setTopAd( $topAd )
	{
		$this->topAd = $topAd;
	}

	/**
	 * @return SecurityBadge|null
	 */
	public function getSecurityBadge()
	{
		return $this->securityBadge;
	}

	/**
	 * @param SecurityBadge|null $securityBadge
	 */
	public function setSecurityBadge( $securityBadge )
	{
		$this->securityBadge = $securityBadge;
	}

	/**
	 * @return Brand|null
	 */
	public function getBrand()
	{
		return $this->brand;
	}

	/**
	 * @param Brand|null $brand
	 */
	public function setBrand( $brand )
	{
		$this->brand = $brand;
	}

}