<?php
namespace common\libraries\feedContentManager\entities;


class Response
{
    public $categories;

    public $products;

    public $filters;

    static public function create($result)
    {
        $r = new self();

        if(isset($result['products']))
        {
            $r->products = $result['products'];
        }

        if(isset($result['filters']))
        {
            $r->filters = $result['filters'];
        }

        if(isset($result['categories']))
        {
            $r->categories = $result['categories'];
        }

        return $r;
    }

}