<?php
namespace common\libraries\feedContentManager\entities;


class Currency
{
    const USD_NAME = 'dollar';
    const USD_CODE = 'USD';
    const USD_SIGN = '$';

    const GBP_NAME = 'pound';
    const GBP_CODE = 'GBP';
    const GBP_SIGN = '£';

    const EURO_NAME = 'euro';
    const EURO_CODE = 'EUR';
    const EURO_SIGN = '€';

    const RUB_NAME = 'руб';
    const RUB_CODE = 'RUB';
    const RUB_SIGN = '₽';


    public $code;

    public $name;

    public $sign;

    static public function createByCode($code)
    {
        $currency = new self();
        $code = trim($code);

        switch(strtoupper($code)) {
            case self::USD_CODE:
                $currency->name = self::USD_NAME;
                $currency->code = self::USD_CODE;
                $currency->sign = self::USD_SIGN;
                break;

            case self::GBP_CODE:
                $currency->name = self::GBP_NAME;
                $currency->code = self::GBP_CODE;
                $currency->sign = self::GBP_SIGN;
                break;

            case self::EURO_CODE:
                $currency->name = self::EURO_NAME;
                $currency->code = self::EURO_CODE;
                $currency->sign = self::EURO_SIGN;
                break;

            case self::RUB_CODE:
                $currency->name = self::RUB_NAME;
                $currency->code = self::RUB_CODE;
                $currency->sign = self::RUB_SIGN;
                break;
        }

        return $currency;
    }

    static public function createBySign($sign)
    {
        $currency = new self();

        switch(trim($sign))
        {
            case self::USD_SIGN:
                $currency->name = self::USD_NAME;
                $currency->code = self::USD_CODE;
                $currency->sign = self::USD_SIGN;
                break;

            case self::GBP_SIGN:
                $currency->name = self::GBP_NAME;
                $currency->code = self::GBP_CODE;
                $currency->sign = self::GBP_SIGN;
                break;

            case self::EURO_SIGN:
                $currency->name = self::EURO_NAME;
                $currency->code = self::EURO_CODE;
                $currency->sign = self::EURO_SIGN;
                break;

            case self::RUB_SIGN:
                $currency->name = self::RUB_NAME;
                $currency->code = self::RUB_CODE;
                $currency->sign = self::RUB_SIGN;
                break;

        }

        return $currency;
    }

    public function getSign()
    {
        return $this->sign;
    }
}