<?php

namespace common\libraries\feedContentManager\entities;


class Booking
{
    /**
     * @var string
     */
    protected $feed;

    /**
     * @var string
     */
    protected $bookingReference;

    /**
     * @var string|null
     */
    protected $agentReference;

    /**
     * @var string|null
     */
    protected $supplierReference;

    /**
     * @var string
     */
    protected $status;

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getBookingReference()
    {
        return $this->bookingReference;
    }

    /**
     * @param string $bookingReference
     */
    public function setBookingReference($bookingReference)
    {
        $this->bookingReference = $bookingReference;
    }

    /**
     * @return string
     */
    public function getAgentReference()
    {
        return $this->agentReference;
    }

    /**
     * @param string $agentReference
     */
    public function setAgentReference($agentReference)
    {
        $this->agentReference = $agentReference;
    }

    /**
     * @return string
     */
    public function getSupplierReference()
    {
        return $this->supplierReference;
    }

    /**
     * @param string $supplierReference
     */
    public function setSupplierReference($supplierReference)
    {
        $this->supplierReference = $supplierReference;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }





}