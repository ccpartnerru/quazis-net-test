<?php
namespace common\libraries\feedContentManager\entities;


class Coupon
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string|null
     */
    protected $description;

    /**
     * Сoupon constructor.
     * @param $code
     */
    public function __construct($code)
    {
        $this->setCode($code);
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}