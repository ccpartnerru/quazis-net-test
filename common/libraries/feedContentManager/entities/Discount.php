<?php
namespace common\libraries\feedContentManager\entities;


class Discount
{
    public $value;

    public $sign;

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setSign($sign)
    {
        $this->sign = $sign;
    }
}