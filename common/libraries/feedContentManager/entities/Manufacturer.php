<?php
namespace common\libraries\feedContentManager\entities;


class Manufacturer
{
    public $id;

    public $name;

    public $site;

    /**
     * @var Image
     */
    public $logo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return Image
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param Image $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }
}