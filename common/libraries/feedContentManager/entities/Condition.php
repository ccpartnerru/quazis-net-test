<?php
namespace common\libraries\feedContentManager\entities;


use common\libraries\feedContentManager\services\feed\Client;

class Condition
{
    const VALUE_NEW                = Client::CONDITION_NEW;
    const VALUE_USED               = Client::CONDITION_USED;
    const VALUE_REFURBISHED        = Client::CONDITION_REFURBISHED;
    const VALUE_ACCEPTABLE         = 'Acceptable';
    const VALUE_PARTS_NOT_WORKING  = 'For parts or not working';

    public $value;

    public $note;

    public function __construct($value = null)
    {
        $this->setValue($value);
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

}