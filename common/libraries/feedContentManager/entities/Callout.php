<?php
namespace common\libraries\feedContentManager\entities;


class Callout
{
	protected $phrase;

	/**
	 * @return mixed
	 */
	public function getPhrase()
	{
		return $this->phrase;
	}

	/**
	 * @param mixed $phrase
	 */
	public function setPhrase( $phrase )
	{
		$this->phrase = $phrase;
	}



}