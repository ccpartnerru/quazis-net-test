<?php
namespace common\libraries\feedContentManager\entities;


class CarRentalOffer
{
    /**
     * @var string
     */
    protected $fuelPolicy;

    /**
     * @var string
     */
    protected $mileagePolicy;

    /**
     * @Serializer\Type("bool")
     * 
     * @var bool
     */
    protected $hasSatelliteNavigation;

    /**
     * @var Supplier
     */
    protected $supplier;

    /**
     * @Serializer\XmlList( entry = "feature" )
     * 
     * @var Feature[]
     */
    protected $features;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $trackUrl;

    /**
     * @var Price
     */
    protected $price;

    /**
     * @var Currency
     */
    protected $currency;

    /**
     * @var Rate|null
     */
    protected $rate;


    /**
     * @return string
     */
    public function getFuelPolicy()
    {
        return $this->fuelPolicy;
    }

    /**
     * @param string $fuelPolicy
     */
    public function setFuelPolicy($fuelPolicy)
    {
        $this->fuelPolicy = $fuelPolicy;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return Feature[]
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param Feature[] $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Price $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return boolean
     */
    public function isHasSatelliteNavigation()
    {
        return $this->hasSatelliteNavigation;
    }

    /**
     * @param boolean $hasSatelliteNavigation
     */
    public function setHasSatelliteNavigation($hasSatelliteNavigation)
    {
        $this->hasSatelliteNavigation = $hasSatelliteNavigation;
    }

    /**
     * @return Rate|null
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param Rate|null $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getMileagePolicy()
    {
        return $this->mileagePolicy;
    }

    /**
     * @param string $mileagePolicy
     */
    public function setMileagePolicy($mileagePolicy)
    {
        $this->mileagePolicy = $mileagePolicy;
    }
}