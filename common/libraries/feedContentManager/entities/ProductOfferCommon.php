<?php
namespace common\libraries\feedContentManager\entities;

use common\models\Feed;

class ProductOfferCommon extends Item
{
    /**
     * @var string|null
     */
    public $parentId;

    /**
     * @var string|null
     */
    public $type;

    /**
     * @var Feed
     */
    public $feed;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $longDescription;

    /**
     * @Serializer\XmlList(entry="code")
     * 
     * @var Code[]
     */
    public $codes;

    /**
     * @var Currency
     */
    public $currency;

    /**
     * @Serializer\XmlList(entry="category")
     * 
     * @var Category[]
     */
    public $categories;

    /**
     * @var Discount
     */
    public $discount;

    /**
     * @var Images
     */
    public $images;

    /**
     * @var Brand
     */
    public $brand;

    /**
     * @var Manufacturer
     */
    public $manufacturer;

    /**
     * @var Rating
     */
    public $rating;

    /**
     * @Serializer\XmlList(entry="rating")
     * 
     * @var Rating[]
     */
    protected $ratingList;

    /**
     * @Serializer\XmlList(entry="feature")
     * 
     * @var Feature[]
     */
    protected $features;

    /**
     * @Serializer\XmlList(entry="siteLink")
     * 
     * @var SiteLink[]
     */
    protected $siteLinks;

    /**
     * @Serializer\XmlList(entry="review")
     * 
     * @var Review[]|null
     */
    protected $reviews;

    /**
     * @Serializer\XmlAttribute(root="reviews", attribute="total")
     * 
     * @var integer
     */
    protected $reviewsTotal;

    /**
     * @Serializer\XmlList(entry="icon")
     * 
     * @var Icon[]
     */
    protected $icons;

    /**
     * @Serializer\XmlList(entry="coupon")
     * 
     * @var Coupon[]
     */
    protected $coupons;

    /**
     * @var Recommendation
     */
    protected $recommendation;

    /**
     * @return Recommendation
     */
    public function getRecommendation()
    {
        return $this->recommendation;
    }

    /**
     * @param Recommendation $recommendation
     */
    public function setRecommendation($recommendation)
    {
        $this->recommendation = $recommendation;
    }

    /**
     * @return Coupon[]
     */
    public function getCoupons()
    {
        return $this->coupons;
    }

    /**
     * @param Coupon $coupon
     */
    public function addCoupon($coupon)
    {
        if( ! is_array($this->coupons)){
            $this->coupons = [];
        }

        $this->coupons[] = $coupon;
    }

    /**
     * @param Coupon[] $coupons
     */
    public function setCoupons($coupons)
    {
        $this->coupons = $coupons;
    }


    /**
     * @return Icon[]
     */
    public function getIcons()
    {
        return $this->icons;
    }

    /**
     * @param Icon[] $icons
     */
    public function setIcons($icons)
    {
        $this->icons = $icons;
    }

    /**
     * @param Icon $icon
     */
    public function addIcon($icon)
    {
        if( ! is_array($this->icons)){
            $this->icons = [];
        }
        $this->icons[] = $icon;
    }

    /**
     * @return Review[]|null
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param Review[]|null $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }

    /**
     * @param Review $review
     */
    public function addReview($review)
    {
        if( ! is_array($this->reviews)){
            $this->reviews = [];
        }

        $this->reviews[] = $review;
    }

    /**
     * @return int
     */
    public function getReviewsTotal()
    {
        return $this->reviewsTotal;
    }

    /**
     * @param int $reviewsTotal
     */
    public function setReviewsTotal($reviewsTotal)
    {
        $this->reviewsTotal = $reviewsTotal;
    }

    /**
     * @return SiteLink[]
     */
    public function getSiteLinks()
    {
        return $this->siteLinks;
    }

    /**
     * @param SiteLink[] $siteLinks
     */
    public function setSiteLinks($siteLinks)
    {
        $this->siteLinks = $siteLinks;
    }

    /**
     * @param SiteLink $siteLink
     */
    public function addSiteLink($siteLink)
    {
        if( ! is_array($this->siteLinks)){
            $this->siteLinks = [];
        }

        $this->siteLinks[] = $siteLink;
    }

    /**
     * @return Feature[]
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param Feature[] $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @param Feature $feature
     */
    public function addFeature($feature)
    {
        if( ! is_array($this->features)){
            $this->features = [];
        }

        $this->features[] = $feature;
    }

    /**
     * @return Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param mixed $parentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * @param mixed $codes
     */
    public function setCodes($codes)
    {
        $this->codes = $codes;
    }

    /**
     * @param Code $code
     */
    public function addCode($code)
    {
        if( ! is_array($this->codes)){
            $this->codes = [];
        }

        $this->codes[] = $code;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return Images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Images $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Category[]|null
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category[] $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @var Category $category
     */
    public function addCategory($category)
    {
        if( ! is_array($this->categories)){
            $this->categories = [];
        }

        $this->categories[] = $category;
    }

    /**
     * @return Discount|null
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param Discount $discount
     */
    public function setDiscount(Discount $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return Rating[]null
     */
    public function getRatingList()
    {
        return $this->ratingList;
    }

    /**
     * @param Rating $rating
     */
    public function addRatingInRatingList($rating)
    {
        if( ! is_array($this->ratingList) ){
            $this->ratingList = [];
        }

        $this->ratingList[] = $rating;
    }

    /**
     * @param Rating[] $ratingList
     */
    public function setRatingList($ratingList)
    {
        $this->ratingList = $ratingList;
    }

    /**
     * @return string
     */
    public function getLongDescription()
    {
        return $this->longDescription;
    }

    /**
     * @param string $longDescription
     */
    public function setLongDescription($longDescription)
    {
        $this->longDescription = $longDescription;
    }
}