<?php
namespace common\libraries\feedContentManager\entities;

class CarRental
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $description;

    /** @var string */
    protected $feed;

    /** @var  Car */
    protected $car;

    /** @var  CarRentalLocation  */
    protected $pickUpLocation;

    /** @var  CarRentalLocation */
    protected $dropOffLocation;

    /** 
     * @Serializer\XmlList( entry = "carRentalOffer" )
     * 
     * @var  CarRentalOffer[]
     */
    protected $offers;

    /** @var  Images */
    protected $images;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return Car
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param Car $car
     */
    public function setCar($car)
    {
        $this->car = $car;
    }

    /**
     * @return CarRentalOffer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param CarRentalOffer[] $offers
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Images $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return CarRentalLocation
     */
    public function getPickUpLocation()
    {
        return $this->pickUpLocation;
    }

    /**
     * @param CarRentalLocation $pickUpLocation
     */
    public function setPickUpLocation($pickUpLocation)
    {
        $this->pickUpLocation = $pickUpLocation;
    }

    /**
     * @return CarRentalLocation
     */
    public function getDropOffLocation()
    {
        return $this->dropOffLocation;
    }

    /**
     * @param CarRentalLocation $dropOffLocation
     */
    public function setDropOffLocation($dropOffLocation)
    {
        $this->dropOffLocation = $dropOffLocation;
    }
}