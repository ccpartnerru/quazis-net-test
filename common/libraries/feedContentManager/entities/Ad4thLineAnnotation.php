<?php
namespace common\libraries\feedContentManager\entities;


class Ad4thLineAnnotation
{

	protected $text;

	/**
	 * @return mixed
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @param mixed $text
	 */
	public function setText( $text )
	{
		$this->text = $text;
	}

}