<?php
/**
 * Created by PhpStorm.
 * User: pasha
 * Date: 22.08.16
 * Time: 15:30
 */

namespace common\libraries\feedContentManager\entities;


use ApiBundle\Entity\CategoryPartner;
use ApiBundle\Serializer\Annotation as Serializer;
use common\models\Feed;

class Category
{
    public $id;

    public $name;

    public $level;

    public $priority;

    public $parentId;

    /** @var  Feed */
    public $feed;

    public $countResults;

    /**
     * @Serializer\XmlList( entry = "category" )
     */
    public $categories;

    public function addCategory(Category $category)
    {
        if( is_null($this->categories) ) {
            $this->categories = [];
        }

        $this->categories[] = $category;
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function setCategoriesAndReturn(array $array_in)
    {
        $array_out = array();
        foreach ($array_in as $item) {
            $array_out[] = (new $this)->setNameAndReturn($item);
        }

        return $array_out;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCountResults($countResults)
    {
        $this->countResults = $countResults;
    }

    public function getCountResults()
    {
        return $this->countResults;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setNameAndReturn($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function getParentId()
    {
        return $this->parentId;
    }

    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    public static function createByCategoryPartner(CategoryPartner $categoryPartner)
    {
        $category = new static();
        $category->fillByCategoryPartner($categoryPartner);

        return $category;
    }

    public function fillByCategoryPartner(CategoryPartner $categoryPartner)
    {
        $this->setName($categoryPartner->getName());
        $this->setLevel($categoryPartner->getFeedCategoryLevel());
        if(!empty($categoryPartner->getFeedParentId())) {
            $this->setParentId($categoryPartner->getFeedParentId());
        }
        $this->setId($categoryPartner->getFeedCategoryId());
    }
}