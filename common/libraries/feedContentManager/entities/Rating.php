<?php
namespace common\libraries\feedContentManager\entities;


class Rating
{
    /**
     * @var float
     */
    public $value;

    /**
     * @var integer
     */
    public $count;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $trackUrl;

    /**
     * @var Image
     */
    public $image;

    /**
     * @var integer
     */
    public $timestamp;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $maxValue;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return integer
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param integer $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }


    public function setValue($value)
    {
        $this->value = $value;
    }

    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getMaxValue()
    {
        return $this->maxValue;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    public function getValue()
    {
        return $this->value;
    }
}