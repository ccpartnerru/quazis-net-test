<?php
namespace common\libraries\feedContentManager\entities;


class Brand
{
    public $id;

    public $name;

    public $logo;


    public function __construct($id = null, $name = null)
    {
        if( ! is_null($id))
        {
            $this->setId($id);
        }

        if( ! is_null($name))
        {
            $this->setName($name);
        }
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}