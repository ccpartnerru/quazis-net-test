<?php
namespace common\libraries\feedContentManager\entities;

use common\models\Feed;

class Item
{
    const ITEM_TYPE_OFFER   = 'offer';
    const ITEM_TYPE_PRODUCT = 'product';
    const ITEM_TYPE_UNKNOWN = 'unknown';

    /**
     * @var Feed
     */
    public $feed;

    /**
     * @return string
     */
    public function getItemType()
    {
        if ($this instanceof Offer) {
            return self::ITEM_TYPE_OFFER;
        } elseif ($this instanceof Product) {
            return self::ITEM_TYPE_PRODUCT;
        }

        return self::ITEM_TYPE_UNKNOWN;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return null;
    }
}