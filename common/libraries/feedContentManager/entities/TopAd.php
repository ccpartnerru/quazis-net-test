<?php
namespace common\libraries\feedContentManager\entities;


class TopAd
{
	/**
	 * @Serializer\Type("bool")
	 */
	protected $isTopAd;

	protected $adDomainName;

	protected $adDomainClicks;

	/**
	 * @return mixed
	 */
	public function getIsTopAd()
	{
		return $this->isTopAd;
	}

	/**
	 * @param mixed $isTopAd
	 */
	public function setIsTopAd( $isTopAd )
	{
		$this->isTopAd = $isTopAd;
	}

	/**
	 * @return mixed
	 */
	public function getAdDomainName()
	{
		return $this->adDomainName;
	}

	/**
	 * @param mixed $adDomainName
	 */
	public function setAdDomainName( $adDomainName )
	{
		$this->adDomainName = $adDomainName;
	}

	/**
	 * @return mixed
	 */
	public function getAdDomainClicks()
	{
		return $this->adDomainClicks;
	}

	/**
	 * @param mixed $adDomainClicks
	 */
	public function setAdDomainClicks( $adDomainClicks )
	{
		$this->adDomainClicks = $adDomainClicks;
	}

}