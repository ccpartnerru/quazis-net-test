<?php
namespace common\libraries\feedContentManager\entities;


class Article
{
	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var string
	 */
	private $site;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var string
	 */
	private $descriptionSnippet;

	/**
	 * @var string
	 */
	private $descriptionHighlighted;

	/**
	 * @var string
	 */
	private $content;

	/**
	 * @var string
	 */
	private $contentSnippet;

	/**
	 * @var string
	 */
	private $contentHighlighted;

	/**
     * @var string
     */
    protected $timestamp;

	/**
	 * @var string
	 */
	private $lang;

	/**
	 * @var string
	 */
	private $image_preview;

	/**
	 * @var string
	 */
	private $author;

	/**
	 * @var array
	 */
	private $series;

	/**
	 * @var array
	 */
	private $categories;

	/**
	 * @var array
	 */
	private $tags;

	/**
	 * @var array
	 */
	private $keywords;

	/**
	 * @var string
	 */
	private $md5;

	/**
	 * @var string
	 */
	private $feed_code;


	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return Article
	 */
	public function setTitle($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 * @return Article
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSite()
	{
		return $this->site;
	}

	/**
	 * @param string $site
	 * @return Article
	 */
	public function setSite($site)
	{
		$this->site = $site;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return Article
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescriptionSnippet()
	{
		return $this->descriptionSnippet;
	}

	/**
	 * @param string $descriptionSnippet
	 * @return Article
	 */
	public function setDescriptionSnippet($descriptionSnippet)
	{
		$this->descriptionSnippet = $descriptionSnippet;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescriptionHighlighted()
	{
		return $this->descriptionHighlighted;
	}

	/**
	 * @param string $descriptionHighlighted
	 * @return Article
	 */
	public function setDescriptionHighlighted($descriptionHighlighted)
	{
		$this->descriptionHighlighted = $descriptionHighlighted;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 * @return Article
	 */
	public function setContent($content)
	{
		$this->content = $content;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContentSnippet()
	{
		return $this->contentSnippet;
	}

	/**
	 * @param string $contentSnippet
	 * @return Article
	 */
	public function setContentSnippet($contentSnippet)
	{
		$this->contentSnippet = $contentSnippet;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getContentHighlighted()
	{
		return $this->contentHighlighted;
	}

	/**
	 * @param string $contentHighlighted
	 * @return Article
	 */
	public function setContentHighlighted($contentHighlighted)
	{
		$this->contentHighlighted = $contentHighlighted;
		return $this;
	}

	/**
     * @return string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
		return $this;
    }

	/**
	 * @return string
	 */
	public function getLang()
	{
		return $this->lang;
	}

	/**
	 * @param string $lang
	 * @return Article
	 */
	public function setLang($lang)
	{
		$this->lang = $lang;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImagePreview()
	{
		return $this->image_preview;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setImagePreview($image_preview)
	{
		$this->image_preview = $image_preview;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSeries()
	{
		return $this->series;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setSeries($series)
	{
		$this->series = $series;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setCategories(array $categories)
	{
		$this->categories = $categories;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setTags(array $tags)
	{
		$this->tags = $tags;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getKeywords()
	{
		return $this->keywords;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setKeywords(array $keywords)
	{
		$this->keywords = $keywords;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMd5()
	{
		return $this->md5;
	}

	/**
	 * @param string
	 * @return Article
	 */
	public function setMd5($md5)
	{
		$this->md5 = $md5;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFeedCode()
	{
		return $this->feed_code;
	}

	/**
	 * @param string $id
	 * @return Article
	 */
	public function setFeedCode($feed_code)
	{
		$this->feed_code = $feed_code;
		return $this;
	}
}