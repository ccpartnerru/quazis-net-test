<?php
namespace common\libraries\feedContentManager\entities;


class Phone
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $displayNumber;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $trackUrl;

    /**
     * @var string
     */
    protected $clickToCall;


    public function __construct($number = null)
    {
        $this->setNumber($number);
        $this->setDisplayNumber($number);
    }

    /**
     * @return string
     */
    public function getClickToCall()
    {
        return $this->clickToCall;
    }

    /**
     * @param string $clickToCall
     */
    public function setClickToCall($clickToCall)
    {
        $this->clickToCall = $clickToCall;
    }

    /**
     * @return string
     */
    public function getDisplayNumber()
    {
        return $this->displayNumber;
    }

    /**
     * @param string $displayNumber
     */
    public function setDisplayNumber($displayNumber)
    {
        $this->displayNumber = $displayNumber;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

}