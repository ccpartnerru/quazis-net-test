<?php
namespace common\libraries\feedContentManager\entities;

class Series
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return series
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function setSeries(array $array_in)
	{
		$array_out = array();
		foreach ($array_in as $item) {
			$array_out[] = (new $this)->setName($item);
		}

		return $array_out;
	}
}