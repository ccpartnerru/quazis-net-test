<?php
namespace common\libraries\feedContentManager\entities;


class Tag
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return tag
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function setTags(array $array_in)
	{
		$array_out = array();
		foreach ($array_in as $item) {
			$array_out[] = (new $this)->setName($item);
		}

		return $array_out;
	}
}