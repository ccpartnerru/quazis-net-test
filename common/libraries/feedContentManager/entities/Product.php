<?php
namespace common\libraries\feedContentManager\entities;

class Product extends ProductOfferCommon
{
    /**
     * @Serializer\Exclude(format="xml")
     * 
     * @var string
     */
    public $type = 'product';


    public $id;


    public $prices;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $trackUrl;

    /**
     * @Serializer\XmlList( entry = "offer" )
     * 
     * @var Offer[]|null
     */
    public $offers;

    /**
     * @Serializer\XmlAttribute(root="offers", attribute="total")
     * 
     * @var integer
     */
    public $offersTotal;

    /**
     * @Serializer\XmlList( entry = "feature" )
     * 
     * @var Feature[]|null
     */
    protected $features;

    /**
     * @Serializer\XmlList(entry="rating")
     * 
     * @var Rating[]
     */
    protected $ratingList;

    /**
     * @Serializer\XmlList(entry="siteLink")
     * 
     * @var SiteLink[]
     */
    protected $siteLinks;

    /**
     * @Serializer\XmlList(entry="review")
     * 
     * @var Review[]|null
     */
    protected $reviews;

    /**
     * @Serializer\XmlAttribute(root="reviews", attribute="total")
     * 
     * @var integer
     */
    protected $reviewsTotal;

    /**
     * @Serializer\XmlList(entry="icon")
     * 
     * @var Icon[]
     */
    protected $icons;

    /**
     * @Serializer\XmlList(entry="coupon")
     * 
     * @var Coupon[]
     */
    protected $coupons;

    /**
     * @Serializer\XmlList(entry="category")
     * 
     * @var Category[]
     */
    public $categories;

    /**
     * @Serializer\XmlList(entry="code")
     * 
     * @var Code[]
     */
    public $codes;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Prices|null
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param mixed $prices
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    }

    /**
     * @return int
     */
    public function getOffersTotal()
    {
        return $this->offersTotal;
    }

    /**
     * @param int $offersTotal
     */
    public function setOffersTotal($offersTotal)
    {
        $this->offersTotal = $offersTotal;
    }

    public function getOffers()
    {
        return $this->offers;
    }

    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    public function addOffer($offer)
    {
        if( ! is_array($this->offers)) {
            $this->offers = [];
        }

        $this->offers[] = $offer;
    }

}
