<?php
namespace common\libraries\feedContentManager\entities;


class Rate
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $title;


    public function __construct($type = null, $title = null)
    {
        $this->setType($type);
        $this->setTitle($title);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}