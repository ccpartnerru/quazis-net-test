<?php
namespace common\libraries\feedContentManager\entities;

use common\libraries\feedContentManager\entities\filter\Value;
use common\models\Feed;

class Filter
{
    const TYPE_ENUM    = 'enum';
    const TYPE_BOOLEAN = 'bool';

    public $name;

    public $id;
    
    /**
     * @Serializer\XmlList( entry = "value" )
     */
    public $values;

    /** @var  Feed */
    public $feed;

    protected $type;

    protected $description;

    public function __construct()
    {
        $this->setType(self::TYPE_ENUM);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param mixed $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param Value $value
     */
    public function addValue($value)
    {
        if( ! is_array($this->values) ){
            $this->values = [];
        }

        $this->values[] = $value;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }
}



