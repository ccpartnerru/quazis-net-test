<?php
namespace common\libraries\feedContentManager\entities;

class Car
{
    const TRANSMISSION_MANUAL = 'manual';
    const TRANSMISSION_AUTO   = 'auto';

    const DRIVE_4WD           = '4WD';
    const DRIVE_AWD           = 'AWD';

    const FUEL_DIESEL         = 'diesel';
    const FUEL_HYBRID         = 'hybrid';
    const FUEL_ELECTRIC       = 'electric';
    const FUEL_GAS            = 'gas';
    const FUEL_HYDROGEN       = 'hydrogen';
    const FUEL_MULTI          = 'multi';
    const FUEL_PETROL         = 'petrol';
    const FUEL_ETHANOL        = 'ethanol';

    /**
     * @var string
     */
    protected $model;

    /**
     * @var Code
     */
    protected $code;

    /**
     * @var string
     */
    protected $feed;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $images;

    /**
     * @Serializer\Type("bool")
     * 
     * @var bool
     */
    protected $hasAirCondition;

    /**
     * @var string
     */
    protected $transmission;

    /**
     * @var string
     */
    protected $drive;

    /**
     * @var string
     */
    protected $fuel;

    /**
     * @Serializer\XmlList( entry = "space" )
     * 
     * @var Space[]
     */
    protected $seats;

    /**
     * @Serializer\XmlList( entry = "space" )
     * 
     * @var Space[]
     */
    protected $bags;

    /**
     * @return Space[]
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param Space[] $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @return Space[]
     */
    public function getBags()
    {
        return $this->bags;
    }

    /**
     * @param Space[] $bags
     */
    public function setBags($bags)
    {
        $this->bags = $bags;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return Code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param Code $code
     */
    public function setCode($code)
    {
        $this->code = $code;

        $codeValue = $code->getValue();

        //https://en.wikipedia.org/wiki/ACRISS_Car_Classification_Code
        $transmissionDriveCode = $codeValue[2];
        $fuelAirCode           = $codeValue[3];

        //Transmission
        if( in_array($transmissionDriveCode, ['M', 'N', 'C'])) {
            $this->setTransmission(self::TRANSMISSION_MANUAL);
        }
        elseif( in_array($transmissionDriveCode, ['A', 'B', 'D'])) {
            $this->setTransmission(self::TRANSMISSION_AUTO);
        }

        //Drive
        if( in_array($transmissionDriveCode, ['N', 'B'])) {
            $this->setDrive(self::DRIVE_4WD);
        }
        elseif( in_array($transmissionDriveCode, ['C', 'D'])) {
            $this->setDrive(self::DRIVE_AWD);
        }

        //Air Cond
        if( in_array($fuelAirCode, ['N', 'Q', 'I', 'C', 'S', 'B', 'F', 'Z', 'X'])) {
            $this->setHasAirCondition(false);
        }
        elseif( in_array($fuelAirCode, ['R', 'D', 'H', 'E', 'L', 'A', 'M', 'V', 'U'])) {
            $this->setHasAirCondition(true);
        }

        //Fuel
        if( in_array($fuelAirCode, ['D', 'Q'])) {
            $this->setFuel(self::FUEL_DIESEL);
        }
        elseif( in_array($fuelAirCode, ['H', 'I'])) {
            $this->setFuel(self::FUEL_HYBRID);
        }
        elseif( in_array($fuelAirCode, ['E', 'C'])) {
            $this->setFuel(self::FUEL_ELECTRIC);
        }
        elseif( in_array($fuelAirCode, ['L', 'S'])) {
            $this->setFuel(self::FUEL_GAS);
        }
        elseif( in_array($fuelAirCode, ['A', 'B'])) {
            $this->setFuel(self::FUEL_HYDROGEN);
        }
        elseif( in_array($fuelAirCode, ['M', 'M'])) {
            $this->setFuel(self::FUEL_MULTI);
        }
        elseif( in_array($fuelAirCode, ['V', 'Z'])) {
            $this->setFuel(self::FUEL_PETROL);
        }
        elseif( in_array($fuelAirCode, ['U', 'X'])) {
            $this->setFuel(self::FUEL_ETHANOL);
        }
    }

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param string $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return boolean
     */
    public function isHasAirCondition()
    {
        return $this->hasAirCondition;
    }

    /**
     * @param boolean $hasAirCondition
     */
    public function setHasAirCondition($hasAirCondition)
    {
        $this->hasAirCondition = $hasAirCondition;
    }

    /**
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * @param string $transmission
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;
    }

    /**
     * @return string
     */
    public function getDrive()
    {
        return $this->drive;
    }

    /**
     * @param string $drive
     */
    public function setDrive($drive)
    {
        $this->drive = $drive;
    }

    /**
     * @return string
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * @param string $fuel
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}