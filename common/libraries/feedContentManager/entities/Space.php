<?php
namespace common\libraries\feedContentManager\entities;


class Space
{
    const TYPE_LARGE  = 'large';
    const TYPE_MEDIUM = 'medium';
    const TYPE_SMALL  = 'small';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $count;


    public function __construct($type = null, $count = null)
    {
        $this->setType($type);
        $this->setCount($count);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }
}