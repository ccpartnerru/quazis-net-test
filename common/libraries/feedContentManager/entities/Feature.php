<?php
namespace common\libraries\feedContentManager\entities;

class Feature
{
    /** @var  string */
    protected $name;

    /**
     * @XmlList(entry = "value")
     * @var string[]
     */
    protected $values;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \string[]
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param \string[] $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param \string $value
     */
    public function addValue($value)
    {
        if( ! is_array($this->values)){
            $this->values = [];
        }

        $this->values[] = $value;
    }

}