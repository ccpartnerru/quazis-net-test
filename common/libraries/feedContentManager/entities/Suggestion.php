<?php

namespace common\libraries\feedContentManager\entities;


class Suggestion
{
    protected $text;

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getText()
    {
        return $this->text;
    }
}