<?php
namespace common\libraries\feedContentManager\entities;

class Keyword
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return Keyword
	 */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	public function setKeywords(array $array_in)
	{
		$array_out = array();
		foreach ($array_in as $item) {
			$array_out[] = (new $this)->setName($item);
		}

		return $array_out;
	}
}