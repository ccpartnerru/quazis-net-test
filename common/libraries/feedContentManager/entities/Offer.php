<?php
namespace common\libraries\feedContentManager\entities;


class Offer extends ProductOfferCommon
{
    /**
     * @Serializer\Exclude(format="xml")
     * 
     * @var string
     */
    public $type = 'offer';

    /**
     * @var string
     */
    protected $id;

    public $url;

    public $trackUrl;

    public $availability;

    public $price;

    public $condition;

    public $delivery;

    public $merchant;

    public $merchantActual;

    protected $location;

    /** 
     * @Serializer\Type("bool")
     * 
     * @var  bool
     */
    public $isPrime = false;

    /**
     * @var boolean|null
     */
    protected $featured;

    /**
     * @var boolean|null
     */
    protected $premium;

    /**
     * @var boolean|null
     */
    protected $flexible;

    /**
     * @Serializer\XmlList( entry = "style" )
     * 
     * @var Style[]|null
     */
    protected $styles;

    /**
     * @Serializer\XmlList( entry = "feature" )
     * 
     * @var Feature[]|null
     */
    protected $features;

    /**
     * @Serializer\XmlList(entry="rating")
     * 
     * @var Rating[]
     */
    protected $ratingList;

    /**
     * @Serializer\XmlList(entry="siteLink")
     * 
     * @var SiteLink[]
     */
    protected $siteLinks;

    /**
     * @Serializer\XmlList(entry="review")
     * 
     * @var Review[]|null
     */
    protected $reviews;

    /**
     * @Serializer\XmlAttribute(root="reviews", attribute="total")
     * 
     * @var integer
     */
    protected $reviewsTotal;

    /**
     * @Serializer\XmlList(entry="icon")
     * 
     * @var Icon[]
     */
    protected $icons;

    /**
     * @Serializer\XmlList(entry="coupon")
     * 
     * @var Coupon[]
     */
    protected $coupons;

    /**
     * @Serializer\XmlList(entry="category")
     * 
     * @var Category[]
     */
    public $categories;

    /**
     * @Serializer\XmlList(entry="code")
     * 
     * @var Code[]
     */
    public $codes;

    /**
     * @return Style[]|null
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * @param Style[] $styles
     */
    public function setStyles($styles)
    {
        $this->styles = $styles;
    }

    /**
     * @return Feature[]|null
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param Feature[] $feature
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @param Style $style
     */
    public function addStyle($style)
    {
        if( ! is_array($this->styles)){
            $this->styles = [];
        }

        $this->styles[] = $style;
    }

    /**
     * @return bool|null
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * @param bool|null $featured
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;
    }

    /**
     * @return bool|null
     */
    public function getFlexible()
    {
        return $this->flexible;
    }

    /**
     * @param bool|null $flexible
     */
    public function setFlexible($flexible)
    {
        $this->flexible = boolean_value($flexible);
    }

    /**
     * @return bool|null
     */
    public function getPremium()
    {
        return $this->premium;
    }

    /**
     * @param bool|null $premium
     */
    public function setPremium($premium)
    {
        $this->premium = $premium;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return Merchant|null
     */
    public function getMerchant()
    {
        return $this->merchant;
    }

    public function setMerchant($merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @return Merchant|null
     */
    public function getMerchantActual()
    {
        return $this->merchantActual;
    }

    public function setMerchantActual($merchant)
    {
        $this->merchantActual = $merchant;
    }

    /**
     * @return Condition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param mixed $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return Availability
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param mixed $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return Price|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Delivery|null
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * @param mixed $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * @return Location|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return bool
     */
    public function isIsPrime()
    {
        return $this->isPrime;
    }

    /**
     * @param bool $isPrime
     */
    public function setIsPrime($isPrime)
    {
        $this->isPrime = $isPrime;
    }
}