<?php
namespace common\libraries\feedContentManager\entities;


class Award
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $year;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }
}