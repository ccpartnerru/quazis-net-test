<?php
namespace common\libraries\feedContentManager\entities;


class SiteLink
{
    public $title;

    public $url;

    public $trackUrl;

    /**
     * @XmlList(entry = "description")
     * @var string[]
     */
    protected $descriptions;

    /**
     * @return \string[]
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param \string $description
     */
    public function addDescription($description)
    {
        $this->descriptions[] = $description;
    }


    /**
     * @return mixed
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param mixed $trackUrl
     */
    public function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return SiteLink
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     * @return SiteLink
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
}