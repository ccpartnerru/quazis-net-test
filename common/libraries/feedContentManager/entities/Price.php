<?php
namespace common\libraries\feedContentManager\entities;


class Price
{
    public $value;

    public $original;

    public $type;

    public function __construct($price = null)
    {
        if( ! is_null($price) ) {
            $this->value = floatval($price);
            $this->original = $price;
        }
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setOriginal($original)
    {
        $this->original = $original;
    }
}
