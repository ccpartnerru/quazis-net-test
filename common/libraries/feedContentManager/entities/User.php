<?php
namespace common\libraries\feedContentManager\entities;


class User
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $surname;

    /**
     * @var Image
     */
    protected $avatar;

    /**
     * @var integer
     */
    protected $reviewsTotal;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return Image
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param Image $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return int
     */
    public function getReviewsTotal()
    {
        return $this->reviewsTotal;
    }

    /**
     * @param int $reviewsTotal
     */
    public function setReviewsTotal($reviewsTotal)
    {
        $this->reviewsTotal = $reviewsTotal;
    }
}