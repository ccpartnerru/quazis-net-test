<?php
namespace common\libraries\feedContentManager\entities;


class Availability
{
    public $value;

    public $note;


    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function getNote()
    {
        return $this->note;
    }

}