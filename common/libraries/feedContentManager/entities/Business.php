<?php
namespace common\libraries\feedContentManager\entities;


class Business
{
    const TYPE_AIRPORT  = 'airport';
    const TYPE_CITY     = 'city';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $feed;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var Phone
     */
    protected $phone;

    /**
     * @var Location
     */
    protected $location;

    /**
     * @var Rating
     */
    protected $rating;

    /**
     * @var Images
     */
    protected $images;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $displayUrl;

    /**
     * @Serializer\XmlList( entry = "siteLink" )
     * 
     * @var SiteLink[]
     */
    protected $siteLinks;

    /**
     * @Serializer\XmlList( entry = "category" )
     * 
     * @var Category[]
     */
    protected $categories;

    /**
     * @Serializer\XmlList( entry = "award" )
     * 
     * @var Award[]
     */
    protected $awards;

    /**
     * @Serializer\XmlList( entry = "review" )
     * 
     * @var Review[]
     */
    protected $reviews;

    /**
     * @Serializer\XmlList( entry = "offer" )
     * 
     * @var Offer[]
     */
    protected $offers;

    /**
     * @Serializer\XmlAttribute(root="reviews", attribute="total")
     * @var integer
     */
    protected $reviewsTotal;

    /**
     * @var string
     */
    protected $workingHours;

    /**
     * @var Feature[]
     */
    protected $features;

    /**
     * @return string
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param string $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return Feature[]
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param Feature[] $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @param Feature $feature
     */
    public function addFeature($feature)
    {
        if( ! is_array($this->features)){
            $this->features = [];
        }
        $this->features[] = $feature;
    }

    /**
     * @return string
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * @param string $workingHours
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = $workingHours;
    }

    /**
     * @return Offer[]
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param Offer[] $offers
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    /**
     * @param Offer $offer
     */
    public function addOffer($offer)
    {
        if( ! is_array($this->offers)){
            $this->offers = [];
        }

        $this->offers[] = $offer;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Award[]
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * @param Award[] $awards
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;
    }

    /**
     * @param Award $award
     */
    public function addAward(Award $award)
    {
        if( ! is_array($this->awards)) {
            $this->awards = [];
        }

        $this->awards[] = $award;
    }

    /**
     * @return string
     */
    public function getDisplayUrl()
    {
        return $this->displayUrl;
    }

    /**
     * @param string $displayUrl
     */
    public function setDisplayUrl($displayUrl)
    {
        $this->displayUrl = $displayUrl;
    }

    /**
     * @return SiteLink[]
     */
    public function getSiteLinks()
    {
        return $this->siteLinks;
    }

    /**
     * @param SiteLink[] $siteLinks
     */
    public function setSiteLinks($siteLinks)
    {
        $this->siteLinks = $siteLinks;
    }

    /**
     * @param SiteLink $siteLink
     */
    public function addSiteLink($siteLink)
    {
        if( ! is_array($this->siteLinks)){
            $this->siteLinks = [];
        }

        $this->siteLinks[] = $siteLink;
    }

    /**
     * @return Review[]
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @return Images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Images $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return int
     */
    public function getReviewsTotal()
    {
        return $this->reviewsTotal;
    }

    /**
     * @param Review[] $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }

    /**
     * @param Review $review
     */
    public function addReview($review)
    {
        if( ! is_array($this->reviews)){
            $this->reviews = [];
        }

        $this->reviews[] = $review;
    }

    /**
     * @param integer $total
     */
    public function setTotalReviews($total)
    {
        $this->reviewsTotal = $total;
    }

    /**
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param Category $category
     */
    public function addCategory($category)
    {
        if( !is_array($this->categories)){
            $this->categories = [];
        }

        $this->categories[] = $category;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Phone $phone
     */
    public function setPhone(Phone $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}