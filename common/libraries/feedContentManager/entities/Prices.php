<?php
namespace common\libraries\feedContentManager\entities;

class Prices
{
    public $min;

    public $max;

    /**
     * @Serializer\XmlList( entry = "price" )
     */
    public $prices;

    public function setMin($min)
    {
        if( $min instanceof Price ){
            $this->min = $min;
        }
        else {
            $this->min = new Price($min);
        }
    }

    public function setMax($max)
    {
        if( $max instanceof Price ){
            $this->max = $max;
        }
        else {
            $this->max = new Price($max);
        }
    }

    /**
     * @return Price|null
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return Price|null
     */
    public function getMax()
    {
        return $this->max;
    }

    public function addPrice(Price $price)
    {
        if( ! is_array($this->prices) ){
            $this->prices = [];
        }

        $this->prices[] = $price;
    }
}