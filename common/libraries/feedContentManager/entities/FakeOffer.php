<?php
namespace common\libraries\feedContentManager\entities;

use common\helpers\Base;

class FakeOffer extends Offer
{
    public  function __construct($info)
    {
        $merchant = new Merchant();

        if( $value = Base::array_get($info, 'retailer')) {
            $merchant->setName($value);
        }

        if( $value = Base::array_get($info, 'retailer_logo')) {
            $logo = new Image($value);
            $merchant->setLogo($logo);
        }

        $this->setMerchant($merchant);

        if( $value = Base::array_get($info, 'price')){
            $value = preg_replace('/[^\d\.,]/i', '', $value);
            $price = new Price();
            $price->setValue($value);
            $this->setPrice($price);
        }

        if( $value = Base::array_get($info, 'url')){
            $this->setUrl($value);
        }

        if( $value = Base::array_get($info, 'rating')){
            $rating = new Rating();
            $rating->setValue($value);
            $this->setRating($rating);
        }
    }
}