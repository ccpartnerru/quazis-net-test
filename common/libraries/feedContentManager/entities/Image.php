<?php
namespace common\libraries\feedContentManager\entities;

class Image
{
    public $url;

    public $height;

    public $width;


    public function __construct($item = null)
    {
        if( $item instanceof self)
        {
            $this->setUrl($item->getUrl());
            $this->setHeight($item->getHeight());
            $this->setWidth($item->getWidth());
        }

        if( is_scalar($item) )
        {
            $this->setUrl($item);
        }
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}

