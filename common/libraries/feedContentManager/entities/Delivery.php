<?php
namespace common\libraries\feedContentManager\entities;

class Delivery
{
    const FREE_SHIPPING = 'Free Shipping';
    const POSSIBLE_PAY  = 'Possible Delivery Pay';

    /**
     * @var string
     */
    protected $text;

    /**
     * @var Price
     */
    protected $price;

    /**
     * @Serializer\Type("bool")
     * 
     * @var boolean
     */
    protected $isPrimed;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Price $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return boolean
     */
    public function getIsPrimed()
    {
        return $this->isPrimed;
    }

    /**
     * @param boolean $isPrimed
     */
    public function setIsPrimed($isPrimed)
    {
        $this->isPrimed = boolean_value($isPrimed);
    }
}