<?php
namespace common\libraries\feedContentManager\entities;

class Images
{
    /** @var  Image */
    public $preview;

    /** @var  Image */
    public $big;

    /**
     * @Serialize\XmlList(entry = "image")
     * 
     * @var Image[]|null
     */
    public $list = [];

    /**
     * @param Image|string $image
     */
    public function setPreview($image)
    {
        $this->preview = $this->createImage($image);
    }

    /**
     * @param Image|string $image
     */
    public function setBig($image)
    {
        $this->big = $this->createImage($image);

    }

    /**
     * @return Image
     */
    public function getBig()
    {
        return $this->big;
    }

    /**
     * @return Image
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param Image|string $image
     */
    public function addImage($image)
    {
        if($image = $this->createImage($image)) {
            $this->list[] = $image;
        }

    }

    protected function createImage($image)
    {
        if( $image instanceof Image)
        {
            return $image;
        }
        elseif(is_string($image))
        {
            return new Image($image);
        }

        return null;

    }

    /**
     * @return Image[]|null
     */
    public function getList()
    {
        return $this->list;
    }
}

