<?php


namespace common\libraries\feedContentManager\tools;


class UrlServicesGroup
{
    /** @var  Url */
    protected $url;

    /** @var int[]  */
    protected $serviceIds;

    /**
     * @return Url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param Url $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return \int[]
     */
    public function getServiceIds()
    {
        return $this->serviceIds;
    }

    /**
     * @param \int[] $serviceIds
     */
    public function setServiceIds($serviceIds)
    {
        $this->serviceIds = $serviceIds;
    }

    /**
     * @param \int[] $serviceId
     */
    public function addServiceId($serviceId)
    {
        $this->serviceIds[] = $serviceId;
    }
}