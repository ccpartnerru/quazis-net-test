<?php


namespace common\libraries\feedContentManager\tools;


class Url
{
    const METHOD_POST = 'POST';
    const METHOD_GET  = 'GET';

    protected $feedShortName = null;
    protected $uri           = null;
    protected $timeout       = null;
    protected $action        = null;
    protected $cacheId       = null;
    protected $cacheTTL      = 3600;
    protected $feedCode      = null;
    protected $headers       = [];
    protected $useCache      = true;
    protected $method        = 'GET';
    protected $body          = null;
    protected $options       = [];

    public function __construct($feedShortName, $uri, $action = null, $timeout = 3, $useCache = true)
    {
        $this->setFeedShortName($feedShortName);
        $this->setUri($uri);
        $this->setAction($action);
        $this->setTimeout($timeout);
        $this->setUseCache($useCache);
    }

    /**
     * @return null
     */
    public function getFeedShortName()
    {
        return $this->feedShortName;
    }

    /**
     * @param null $feedShortName
     */
    public function setFeedShortName($feedShortName)
    {
        $this->feedShortName = $feedShortName;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return strtoupper($this->method);
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = strtoupper($method);
    }

    /**
     * @return null
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param null $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @param $header string Example: "Content-Type: : application/json"
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * @return string[] Example: ["Content-Type: : application/json", ...]
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders($headers)
    {
        return $this->headers = $headers;
    }

    /**
     * @return boolean
     */
    public function isUseCache()
    {
        return $this->useCache;
    }

    /**
     * @param boolean $useCache
     */
    public function setUseCache($useCache)
    {
        $this->useCache = $useCache;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setCacheId($id)
    {
        $this->cacheId = $id;
    }

    public function getCacheId()
    {
        if( ! $this->useCache ) return null;

        if( $this->cacheId )
            return  $this->cacheId;

        return $this->method . ':'. $this->feedShortName .':' . $this->action . ':' . md5($this->uri . $this->body);
    }

    public function getCacheTTL()
    {
        return (int)$this->cacheTTL;
    }

    public function setCacheTTL($ttl)
    {
        $this->cacheTTL = $ttl;
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    public function getOptions()
    {
        return $this->options;
    }

}