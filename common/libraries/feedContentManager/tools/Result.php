<?php


namespace common\libraries\feedContentManager\tools;


class Result
{
    /**
     * @var string
     */
    protected $content;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var array
     */
    protected $info;


    public function __construct($content, $headers = null, $info = null)
    {
        $this->setContent($content);

        if( $headers ) $this->setHeaders($headers);

        if( $info ) $this->setInfo($info);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        if( is_string($headers) ) {
            $headers = explode(PHP_EOL, trim($headers));

        }

        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param array $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return integer
     */
    public function getStatus()
    {
        return self::array_get($this->info, 'http_code');
    }

    protected static function array_get($array, $key, $default = null)
    {
        if( ! is_array($array) && ! is_iterable($array) ) {
            throw new \Exception(__FUNCTION__ . "(). The 1st argument should be array.");
        }

        if( isset($array[$key]) ) return $array[$key];

        $key = explode('.', $key);

        $last = [];

        while(count($key) > 1)
        {
            $last[] = array_pop($key);

            $index = implode('.', $key);

            if( isset($array[$index]) )
            {
                return self::array_get($array[$index], implode('.', $last), $default);
            }
        }

        return $default;
    }


}