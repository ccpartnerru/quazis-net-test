<?php
namespace common\libraries\feedContentManager\tools;


use common\libraries\feedContentManager\ContentManagerSubRequest;

class Pagination
{
    /** @var  int */
    protected $page;

    /** @var  int */
    protected $count;

    /** @var  int */
    protected $total;

    /** @var  int */
    protected $offset;

    /** @var  int */
    protected $limit;

    protected $prevArgs;

    protected $nextArgs;

    /**
     * @param ContentManagerSubRequest $request
     * @return Pagination
     */
    public static function createFromRequest($request)
    {
        $self = new self();

        $self->offset = (int)$request->get('offset');
        $self->limit = $request->getPerPage();
        $self->total = 0;
        $self->count = 0;

        return $self;
    }

	/**
	 * @param int $offset
	 */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

	/**
	 * @return int
	 */
    public function getOffset()
    {
        return $this->offset;
    }

	/**
	 * @param int $limit
	 */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

	/**
	 * @return int
	 */
    public function getLimit()
    {
        return $this->limit;
    }

	/**
	 * @param int $total
	 */
    public function setTotal($total)
    {
        $this->total = $total;
    }

	/**
	 * @return int
	 */
    public function getTotal()
    {
        if( is_null($this->total) )
        {
            return $this->getCount();
        }

        return $this->total;
    }

	/**
	 * @param int $count
	 */
    public function setCount($count)
    {
        $this->count = $count;
    }

	/**
	 * @return int
	 */
    public function getCount()
    {
        return $this->count;
    }

	/**
	 * @return mixed
	 */
	public function getPrevArgs()
	{
		return $this->prevArgs;
	}

	/**
	 * @param mixed $prevArgs
	 */
	public function setPrevArgs($prevArgs)
	{
		$this->prevArgs = $prevArgs;
	}

	/**
	 * @return mixed
	 */
	public function getNextArgs()
	{
		return $this->nextArgs;
	}

	/**
	 * @param mixed $nextArgs
	 */
	public function setNextArgs($nextArgs)
	{
		$this->nextArgs = $nextArgs;
	}
}