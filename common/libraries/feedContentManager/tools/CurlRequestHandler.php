<?php


namespace common\libraries\feedContentManager\tools;


class CurlRequestHandler
{
    /** @var Url[] */
    protected $urls = [];

    protected $mh = null;

    /**
     * @return Url[]
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * @param Url[] $urls
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * @return Result[]
     */
    public function exec()
    {
        $result = array();

        $mh = curl_multi_init();

        $handlers = array();

        foreach ($this->getUrls() as $key => $url)
        {
            $uri = $url->getUri();
            $timeout = $url->getTimeout();

            if ($timeout <= 0) {
                $timeout = 1;
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $uri);
            foreach ($url->getOptions() as $option => $value) {
                curl_setopt($ch, $option, $value);
            }
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout * 1000);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $headers = ['Accept-Encoding: gzip'];
            if( $additionalHeaders = $url->getHeaders() ) {
                $headers = array_merge($headers, $additionalHeaders);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            if($url->getMethod() != 'GET'){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $url->getMethod());
                if( $body = $url->getBody() ) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
                }
            }

            $handlers[$key] = $ch;
            curl_multi_add_handle($mh, $ch);
        }

        do {
            $mrc = curl_multi_exec($mh, $handlersCount);
        }
        while ($mrc == CURLM_CALL_MULTI_PERFORM);

        $handlersCountLast = null;

        do {
            usleep(100);
            $mrc = curl_multi_exec($mh, $handlersCount);
        }
        while ($handlersCount && $mrc == CURLM_OK);

        foreach ($handlers as $key => $ch) {
            $url  = self::array_get($this->getUrls(), $key);
            $info = curl_getinfo($ch);

            $error = curl_error($ch);
            if( empty($error) || ! preg_match('/Operation timed out/i', $error) ) {
                $result[$key] = $this->createResult($ch, $url);
            }

            curl_multi_remove_handle($mh, $ch);
        }

        curl_multi_close($mh);

        return $result;
    }

    protected static function array_get($array, $key, $default = null)
    {
        if( ! is_array($array) && ! is_iterable($array) ) {
            throw new \Exception(__FUNCTION__ . "(). The 1st argument should be array.");
        }

        if( isset($array[$key]) ) return $array[$key];

        $key = explode('.', $key);

        $last = [];

        while(count($key) > 1)
        {
            $last[] = array_pop($key);

            $index = implode('.', $key);

            if( isset($array[$index]) )
            {
                return self::array_get($array[$index], implode('.', $last), $default);
            }
        }

        return $default;
    }

    /**
     * @param $ch
     * @param Url|null $url
     * @return Result
     */
    protected function createResult($ch, $url)
    {
        $response = curl_multi_getcontent($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);

        //ob_start();
//

    if (preg_match('/content-encoding:( +)gzip/i', $header)) {

        if( strlen($body) >= 10 ) {
            $encodedBody = substr($body, 10);
            try{
                $decodedBody = gzinflate($encodedBody);
            } catch(\Exception $e) {
                $decodedBody = false;   
            }

            if( $decodedBody === false ) {
                $uri = null;
                if( $url ) {
                    $uri = $url->getUri();
                }

                $info = curl_getinfo($ch);
            } else {
                $body = $decodedBody;
            }
        }


        $uri = null;
        if( $url ) {
            $uri = $url->getUri();
        }
    

        if( preg_match('/amazon/', $uri) ) {

            $a = 1;
           // echo '<pre>';
           // echo $body;

           // die('STOP');
        }
    }
        //print_r($body);
        //print_r(curl_getinfo($ch));
//
        //$content = ob_get_contents();
        //file_put_contents(__FILE__ . '.txt', $content);
        //ob_end_clean();
        //die('STPO');

        return new Result($body, $header, curl_getinfo($ch) );
    }
}