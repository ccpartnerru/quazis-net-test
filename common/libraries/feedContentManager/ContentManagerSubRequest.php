<?php


namespace common\libraries\feedContentManager;


use common\libraries\feedContentManager\services\feed\Service;

class ContentManagerSubRequest
{
    /** @var  string */
    protected $subRequestId;

    /** @var  int */
    protected $partnerId;

    /** @var  string */
    protected $keyword;

    /** @var  int */
    protected $page;

    /** @var  int */
    protected $perPage;

    /** @var  string */
    protected $action;

    /** @var  string */
    protected $itemId;

    /**
     * @return string
     */
    public function getSubRequestId()
    {
        if (empty($this->subRequestId)) {
            $this->subRequestId = spl_object_hash($this);
        }

        return $this->subRequestId;
    }

    /**
     * @param string $subRequestId
     */
    public function setSubRequestId($subRequestId)
    {
        $this->subRequestId = $subRequestId;
    }

    /**
     * @return int
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param int $partnerId
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }
}