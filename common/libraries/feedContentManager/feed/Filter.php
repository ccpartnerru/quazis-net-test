<?php

namespace common\libraries\feedContentManager\feed;

class Filter
{
    const FILTER_NAME_MIN_PRICE = 'min_price';
    const FILTER_NAME_MAX_PRICE = 'max_price';

    /** @var  string */
    protected $name;

    /** @var  mixed */
    protected $value;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}