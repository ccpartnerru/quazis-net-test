<?php

namespace common\libraries\feedContentManager\feed;

class Sort
{
    const SORT_VALUE_POPULARITY = 'popularity';
    const SORT_VALUE_RELEVANCE = 'relevance';
    const SORT_VALUE_RATING = 'rating';
    const SORT_VALUE_PRICE = 'price';

    const SORT_TYPE_ASC  = 'asc';
    const SORT_TYPE_DESC = 'desc';

    /** @var  string */
    protected $value;

    /** @var  string */
    protected $type;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}