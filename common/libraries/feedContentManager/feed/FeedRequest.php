<?php

namespace common\libraries\feedContentManager\feed;

use common\libraries\feedContentManager\services\feed\ServiceAction;
use common\models\Feed;

class FeedRequest
{
    /** @var  string */
    protected $requestId;

    /** @var  Feed */
    protected $feed;

    /** @var  string */
    protected $itemId;

    /** @var  string */
    protected $keyword;

    /** @var  int */
    protected $page;

    /** @var  int */
    protected $perPage;

    /** @var  Filter[] */
    protected $filters;

    /** @var  Sort|null */
    protected $sort;

    /**
     * @var string
     * @see ServiceAction
     */
    protected $action;

    /**
     * @return string
     */
    public function getRequestId()
    {
        if (empty($this->requestId)) {
            $this->requestId = spl_object_hash($this);
        }

        return $this->requestId;
    }

    /**
     * @param string $requestId
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return string
     * @see ServiceAction
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param string $itemId
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * @return Filter[]
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param Filter[] $filters
     */
    public function setFilters($filters)
    {
        if (count($filters)) {
            foreach ($filters as $filter) {
                $this->addFilter($filter);
            }
        }
    }

    /**
     * @param Filter $filter
     */
    public function addFilter($filter)
    {
        $this->filters[$filter->getName()] = $filter;
    }

    /**
     * @return Sort|null
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param Sort $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return float|null
     */
    public function getMinPrice()
    {
        $filters = $this->getFilters();
        if (isset($filters[Filter::FILTER_NAME_MIN_PRICE])) {
            return floatval($filters[Filter::FILTER_NAME_MIN_PRICE]->getValue());
        }

        return null;
    }

    /**
     * @return float|null
     */
    public function getMaxPrice()
    {
        $filters = $this->getFilters();
        if (isset($filters[Filter::FILTER_NAME_MAX_PRICE])) {
            return floatval($filters[Filter::FILTER_NAME_MAX_PRICE]->getValue());
        }

        return null;
    }
}