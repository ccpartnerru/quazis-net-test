<?php

namespace common\libraries\feedContentManager\feed;

use common\libraries\feedContentManager\services\feed\Result;

class FeedResponse
{
    /** @var  Result */
    protected $result;

    /**
     * @return Result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }
}