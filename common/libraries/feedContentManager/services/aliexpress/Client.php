<?php


namespace common\libraries\feedContentManager\services\aliexpress;


use common\libraries\feedContentManager\feed\Sort;
use common\libraries\feedContentManager\services\feed\ServiceAction;
use common\libraries\feedContentManager\tools\Url;

class Client extends \common\libraries\feedContentManager\services\feed\Client
{
    const SORT_PRICE_ASC = 'orignalPriceUp';
    const SORT_PRICE_DESC = 'orignalPriceDown';
    const SORT_RATING_DESC = 'sellerRateDown';

    /** @var string  */
    protected $apiKey = '92832';

    /** @var string  */
    protected $signature = 'kCu1JRQuA6';

    /** @var string  */
    protected $trackingId = 'partpriceru';

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * @param string $trackingId
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
    }

    public function prepareSearchUrls()
    {
        $request = $this->getService()->getRequest();

        $params = [
            'keywords' => $request->getKeyword(),
            'categoryId' => '',
            'originalPriceFrom' => $this->getMinPrice(),
            'originalPriceTo' => $this->getMaxPrice(),
            'localCurrency' => $this->getCurrency(),
            'language' => $this->getLanguage(),
            'pageNo' => $request->getPage(),
            'pageSize' => $request->getPerPage(),
            'sort' => $this->getFeedSort(),
            'fields' => implode(',', [
                'productId',
                'productUrl',
                'totalResults',
                'productTitle',
                'imageUrl',
                'originalPrice',
                'discount',
                'evaluateScore',
                'localPrice',
                'allImageUrls'
            ])
        ];

        $this->createUrl($params, 'listPromotionProduct', $this->getAction(), true);
    }

    protected function getFeedSort()
    {
        $sort = $this->getSort();
        if ($sort) {
            $sortValue = $sort->getValue();

            switch ($sortValue) {
                case Sort::SORT_VALUE_PRICE:
                    if ($sort->getValue() && $sort->getValue() == Sort::SORT_TYPE_DESC) {
                        return self::SORT_PRICE_DESC;
                    } else {
                        return self::SORT_PRICE_ASC;
                    }
                case Sort::SORT_VALUE_RATING:
                    return self::SORT_RATING_DESC;
                default:
                    return null;
            }
        }

        return null;
    }

    public function prepareSignUrl($urls)
    {
        $params = [
            'trackingId' => $this->getTrackingId(),
            'urls' => implode(',', $urls),
            'fields' => 'url,promotionUrl'
        ];

        $this->createUrl($params, 'getPromotionLinks', Service::ACTION_SIGN);
    }

    public function prepareDetailUrlsMany($ids, $full = false, $useCache = true)
    {
        foreach($ids as $id) {
            $params = [
                'productId' => $id,
                'localCurrency' => $this->getCurrency(),
                'language' => $this->getLanguage(),
                'fields' => implode(',', [
                    'productId',
                    'storeName',
                    'storeUrl'
                ])
            ];

            if ($full) {
                $params['fields'] = implode(',', [
                    'productId',
                    'storeName',
                    'storeUrl',
                    'productUrl',
                    'totalResults',
                    'productTitle',
                    'imageUrl',
                    'originalPrice',
                    'discount',
                    'evaluateScore',
                    'localPrice',
                    'allImageUrls'
                ]);
            }

            $this->createUrl($params, 'getPromotionProductDetail', ServiceAction::ACTION_DETAIL, $useCache);
        }
    }

    public function prepareDetailUrls()
    {
        $this->prepareDetailUrlsMany([$this->getService()->getRequest()->getItemId()], true, true);
    }

    protected function createUrl($params, $endpoint, $action, $useCache = false)
    {
        $uri = 'http://gw.api.alibaba.com/openapi/param2/2/portals.open/api.' .
            $endpoint . '/' . $this->getApiKey() . '?' . http_build_query($params);

        $url = new Url($this->getService()->getFeed()->short_name, $uri, $action);
        $url->setUseCache(false);
        if( $useCache ) {
            $url->setUseCache(true);
            $url->setCacheTTL(10800);//3 * 60 * 60
        }

        $this->addUrl($url);
    }

    public function getCurrency()
    {
        return 'USD';
    }

    protected function getLanguage()
    {
        return 'en';
    }

    public function getPossibleSorts()
    {
        return [
            Client::SORT_PRICE_ASC   => 'orignalPriceUp',
            Client::SORT_PRICE_DESC  => 'orignalPriceDown',
            Client::SORT_RATING_DESC => 'sellerRateDown',
        ];
    }
}