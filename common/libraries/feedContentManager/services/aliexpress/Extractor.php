<?php


namespace common\libraries\feedContentManager\services\aliexpress;


use common\libraries\feedContentManager\entities\Category;
use common\libraries\feedContentManager\entities\Condition;
use common\libraries\feedContentManager\entities\Currency;
use common\libraries\feedContentManager\entities\Delivery;
use common\libraries\feedContentManager\entities\Discount;
use common\libraries\feedContentManager\entities\Filter;
use common\libraries\feedContentManager\entities\Images;
use common\libraries\feedContentManager\entities\Merchant;
use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Price;
use common\libraries\feedContentManager\entities\Product;
use common\libraries\feedContentManager\entities\Rating;
use common\helpers\Base;

class Extractor extends \common\libraries\feedContentManager\services\feed\Extractor
{
    protected $offers = [];

    protected $urls = [];

    public function parse($content, $action)
    {
        if( Service::ACTION_SIGN == $action ){
            return $this->parseSignUrls($content);
        }

        return parent::parse($content, $action);
    }

    public function parseSignUrls($content)
    {
        $content = $this->parseJson($content);
        if(empty($content) || empty($content->result)) return false;

        /** @var Offer[]|Product[] $offers */
        $offers = $this->getResult()->getItems();

        $result = $content->result;
        if( !empty($result->promotionUrls)){
            $urls = [];
            foreach($result->promotionUrls as $url) {
                $promotionUrl = $url->promotionUrl;
                $url = $url->url;
                if( $offerId = array_search($url, $this->urls) ) {
                    $urls[$offerId] = $promotionUrl;
                }
            }

            $position = 0;
            foreach($offers as $offer) {
                $promotionUrl = Base::array_get($urls, $offer->getId());
                if( $promotionUrl ){
                    $offer->setUrl($promotionUrl);
                    $position++;
                }
            }
        }

        return true;
    }

    public function parseSearch($content)
    {
        $content = $this->parseJson($content);
        if(empty($content) || empty($content->result)) return false;

        /** @var Client $client */
        $client = $this->getClient();

        $limit = $client->getLimit();
        $count = 0;
        $offerIds = [];

        $result = $content->result;
        if( !empty($result->products)){
            foreach($result->products as $offer){
                $offer = $this->parseOffer($offer);
                $this->offers[$offer->getId()] = $offer;
                $this->saveItem($offer);
                if( ++$count >= $limit ) break;
            }
        }

        $total = $count;
        if( !empty($result->totalResults)){
            $total = $result->totalResults;
        }

        $perPage = $this->getRequest()->getPerPage();
        if ($perPage > 0) {
            $this->saveTotalPagesCount(ceil($total / $perPage));
        }

        return true;
    }

    public function parseDetail($content)
    {
        $content = $this->parseJson($content);
        if(empty($content) || empty($content->result)) return false;
        $result = $content->result;
        $offer = $this->parseOffer($result);

        if ($addedOffer = Base::array_get($this->offers, $offer->getId())){
            $addedOffer->setMerchant($offer->getMerchant());
        } else {
            $this->offers[$offer->getId()] = $offer;
            $this->saveItem($offer);
            /** @var Client $client */
            $client = $this->getClient();
            $client->prepareSignUrl($this->urls);
        }

        return true;
    }

    /**
     * @param \stdClass $stdObject
     * @return Offer
     */
    protected function parseOffer($stdObject)
    {
        $offer = new Offer();
        $offer->setFeed($this->getFeed());

        if( ! empty($stdObject->productId)){
            $offer->setId($stdObject->productId);
            if( ! empty($stdObject->productUrl)) {
                $this->urls[$stdObject->productId] = $stdObject->productUrl;
            }
        }

        if(!empty($stdObject->productTitle)){
            $offer->setTitle(strip_tags($stdObject->productTitle));
        }

        if(!empty($stdObject->imageUrl)){
            $images = new Images();
            $images->setPreview($stdObject->imageUrl);
            $offer->setImages($images);

            if( ! empty($stdObject->allImageUrls) ){
                $urls = explode(',', $stdObject->allImageUrls);
                foreach($urls as $url){
                    $images->addImage($url);
                    if( ! $images->getBig() ) {
                        $images->setBig($url);
                    }
                }
            }
        }

        if( isset($stdObject->discount)){
            $discountValue = floatval($stdObject->discount);
            if( $discountValue > 0 ) {
                $discount = new Discount();
                $discount->setSign('%');
                $discount->setValue($discountValue);
                $offer->setDiscount($discount);
            }
        }

        if( !empty($stdObject->evaluateScore)){
            $ratingValue = floatval($stdObject->evaluateScore);
            if( $ratingValue > 0 ) {
                $rating = new Rating();
                $rating->setValue($ratingValue);
                $offer->setRating($rating);
            }
        }

        $price = new Price();
        if( !empty($stdObject->localPrice)) {
            $price->setValue(floatval($stdObject->localPrice));
            $price->setOriginal(floatval($stdObject->localPrice));
            if( $offer->getDiscount() ){
                $discount = $offer->getDiscount()->getValue();
                $originalPrice = $price->getValue() / (1 - $discount / 100 );
                $price->setOriginal(round($originalPrice, 2));
            }
        }

        if( ! empty($stdObject->storeName)){
            $merchant = new Merchant();
            $merchant->setName($stdObject->storeName);
            $offer->setMerchant($merchant);
        }
        $offer->setPrice($price);

        /** @var Client $client */
        $client = $this->getClient();
        $curCode = $client->getCurrency();
        $currency = Currency::createByCode($curCode);
        $offer->setCurrency($currency);

        return $offer;
    }
}