<?php


namespace common\libraries\feedContentManager\services\ebay;


use common\libraries\feedContentManager\entities\Category;
use common\libraries\feedContentManager\entities\Condition;
use common\libraries\feedContentManager\entities\Currency;
use common\libraries\feedContentManager\entities\Delivery;
use common\libraries\feedContentManager\entities\Discount;
use common\libraries\feedContentManager\entities\Filter;
use common\libraries\feedContentManager\entities\filter\Value;
use common\libraries\feedContentManager\entities\Images;
use common\libraries\feedContentManager\entities\Merchant;
use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Price;
use common\libraries\feedContentManager\entities\Rating;

class Extractor extends \common\libraries\feedContentManager\services\feed\Extractor
{
    protected $merchantDefaultName = 'eBay';

    /**
     * @param string $content
     * @return bool
     */
    public function parseSearch($content)
    {
        $content = $this->parseXml($content);
        if( is_null($content) ) return false;
        if( ! isset($content->ack) || (string)$content->ack != 'Success') return false;
        if( ! isset($content->searchResult) ) return false;

        $searchContent = $content->searchResult;

        $count = 0;
        $limit = $this->getClient()->getLimit();
        if( isset($searchContent->item) ) {
            $items = $searchContent->item;
            foreach( $items as $item ){
                $offer = $this->makeOffer($item);
                $this->saveItem($offer);
                $count++;
                if( $count >= $limit){
                    break;
                }
            }
        }

        if( !empty($content->aspectHistogramContainer->aspect) ){
            $xmlFilters = $content->aspectHistogramContainer->aspect;
            foreach($xmlFilters as $xmlFilter){
                if( $filter = $this->makeFilter($xmlFilter) ) {
                    $this->saveFilter($filter);
                }
            }
        }

        $total = $count;
        if (!empty($content->paginationOutput) && !empty($content->paginationOutput->totalEntries)) {
            $total = intval((string)$content->paginationOutput->totalEntries);
        }

        $perPage = $this->getRequest()->getPerPage();
        if ($perPage > 0) {
            $this->saveTotalPagesCount(ceil($total / $perPage));
        }

        return true;
    }

    /**
     * @param string $content
     * @return bool
     */
    public function parseDetail($content)
    {
        return $this->parseSearch($content);
    }

    /**
     * @param \SimpleXMLElement $xml
     * @return Filter
     */
    protected function makeFilter($xml)
    {
        $filter = new Filter();

        $attrs = $xml->attributes();
        if( empty($attrs['name']) ) return null;

        $name = (string)$attrs['name'];
        $filter->setName($name);
        $filter->setFeed($this->getFeed());
        $filter->setId($name);

        if( empty($xml->valueHistogram) ) return null;

        foreach($xml->valueHistogram as $xmlValue){
            $attrs = $xmlValue->attributes();
            if( empty($attrs['valueName']) ) continue;
            $value = (string)$attrs['valueName'];
            $filterValue = new Value();
            $filterValue->setId($value);
            $filterValue->setName($value);

            if( ! empty($xmlValue->count) ){
                $filterValue->setCount((int)$xmlValue->count);
            }

            $filter->addValue($filterValue);
        }

        return $filter;
    }


    /**
     * @param \SimpleXMLElement $xml
     * @return Offer
     */
    protected function makeOffer($xml)
    {
        $offer = new Offer();
        $offer->setFeed($this->getFeed());

        if( !empty($xml->itemId) ) {
            $offer->setId((string)$xml->itemId);
        }

        if( !empty($xml->title) ) {
            $offer->setTitle((string)$xml->title);
        }

        if( !empty($xml->subtitle) ) {
            $offer->setDescription((string)$xml->subtitle);
        }

        if( !empty($xml->viewItemURL) ) {
            $url = (string)$xml->viewItemURL;
            $offer->setUrl($url);
        }

        if( !empty($xml->galleryURL) ){
            $imageUrl = (string)$xml->galleryURL;
            $images = new Images();
            $images->setPreview(
                $this->removeProtocolFromUrl($imageUrl)
            );
            $images->setBig(
                $this->removeProtocolFromUrl($imageUrl)
            );

            $offer->setImages($images);
        }

        if( !empty($xml->primaryCategory) ){
            $category = new Category();
            $category->setId((string)$xml->primaryCategory->categoryId);
            $category->setName((string)$xml->primaryCategory->categoryName);
            $category->setFeed($this->getFeed());

            $offer->addCategory($category);
        }

        if( ! empty($xml->condition) ){
            $condition = null;
            switch((int)$xml->condition->conditionId){
                case 1000:
                case 1500:
                case 1750:
                    $condition = Condition::VALUE_NEW;
                    break;
                case 2000:
                case 2500:
                    $condition = Condition::VALUE_REFURBISHED;
                    break;
                case 3000:
                case 4000:
                case 5000:
                    $condition = Condition::VALUE_USED;
                    break;
                case 6000:
                    $condition = Condition::VALUE_ACCEPTABLE;
                    break;
                case 7000:
                    $condition = Condition::VALUE_PARTS_NOT_WORKING;
                    break;
                default:
                    break;
            }

            if( $condition ) {
                $offer->setCondition(new Condition($condition));
            }
        }

        if( ! empty($xml->sellingStatus) ){
            $sellingStatus = $xml->sellingStatus;
            if( isset($sellingStatus->convertedCurrentPrice) ){
                $priceValue = (float)$sellingStatus->convertedCurrentPrice;
            }
            elseif( isset($sellingStatus->currentPrice) ){
                $priceValue = (float)$sellingStatus->currentPrice;
            }
            if( isset($priceValue) ) {
                $price = new Price((float)$xml->sellingStatus->currentPrice);
                $offer->setPrice($price);
            }
        }

        if( ! empty($xml->discountPriceInfo) ){
            $xmlDiscount = $xml->discountPriceInfo;
            $originalPrice = 0;
            if( ! empty($xmlDiscount->originalRetailPrice) ) {
                $originalPrice = (float)$xmlDiscount->originalRetailPrice;
            }

            if( $originalPrice > 0 && $price = $offer->getPrice() ){
                $discountValue = $originalPrice - $price->getValue();
                $price->setOriginal($originalPrice);

                $discountValue = round($discountValue / $originalPrice * 100);
                $discount = new Discount();
                $discount->setValue($discountValue);
                $discount->setSign('%');

                $offer->setDiscount($discount);
            }
        }

        $offer->setCurrency($this->makeCurrency());

        if( !empty($xml->shippingInfo) ){
            $delivery = new Delivery();
            $shipInfo = $xml->shippingInfo;
            $text = '';

            if( !empty($shipInfo->shippingServiceCost) ) {
                $priceValue = (float)$shipInfo->shippingServiceCost;
                if( $priceValue > 0 ) {
                    $price = new Price((float)$shipInfo->shippingServiceCost);
                    $delivery->setPrice($price);
                }
            }

            if( !empty($shipInfo->shippingType)  && (string)$shipInfo->shippingType === 'Free') {
                $text = 'Free';

                if (!empty($shipInfo->shipToLocations) && (string)$shipInfo->shipToLocations === 'Worldwide') {
                    $text .= ' Worldwide';
                }

                $text .= ' Shipping';
            }

            if( !empty($text) ) {
                $delivery->setText($text);
            }

            $offer->setDelivery($delivery);
        }

        if( ! empty($xml->storeInfo) ){
            $merchant = new Merchant();
            $merchant->setName($this->merchantDefaultName);
            $merchant->setSiteUrl((string)$xml->storeInfo->storeURL);
            $offer->setMerchant($merchant);

            $merchantActual = new Merchant();
            $merchantActual->setName((string)$xml->storeInfo->storeName);
            $merchantActual->setSiteUrl((string)$xml->storeInfo->storeURL);
            $offer->setMerchantActual($merchantActual);
        }

        if( ! empty($xml->sellerInfo)){
            $merchant = $offer->getMerchant();
            $merchantActual = $offer->getMerchantActual();
            if( ! $merchant ) {
                $merchant = new Merchant();
                $merchantActual = new Merchant();
            }
            $merchant->setName($this->merchantDefaultName);
            $merchantActual->setName((string)$xml->sellerInfo->sellerUserName);

            $rating = new Rating();
            $ratingValue = (float)$xml->sellerInfo->positiveFeedbackPercent / 20;
            $ratingValue = ceil($ratingValue/0.5)*0.5;
            $rating->setValue($ratingValue);
            $rating->setCount((int)$xml->sellerInfo->feedbackScore);

            $merchant->setRating($rating);
            $merchantActual->setRating($rating);

            $offer->setMerchant($merchant);
            $offer->setMerchantActual($merchantActual);
        }

        return $offer;
    }

    /**
     * @param string $url
     * @return string
     */
    protected function removeProtocolFromUrl($url)
    {
        if (strpos($url, 'http://') === 0)
            return substr($url, 5);
        else
            return $url;
    }

    /**
     * @return Currency|null
     */
    protected function makeCurrency()
    {
        return Currency::createByCode(Currency::USD_CODE);
    }
}