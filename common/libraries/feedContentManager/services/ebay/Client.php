<?php


namespace common\libraries\feedContentManager\services\ebay;


use common\libraries\feedContentManager\feed\Sort;
use common\libraries\feedContentManager\tools\Url;

class Client extends \common\libraries\feedContentManager\services\feed\Client
{
    const SORT_PRICE_DESC = 'PricePlusShippingHighest';
    const SORT_PRICE_ASC = 'PricePlusShippingLowest';
    const SORT_RELEVANCY_DESC = 'BestMatch';

    /** @var bool  */
    protected $isDetail = false;

    public function prepareSearchUrls()
    {
        $request = $this->getRequest();

        $params = [
            'paginationInput.entriesPerPage' => $request->getPerPage(),
            'paginationInput.pageNumber'     => $request->getPage(),
            'keywords'                       => $this->isDetail ? $request->getItemId() : $request->getKeyword(),
            'OPERATION-NAME'                 => 'findItemsByKeywords'
        ];

        $feedSort = $this->getFeedSort();
        if ($feedSort){
            $params['sortOrder'] = $feedSort;
        }

        $filters = [];
        $filters['HideDuplicateItems'] = 'true';

        if (!$this->isDetail) {
            if ($minPrice = $this->getMinPrice()) {
                $filters['MinPrice'] = $minPrice;
            }

            if ($maxPrice = $this->getMaxPrice()) {
                $filters['MaxPrice'] = $maxPrice;
            }
        }

        $index = 0;
        foreach ($filters as $filter => $value) {
            $params['itemFilter(' . $index . ').name'] = $filter;
            $params['itemFilter(' . $index . ').value'] = $value;
            $index++;
        }

        $selectors = [
            'GalleryInfo',
            'SellerInfo',
            'StoreInfo',
            'AspectHistogram',
            'ConditionHistogram'
        ];

        foreach ($selectors as $key => $selector) {
            $params['outputSelector(' . $key . ')'] = $selector;
        }

        $this->createUrl($params);
    }

    /**
     * @return null|string
     */
    protected function getFeedSort()
    {
        $sort = $this->getSort();
        if ($sort) {
            $sortValue = $sort->getValue();

            switch ($sortValue) {
                case Sort::SORT_VALUE_PRICE:
                    if ($sort->getValue() && $sort->getValue() == Sort::SORT_TYPE_DESC) {
                        return self::SORT_PRICE_DESC;
                    } else {
                        return self::SORT_PRICE_ASC;
                    }
                case Sort::SORT_VALUE_RELEVANCE:
                    return self::SORT_RELEVANCY_DESC;
                default:
                    return null;
            }
        }

        return null;
    }

    public function prepareDetailUrls()
    {
        $this->isDetail = true;

        $this->prepareSearchUrls();
    }

    protected function createUrl($params)
    {
        $params['SECURITY-APPNAME'] = 'Distinct-a408-49cf-8fb8-e7771d433312';
        $params['affiliate.trackingId'] = '5338362792';
        $params['SERVICE-VERSION'] = '1.0.0';
        $params['RESPONSE-DATA-FORMAT'] = 'XML';
        $params['GLOBAL-ID'] = 'EBAY-US';
        $params['affiliate.networkId'] = 9;
        $params['affiliate.customId'] = 'test';

        $uri = 'http://svcs.ebay.com/services/search/FindingService/v1?' . http_build_query($params) . '&REST-PAYLOAD';

        $url = new Url($this->getFeed()->short_name, $uri, $this->getAction(), 3);
        $url->setUseCache(true);
        $url->setCacheTTL(10800); //3 * 60 * 60

        $this->addUrl($url);
    }
}