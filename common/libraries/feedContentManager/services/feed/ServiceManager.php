<?php


namespace common\libraries\feedContentManager\services\feed;


use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Product;
use common\libraries\feedContentManager\tools\CurlRequestHandler;
use common\libraries\feedContentManager\tools\Url;
use common\libraries\feedContentManager\tools\UrlServicesGroup;
use common\helpers\Base;

class ServiceManager
{
    /** @var  Service[] */
    protected $services;

    /**
     * @return Service[]
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param Service[] $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    /**
     * @param Service $service
     */
    public function addService($service)
    {
        $this->services[] = $service;
    }

    /**
     * @return Result[]
     */
    public function getResults()
    {
        $this->exec();

        $results = [];
        $services = $this->getServices();
        foreach ($services as $service) {
            $serviceResults = $service->getExtractor()->getResult();
            if ($serviceResults) {
                $results[$serviceResults->getRequest()->getRequestId()] = $serviceResults;
            }
        }

        return $results;
    }

    protected function exec()
    {
        $this->prepareServices();

        $maxSteps = 3;
        do {
            $contents = [];

            $urlServicesGroups = $this->getUrlServicesGroups();

            if (count($urlServicesGroups)) {
                $nonCachedUrls = $this->getNonCachedUrlByUrlServicesGroups($urlServicesGroups);

                if (count($nonCachedUrls)) {
                    $curl = new CurlRequestHandler();
                    $curl->setUrls($nonCachedUrls);
                    $results = $curl->exec();

                    foreach ($results as $index => $result) {
                        $contents[$index] = $result->getContent();
                    }
                }

                foreach ($urlServicesGroups as $key => $urlServicesGroup) {
                    $url = $urlServicesGroup->getUrl();
                    $serviceIds = $urlServicesGroup->getServiceIds();

                    $isContentFromCache = false;
                    if( key_exists($key, $contents) ) {
                        $content = Base::array_get($contents, $key);
                    } else {
                        $content = $this->getCache($url->getCacheId());
                        $isContentFromCache = true;
                    }

                    foreach ($serviceIds as $serviceId) {
                        $isParseStatusOk = false;
                        if( $service = Base::array_get($this->getServices(), $serviceId) ) {
                            $isParseStatusOk = $service->parse($content, $url);
                        }

                        //save in cache
                        if (!$isContentFromCache && $url->isUseCache() && $isParseStatusOk && $url->getCacheId() && $url->getCacheTTL()) {
                            $cacheTTL = $url->getCacheTTL();
                            $this->saveCache($url->getCacheId(), $content, $cacheTTL);
                        }
                    }
                }
            }

        } while( count( $urlServicesGroups ) && --$maxSteps > 0);
    }

    /**
     * @param string $key
     * @return bool|false|mixed
     */
    protected function getCache($key)
    {
        if($this->existCache($key)) {
            $cache = \Yii::$app->cache;
            return $cache->get($key);
        }

        return false;
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function existCache($key)
    {
        $cache = \Yii::$app->cache;

        return $cache->exists($key);
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function saveCache($key, $value, $duration)
    {
        $cache = \Yii::$app->cache;
        return $cache->set($key, $value, $duration);
    }

    /**
     * @throws \Exception
     */
    protected function prepareServices()
    {
        foreach( $this->getServices() as $key => $service) {
            try {
                $service->prepare();
            }catch (\Exception $e){
                throw $e;
            }
        }
    }

    /**
     * @return Url[]
     */
    protected function getUrls()
    {
        $urls = [];
        foreach ($this->getServices() as $service) {
            $urls = array_merge($urls, $service->getClient()->getUrls());
        }

        return $urls;
    }

    /**
     * @return UrlServicesGroup[]
     */
    protected function getUrlServicesGroups()
    {
        $urlServicesGroups = [];
        foreach ($this->getServices() as $serviceId => $service) {
            $urls = $service->getClient()->pullUrls();
            if (count($urls)) {
                foreach ($urls as $url) {
                    $urlServicesGroup = new UrlServicesGroup();
                    $urlServicesGroup->setUrl($url);
                    $urlServicesGroup->addServiceId($serviceId);

                    $urlServicesGroups[] = $urlServicesGroup;
                }
            }
        }

        return $urlServicesGroups;
    }

    /**
     * @param Url[] $urls
     * @return Url[]
     */
    protected function getNonCachedUrls($urls)
    {
        return $urls;
    }

    /**
     * @param UrlServicesGroup[] $urlServicesGroups
     * @return Url[]
     */
    protected function getNonCachedUrlByUrlServicesGroups($urlServicesGroups)
    {
        $nonCachedUrls = [];
        if (count($urlServicesGroups)) {
            foreach ($urlServicesGroups as $key => $urlServicesGroup) {
                $url = $urlServicesGroup->getUrl();

                if(!$url->isUseCache()) {
                    $nonCachedUrls[$key] = $url;
                } elseif (!$url->getCacheId()) {
                    $nonCachedUrls[$key] = $url;
                } elseif (!$this->existCache($url->getCacheId())) {
                    $nonCachedUrls[$key] = $url;
                }
            }
        }

        return $nonCachedUrls;
    }

}