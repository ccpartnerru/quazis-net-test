<?php


namespace common\libraries\feedContentManager\services\feed;

use common\libraries\feedContentManager\entities\Filter;
use common\libraries\feedContentManager\entities\Item;
use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Product;
use common\libraries\feedContentManager\feed\FeedRequest;
use common\libraries\feedContentManager\tools\Pagination;
use common\models\Feed;

class Extractor
{
    /** @var  Service|null */
    protected $service;

    /** @var  Result|null */
    protected $result;

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return Result|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param Result|null $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @param $content string
     * @return null|\SimpleXMLElement
     */
    public function parseXml($content)
    {
        if( empty($content) ) return null;

        try{
            $content = simplexml_load_string($content);
            if( ! $content instanceof \SimpleXMLElement){
                $content = null;
            }
        }catch (\Exception $e){
            $content = null;
        }

        return $content;
    }

    /**
     * @param $content string
     * @param $assoc boolean
     * @return null|\stdClass
     */
    public function parseJson($content, $assoc = false)
    {
        if( empty($content) ) return null;

        try{
            $content = json_decode($content, $assoc);
            if( json_last_error() ){
                $content = null;
            }
        }catch (\Exception $e){
            $content = null;
        }

        return $content;
    }

    public function parse($content, $action)
    {
        $success = null;

        switch($action) {
            case ServiceAction::ACTION_SEARCH:
                $success = $this->parseSearch($content);
                break;
            case ServiceAction::ACTION_DETAIL:
                $success = $this->parseDetail($content);
                break;
            case ServiceAction::ACTION_CATEGORY:
                $success = $this->parseCategory($content);
                break;
        }

        return $success;
    }

    /**
     * @param $content
     * @return bool
     */
    public function parseSearch($content)
    {
        return false;
    }

    /**
     * @param $content
     * @return bool
     */
    public function parseDetail($content)
    {
        return false;
    }

    /**
     * @param $content
     * @return bool
     */
    public function parseCategory($content)
    {
        return false;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->getService()->getClient();
    }

    /**
     * @param Item $item
     */
    public function saveItem($item)
    {
        $this->initResult();
        $this->getResult()->addItem($item);
    }

    /**
     * @param int $totalPagesCount
     */
    public function saveTotalPagesCount($totalPagesCount)
    {
        $this->initResult();
        $this->getResult()->setTotalPagesCount($totalPagesCount);
    }

    /**
     * @param Filter $filter
     */
    public function saveFilter($filter)
    {
        $this->initResult();
        $this->getResult()->addFilter($filter);
    }

    private function initResult()
    {
        if (is_null($this->result)) {
            $result = new Result();
            $result->setRequest($this->getRequest());
            $this->setResult($result);
        }
    }

    /**
     * @return FeedRequest
     */
    protected function getRequest()
    {
        if ($this->getService()) {
            return $this->getService()->getRequest();
        }

        return null;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->getService()->getFeed();
    }
}