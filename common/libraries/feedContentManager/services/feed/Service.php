<?php


namespace common\libraries\feedContentManager\services\feed;


use common\libraries\feedContentManager\feed\FeedRequest;
use common\libraries\feedContentManager\tools\Url;
use common\models\Feed;

abstract class Service
{
    /** @var  FeedRequest */
    protected $request;

    /** @var  Client */
    protected $client;

    /** @var  Extractor */
    protected $extractor;

    /**
     * @return FeedRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param FeedRequest $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
        $client->setService($this);
    }

    /**
     * @return Extractor
     */
    public function getExtractor()
    {
        return $this->extractor;
    }

    /**
     * @param Extractor $extractor
     */
    public function setExtractor($extractor)
    {
        $this->extractor = $extractor;
        $extractor->setService($this);
    }

    /**
     * Service constructor.
     * @param Client $client
     * @param Extractor $extractor
     */
    function __construct($client, $extractor)
    {
        $this->setClient($client);
        $this->setExtractor($extractor);
    }

    public function prepare()
    {
        $this->getClient()->prepareUrls();
    }

    /**
     * Parse results. Returns TRUE if ok, FALSE if error
     *
     * @param $content
     * @param Url $url
     * @return bool|mixed
     */
    public function parse($content, $url)
    {
        $result = $this->getExtractor()->parse($content, $url->getAction());

        return $result;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->getRequest()->getAction();
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->getRequest()->getFeed();
    }
}