<?php


namespace common\libraries\feedContentManager\services\feed;


class ServiceAction
{
    const ACTION_SEARCH    = 'search';
    const ACTION_DETAIL    = 'detail';
    const ACTION_CATEGORY  = 'category';
}