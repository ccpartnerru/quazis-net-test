<?php
namespace common\libraries\feedContentManager\services\feed;


use common\libraries\feedContentManager\ContentManagerSubRequest;
use common\libraries\feedContentManager\entities\Filter;
use common\libraries\feedContentManager\entities\Item;
use common\libraries\feedContentManager\feed\FeedRequest;

class Result
{
    /** @var  Item[] */
    protected $items = [];

    /** @var  Filter */
    protected $filters = [];

    /** @var  FeedRequest */
    protected $request;

    /** @var  int */
    protected $totalPagesCount = 0;

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param Item $item
     */
    public function addItem($item)
    {
        $this->items[] = $item;
    }

    /**
     * @return Filter
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param Filter $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param Filter $filter
     */
    public function addFilter($filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * @return FeedRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param FeedRequest $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return int
     */
    public function getTotalPagesCount()
    {
        return $this->totalPagesCount;
    }

    /**
     * @param int $totalPagesCount
     */
    public function setTotalPagesCount($totalPagesCount)
    {
        $this->totalPagesCount = $totalPagesCount;
    }

}