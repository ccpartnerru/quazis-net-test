<?php


namespace common\libraries\feedContentManager\services\feed;

use common\libraries\feedContentManager\feed\Sort;
use common\libraries\feedContentManager\tools\Url;

class Client
{
    /** Sort  */
    const SORT_PRICE_ASC       = 'price_asc';
    const SORT_PRICE_DESC      = 'price_desc';
    const SORT_NAME_ASC        = 'name_asc';
    const SORT_NAME_DESC       = 'name_desc';
    const SORT_POPULARITY_ASC  = 'popular_asc';
    const SORT_POPULARITY_DESC = 'popular_desc';
    const SORT_RELEVANCY_ASC   = 'relevancy_asc';
    const SORT_RELEVANCY_DESC  = 'relevancy_desc';
    const SORT_RATING_ASC      = 'rating_asc';
    const SORT_RATING_DESC     = 'rating_desc';
    const SORT_DISCOUNT_ASC    = 'discount_asc';
    const SORT_DISCOUNT_DESC   = 'discount_desc';
    const SORT_DATE_ASC        = 'date_asc';
    const SORT_DATE_DESC       = 'date_desc';
    const SORT_REVIEWS_DESC    = 'review_desc';
    const SORT_REVIEWS_ASC     = 'review_asc';
    const SORT_DISTANCE_ASC    = 'distance_asc';
    const SORT_DISTANCE_DESC   = 'distance_desc';
    const SORT_PROMINENCE_ASC  = 'prominence_asc';
    const SORT_PROMINENCE_DESC = 'prominence_DESC';
    const SORT_NEW             = 'new';
    const SORT_YIELD_ASC       = 'yield_asc';
    const SORT_YIELD_DESC      = 'yield_desc';
    const SORT_SDC_REVENUE_ASC   = 'sdc_revenue_asc';
    const SORT_SDC_REVENUE_DESC  = 'sdc_revenue_desc';
    const SORT_RPC_DESC          = 'rpc_desc';

    /* Condition */
    const CONDITION_NEW         = 'new';
    const CONDITION_USED        = 'used';
    const CONDITION_COLLECTIBLE = 'collectible';
    const CONDITION_REFURBISHED = 'refurbished';

    /** @var  Url[] */
    protected $urls;

    /** @var  Service */
    protected $service;

    /**
     * @return Url[]
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * @param Url[] $urls
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * @param Url $url
     */
    public function addUrl($url)
    {
        $this->urls[] = $url;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return null|string
     */
    public function getAction()
    {
        if($service = $this->getService()){
            return $service->getAction();
        }

        return null;
    }

    public function prepareUrls()
    {
        $this->setUrls([]);

        switch ($this->getAction()) {
            case ServiceAction::ACTION_SEARCH:
                $this->prepareSearchUrls();
                break;

            case ServiceAction::ACTION_DETAIL:
                $this->prepareDetailUrls();
                break;

            case ServiceAction::ACTION_CATEGORY:
                $this->prepareCategoryUrls();
                break;
        }
    }

    public function prepareSearchUrls()
    {
        //TODO
    }

    public function prepareDetailUrls()
    {
        //TODO
    }

    public function prepareCategoryUrls()
    {
        //TODO
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->getService()->getRequest()->getPerPage();
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return 'EN_en';
    }

    public function pullUrls()
    {
        $urls = $this->getUrls();
        $this->setUrls([]);

        return $urls;
    }

    /**
     * @return \common\libraries\feedContentManager\feed\FeedRequest
     */
    public function getRequest() {
        return $this->getService()->getRequest();
    }

    /**
     * @return \common\models\Feed
     */
    public function getFeed()
    {
        return $this->getService()->getRequest()->getFeed();
    }

    /**
     * @return float|null
     */
    public function getMinPrice()
    {
        return $this->getRequest()->getMinPrice();
    }

    /**
     * @return float|null
     */
    public function getMaxPrice()
    {
        return $this->getRequest()->getMaxPrice();
    }

    /**
     * @return Sort|null
     */
    public function getSort()
    {
        return $this->getRequest()->getSort();
    }
}