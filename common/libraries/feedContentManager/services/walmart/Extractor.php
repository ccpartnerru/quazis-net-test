<?php


namespace common\libraries\feedContentManager\services\walmart;


use common\libraries\feedContentManager\entities\Category;
use common\libraries\feedContentManager\entities\Condition;
use common\libraries\feedContentManager\entities\Currency;
use common\libraries\feedContentManager\entities\Delivery;
use common\libraries\feedContentManager\entities\Discount;
use common\libraries\feedContentManager\entities\Image;
use common\libraries\feedContentManager\entities\Images;
use common\libraries\feedContentManager\entities\Merchant;
use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Price;
use common\libraries\feedContentManager\entities\Rating;

class Extractor extends \common\libraries\feedContentManager\services\feed\Extractor
{
    /**
     * @param string $content
     * @return bool
     */
    public function parseSearch($content)
    {
        $content = $this->parseJson($content);
        if( is_null($content) ) return false;

        $count = 0;
        if (isset($content->items) && count($content->items)) {
            foreach ($content->items as $item) {
                $offer = $this->makeOffer($item);
                $this->saveItem($offer);
                $count++;
            }
        }

        $total = $count;
        if (!empty($content->totalResults)) {
            $total = intval($content->totalResults);
        }

        $perPage = $this->getRequest()->getPerPage();
        if ($perPage > 0) {
            $this->saveTotalPagesCount(ceil($total / $perPage));
        }

        return true;
    }

    /**
     * @param string $content
     * @return bool
     */
    public function parseDetail($content)
    {
        $content = $this->parseJson($content);
        if( is_null($content) ) return false;

        if (!empty($content)) {
            $offer = $this->makeOffer($content);
            $this->saveItem($offer);
        }

        return true;
    }

    /**
     * @param \stdClass $stdObject
     * @return Offer
     */
    protected function makeOffer($stdObject)
    {
        $offer = new Offer();
        $offer->setFeed($this->getFeed());

        if( !empty($stdObject->itemId) ) {
            $offer->setId($stdObject->itemId);
        }

        if( !empty($stdObject->name) ) {
            $offer->setTitle($stdObject->name);
        }

        if( !empty($stdObject->shortDescription) ) {
            $offer->setDescription($stdObject->shortDescription);
        }

        if( !empty($stdObject->longDescription) ) {
            $offer->setLongDescription(htmlspecialchars_decode((string)$stdObject->longDescription));
        }

        if( !empty($stdObject->productUrl) ) {
            $offer->setUrl($stdObject->productUrl);
        }

        $images = new Images();
        if (!empty($stdObject->thumbnailImage)) {
            $images->setPreview($stdObject->thumbnailImage);
        }

        if (!empty($stdObject->largeImage)) {
            $images->setBig($stdObject->largeImage);
        }

        if (!empty($stdObject->imageEntities) && count($stdObject->imageEntities)) {
            foreach ($stdObject->imageEntities as $imageEntity) {
                $image = new Image();
                $image->setUrl($imageEntity->largeImage);

                $images->addImage($image);

                if (is_null($images->getPreview())) {
                    $images->setPreview($image);
                }

                if (is_null($images->getBig())) {
                    $images->setBig($image);
                }
            }
        }

        $offer->setImages($images);

        if (!empty($stdObject->categoryNode) && !empty($stdObject->categoryPath)) {
            $categoryPath = explode('/', $stdObject->categoryPath);
            $categoryNode = explode('_', $stdObject->categoryNode);
            $categoryName = $categoryPath[count($categoryNode) - 1] ?  : '';

            if (!empty($categoryName) && count($categoryNode) && isset($categoryPath[count($categoryNode)])) {
                $category = new Category();
                $category->setId($stdObject->categoryNode);
                $category->setName($categoryPath[count($categoryNode)]);
                $category->setFeed($this->getFeed());

                $offer->addCategory($category);
            }
        }

        if (!empty($stdObject->salePrice) || !empty($stdObject->msrp)) {
            $price = new Price();
            $priceValue = !empty($stdObject->salePrice) ? $stdObject->salePrice : $stdObject->msrp;
            $price->setValue($priceValue);

            $offer->setPrice($price);
        }

        $offer->setCurrency($this->makeCurrency());

        return $offer;
    }

    protected function makeCurrency()
    {
        return Currency::createByCode(Currency::USD_CODE);
    }
}