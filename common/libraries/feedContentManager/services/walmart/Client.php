<?php


namespace common\libraries\feedContentManager\services\walmart;


use common\libraries\feedContentManager\feed\Sort;
use common\libraries\feedContentManager\services\feed\ServiceAction;
use common\libraries\feedContentManager\tools\Url;

class Client extends \common\libraries\feedContentManager\services\feed\Client
{
    const SORT_TYPE_RELEVANCE       = 'relevance';
    const SORT_TYPE_PRICE           = 'price';
    const SORT_TYPE_TITLE           = 'title';
    const SORT_TYPE_BESTSELLER      = 'bestseller';
    const SORT_TYPE_CUSTOMER_RATING = 'customerRating';
    const SORT_TYPE_NEW             = 'new';

    const ORDER_TYPE_ASC    = 'asc';
    const ORDER_TYPE_DESC   = 'desc';

    /** @var string  */
    protected  $apiKey = '7gfh3q5928j6waweazptfwhq';

    /** @var string  */
    protected $publisherId = '';

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getPublisherId()
    {
        return $this->publisherId;
    }

    /**
     * @param string $publisherId
     */
    public function setPublisherId($publisherId)
    {
        $this->publisherId = $publisherId;
    }

    public function prepareSearchUrls()
    {
        $request = $this->getService()->getRequest();
        $perPage = $request->getPerPage() > 25 ? 25 : $request->getPerPage();
        $page = $request->getPage() > 1000 ? 1000 : $request->getPage();

        $start = ($page > 1 ? ($page - 1) * $perPage : 0) + 1;

        $params = [
            'query'         => $request->getKeyword(),
            'start'         => $start,
            'numItems'      => $perPage,
            'responseGroup' => 'full',
        ];

        $minPrice = $this->getMinPrice() ? $this->getMinPrice() : 0;
        $maxPrice = $this->getMaxPrice() ? $this->getMaxPrice() : 9999;
        $params['facet.range'] = 'price:[' . $minPrice . ' TO ' . $maxPrice . ']';

        $sort = $this->getSort();
        if ($sort) {
            $sortType = $this->getSortType($sort);
            if ($sortType) {
                $params['sort'] = $sortType;

                $orderType = $this->getOrderType($sort);
                if ($orderType) {
                    //This parameter is needed only for the sort types [price, title, customerRating].
                    $params['order'] = $orderType;
                }
            }
        }

        $this->createUrl($params);
    }

    /**
     * @param Sort $sort
     * @return string|null
     */
    protected function getSortType($sort)
    {
        switch ($sort->getValue()) {
            case Sort::SORT_VALUE_POPULARITY:
                return self::SORT_TYPE_BESTSELLER;
            case Sort::SORT_VALUE_PRICE:
                return self::SORT_TYPE_PRICE;
            case Sort::SORT_VALUE_RATING:
                return self::SORT_TYPE_CUSTOMER_RATING;
            case Sort::SORT_VALUE_RELEVANCE:
                return self::SORT_TYPE_RELEVANCE;
            default:
                return null;
        }
    }

    /**
     * @param Sort $sort
     * @return string|null
     */
    protected function getOrderType($sort) {
        $sortType = $this->getSortType($sort);
        if (in_array($sortType, [
            self::SORT_TYPE_PRICE,
            self::SORT_TYPE_TITLE,
            self::SORT_TYPE_CUSTOMER_RATING,
        ])) {
            switch ($sort->getType()) {
                case Sort::SORT_TYPE_ASC:
                    return self::ORDER_TYPE_ASC;
                case Sort::SORT_TYPE_DESC:
                    return self::ORDER_TYPE_DESC;
                default:
                    return null;
            }
        }

        return null;
    }

    public function prepareDetailUrls()
    {
        $request = $this->getService()->getRequest();
        $params = [
            'itemId' => $request->getItemId()
        ];

        $this->createUrl($params);
    }

    protected function createUrl($params)
    {
        $params['apiKey']        = $this->getApiKey();
        $params['lsPublisherId'] = $this->getPublisherId();
        $params['format']        = 'json';

        $action = $this->getAction();
        switch ($action) {
            case ServiceAction::ACTION_SEARCH:
                $uri = 'http://api.walmartlabs.com/v1/search?' . http_build_query($params);
                break;
            case ServiceAction::ACTION_DETAIL:
                $itemId = $params['itemId'];
                $_params = $params;
                unset($_params['itemId']);
                $uri = 'http://api.walmartlabs.com/v1/items/' . $itemId . '?' . http_build_query($_params);
                break;
            default:
                $uri = 'http://api.walmartlabs.com/v1/search?' . http_build_query($params);
                break;
        }

        $url = new Url($this->getService()->getFeed()->short_name, $uri, $action, 3);
        $url->setUseCache(true);
        $url->setCacheTTL(10800);//3 * 60 * 60

        $this->addUrl($url);
    }
}