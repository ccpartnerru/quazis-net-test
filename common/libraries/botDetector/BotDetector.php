<?php


namespace common\libraries\botDetector;

use Yii;
use common\components\User;

class BotDetector
{
    const BOT_REASON_NON_BOT            = 0;
    const BOT_REASON_USER_AGENT         = 1;
    const BOT_REASON_CLID_DUPLICATE     = 2;
    const BOT_REASON_IP_BLACK_LIST      = 3;

    /** @var  User */
    public $user;

    /** @var  int */
    public $botReason = self::BOT_REASON_NON_BOT;

    /**
     * @param null|User $user
     * @return bool
     */
    public function checkUser($user = null)
    {
        if (is_null($user)) {
            /** @var User $user */
            $this->user = Yii::$app->user;
        } else {
            $this->user = $user;
        }

        if ($this->checkUserByUserAgent())
        {
            $this->botReason = self::BOT_REASON_USER_AGENT;
            return true;
        }

        /*if ($this->checkByCLIDuplication())
        {
            $this->botReason = self::BOT_REASON_CLID_DUPLICATE;
            return true;
        }*/

        if ($this->checkUserByIPBlacklist())
        {
            $this->botReason = self::BOT_REASON_IP_BLACK_LIST;
            return true;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function checkUserByUserAgent()
    {
        $userAgent = $this->user->getUserAgent();

        return preg_match( '/(AhrefsBot|Pingdom.com_bot|google|preview|Google Web Preview|Web Preview|yahoo|msn|MixrankBot|crawler@mixrank.com|bingbot|yandexbot|python-requests|\bbot\b|watson-url-fetcher)/i', $userAgent );
    }

    /**
     * @return bool
     */
    private function checkByCLIDuplication()
    {
        $sourceClickId = $this->user->getSourceClickId();

        if ($sourceClickId == '{msclkid}') {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkUserByIPBlacklist()
    {
        $userIp = $this->user->getIp();
        $userIpNum = ip2long($userIp);

        $rules = $this->getUserIpBlacklistRules();
        if ($rules && count($rules)) {
            foreach ($rules as $rule) {
                $ipStart = ip2long($rule['ip_start']);
                $ipEnd   = ip2long($rule['ip_end']);

                if ($userIpNum >= $ipStart && $userIpNum <= $ipEnd) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function getUserIpBlacklistRules()
    {
        return [
            [
                'ip_start' => '66.249.82.140',
                'ip_end'   => '66.249.82.140',
            ],
            [
                'ip_start' => '66.249.85.110',
                'ip_end'   => '66.249.85.110',
            ],
        ];
    }
}