<?php


namespace common\libraries\safeDetector;

use common\helpers\SiteSettings;
use common\models\Source;
use Yii;
use common\components\User;

class SafeDetector
{
    const SAFE_REASON_SAFE              = 0;
    const SAFE_REASON_BOT               = 1;
    const SAFE_REASON_EMPTY_CLID        = 2;
    const SAFE_REASON_ORGANIC           = 3;
    const SAFE_REASON_MARKET            = 4;
    const SAFE_REASON_NOT_SAFE_LOCATION = 5;
    const SAFE_REASON_PROXY             = 6;

    /** @var  User */
    public $user;

    /** @var  int */
    public $notSafeReason = self::SAFE_REASON_SAFE;

    /**
     * @param null|User $user
     * @return bool
     */
    public function checkUser($user = null)
    {
        if (is_null($user)) {
            /** @var User $user */
            $this->user = Yii::$app->user;
        } else {
            $this->user = $user;
        }

        if ($this->checkUserIsBot()) {
            $this->notSafeReason = self::SAFE_REASON_BOT;
            return true;
        }

        if ($this->checkClidExist()) {
            $this->notSafeReason = self::SAFE_REASON_EMPTY_CLID;
            return true;
        }

        if ($this->checkOrganic()) {
            $this->notSafeReason = self::SAFE_REASON_ORGANIC;
            return true;
        }

        if ($this->checkMarket()) {
            $this->notSafeReason = self::SAFE_REASON_MARKET;
            return true;
        }

        if ($this->checkLocation()) {
            $this->notSafeReason = self::SAFE_REASON_NOT_SAFE_LOCATION;
            return true;
        }

        if ($this->checkProxy()) {
            $this->notSafeReason = self::SAFE_REASON_PROXY;
            return true;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function checkUserIsBot()
    {
        return $this->user->isBot();
    }

    /**
     * @return bool
     */
    private function checkClidExist()
    {
        $user = $this->user;
        $sourcePartnerId = $user->getSourcePartnerId();

        if ($sourcePartnerId && in_array($sourcePartnerId, [Source::PARTNER_ID_ADWORDS_SEARCH]) && empty($user->getSourceClickId())) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkOrganic()
    {
        if (empty($this->user->getSourcePartnerId())) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkMarket()
    {
        if ($this->user->getMarket()->id != SiteSettings::getSiteMarketId()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkLocation()
    {
        $user = $this->user;
        $userCity = mb_strtolower($user->getCity());
        $userRegion = mb_strtolower($user->getRegion());

        $notSafeLocationsConfig = $this->getNotSafeLocationsConfig();
        if ($notSafeLocationsConfig && count($notSafeLocationsConfig)) {
            foreach ($notSafeLocationsConfig as $notSafeLocation) {
                $locationCity = mb_strtolower($notSafeLocation['city']);
                $locationRegion = mb_strtolower($notSafeLocation['region']);

                if ($userCity == $locationCity && $userRegion == $locationRegion) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function getNotSafeLocationsConfig()
    {
        return [
            [
                "city" => "pompano beach",
                "region" => "fl"
            ],
            [
                "city" => "mountain view",
                "region" => "ca"
            ],
            [
                "city" => "stanford",
                "region" => "ca"
            ],
            [
                "city" => "council bluffs",
                "region" => "ia"
            ],
            [
                "city" => "provo",
                "region" => "ut"
            ],
            [
                "city" => "princeton",
                "region" => "nj"
            ],
            [
                "city" => "kansas city",
                "region" => "ks"
            ]
        ];
    }

    /**
     * @return bool
     */
    private function checkProxy()
    {
        if ($_SERVER['HTTP_X_REAL_IP'] != $_SERVER['HTTP_X_FORWARDED_FOR']) {
            return true;
        }

        if (!empty($_SERVER['HTTP_VIA'])) {
            return true;
        }

        return false;
    }
}