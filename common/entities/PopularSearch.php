<?php


namespace common\entities;


use common\models\SearchKeyword;

class PopularSearch
{
    /** @var  string */
    public $name;

    /**
     * @param int $count
     * @return PopularSearch[]
     */
    public static function getPopularSearches($count)
    {
        $popularSearches = [];
        $keywords = self::getDefaultKeywords();
        $keywordKeys = array_rand($keywords, $count);

        foreach ($keywordKeys as $keywordKey) {
            $keyword = $keywords[$keywordKey];
            $popularSearch = new self();
            $popularSearch->name = $keyword;

            $popularSearches[] = $popularSearch;
        }

        shuffle($popularSearches);

        return $popularSearches;
    }

    /**
     * @return array
     */
    protected static function getDefaultKeywords()
    {
        return [
            'Gaming Laptops',
            'iPhone XS',
            'Samsung TVs',
            'iPad Pro',
            'Laptop Sale',
            'Kitchen Appliances',
            'Tires',
            'Tommy Hilfiger Outlet',
            'Home Depot',
            'Washing Machines',
            'Handbags',
            'Smartphones',
            'Appliances Sale',
            'Fitbit',
            'iPhone XR',
            'Samsung Galaxy',
            'Apple Watch Series 4',
            'Microwave Oven',
            'Vacuum Cleaners',
            'Canada Goose Jackets'
        ];
    }
}