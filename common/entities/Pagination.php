<?php


namespace common\entities;


use common\helpers\Url;

class Pagination
{
    /** @var  int */
    private $totalPagesCount = 0;

    /**
     * @return int
     */
    public function getTotalPagesCount()
    {
        return $this->totalPagesCount;
    }

    /**
     * @param int $totalPagesCount
     */
    public function setTotalPagesCount($totalPagesCount)
    {
        $this->totalPagesCount = $totalPagesCount;
    }

    /**
     * @return bool
     */
    public function isAvailableNextPage()
    {
        $totalPagesCount = $this->getTotalPagesCount();
        $currentPageNumber = Url::getPageNumber();

        if ($currentPageNumber + 1 <= $totalPagesCount) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAvailablePrevPage()
    {
        $currentPage = Url::getPageNumber();

        if ($currentPage <= 1) {
            return false;
        }

        return true;
    }

    /**
     * @param int $pageNumber
     * @return bool
     */
    public function isAvailablePage($pageNumber)
    {
        if ($pageNumber >= 1) {
            true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getLinkNextPage()
    {
        $currentPage = Url::getPageNumber();
        $nextPage = $currentPage < 1 ? 1 : $currentPage + 1;
        $pageNumberParam = Url::getPageNumberParam();

        $params = [
            $pageNumberParam => $nextPage
        ];

        return Url::modifyUrlParams($params);
    }

    /**
     * @return string
     */
    public function getLinkPrevPage()
    {
        $currentPage = Url::getPageNumber();
        $prevPage = $currentPage <= 1 ? 1 : $currentPage - 1;
        $pageNumberParam = Url::getPageNumberParam();

        $params = [
            $pageNumberParam => $prevPage
        ];

        return Url::modifyUrlParams($params);
    }

    /**
     * @param int $pageNumber
     * @return string
     */
    public function getLinkByPage($pageNumber)
    {
        $_pageNumber = $pageNumber <= 1 ? 1 : $pageNumber;
        $pageNumberParam = Url::getPageNumberParam();

        $params = [
            $pageNumberParam => $_pageNumber
        ];

        return Url::modifyUrlParams($params);
    }
}