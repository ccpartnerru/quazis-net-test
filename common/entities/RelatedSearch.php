<?php


namespace common\entities;


use common\models\SearchKeyword;

class RelatedSearch
{
    /** @var  string */
    public $name;

    /**
     * @param string $keyword
     * @return RelatedSearch[]
     */
    public static function getByKeyword($keyword)
    {
        $searchKeyword = SearchKeyword::findOne(['keyword' => $keyword]);
        if (is_null($searchKeyword) || count($searchKeyword->relatedKeywords) == 0) {
            return [];
        }

        $result = [];
        foreach ($searchKeyword->relatedKeywords as $relatedKeyword) {
            $relatedSearch = new self();
            $relatedSearch->name = $relatedKeyword->keyword_related;
            $result[] = $relatedSearch;
        }

        return $result;
    }

    /**
     * @return array
     */
    protected static function getDefaultRelatedKeywords()
    {
        return [
            'laptops',
            'gaming laptops',
            'best laptops',
            'laptops for school',
            'laptops',
            'gaming laptops',
            'best laptops',
            'laptops for school',
            'laptops',
            'gaming laptops',
            'best laptops',
            'laptops for school',
        ];
    }
}