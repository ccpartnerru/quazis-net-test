<?php


namespace common\entities\statClick;

class Response
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR   = 2;

    const ERROR_CODE_DEFAULT = 1;

    /** @var  int */
    protected $status;

    /** @var  int */
    protected $errorCode;

    /** @var  string */
    protected $message;

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        if (empty($this->message)) {
            $this->message = $this::getMessageByErrorCode($this->getErrorCode());
        }

        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param $code
     * @return string
     * @throws \Exception
     */
    protected static function getMessageByErrorCode($code)
    {
        switch ($code) {
            case self::ERROR_CODE_DEFAULT:
                return 'An error has occurred!';
                break;
            default:
                throw new \Exception('Unknown error code: ' . $code);
                break;
        }
    }

    /**
     * @return array
     */
    public function toArray() {
        $a = [];

        $a['status'] = $this->getStatus();
        if ($this->getStatus() == self::STATUS_ERROR) {
            $a['errorCode'] = $this->getErrorCode();
            $a['message'] = $this->getMessage();
        }

        return $a;
    }
}