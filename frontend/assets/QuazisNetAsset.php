<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class QuazisNetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/quazis_net/assets';
    public $sourcePath = '@app/themes/quazis_net/assets';
    public $publishOptions = [
        'forceCopy'=>true
    ];

    public $css = [
        'css/theme-default/main.css'
    ];

    public $js = [
        'js/main.js'
    ];

    public $depends = [
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_END];
}
