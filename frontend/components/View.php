<?php


namespace frontend\components;


class View extends \yii\web\View
{
    public $bodyClasses = [];

    /**
     * @param array $classes
     */
    public function addBodyClasses($classes) {
        $this->bodyClasses = array_merge($this->bodyClasses, $classes);
    }
}