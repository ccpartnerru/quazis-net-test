<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $execution_time int */
/* @var $resultItems \common\libraries\feedContentManager\entities\Item[] */
/* @var $keyword string */
/* @var $keywordYahoo string */
/* @var $keywordEbay string */
/* @var $keywordWalmart string */
/* @var $keywordAliexpress string */

$this->title = 'My Yii Application';

?>

<div class="shopping-search">
    <div class="search-form" style="margin-bottom: 20px">
        <?= Html::beginForm([''], 'GET'); ?>
        <?= Html::textInput(\common\helpers\Url::getKeywordParam(), $keyword, ['placeholder' => 'search']) ?>
        <?= Html::endForm(); ?>
    </div>
    <div style="margin-bottom: 20px">
        <div>keyword: <?= $keyword ?></div>
        <div>Yahoo: <?= $keywordYahoo ?></div>
        <div>Ebay: <?= $keywordEbay ?></div>
        <div>Walmart: <?= $keywordWalmart ?></div>
        <div>Aliexpress: <?= $keywordAliexpress ?></div>
    </div>
    <b>Total Execution Time:</b> <?= $execution_time ?> Sec <br>
    <div>
        <?php

        foreach ($resultItems as $item) {
            $itemType = $item->getItemType();
            echo '<div data-item-feed-partner-id="' . $item->getFeed()->id . '">';
            switch ($itemType) {
                case $item::ITEM_TYPE_OFFER:
                    /** @var \common\libraries\feedContentManager\entities\Offer $item */
                    echo 'this is offer<br>';
                    echo 'id: ' . $item->getId() . '<br>';
                    echo 'feed name: ' . $item->getFeed()->name . '<br>';
                    echo 'title: ' . $item->getTitle() . '<br>';
                    if ($item->getPrice()) {
                        echo 'price: ' . $item->getPrice()->getValue() . ' ' . $item->getCurrency()->getSign() . '<br>';
                    }
                    echo 'url: ' . $item->getUrl() . '<br>';
                    $detailUrl = \yii\helpers\Url::toRoute(['shopping/detail', 'feedPartnerShortName' => $item->getFeed()->short_name, 'itemId' => $item->getId()], true);
                    echo 'detail page: ' . $detailUrl . '<br>';
                    if ($item->getUrl()) {
                        echo Html::a('item direct url', $item->getUrl(), ['class' => 'trk', 'target' => '_blank']);
                    } else {
                        echo Html::a('item detail page', $detailUrl);
                    }
                    echo '<br>';
                    break;
                case $item::ITEM_TYPE_PRODUCT:
                    /** @var \common\libraries\feedContentManager\entities\Product $item */
                    echo 'this is product<br>';
                    echo 'id: ' . $item->getId() . '<br>';
                    echo 'feed: ' . $item->getFeed()->name . '<br>';
                    echo 'title: ' . $item->getTitle() . '<br>';
                    echo 'price: ' . $item->getPrices()->getMin()->getValue() . ' ' . $item->getCurrency()->getSign() . '<br>';
                    echo 'url: ' . $item->getUrl() . '<br>';
                    $detailUrl = \yii\helpers\Url::toRoute(['shopping/detail', 'feedPartnerShortName' => $item->getFeed()->short_name, 'itemId' => $item->getId()], true);
                    echo 'detail page: ' . $detailUrl . '<br>';
                    if ($item->getUrl()) {
                        echo Html::a('item direct url', $item->getUrl(), ['class' => 'trk', 'target' => '_blank']);
                    } else {
                        echo Html::a('item detail page', $detailUrl);
                    }
                    echo '<br>';
                    break;
                case $item::ITEM_TYPE_UNKNOWN:
                    echo 'this is unknown item<br>';
                    break;
            }
            echo '</div>';

            echo '<hr>';
        }

        ?>
    </div>
</div>

<?php

/** @var \common\components\User $user */
$user = Yii::$app->user;
$inboundStatClickId = $user->getInboundStatClickId();
$userLinkKey = $user->getLinkKey();

$currentLinkKey = \common\helpers\Url::getLinkKey();

?>

<?php if (!$inboundStatClickId || ($currentLinkKey && $userLinkKey != $currentLinkKey)) { ?>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        var url = '<?= \common\helpers\StatClickUrlGenerator::generateInboundClickUrl() ?>';

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    });
</script>
<?php } ?>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        var trkElements = [].slice.call(document.querySelectorAll(".trk"));

        if (trkElements.length) {
            trkElements.forEach(function(node){
                node.addEventListener('click', function(e) {
                    var requestReferParam = '<?= \common\helpers\StatClickUrlGenerator::getRequestReferParam() ?>';
                    var requestRefer = btoa(node.getAttribute('href'));

                    var feedPartnerIdParam = '<?= \common\helpers\StatClickUrlGenerator::getRequestFeedPartnerIdParam() ?>';
                    var feedPartnerId = getElementFeedPartnerId(node);

                    var url = '<?= \common\helpers\StatClickUrlGenerator::generateOutboundClickUrl() ?>';
                    url += '&' + requestReferParam + '=' + requestRefer;
                    url += '&' + feedPartnerIdParam + '=' + feedPartnerId;

                    var xmlhttp = new XMLHttpRequest();
                     xmlhttp.open("GET", url, true);
                     xmlhttp.send();
                });
            });
        }

        function getElementFeedPartnerId(e, iteration) {
            if (!iteration) {
                iteration = 0;
            }

            if (iteration > 20) {
                return null;
            }

            var feedPartnerId = e.getAttribute('data-item-feed-partner-id');

            if (!feedPartnerId && e.parentElement) {
                return getElementFeedPartnerId(e.parentElement, iteration + 1);
            }

            return feedPartnerId;
        }
    });
</script>
