<?php

/* @var $this yii\web\View */
/* @var $execution_time int */
/* @var $item \common\components\contentManager\entities\Item */

$this->title = 'My Yii Application';

?>

<div class="shopping-search">
    <b>Total Execution Time:</b> <?= $execution_time ?> Sec <br>
    <div>
        <pre>
            <?php print_r($item); ?>
        </pre>
        <?php

        if ($item) {
            switch ($item->getItemType()) {
                case $item::ITEM_TYPE_OFFER:
                    /** @var \common\components\contentManager\entities\Offer $item */
                    echo 'this is offer<br>';
                    echo 'partner: ' . $item->getFeed()->name . '<br>';
                    echo 'title: ' . $item->getTitle() . '<br>';
                    if ($item->getPrice()) {
                        echo 'price: ' . $item->getPrice()->getValue() . ' ' . $item->getCurrency()->getSign() . '<br>';
                    }
                    echo 'url: ' . $item->getUrl() . '<br>';
                    break;
                case $item::ITEM_TYPE_PRODUCT:
                    /** @var \common\components\contentManager\entities\Product $item */
                    echo 'this is product<br>';
                    echo 'partner: ' . $item->getFeed()->name . '<br>';
                    echo 'title: ' . $item->getTitle() . '<br>';
                    echo 'price: ' . $item->getPrices()->getMin()->getValue() . ' ' . $item->getCurrency()->getSign() . '<br>';
                    echo 'url: ' . $item->getUrl() . '<br>';
                    break;
                case $item::ITEM_TYPE_UNKNOWN:
                    echo 'this is unknown item<br>';
                    break;
            }
        }

        ?>
    </div>
</div>
