require('events').EventEmitter.prototype._maxListeners = 100;

const path = require('path');
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const minifyCSS = require('gulp-clean-css');
const notify= require('gulp-notify');
const webpackStream = require('webpack-stream');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

//--------------------------------------------------------------------

var sass_path = 'sass/**/*.scss';
var css_path = '../assets/css/';
var js_res = 'js/';
var js_dest = '../assets/js/';

// css production
//--------------------------------------------------------------------
gulp.task('css-prod', function () {

    gulp.src(sass_path)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['> 0.2%', 'ie 9']
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest(css_path));
    });

// css development
//--------------------------------------------------------------------
gulp.task('css-dev', function () {
    gulp.src(sass_path)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .on('error', notify.onError({
        title: "Gulp",
        message: 'SASS compiling error'
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(css_path))
});

const webpack_config_dev = {
  mode: 'development',
  entry: {
    main: './js/main.js'
  },
  output: {
    path: path.resolve(__dirname, 'assets/js'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  devtool: 'eval',
}

const webpack_config_prod = {
  mode: 'production',
  entry: {
    //   test: './js/test.js',
    main: './js/main.js'
  },
  output: {
    path: path.resolve(__dirname, 'assets/js'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
//  devtool: 'source-map',
  plugins: [
    new UglifyJSPlugin({ sourceMap: false })
  ],
  // externals: {
  //   jquery: 'jQuery'
  // }
}

// javascripts
//--------------------------------------------------------------------
gulp.task('js-dev', function() {
  return gulp.src('./js/index.js')
    .pipe(webpackStream( webpack_config_dev))
    .on('error', function handleError() {
      this.emit('end'); // Recover from errors
    })
    .pipe(gulp.dest('../assets/js'));
});

gulp.task('js-prod', function() {
  return gulp.src('./js/index.js')
    .pipe(webpackStream( webpack_config_prod))
    .on('error', function handleError() {
      this.emit('end'); // Recover from errors
    })
    .pipe(gulp.dest('../assets/js'));
});


// watch
//--------------------------------------------------------------------
gulp.task('watch', function(){
    gulp.watch('sass/**/**/*.scss' , ['css-dev']);
    gulp.watch('js/**/*.js' , ['js-dev']);
});

// default
//--------------------------------------------------------------------
gulp.task('default', ['css-prod', 'js-prod']);

