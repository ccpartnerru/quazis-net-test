const categories = [].slice.call(document.querySelectorAll(".category"));

if (categories.length) {

  categories.forEach(function(category){
    const list = category.querySelector(".category__list");
    const hidden = category.querySelector(".category__hidden");
    const btn = category.querySelector(".category__more");

    hidden.style.maxHeight = hidden.offsetHeight + 'px';
    hidden.classList.add('is-invisible')

    if (list && hidden && btn) {
      btn.addEventListener('click', function(e){
        // toggle
        hidden.classList.toggle('is-invisible')
        btn.classList.toggle('icon-chevron-down')
        btn.classList.toggle('icon-chevron-up')
      })

      list.addEventListener('mouseover', function(e){
        // open
        hidden.classList.remove('is-invisible')
        btn.classList.remove('icon-chevron-down')
        btn.classList.add('icon-chevron-up')
      })
      list.addEventListener('mouseout', function(e){
        // hide
        hidden.classList.add('is-invisible')
        btn.classList.add('icon-chevron-down')
        btn.classList.remove('icon-chevron-up')
      })

      // hide dropdowns when clicking outside
      document.addEventListener('click', function(event) {
        const isClickInside = category.contains(event.target);
        if (!isClickInside) {
          // hide
          hidden.classList.add('is-invisible')
          btn.classList.add('icon-chevron-down')
          btn.classList.remove('icon-chevron-up')
        }
      });
    }//end if

  })// end loop
}// end if
