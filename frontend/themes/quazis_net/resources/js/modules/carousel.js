import Siema from 'siema'

// Arrow buttons
//-------------------------------------------------------------------------------------
Siema.prototype.addArrows = function() {
  if (this.innerElements.length > this.perPage){
    // make buttons & append them inside Siema's container
    this.prevArrow = document.createElement('div');
    this.nextArrow = document.createElement('div');
    this.prevArrow.classList.add('carousel__prev');
    this.nextArrow.classList.add('carousel__next');
    this.prevArrow.innerHTML = '<i class="icon icon-chevron-left"></i>';
    this.nextArrow.innerHTML = '<i class="icon icon-chevron-right"></i>';
    this.selector.appendChild(this.prevArrow)
    this.selector.appendChild(this.nextArrow)

    // event handlers on buttons
    this.prevArrow.addEventListener('click', () => this.prev(this.perPage));
    this.nextArrow.addEventListener('click', () => this.next(this.perPage));
  }
}

Siema.prototype.hideInactiveArrows = function() {
  if (this.nextArrow && this.prevArrow) {

    if ((this.currentSlide + this.perPage) == this.innerElements.length) {
      this.nextArrow.classList.add('is-hidden')
    }
    else{
      this.nextArrow.classList.remove('is-hidden')
    }

    if ((this.currentSlide) == 0) {
      this.prevArrow.classList.add('is-hidden')
    }
    else{
      this.prevArrow.classList.remove('is-hidden')
    }
  }
}

//// Pagination
////-------------------------------------------------------------------------------------
//const CarouselPager = function(carousel, page_item_selector){
//  const self = this;
//  this.carousel = carousel;
//  this.pages = [].slice.call(document.querySelectorAll(page_item_selector));
//  this.pages.forEach(function(el,i){
//    el.addEventListener('click', function(e) {
//      e.preventDefault();
//      self.pages.forEach((p) => p.classList.remove('is-active'));
//      self.carousel.goTo(i);
//      el.classList.add('is-active');
//    })
//  })
//}

//CarouselPager.prototype.update = function() {
//  this.pages.forEach((el) => el.classList.remove('is-active'));
//  this.pages[Math.round(this.carousel.currentSlide)].classList.add('is-active');
//}


// Reinit on resize
//-------------------------------------------------------------------------------------
Siema.prototype.reinit = function(breakpoint = 0) {
  if (window.innerWidth >= breakpoint) {
    this.init()
  }
  else{
    this.destroy(true)
  }
}

document.addEventListener('DOMContentLoaded', function () {

  // Articles carousel
  //-------------------------------------------------------------------------------------
  const articles_carousels = [].slice.call(document.querySelectorAll('.articles-carousel'));

  if (articles_carousels.length) {

    articles_carousels.forEach(selector => {

      const articles_carousel = new Siema({
        selector: selector.querySelector('.carousel__wrap'),
        duration: 200,
        easing: 'ease-out',
        perPage: {0: 2, 640: 3, 820:4},
        startIndex: 0,
        draggable: true,
        multipleDrag: true,
        threshold: 20,
        loop: false,
        onInit: function() {
          this.addArrows()
          this.hideInactiveArrows();
        },
        onChange: function(){
          this.hideInactiveArrows();
        },
      })

      articles_carousel.reinit(641);
      window.addEventListener('resize', function() {
        articles_carousel.reinit(641);
      })

    })// end loop
  }//end if


// Products carousel
//-------------------------------------------------------------------------------------
const product_carousels = [].slice.call(document.querySelectorAll('.products-carousel'));

if (product_carousels) {
  product_carousels.forEach(selector => {
    const products_carousel = new Siema({
      selector: selector.querySelector('.carousel__wrap'),
      duration: 200,
      easing: 'ease-out',
      perPage: {0:1.75, 359:2.1, 520:3.5,  768:3.5, 820: 4, 920:5},
      startIndex: 0,
      draggable: true,
      multipleDrag: true,
      threshold: 20,
      loop: false,
      onInit: function() {
        this.addArrows()
        this.hideInactiveArrows();

      },
      onChange: function(){
        this.hideInactiveArrows();
      },
    })

    window.addEventListener('resize', function() {
      products_carousel.reinit();
    })

  })// end loop


}//end if




}) //DOMContentLoaded
