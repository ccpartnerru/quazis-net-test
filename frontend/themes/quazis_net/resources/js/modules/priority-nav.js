import priorityNav from 'priority-nav';

var nav = priorityNav.init({
  initClass:                  "js-priority-nav", // Class that will be printed on html element to allow conditional css styling.
  mainNavWrapper:             "nav.priority-nav", // mainnav wrapper selector (must be direct parent from mainNav)
  mainNav:                    "ul", // mainnav selector. (must be inline-block)
  navDropdownClassName:       "priority-nav__dropdown", // class used for the dropdown - this is a class name, not a selector.
  navDropdownToggleClassName: "priority-nav__dropdown-toggle", // class used for the dropdown toggle - this is a class name, not a selector.
  navDropdownLabel:           "", // Text that is used for the dropdown toggle.
  navDropdownBreakpointLabel: "More", //button label for navDropdownToggle when the breakPoint is reached.
  breakPoint:                 320, //amount of pixels when all menu items should be moved to dropdown to simulate a mobile menu
  throttleDelay:              50, // this will throttle the calculating logic on resize because i'm a responsible dev.
  offsetPixels:               0, // increase to decrease the time it takes to move an item.
  count:                      true, // prints the amount of items are moved to the attribute data-count to style with css counter.
});

