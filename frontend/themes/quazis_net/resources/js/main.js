// Object fit polyfill for IE/Edge
import objectFitImages from 'object-fit-images';
objectFitImages();

// Support for classList in IE9
import 'classlist-polyfill';

// Header dropdown menu
//import './modules/dropdown.js';

// Categories dropdown
//import './modules/categories-dropdown.js';

// Priority nav
import './modules/priority-nav.js';

// Tabs
import './modules/tabs.js';

// Home page carousels
import './modules/carousel.js';

// Range slider
import './modules/range-slider.js';


