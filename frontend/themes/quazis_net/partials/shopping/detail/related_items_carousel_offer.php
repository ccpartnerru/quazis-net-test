<?php

/** @var \frontend\components\View $this */
/** @var float $percentageDiscount */
/** @var string $url */
/** @var string $imageUrl */
/** @var string $target */
/** @var string $title */
/** @var string $currencySymbol */
/** @var float $price */

?>
<article class="prod-card carousel__item">
    <?php if ($percentageDiscount) { ?>
    <div class="prod-card__label">-<?= $percentageDiscount ?>%</div>
    <?php } ?>
    <a class="prod-card__img" href="<?= $url ?>">
        <img src="<?= $imageUrl ?>" alt="">
    </a>
    <div class="prod-card__body">
        <h3 class="prod-card__name">
            <a href="<?= $url ?>" target="<?= $target ?>"><?= $title ?></a>
        </h3>
        <div class="prod-card__price"><?= $currencySymbol ?><?= $price ?></div>
    </div>
</article>