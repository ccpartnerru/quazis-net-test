<?php

/** @var $this \frontend\components\View */
/** @var string $title */
/** @var string $description */
/** @var string $longDescription */
/** @var string $url */
/** @var string $target */
/** @var string $imageUrl */
/** @var float $price */
/** @var float $percentageDiscount */
/** @var float $oldPrice */
/** @var string $currencySymbol */
/** @var array $imagesUrl */
/** @var int $feedId */
/** @var string $feedShortName */
/** @var string $merchantLogoUrl */
/** @var float $rating */
/** @var array $offers */
/** @var string $shippingText */
/** @var int $customerReviewsNumber */
/** @var array $similarOffers */

$assetFolderUrl = $this->theme->baseUrl . '/assets';
?>

<section class="product-detail" data-item-feed-partner-id="<?= $feedId ?>">
    <div class="product-detail__layout">

        <header class="product-detail__header sm2-show">
            <h1 class="product-detail__name" ><?= $title ?></h1>
            <?php if ($rating || $customerReviewsNumber || $shippingText) { ?>
            <div class="product-detail__secondary-info">
                <?php if ($rating || $customerReviewsNumber) { ?>
                <div class="product-detail__rating">
                    <?php if ($rating) { ?>
                        <div class="rating-stars" data-value="<?= \common\helpers\Base::ratingHalfFormat($rating) ?>"></div>
                    <?php } ?>
                    <?php if ($customerReviewsNumber) { ?>
                        <?= Yii::t('app', '{customerReviewsNumber} customer reviews', ['customerReviewsNumber' => $customerReviewsNumber]) ?>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if ($shippingText) { ?>
                    <div class="product-detail__shipping"> <i class="icon icon-truck"></i> <?= $shippingText ?></div>
                <?php } ?>
            </div>
            <?php } ?>
        </header>

        <?php if ($imageUrl) ?>
        <div class="product-detail__img-wrap">
            <a class="product-detail__img trk" href="<?= $url ?>" target="<?= $target?>"> <img src="<?= $imageUrl ?>" alt=""> </a>

            <div class="product-detail__thumbs">
                <div class="product-detail__thumb is-active"> <img src="<?= $imageUrl ?>" alt=""> </div>
                <?php if ($imagesUrl && count($imagesUrl)) { ?>
                    <?php foreach ($imagesUrl as $_imageUrl) { ?>
                        <div class="product-detail__thumb"> <img src="<?= $_imageUrl ?>" alt=""> </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var mainImg = document.querySelector('.product-detail__img img');
                var thumbs = [].slice.call(document.querySelectorAll('.product-detail__thumb'));
                if (thumbs.length && mainImg){
                    thumbs.forEach(function(thumb){
                        thumb.addEventListener('click', function(){
                            thumbs.forEach(function(el) {el.classList.remove('is-active')});
                            thumb.classList.add('is-active');
                            var thumbImg = thumb.querySelector('img');
                            mainImg.setAttribute('src', thumbImg.getAttribute('src'));
                            // IE object-fit polyfill
                            if(thumbImg.getAttribute('data-ofi-src')) {
                                mainImg.style.backgroundImage = 'url(' + thumbImg.getAttribute('data-ofi-src') + ')';
                            }
                        })
                    })
                }
            })
        </script>

        <div class="product-detail__main">

            <header class="product-detail__header sm2-hide">
                <h1 class="product-detail__name" ><?= $title ?></h1>
                <?php if ($rating || $customerReviewsNumber || $shippingText) { ?>
                    <div class="product-detail__secondary-info">
                        <?php if ($rating || $customerReviewsNumber) { ?>
                            <div class="product-detail__rating">
                                <?php if ($rating) { ?>
                                    <div class="rating-stars" data-value="<?= \common\helpers\Base::ratingHalfFormat($rating) ?>"></div>
                                <?php } ?>
                                <?php if ($customerReviewsNumber) { ?>
                                    <?= Yii::t('app', '{customerReviewsNumber} customer reviews', ['customerReviewsNumber' => $customerReviewsNumber]) ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if ($shippingText) { ?>
                            <div class="product-detail__shipping"> <i class="icon icon-truck"></i> <?= $shippingText ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </header>

            <div class="product-detail__price price-area">
                <div class="price-area__price">
                    <div class="price-area__label"> <span><?= Yii::t('app', 'Lowest price') ?></span> </div>
                    <div class="price-area__value"> <?= $currencySymbol ?><?= number_format($price, 2)?> </div>
                    <?php if ($oldPrice) { ?>
                    <s class="price-area__old"> <?= $currencySymbol ?><?= number_format($oldPrice, 2)?> </s>
                    <?php } ?>
                </div>
                <div class="price-area__merchant">
                    <?php if ($merchantLogoUrl) { ?>
                    <a class="price-area__merchant-img trk" href="<?= $url ?>" target="<?= $target ?>">
                        <img src="<?= $merchantLogoUrl ?>" alt="">
                    </a>
                    <?php } ?>
                </div>
                <div class="price-area__btn-wrap">
                    <a class="price-area__btn btn btn--rnd btn--c2 trk" href="<?= $url ?>" target="<?= $target ?>"><?= Yii::t('app', 'Go to Store') ?></a>
                </div>
            </div>


            <div class="tabs">

                <nav class="product-detail__nav priority-nav tabs__nav">
                    <ul class="priority-nav__wrap">
                        <?php
                        $tabs = [];
                        if ($description || $longDescription) {
                            $tabs[] = Yii::t('app', 'Description');
                        }
                        $tabs[] = Yii::t('app', 'Compare Stores');

                        ?>
                        <?php foreach ($tabs as $tabNum => $tab) { ?>
                            <li><a class="<?= $tabNum == 0 ? 'is-active' : '' ?>" href="#"><?= $tab ?></a></li>
                        <?php } ?>
                    </ul>
                </nav>

                <?php if ($description || $longDescription) { ?>
                <div class="tabs__item">
                    <div class="product-detail__descr">
                        <div style="margin-bottom: 10px;"><?= $description ?></div>
                        <?php if ($longDescription) { ?>
                            <div>
                                <?= $longDescription ?>
                            </div>
                        <?php }?>
                    </div>
                </div><!-- tabs__item -->
                <?php } ?>

                <div class="tabs__item">
                    <?php if (($offers && count($offers)) || ($similarOffers && count($similarOffers))) { ?>
                        <table class="stores">
                            <thead>
                            <tr class="stores__item">
                                <th><?= Yii::t('app', 'Shop') ?></th>
                                <th><?= Yii::t('app', 'Store Rating') ?></th>
                                <th><?= Yii::t('app', 'Stock') ?></th>
                                <th><?= Yii::t('app', 'Shipping') ?></th>
                                <th><?= Yii::t('app', 'Price') ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if ($offers && count($offers)) { ?>
                                    <?php foreach ($offers as $_offer) { ?>
                                    <?php
                                        $_offerMerchantLogoUrl = $_offer['merchantLogoUrl'] ? $_offer['merchantLogoUrl'] : '';
                                        $_offerRating = $_offer['rating'] ? $_offer['rating'] : 5;
                                        $_offerStockText = $_offer['stockText'] ? $_offer['stockText'] : Yii::t('app', 'In stock');
                                        $_offerShippingText = $_offer['shippingText'] ? $_offer['shippingText'] : Yii::t('app', 'Free worldwide');
                                        $_offerPrice = $_offer['price'] ? $_offer['price'] : 0;
                                        $_offerCurrencySymbol = $_offer['currencySymbol'] ? $_offer['currencySymbol'] : '$';
                                        $_offerUrl = $_offer['url'];
                                        $_offerTarget = $_offer['target'];
                                    ?>
                                    <tr class="stores__item">
                                        <td class="stores__logo">
                                            <a class="stores__logo" href="<?= $_offerUrl ?>" target="<?= $_offerTarget ?>">
                                                <img src="<?= $_offerMerchantLogoUrl ?>" alt="">
                                            </a>
                                        </td>
                                        <td class="stores__rating">
                                            <div class="rating-stars" data-value="<?= \common\helpers\Base::ratingHalfFormat($_offerRating) ?>"></div>
                                        </td>
                                        <td class="stores__stock"><?= $_offerStockText ?></td>
                                        <td class="stores__shipping"><?= $_offerShippingText ?></td>
                                        <td class="stores__price"><?= $_offerCurrencySymbol ?><?= number_format($_offerPrice, 2) ?></td>
                                        <td class="stores__btn-col">
                                            <a class="stores__btn btn--out btn--c3 btn--sm btn--rnd"  href="<?= $_offerUrl ?>" target="<?= $_offerTarget ?>">
                                                <?= Yii::t('app', 'See it') ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($similarOffers && count($similarOffers)) { ?>
                                    <tr class="stores__divider">
                                        <td colspan="6"><?= Yii::t('app', 'Similar offers') ?></td>
                                    </tr>
                                    <?php foreach ($similarOffers as $_offerNum => $_offer) { ?>
                                        <?php
                                        if ($_offerNum >= 10) break;

                                        $_offerMerchantLogoUrl = $_offer['merchantLogoUrl'] ? $_offer['merchantLogoUrl'] : '';
                                        $_offerRating = $_offer['rating'] ? $_offer['rating'] : 5;
                                        $_offerStockText = $_offer['stockText'] ? $_offer['stockText'] : Yii::t('app', 'In stock');
                                        $_offerShippingText = $_offer['shippingText'] ? $_offer['shippingText'] : Yii::t('app', 'Free worldwide');
                                        $_offerPrice = $_offer['price'] ? $_offer['price'] : 0;
                                        $_offerCurrencySymbol = $_offer['currencySymbol'] ? $_offer['currencySymbol'] : '$';
                                        $_offerUrl = $_offer['url'];
                                        $_offerTarget = $_offer['target'];

                                        ?>
                                        <tr class="stores__item">
                                            <td class="stores__logo">
                                                <a class="stores__logo" href="<?= $_offerUrl ?>" target="<?= $_offerTarget ?>">
                                                    <img src="<?= $_offerMerchantLogoUrl ?>" alt="">
                                                </a>
                                            </td>
                                            <td class="stores__rating">
                                                <div class="rating-stars" data-value="<?= \common\helpers\Base::ratingHalfFormat($_offerRating) ?>"></div>
                                            </td>
                                            <td class="stores__stock"><?= $_offerStockText ?></td>
                                            <td class="stores__shipping"><?= $_offerShippingText ?></td>
                                            <td class="stores__price"><?= $_offerCurrencySymbol ?><?= number_format($_offerPrice, 2) ?></td>
                                            <td class="stores__btn-col">
                                                <a class="stores__btn btn--out btn--c3 btn--sm btn--rnd"  href="<?= $_offerUrl ?>" target="<?= $_offerTarget ?>">
                                                    <?= Yii::t('app', 'See it') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>

                    <?php } ?>
                </div><!-- tabs__item -->
            </div>
        </div>
    </div><!-- product__layout -->

</section>