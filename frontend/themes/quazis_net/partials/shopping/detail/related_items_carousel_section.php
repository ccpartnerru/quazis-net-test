<?php

/* @var $this \frontend\components\View */
/** @var \common\libraries\feedContentManager\entities\Item[] $relatedItems */

?>

<?php if ($relatedItems && count($relatedItems)) { ?>
<section class="related-products">
    <h2 class="related-products__heading section-heading" ><?= Yii::t('app', 'Related products') ?></h2>
    <div class="carousel products-carousel">
        <div class="carousel__wrap">
            <?php foreach ($relatedItems as $relatedItem) { ?>
                <?php
                $item = $relatedItem;
                $itemType = $item->getItemType();
                switch ($itemType) {
                    case $item::ITEM_TYPE_OFFER:
                        /** @var \common\libraries\feedContentManager\entities\Offer $item */
                        $title = $item->getTitle();
                        $description = $item->getDescription();
                        $url = \common\helpers\Url::generateShoppingDetailUrl($item->getFeed()->short_name, $item->getId());
                        $target = '_black';
                        $imageUrl = $item->getImages() && $item->getImages()->getPreview() ? $item->getImages()->getPreview()->getUrl() : '';
                        $price = $item->getPrice()->getValue();
                        $percentageDiscount = $item->getDiscount() ? $item->getDiscount()->getValue() : 0;
                        $currencySymbol = $item->getCurrency()->getSign();

                        echo $this->render('related_items_carousel_offer', [
                            'title' => $title,
                            'description' => $description,
                            'url' => $url,
                            'target' => $target,
                            'imageUrl' => $imageUrl,
                            'price' => $price,
                            'percentageDiscount' => $percentageDiscount,
                            'currencySymbol' => $currencySymbol,
                        ]);
                        break;
                    case $item::ITEM_TYPE_PRODUCT:
                        /** @var \common\libraries\feedContentManager\entities\Product $item */
                        $title = $item->getTitle();
                        $description = $item->getDescription();
                        $url = \common\helpers\Url::generateShoppingDetailUrl($item->getFeed()->short_name, $item->getId());
                        $target = '_black';
                        $imageUrl = $item->getImages()->getPreview()->getUrl();
                        $price = $item->getPrices()->getMin()->getValue();
                        $percentageDiscount = $item->getDiscount()->getValue();
                        $currencySymbol = $item->getCurrency()->getSign();

                        echo $this->render('related_items_carousel_offer', [
                            'title' => $title,
                            'description' => $description,
                            'url' => $url,
                            'target' => $target,
                            'imageUrl' => $imageUrl,
                            'price' => $price,
                            'percentageDiscount' => $percentageDiscount,
                            'currencySymbol' => $currencySymbol,
                        ]);
                        break;
                }
                ?>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
