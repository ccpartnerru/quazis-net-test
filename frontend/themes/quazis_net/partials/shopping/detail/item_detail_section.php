<?php

/* @var $this \frontend\components\View */
/** @var \common\libraries\feedContentManager\entities\Item|null $item */
/** @var \common\libraries\feedContentManager\entities\Item[] $similarItems */

$assetFolderUrl = $this->theme->baseUrl . '/assets';

if ($item instanceof \common\libraries\feedContentManager\entities\Item) {
    $itemType = $item->getItemType();
    switch ($itemType) {
        case $item::ITEM_TYPE_OFFER:
            /** @var \common\libraries\feedContentManager\entities\Offer $item */
            $title = $item->getTitle();
            $description = $item->getDescription();
            $longDescription = $item->getLongDescription();
            $url = $item->getUrl();
            $target = '_black';
            $imageUrl = $item->getImages() && $item->getImages()->getPreview() ? $item->getImages()->getPreview()->getUrl() : '';
            $price = $item->getPrice() ? $item->getPrice()->getValue() : 0;
            $percentageDiscount = $item->getDiscount() ? $item->getDiscount()->getValue() : 0;
            $oldPrice = null;
            if ($price && $percentageDiscount && $percentageDiscount > 0) {
                $oldPrice = $price * (1 + $percentageDiscount / 100);
            }
            $currencySymbol = $item->getCurrency() ? $item->getCurrency()->getSign() : '$';
            $imagesUrl = [];
            if ($item->getImages() && count($item->getImages()->getList())) {
                foreach ($item->getImages()->getList() as $imageNum => $image) {
                    if ($imageNum == 0 && $item->getFeed()->id == \common\models\Feed::PARTNER_ID_WALMART) { //replace first image from gallery
                        $imageUrl = $image->getUrl();
                    } else {
                        $imagesUrl[] = $image->getUrl();
                    }
                }
            }
            $feedId = $item->getFeed()->id;
            $feedShortName = $item->getFeed()->short_name;
            $merchantLogoUrl = getFeedLogoById($feedId);

            $rating = $item->getRating() ? $item->getRating()->getValue() : null;
            $shippingText = $item->getDelivery() && $item->getDelivery()->getText() ? $item->getDelivery()->getText() : Yii::t('app', 'Check At Store');
            $customerReviewsNumber = null;

            $offers = getItemOffers($item);

            $similarOffers = [];
            foreach ($similarItems as $similarItem) {
                $similarOffers = array_merge($similarOffers, getItemOffers($similarItem));
            }

            if (count($similarOffers)) {
                foreach (array_keys($similarOffers) as $key) {
                    if ($similarOffers[$key]['id'] == $item->getId()) {
                        unset($similarOffers[$key]);
                    }
                }

                $similarOffers = array_values($similarOffers);
            }

            echo $this->render('offer_detail_section', [
                'title' => $title,
                'description' => $description,
                'longDescription' => $longDescription,
                'url' => $url,
                'target' => $target,
                'imageUrl' => $imageUrl,
                'price' => $price,
                'percentageDiscount' => $percentageDiscount,
                'oldPrice' => $oldPrice,
                'currencySymbol' => $currencySymbol,
                'imagesUrl' => $imagesUrl,
                'feedId' => $feedId,
                'feedShortName' => $feedShortName,
                'merchantLogoUrl' => $merchantLogoUrl,
                'rating' => $rating,
                'offers' => $offers,
                'shippingText' => $shippingText,
                'customerReviewsNumber' => $customerReviewsNumber,
                'similarOffers' => $similarOffers
            ]);

            break;
        case $item::ITEM_TYPE_PRODUCT:
            /** @var \common\libraries\feedContentManager\entities\Product $item */
            $title = $item->getTitle();
            $description = $item->getDescription();
            $url = \common\helpers\Url::generateShoppingDetailUrl($item->getFeed()->short_name, $item->getId());
            $target = '_black';
            $imageUrl = $item->getImages()->getPreview()->getUrl();
            $price = $item->getPrices()->getMin()->getValue();
            $percentageDiscount = $item->getDiscount()->getValue();
            $currencySymbol = $item->getCurrency()->getSign();

            echo $this->render('offer_detail_section', [
                'title' => $title,
                'description' => $description,
                'url' => $url,
                'target' => $target,
                'imageUrl' => $imageUrl,
                'price' => $price,
                'percentageDiscount' => $percentageDiscount,
                'currencySymbol' => $currencySymbol,
            ]);
            break;
    }
}

/**
 * @param \common\libraries\feedContentManager\entities\Item $item
 * @return array
 */
function getItemOffers($item)
{
    if (empty($item)) {
        return [];
    }

    $itemOffers = [];
    switch($item->getItemType()) {
        case \common\libraries\feedContentManager\entities\Item::ITEM_TYPE_OFFER:
            /** @var \common\libraries\feedContentManager\entities\Offer $item */
            $offer = [];

            $feedId = $item->getFeed()->id;
            $offer['merchantLogoUrl'] = getFeedLogoById($feedId);
            $offer['rating'] = $item->getRating() ? $item->getRating()->getValue() : null;
            $offer['stockText'] = $item->getAvailability() ? $item->getAvailability()->getNote() : Yii::t('app', 'Check At Store');
            $offer['shippingText'] = ($item->getDelivery() && $item->getDelivery()->getText()) ? $item->getDelivery()->getText() : Yii::t('app', 'Check At Store');
            $offer['price'] = $item->getPrice() ? $item->getPrice()->getValue() : 0;
            $offer['currencySymbol'] = $item->getCurrency() ? $item->getCurrency()->getSign() : '$';
            $offer['url'] = $item->getUrl();
            $offer['target'] = '_blank';
            $offer['id'] = $item->getId();

            $itemOffers[] = $offer;
            break;
        case \common\libraries\feedContentManager\entities\Item::ITEM_TYPE_PRODUCT:
            /** @var \common\libraries\feedContentManager\entities\Product $item */
            break;
        default;
            return [];
            break;
    }

    return $itemOffers;
}

/**
 * @param int $feedId
 * @return string
 */
function getFeedLogoById($feedId)
{
    $assetFolderUrl = Yii::$app->getView()->theme->baseUrl . '/assets';

    $merchantLogoUrl = '';
    switch ($feedId) {
        case \common\models\Feed::PARTNER_ID_EBAY:
            $merchantLogoUrl = $assetFolderUrl . '/images/ebay-logo.png';
            break;
        case \common\models\Feed::PARTNER_ID_WALMART:
            $merchantLogoUrl = $assetFolderUrl . '/images/walmart-logo.png';
            break;
        case \common\models\Feed::PARTNER_ID_ALIEXPRESS:
            $merchantLogoUrl = $assetFolderUrl . '/images/aliexpress-logo.png';
            break;
    }

    return $merchantLogoUrl;
}