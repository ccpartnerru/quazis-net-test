<?php

/** @var \common\libraries\feedContentManager\entities\Item $item */

$itemType = $item->getItemType();
switch ($itemType) {
    case $item::ITEM_TYPE_OFFER:
        /** @var \common\libraries\feedContentManager\entities\Offer $item */
        $title = $item->getTitle();
        $description = $item->getDescription();
        $url = \common\helpers\Url::generateShoppingDetailUrl($item->getFeed()->short_name, $item->getId());
        $target = '_black';
        $imageUrl = $item->getImages() && $item->getImages()->getPreview() ? $item->getImages()->getPreview()->getUrl() : '';
        $price = $item->getPrice() ? $item->getPrice()->getValue() : 0;
        $percentageDiscount = $item->getDiscount() ? $item->getDiscount()->getValue() : 0;
        $currencySymbol = $item->getCurrency()->getSign();

        echo $this->render('offer', [
            'title' => $title,
            'description' => $description,
            'url' => $url,
            'target' => $target,
            'imageUrl' => $imageUrl,
            'price' => $price,
            'percentageDiscount' => $percentageDiscount,
            'currencySymbol' => $currencySymbol,
        ]);
        break;
    case $item::ITEM_TYPE_PRODUCT:
        /** @var \common\libraries\feedContentManager\entities\Product $item */
        $title = $item->getTitle();
        $description = $item->getDescription();
        $url = \common\helpers\Url::generateShoppingDetailUrl($item->getFeed()->short_name, $item->getId());
        $target = '_black';
        $imageUrl = $item->getImages()->getPreview()->getUrl();
        $price = $item->getPrices()->getMin()->getValue();
        $percentageDiscount = $item->getDiscount()->getValue();
        $currencySymbol = $item->getCurrency()->getSign();

        echo $this->render('offer', [
            'title' => $title,
            'description' => $description,
            'url' => $url,
            'target' => $target,
            'imageUrl' => $imageUrl,
            'price' => $price,
            'percentageDiscount' => $percentageDiscount,
            'currencySymbol' => $currencySymbol,
        ]);
        break;
}

?>