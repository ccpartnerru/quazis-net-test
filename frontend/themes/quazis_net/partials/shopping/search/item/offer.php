<?php

/* @var $this \frontend\components\View */
/** @var string $title */
/** @var string $description */
/** @var string $url */
/** @var string $target */
/** @var string $imageUrl */
/** @var float $price */
/** @var float $percentageDiscount */
/** @var string $currencySymbol */

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$lazyLoadImage = $assetFolderUrl . '/images/no-image.svg'

?>
<article class="prod-media">
    <div class="prod-media__left">
        <?php if ($percentageDiscount) { ?>
        <div class="prod-media__label">-<?= number_format($percentageDiscount, 0) ?>%</div>
        <?php } ?>
        <a class="prod-media__img" href="<?= $url ?>" target="<?= $target ?>"> <img class="lazy" src="<?= $lazyLoadImage ?>" data-src="<?= $imageUrl ?>" alt="<?= $title ?>"> </a>
    </div>
    <div class="prod-media__body">
        <h3 class="prod-media__name"><a href="<?= $url ?>" target="<?= $target ?>"><?= $title ?></a></h3>
        <div class="prod-media__descr"><?= strlen($description) > 256 ? mb_substr($description, 0, 256) . '...' : $description ?></div>
    </div>
    <div class="prod-media__right">
        <div class="prod-media__price"><span>from</span> <?= $currencySymbol ?><?= number_format($price, 2) ?></div>
        <a class="prod-media__btn" href="<?= $url ?>" target="<?= $target ?>"><?= Yii::t('app', 'Compare') ?></a>
    </div>
</article>