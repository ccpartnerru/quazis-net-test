<?php

/* @var $this \frontend\components\View */

$assetFolderUrl = $this->theme->baseUrl . '/assets';

?>
<header class="header">
    <div class="wrapper">
        <div class="header__logo-wrap"><a class="header__logo" href="/"><img src="<?= $assetFolderUrl ?>/images/logo.svg" alt=""></a></div>
        <div class="header__search">
            <form class="header__search-bar search-bar" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
                <input class="search-bar__input" name="<?= \common\helpers\Url::getKeywordParam() ?>" placeholder="<?= \Yii::t('app', 'Search') ?>..." type="search" required value="<?= \common\helpers\Url::getKeyword(false) ?>">
                <button class="search-bar__btn" value="" type="submit"></button>
            </form>
        </div>
        <div class="header__menu-btn"><i class="icon icon-menu"></i></div>
    </div>
</header>