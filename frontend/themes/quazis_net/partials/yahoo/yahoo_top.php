<?php

$assetFolderUrl = $this->theme->baseUrl . '/assets';

?>

<!--
<script src='https://s.yimg.com/uv/dm/scripts/syndication.js'></script>

<style>#ypaAdWrapper-default_ypa iframe{width:100%}</style>
<div id='ypaAdWrapper-default_ypa'></div>

<script type='text/javascript'>
    window.ypaAds.insertMultiAd({
        ypaAdConfig   : '00000121a',
        ypaAdTypeTag  : '',
        ypaPubParams  : {
            query: "laptop"
        },
        ypaAdSlotInfo : [
            {
                ypaAdSlotId : 'default_ypa',
                ypaAdDivId  : 'ypaAdWrapper-default_ypa',
                ypaAdWidth  : '1250',
                ypaAdHeight : '1318',
                ypaOnNoAd: function (errObj, slotInfo) {
                    console.warn(errObj);
                    console.warn(slotInfo)
                },
                ypaSlotOptions: {
                    AdOptions: {
                        DeskTop: {
                            //AdRange: '1-4',
                            Lat: true,
                            MerchantRating: true,
                            Favicon: true,
                            SiteLink: true,
                            ImageInAds: true,
                            EnhancedSiteLink: true,
                            OfficialSiteBadge: true,
                            SmartAnnotations: true
                        },
                        Mobile: {
                            //AdRange: '1-4',
                            Lat: true,
                            MerchantRating: true,
                            Favicon: true,
                            SiteLink: true,
                            ImageInAds: true,
                            EnhancedSiteLink: true,
                            OfficialSiteBadge: true,
                            SmartAnnotations: true
                        }
                    }
                }
            }
        ]
    });
</script>-->

<!--<script type="text/javascript">
    var tag = 'distinct_xml_us_searchbox_test';

    window.ypaAds.insertMultiAd({
        ypaAdConfig: '000000fc9',
        ypaAdTypeTag: '',
        ypaPubParams  : {
            query: "laptop"
        },
        ypaAdSlotInfo: [
            {
                ypaAdSlotId: 'AdUnit-1',
                ypaAdDivId: 'myDiv-AdUnit-1',
                ypaAdWidth: '600',
                ypaAdHeight: '582'
            }
        ],
        ypaAdTagOptions: {
            SponsoredSearch: {
                Mobile: {
                    SrcTag: tag
                },
                DeskTop: {
                    SrcTag: tag
                }
            }/*,
            clickBeaconServer: 'https://example.com/'*/
        }
    });
</script>-->



<div class="ypaTop">
    <div class="ypaAdBox">
        <div class="ypaAdLabel"><a class="ypaAdAnchor" href="#" onclick=""><span class="ypaAdLabelText" style="visibility:visible">Sponsored Links</span></a></div>
        <div class="ypaAdSS">
            <ul class="ypaAdUnit">
                <li class="ypaAdElement ypaAdSpacing">
                    <div class="ypaAdImageDiv">
                        <a class="ypaAdAnchor" href="link" target="_blank"><span class="ypaAdImageInAd">
                  <img class="ypaAdImageInAdImage" src="<?= $assetFolderUrl ?>/images/prod-sm-01.jpg" width="50" height="50"></span>
                        </a>
                    </div>
                    <div class="ypaAdTextDiv">
                        <div class="AdLine1">
                            <div class="ypaAdTitleAndOfficialSiteBadge">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                    <span class="ypaAdTitle">
                      <span class="ypaAdTitleInner">Dell Laptop - Free Shipping on Everything</span>
                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="ypaAdMerchantRatings">
                            <span class="ypaAdMRImage"> <img class="ypaAdMRImageImg" src="https://s.yimg.com/gi/mr/20131215/4_stars.png" width="76" height="14">&nbsp;&nbsp;</span>
                            <a class="ypaAdAnchor" href="https://search.yahoo.com/reviews?p=Laptop&amp;mrdomain=bestbuy.com&amp;ei=UTF-8" target="_blank">
                                <span="" onclick=""><span class="ypaAdMRCount">129&nbsp;</span><span class="ypaAdMRLabel">review(s)</span><span class="ypaAdMRDomain"> for deals.bestbuy.com</span>
                            </a>
                        </div>
                        <div class="ypaAdLine2">
                            <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                  <span class="ypaAdDesc">
                    <span class="ypaAdDescInner">Up to 80% Off - In Stock Now. Prices start at £120, with 1 Year Warranty and Free UK Delivery as standard</span>
                  </span>
                            </a>
                        </div>
                        <div class="ypaAdSmartAnnotationsDiv"></div>
                        <div class="ypaAdCalloutExtensionDiv"></div>
                        <div class="ypaAdReviewExtensionDiv">
                            <div class="ypaAdReviewExtensionDiv"></div>
                        </div>
                        <div class="ypaAdFourthLineAnnotationDiv">
                            <div class="ypaAdFourthLineAnnotationDiv">
                            </div>
                        </div>
                        <div class="ypaAdLine2">
                            <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                  <span class="ypaAdURL">
                    <span class="ypaAdURLInner">www.cheaplaptopcompany.co.uk/laptops</span>
                  </span>
                            </a>
                        </div>
                        <div class="ypaAdCallExtensionDiv"></div>
                        <div class="ypaAdSiteLinks">
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Special Offers </span></a>
                            </div>
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Dell Laptops</span></a>
                            </div>
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Lenovo Laptops</span></a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="ypaAdElement ypaAdSpacing">
                    <div class="ypaAdImageDiv">
                        <a class="ypaAdAnchor" href="link" target="_blank"><span class="ypaAdImageInAd">
                  <img class="ypaAdImageInAdImage" src="<?= $assetFolderUrl ?>/images/prod-sm-01.jpg" width="50" height="50"></span>
                        </a>
                    </div>
                    <div class="ypaAdTextDiv">
                        <div class="AdLine1">
                            <div class="ypaAdTitleAndOfficialSiteBadge">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                    <span class="ypaAdTitle">
                      <span class="ypaAdTitleInner"> Macbook at Target™ - Save 5% w/ RedCard at Target™</span>
                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="ypaAdMerchantRatings">
                            <span class="ypaAdMRImage"> <img class="ypaAdMRImageImg" src="https://s.yimg.com/gi/mr/20131215/4_stars.png" width="76" height="14">&nbsp;&nbsp;</span>
                            <a class="ypaAdAnchor" href="https://search.yahoo.com/reviews?p=Laptop&amp;mrdomain=bestbuy.com&amp;ei=UTF-8" target="_blank">
                                <span="" onclick=""><span class="ypaAdMRCount">129&nbsp;</span><span class="ypaAdMRLabel">review(s)</span><span class="ypaAdMRDomain"> for deals.bestbuy.com</span>
                            </a>
                        </div>
                        <div class="ypaAdLine2">
                            <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                  <span class="ypaAdDesc">
                    <span class="ypaAdDescInner">Up to 80% Off - In Stock Now. Prices start at £120, with 1 Year Warranty and Free UK Delivery as standard</span>
                  </span>
                            </a>
                        </div>
                        <div class="ypaAdSmartAnnotationsDiv"></div>
                        <div class="ypaAdCalloutExtensionDiv"></div>
                        <div class="ypaAdReviewExtensionDiv">
                            <div class="ypaAdReviewExtensionDiv"></div>
                        </div>
                        <div class="ypaAdFourthLineAnnotationDiv">
                            <div class="ypaAdFourthLineAnnotationDiv">
                            </div>
                        </div>
                        <div class="ypaAdLine2">
                            <a class="ypaAdAnchor" href="link" target="_blank" onclick="">
                  <span class="ypaAdURL">
                    <span class="ypaAdURLInner">www.cheaplaptopcompany.co.uk/laptops</span>
                  </span>
                            </a>
                        </div>
                        <div class="ypaAdCallExtensionDiv"></div>
                        <div class="ypaAdSiteLinks">
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Special Offers </span></a>
                            </div>
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Dell Laptops</span></a>
                            </div>
                            <div class="ypaAdSiteLinkLine">
                                <a class="ypaAdAnchor" href="link" target="_blank" onclick=""><span class="ypaAdKeyword">Special Offers Lenovo laptops</span></a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>