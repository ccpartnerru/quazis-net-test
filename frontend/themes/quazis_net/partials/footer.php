<?php

/** @var \common\components\User $user */
$user = Yii::$app->getUser();

$categories = [
    [
        'name' => 'Technology',
        'items' => [
            ['name' => 'Laptops'],
            ['name' => 'Desktops'],
            ['name' => 'Software'],
            ['name' => 'Smartphones'],
            ['name' => 'GPS Devices'],
        ]
    ],
    [
        'name' => 'Appliances',
        'items' => [
            ['name' => 'Refrigerators'],
            ['name' => 'Microwave Ovens'],
            ['name' => 'Vacuum Cleaners'],
            ['name' => 'Coffee Makers'],
            ['name' => 'Washing Machines'],
        ]
    ],
    [
        'name' => 'Electronics',
        'items' => [
            ['name' => 'Digital Cameras'],
            ['name' => 'Weather Stations'],
            ['name' => 'Televisions'],
            ['name' => 'Home Audio'],
            ['name' => 'Digital Picture Frames'],
        ]
    ],
    [
        'name' => 'Fashion',
        'items' => [
            ['name' => 'Jackets'],
            ['name' => 'Wedding Dresses'],
            ['name' => 'Handbags'],
            ['name' => 'Jeans'],
            ['name' => 'Halloween Costumes'],
        ]
    ],
    [
        'name' => 'Auto',
        'items' => [
            [
                'name' => 'Repair Tools',
                'keyword' => 'auto repair tools'
            ],
            [
                'name' => 'Floor Mats',
                'keyword' => 'auto floor mats'
            ],
            [
                'name' => 'Tires'
            ],
            [
                'name' => 'Security Systems',
                'keyword' => 'car security systems'
            ],
            ['name' => 'Vehicle Lighting']
        ]
    ],
];

?>

<footer class="footer">

    <div class="footer__search">
        <div class="wrapper">
            <form class="footer__search-bar search-bar" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
                <input class="search-bar__input" name="<?= \common\helpers\Url::getKeywordParam() ?>" placeholder="<?= Yii::t('app', 'Looking for something else?')?>" type="search" required value="<?= \common\helpers\Url::getKeyword(false) ?>">
                <button class="search-bar__btn" value="" type="submit"></button>
            </form>
        </div>
    </div>


    <div class="footer__row-top">
        <div class="wrapper">
            <div class="footer__categories">
                <?php foreach ($categories as $category) { ?>
                <div class="footer__category">
                    <h3 class="footer__heading"> <?= Yii::t('app', $category['name']) ?> </h3>
                    <?php foreach ($category['items'] as $categoryItem) { ?>
                        <?php $categoryItemName =  $categoryItem['name'] ?>
                        <?php $categoryItemKeyword = isset($categoryItem['keyword']) ? $categoryItem['keyword'] : $categoryItemName ?>
                        <?php $categoryItemUrl = \common\helpers\Url::generateShoppingSearchUrl($categoryItemKeyword); ?>
                        <a href="<?= $categoryItemUrl ?>"><?= Yii::t('app',$categoryItemName) ?></a>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="footer__bottom">
        <div class="wrapper">
            <div class="footer__copyright"> © <?= date("Y") ?> Quazis.net </div>
            <div class="footer__nav">
                <a href="<?= \yii\helpers\Url::toRoute(['site/about'], true)?>"><?= Yii::t('app', 'About Us') ?></a>
                <a href="<?= \yii\helpers\Url::toRoute(['site/contact'], true)?>"><?= Yii::t('app', 'Contacts') ?></a>
                <a href="<?= \yii\helpers\Url::toRoute(['site/terms'], true)?>"><?= Yii::t('app', 'Terms &amp; Conditions') ?></a>
                <a href="<?= \yii\helpers\Url::toRoute(['site/privacy'], true)?>"><?= Yii::t('app', 'Privacy Policy') ?></a>
                <a href="<?= \yii\helpers\Url::toRoute(['site/cookie'], true)?>"><?= Yii::t('app', 'Cookie Policy') ?></a>
            </div>
        </div>
    </div>

</footer>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        var getCookie = function(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");

            if (parts.length == 2) {
                return parts.pop().split(";").shift();
            } else {
                return false;
            }
        };

        if (getCookie('user_visit') === false) {
            document.cookie = "user_visit=<?= $user->getSessionId() ?>; expires=Tue Jan 19 2038 03:14:07 GMT";
        }
    });
</script>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        var trkElements = [].slice.call(document.querySelectorAll(".trk"));

        if (trkElements.length) {
            trkElements.forEach(function(node){
                node.addEventListener('click', function(e) {
                    var requestReferParam = '<?= \common\helpers\StatClickUrlGenerator::getRequestReferParam() ?>';
                    var requestRefer = btoa(node.getAttribute('href'));

                    var feedPartnerIdParam = '<?= \common\helpers\StatClickUrlGenerator::getRequestFeedPartnerIdParam() ?>';
                    var feedPartnerId = getElementFeedPartnerId(node);

                    var url = '<?= \common\helpers\StatClickUrlGenerator::generateOutboundClickUrl() ?>';
                    url += '&' + requestReferParam + '=' + requestRefer;
                    url += '&' + feedPartnerIdParam + '=' + feedPartnerId;

                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.open("GET", url, true);
                    xmlhttp.send();
                });
            });
        }

        function getElementFeedPartnerId(e, iteration) {
            if (!iteration) {
                iteration = 0;
            }

            if (iteration > 20) {
                return null;
            }

            var feedPartnerId = e.getAttribute('data-item-feed-partner-id');

            if (!feedPartnerId && e.parentElement) {
                return getElementFeedPartnerId(e.parentElement, iteration + 1);
            }

            return feedPartnerId;
        }
    });
</script>

<script>
    (function(w, d){
        var b = d.getElementsByTagName('body')[0];
        var s = d.createElement("script");
        var v = !("IntersectionObserver" in w) ? "8.17.0" : "10.19.0";
        s.async = true; // This includes the script as async. See the "recipes" section for more information about async loading of LazyLoad.
        s.src = "https://cdn.jsdelivr.net/npm/vanilla-lazyload@" + v + "/dist/lazyload.min.js";
        w.lazyLoadOptions = {
            elements_selector: ".lazy"
        };
        b.appendChild(s);
    }(window, document));
</script>