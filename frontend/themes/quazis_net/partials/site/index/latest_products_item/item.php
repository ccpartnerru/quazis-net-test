<?php

/** @var \frontend\components\View $this */
/** @var \common\libraries\feedContentManager\entities\Item $item */

echo $this->render('../discount_item/item', ['item' => $item]);