<?php

/* @var $this \frontend\components\View */
/** @var string $title */
/** @var string $description */
/** @var string $url */
/** @var string $target */
/** @var string $imageUrl */
/** @var float $price */
/** @var float $percentageDiscount */
/** @var string $currencySymbol */

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$lazyLoadImage = $assetFolderUrl . '/images/no-image.svg'

?>

<article class="prod-card carousel__item">
    <?php if ($percentageDiscount) { ?>
    <div class="prod-card__label">-<?= $percentageDiscount ?>%</div>
    <?php } ?>
    <a class="prod-card__img" href="<?= $url ?>" target="<?= $target ?>"> <img class="lazy" src="<?= $lazyLoadImage ?>" data-src="<?= $imageUrl ?>" alt=""> </a>
    <div class="prod-card__body">
        <h3 class="prod-card__name"><a href="<?= $url ?>" target="<?= $target ?>"><?= $title ?></a></h3>
        <div class="prod-card__price"><?= $currencySymbol ?><?= number_format($price, 2) ?></div>
    </div>
</article>