<?php

/* @var $this \frontend\components\View */

$siteName = Yii::$app->params['siteName'];
$pageName = Yii::t('app', 'Contact Us');

$this->title = $pageName . ' - ' . $siteName;
$this->params['breadcrumbs'][] = $pageName;

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Find the lowest prices and the best deals online with {siteName} - more choice, more savings.', [
        'siteName' => $siteName
    ])
]);
$this->addBodyClasses(['page-contact']);

?>
<div class="wrapper">
    <main class="info-page text-content">
        <h1>Contact Us</h1>
        <h2>Get In Touch</h2>
        <p>To help us on our way, you can be more helpful than you think! We would love to hear from you and any and all feedback is much-appreciated. If you have any questions or difficulties, please don’t hesitate to get in touch through our contact forms. After you submit your form, our team will be in touch as quickly as possible.</p>
        <h2>Our Email</h2>
        <p>contact@quazis.net</p>
        <h2>Complaints</h2>
        <p>The same goes if you have any negative feedback. Although, not as easy to hear, it is just as important if we are to reach our goal. If you are having any issues, fill in the contact form and our team will get back to you with an answer as soon as we can. We can’t learn and improve if we don’t know what we are doing wrong.</p>
    </main>
</div>
