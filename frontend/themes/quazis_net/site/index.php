<?php

use common\helpers\Url;

/* @var $this \frontend\components\View */
/** @var \common\libraries\feedContentManager\entities\Item[] $discountsItems */
/** @var \common\libraries\feedContentManager\entities\Item[] $latestProductsItems */
/** @var \common\libraries\feedContentManager\entities\Item[] $featuredProductsItems */
/** @var \common\libraries\feedContentManager\entities\Item[] $bestSellingProductsItems */

$siteName = Yii::$app->params['siteName'];
$pageName = Yii::t('app', 'Homepage');

$this->title = Yii::t('app', 'Compare Best Deals & Lowest Prices from Trusted US Stores');
$this->addBodyClasses(['page-home']);

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Find the lowest prices and the best deals online with {siteName} - more choice, more savings.', [
        'siteName' => $siteName
    ])
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Yii::t('app', 'compare prices, lowest prices, price comparison, best deals, huge savings')
]);

// set homepage layout
$this->context->layout = 'homepage';

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$lazyLoadImage = $assetFolderUrl . '/images/no-image.svg';

$popularSearches = [
    'Gaming Laptops',
    'iPhone XS',
    'Samsung TVs',
    'Handbags',
    'Canon Cameras'
];

?>

<header class="home-hero home-section">
    <div class="home-hero__wrap wrapper">
        <a class="home-hero__logo" href="/"><img src="<?= $assetFolderUrl ?>/images/logo.svg" alt=""></a>
        <h1 class="home-hero__heading"> <?= Yii::t('app', 'Discover the Best Deals Online')?> </h1>
        <form class="home-hero__search-bar search-bar" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
            <input class="search-bar__input" name="<?= \common\helpers\Url::getKeywordParam() ?>" placeholder="<?= Yii::t('app', 'Search')?>..." type="search" required>
            <button class="search-bar__btn" value="" type="submit"></button>
        </form>
        <?php if ($popularSearches && count($popularSearches)) { ?>
        <div class="home-hero__rel-searches">
            <span> <?= Yii::t('app', 'Popular searches') ?>: </span>
            <?php foreach ($popularSearches as $popularSearch) {?>
                <a class="home-hero__rel-btn" href="<?= \common\helpers\Url::generateShoppingSearchUrl($popularSearch) ?>"><?= $popularSearch ?></a>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
</header>

<section class="home-categories home-section">

    <div class="wrapper">
        <h2 class="home-categories__heading section-heading--lg txt-center"><?= Yii::t('app', 'Shop by Category')?></h2>

        <div class="home-categories__grid">

            <article class="home-category home-category--purple">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Laptop') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-computer.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Laptop') ?>>"><?= Yii::t('app', 'Laptops')?></a></h3>
                </div>
            </article>

            <article class="home-category--lg home-category--grad-1">
                <a class="home-category__img--btm" href="<?= Url::generateShoppingSearchUrl('Smartphone') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-smartphones.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Smartphone') ?>"><?= Yii::t('app', 'Smartphones')?></a></h3>
                    <div class="home-category__descr"> Get a new smartphone at the best price </div>
                    <a class="home-category__btn" href="<?= Url::generateShoppingSearchUrl('Smartphone') ?>"> <?= Yii::t('app', 'Buy now')?> </a>
                </div>
            </article>

            <article class="home-category home-category--blue">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Wereable') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-wereable.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Wereable') ?>"><?= Yii::t('app', 'Wereables')?></a></h3>
                </div>
            </article>

            <article class="home-category home-category--salad-green">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Camera') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-camera.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Camera') ?>"><?= Yii::t('app', 'Cameras')?></a></h3>
                </div>
            </article>


            <article class="home-category home-category--green">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Home & Garden') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-home.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Home & Garden') ?>"><?= Yii::t('app', 'Home &amp; Garden')?></a></h3>
                </div>
            </article>

            <article class="home-category home-category--red">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Health & Beauty') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-beauty.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Health & Beauty') ?>"><?= Yii::t('app', 'Health &amp; Beauty')?></a></h3>
                </div>
            </article>

            <article class="home-category--lg home-category--grad-2">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Audio & Music') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-headphones.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Audio & Music') ?>"><?= Yii::t('app', 'Audio &amp; Music')?></a></h3>
                    <div class="home-category__descr">Looking for the best heaphones?</div>
                    <a class="home-category__btn" href="<?= Url::generateShoppingSearchUrl('Audio & Music') ?>"> <?= Yii::t('app', 'See It')?> </a>
                </div>
            </article>


            <article class="home-category  home-category--orange">
                <a class="home-category__img" href="<?= Url::generateShoppingSearchUrl('Sports &amp; Outdoors') ?>"><img src="<?= $assetFolderUrl ?>/images/home-category-sports.png" alt=""></a>
                <div class="home-category__body">
                    <h3 class="home-category__name" ><a href="<?= Url::generateShoppingSearchUrl('Sports &amp; Outdoors') ?>"><?= Yii::t('app', 'Sports &amp; Outdoors')?></a></h3>
                </div>
            </article>

        </div><!-- end home-categories__grid -->
    </div><!-- end wrapper -->

</section>

<?php if ($discountsItems && count($discountsItems)) { ?>
<section class="home-discounts home-section">
    <div class="wrapper">
        <h2 class="home-discounts__heading section-heading--lg txt-center" ><?= Yii::t('app', 'Discounts')?></h2>

        <div class="carousel products-carousel">
            <div class="carousel__wrap">
                <?php
                foreach ($discountsItems as $discountsItem) {
                    echo $this->render('/partials/site/index/discount_item/item', ['item' => $discountsItem]);
                }
                ?>
            </div><!-- end carousel__wrap -->
        </div><!-- end products-carousel -->
    </div><!-- end wrapper -->
</section><!-- end home-discounts section -->
<?php } ?>




<div class="wrapper">

    <section class="phones-promo home-section">
        <div class="phones-promo__wrap">
            <a class="phones-promo__img" href="<?= \common\helpers\Url::generateShoppingSearchUrl('iPhone') ?>">
                <img src="<?= $assetFolderUrl ?>/images/iphones-promo.png" alt="">
            </a>
            <div class="phones-promo__body">
                <h2 class="phones-promo__title"><a href="<?= \common\helpers\Url::generateShoppingSearchUrl('iPhone') ?>"><?= Yii::t('app', 'iPhones')?></a></h2>
                <p class="phones-promo__descr"><?= Yii::t('app', 'Find our best selling iPhone deals here')?></p>
                <a class="phones-promo__btn" href="<?= \common\helpers\Url::generateShoppingSearchUrl('iPhone') ?>"> <?= Yii::t('app', 'See It')?> </a>
            </div>
        </div>
    </section>

    <?php if (count($latestProductsItems) || count($featuredProductsItems) || count($bestSellingProductsItems)) { ?>
    <section class="home-products home-section">
        <h2 class="home-products__heading section-heading--lg txt-center"> <?= Yii::t('app', 'Best selling products')?></h2>

        <div class="tabs">

            <nav class="home-products__nav priority-nav tabs__nav ">
                <ul class="priority-nav__wrap">
                    <?php if ($latestProductsItems && count($latestProductsItems)) { ?>
                    <li><a class="is-active" href="#"><?= Yii::t('app', 'Latest Products')?></a></li>
                    <?php } ?>
                    <?php if ($featuredProductsItems && count($featuredProductsItems)) { ?>
                    <li><a href="#"><?= Yii::t('app', 'Featured Products')?></a></li>
                    <?php } ?>
                    <?php if ($bestSellingProductsItems && count($bestSellingProductsItems)) { ?>
                    <li><a href="#"><?= Yii::t('app', 'Best Selling Products')?></a></li>
                    <?php } ?>
                </ul>
            </nav>

            <?php if (count($latestProductsItems)) { ?>
            <div class="tabs__item">
                <div class="carousel products-carousel">
                    <div class="carousel__wrap">
                        <?php
                        foreach ($latestProductsItems as $latestProductsItem) {
                            echo $this->render('/partials/site/index/latest_products_item/item', ['item' => $latestProductsItem]);
                        }
                        ?>
                    </div><!-- end carousel__wrap -->
                </div><!-- end products-carousel -->
            </div><!-- tabs__item -->
            <?php } ?>

            <?php if (count($featuredProductsItems)) { ?>
                <div class="tabs__item">
                    <div class="carousel products-carousel">
                        <div class="carousel__wrap">
                            <?php
                            foreach ($featuredProductsItems as $featuredProductsItem) {
                                echo $this->render('/partials/site/index/featured_products_item/item', ['item' => $featuredProductsItem]);
                            }
                            ?>
                        </div><!-- end carousel__wrap -->
                    </div><!-- end products-carousel -->
                </div><!-- tabs__item -->
            <?php } ?>

            <?php if (count($bestSellingProductsItems)) { ?>
                <div class="tabs__item">
                    <div class="carousel products-carousel">
                        <div class="carousel__wrap">
                            <?php
                            foreach ($bestSellingProductsItems as $bestSellingProductsItem) {
                                echo $this->render('/partials/site/index/best_selling_products_item/item', ['item' => $bestSellingProductsItem]);
                            }
                            ?>
                        </div><!-- end carousel__wrap -->
                    </div><!-- end products-carousel -->
                </div><!-- tabs__item -->
            <?php } ?>

        </div><!-- end tabs -->
    </section>
    <?php } ?>
</div><!-- end wrapper -->
