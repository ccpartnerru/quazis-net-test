<?php

/* @var $this \frontend\components\View */

$siteName = Yii::$app->params['siteName'];
$pageName = Yii::t('app', 'Terms and Conditions');

$this->title = $pageName . ' - ' . $siteName;
$this->params['breadcrumbs'][] = $pageName;

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Find the lowest prices and the best deals online with {siteName} - more choice, more savings.', [
        'siteName' => $siteName
    ])
]);
$this->addBodyClasses(['page-terms']);

?>
<div class="wrapper">
    <main class="info-page text-content">
        <h1>Terms and Conditions</h1>
        <p>These <strong>terms (‘Terms’)</strong> tell you the rules for using our <strong>website</strong> <?= \yii\helpers\Html::a($siteName, ['site/index']) ?> (our <strong>‘Site’</strong>).</p>
        <h2>BY USING OUR SITE YOU ACCEPT THESE TERMS</h2>
        <p>By using our Site, you confirm that you accept these <strong>terms</strong> of use and that you agree to comply with them.</p>
        <p>If you do not agree to these <strong>terms</strong>, you must not use our Site.</p>
        <p>We recommend that you print a copy of these <strong>terms</strong> for future reference.</p>
        <h2>THERE ARE OTHER TERMS THAT MAY APPLY TO YOU</h2>
        <p>These <strong>terms</strong> of use refer to the following additional <strong>terms</strong>, which also apply to your use of our Site:</p>
        <ul>
            <li>Our <?= \yii\helpers\Html::a('Privacy Policy', ['site/privacy'])?> which sets out the <strong>terms</strong> on which we process any personal data we collect from you, or that you provide to us.</li>
            <li>Our <?= \yii\helpers\Html::a('Cookie Policy', ['site/cookie'])?> which sets out information about the cookies on our Site.</li>
        </ul>
        <h2>WE MAY MAKE CHANGES TO THESE TERMS</h2>
        <p>We amend these <strong>terms</strong> from time to time. Every time you wish to use our Site, please check these <strong>terms</strong> to ensure you understand the <strong>terms</strong> that apply at that time.</p>
        <h2>WE MAY SUSPEND OR WITHDRAW OUR SITE OR TERMINATE ACCESS</h2>
        <p>Our Site is made available free of charge.</p>
        <p>We do not guarantee that our Site, or any content on it, will always be available or be uninterrupted. We may suspend or withdraw or restrict the availability of all or any part of our Site for business and operational reasons. We will try to give you reasonable notice of any suspension or withdrawal.</p>
        <p>You are also responsible for ensuring that all persons who access our Site through your internet connection are aware of these <strong>terms</strong> of use and other applicable <strong>terms</strong> and <strong>conditions</strong>, and that they comply with them.</p>
        <p>We are entitled to terminate your access to our Site any time without giving reasons.</p>
        <h2>HOW YOU MAY USE MATERIAL ON OUR SITE</h2>
        <p>We are the owner or the licensee of all intellectual property rights in our Site, and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</p>
        <p>You may print off one copy, and may download extracts, of any page(s) from our Site for your personal, not commercial use.</p>
        <p>You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.</p>
        <p>Our status (and that of any identified contributors) as the authors of content on our Site must always be acknowledged.</p>
        <p>You must not use any part of the content on our Site for commercial purposes without obtaining a licence to do so from us or our licensors.</p>
        <p>If you print off, copy or download any part of our Site in breach of these <strong>terms</strong> of use, your right to use our Site will cease immediately and you must, at our option, return or destroy any copies of the materials you have made.</p>
        <p>By using our Site you warrant that you are 18 years old and have the right, authority and capacity to enter into and be bound by these Terms.</p>
        <h2>DO NOT RELY ON INFORMATION ON THIS SITE</h2>
        <p>The content on our Site, or any information that we provide you with, is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on our Site.</p>
        <p>Although we make reasonable efforts to update the information on our Site, we make no representations, warranties or guarantees, whether express or implied, that the content on our Site is accurate, complete or up to date.</p>
        <h2>WE ARE NOT RESPONSIBLE FOR WEBSITES WE LINK TO</h2>
        <p>Where our Site contains links to other sites and resources provided by third parties, these links are provided for your information only and we are not responsible for the content on any third party websites which are accessed at your own risk. &nbsp;Such links should not be interpreted as approval by us of those linked <strong>websites</strong> or information or services you may obtain from them.</p>
        <p>We have no control over the contents of those sites or resources.</p>
        <h2>OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY YOU</h2>
        <p>The material contained on the Site, or any information that we provide you with, is for information purposes only and does not constitute advice and you should use your own judgment before doing or not doing anything on the basis of what you see. We give no warranties of any kind in relation to the materials on the Site.</p>
        <p>We exclude our liability to you to the fullest extent permitted by law and are not responsible for any dealings with third parties that link to this Site. We do not exclude or limit in any way our liability to you where it would be unlawful to do so. &nbsp;This includes liability for death or personal injury caused by our negligence or the negligence of our employees, agents or subcontractors and for fraud or fraudulent misrepresentation.</p>
        <p>Please note that we only provide our Site for domestic and private use. You agree not to use our Site for any commercial or business purposes, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.</p>
        <h2>WE ARE NOT RESPONSIBLE FOR VIRUSES AND YOU MUST NOT INTRODUCE THEM</h2>
        <p>We do not guarantee that our Site will be secure or free from bugs or viruses.</p>
        <p>You are responsible for configuring your information technology, computer programmes and platform to access our Site. You should use your own virus protection software.</p>
        <p>You must not misuse our Site by knowingly introducing viruses, trojans, worms, or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to our Site, the server on which our Site is stored or any server, computer or database connected to our Site. You must not attack our Site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use our Site will cease immediately.</p>
        <h2>RULES ABOUT LINKING TO OUR SITE</h2>
        <p>You may link to our home page, provided you do so in a way that is fair and legal and does not damage our reputation or take advantage of it.</p>
        <p>You must not establish a link in such a way as to suggest any form of association, approval or endorsement on our part where none exists.</p>
        <p>You must not establish a link to our Site in any <strong>website</strong> that is not owned by you.</p>
        <p>Our Site must not be framed on any other Site, nor may you create a link to any part of our Site other than the home page.</p>
        <p>We reserve the right to withdraw linking permission without notice.</p>
        <h2>WHO WE ARE AND HOW TO CONTACT US</h2>
        <p>Clicksco Digital Limited (”We”,“Us”) operates this Site and we are registered in Jersey under company number 114200 and our registered office at 15 Esplanade, St. Helier, Jersey JE1 1RB.</p>
        <h2>WHICH COUNTRY'S LAWS APPLY TO ANY DISPUTES?</h2>
        <p>If you are a consumer, please note that these <strong>terms</strong> of use, their subject matter and their formation, are governed by English law. You and we both agree that the courts of England and Wales will have exclusive jurisdiction in relation to any disputes.</p>
    </main>
</div>
