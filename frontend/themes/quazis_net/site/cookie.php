<?php

/* @var $this \frontend\components\View */

$siteName = Yii::$app->params['siteName'];
$pageName = Yii::t('app', 'Cookie Policy');

$this->title = $pageName . ' - ' . $siteName;
$this->params['breadcrumbs'][] = $pageName;

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Find the lowest prices and the best deals online with {siteName} - more choice, more savings.', [
        'siteName' => $siteName
    ])
]);
$this->addBodyClasses(['page-cookie']);

?>
<div class="wrapper">
    <main class="info-page text-content">
        <h1>Information About Our Use of Cookies</h1>
        <p>Our website uses cookies to distinguish you from other users of our website. This helps us to provide you
            with a good experience when you browse our website and also allows us to improve our site. By continuing
            to browse the site, you are agreeing to our use of cookies.</p>
        <p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your
            computer if you agree. Cookies contain information that is transferred to your computer's hard
            drive.</p>
        <p>We use the following cookies:</p>
        <ul>
            <li>Strictly necessary cookies. These are cookies that are required for the operation of our website.
                They include, for example, cookies that enable you to log into secure areas of our website, use a
                shopping cart or make use of e-billing services.
            </li>
            <li>Analytical/performance cookies. They allow us to recognise and count the number of visitors and to
                see how visitors move around our website when they are using it. This helps us to improve the way
                our website works, for example, by ensuring that users are finding what they are looking for easily.
            </li>
            <li>Functionality cookies. These are used to recognise you when you return to our website. This enables
                us to personalise our content for you, greet you by name and remember your preferences (for example,
                your choice of language or region).
            </li>
            <li>Targeting cookies. These cookies record your visit to our website, the pages you have visited and
                the links you have followed. We will use this information to make our website and the advertising
                displayed on it more relevant to your interests. We may also share this information with third
                parties for this purpose.
            </li>
        </ul>
        <p>You can find more information about the individual cookies we use and the purposes for which we use them
            in the table below:</p>

        <h2>STRICTLY NECESSARY COOKIES</h2>
        <table class="table-bordered">
            <tbody><tr>
                <th>Cookie Provider</th>
                <th>Cookie Name</th>
                <th>First or Third Party Cookie</th>
                <th>Persistent or Session Cookie</th>
                <th>Purpose of Cookie</th>
            </tr>
            <tr>
                <td></td>
                <td>_csrf-backend</td>
                <td>First Party</td>
                <td>Persistent</td>
                <td>CSRF secret key to protect the backend forms</td>
            </tr>
            <tr>
                <td></td>
                <td>_identity-backend</td>
                <td>First Party</td>
                <td>Persistent</td>
                <td>User identity cookie</td>
            </tr>
            <tr>
                <td></td>
                <td>advanced-backend</td>
                <td>First Party</td>
                <td>Persistent</td>
                <td>This is the name of the session cookie used for login on the backend</td>
            </tr>
            <tr>
                <td></td>
                <td>advanced-frontend</td>
                <td>First Party</td>
                <td>Persistent</td>
                <td>This is the name of the session cookie used for login on the frontend</td>
            </tr>
            <tr>
                <td></td>
                <td>user_visit</td>
                <td>First Party</td>
                <td>Persistent</td>
                <td>Current user ID hash</td>
            </tr>
            </tbody>
        </table>

        <h2>PERFORMANCE COOKIES</h2>
        <table class="table-bordered">
            <tbody><tr>
                <th>Cookie Provider</th>
                <th>Cookie Name</th>
                <th>First or Third Party Cookie</th>
                <th>Persistent or Session Cookie</th>
                <th>Purpose of Cookie</th>
            </tr>
            <tr>
                <td>Google</td>
                <td>1P_JAR<br>HSID<br>SAPISID<br>SID<br>SAPISID<br>APISID<br>SSID<br>HSID<br>NID<br>PREF<br>Further
                    details can be found here: <a href="https://www.google.com/intl/en/policies/technologies/types/">https://www.google.com/intl/en/policies/technologies/types/</a>
                </td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Google set a number of cookies on any page that includes a Google Map. While we have no control
                    over the cookies set by Google, they appear to include a mixture of pieces of information to
                    measure the number and behaviour of Google Maps users. These cookies are used to gather website
                    statistics, and track conversion rates.
                </td>
            </tr>
            </tbody>
        </table>

        <h2>FUNCTIONALITY COOKIES</h2>
        <table class="table-bordered">
            <tbody><tr>
                <th>Cookie Provider</th>
                <th>Cookie Name</th>
                <th>First or third party cookie</th>
                <th>Persistent or session cookie</th>
                <th>Purpose cookie used for</th>
            </tr>
            <tr>
                <td rowspan="4">Google Tag Manager</td>
                <td>_gali</td>
                <td>Third Party</td>
                <td>Session</td>
                <td>Google Analyitics – Cookies of third parties used to collect information on how users use the
                    website to guarantee the best accuracy in the reporting activity. Cookies collect data in
                    anonymous form, including the number of website visitors, how they have reached the website and
                    which pages they visited
                </td>
            </tr>
            <tr>
                <td>gtm_user_created_at</td>
                <td>Third Party</td>
                <td>Persistent, 1 week</td>
                <td></td>
            </tr>
            <tr>
                <td>gtm_user_email</td>
                <td>Third Party</td>
                <td>Persistent, 1 week</td>
                <td></td>
            </tr>
            <tr>
                <td>gtm_user_id</td>
                <td>Third Party</td>
                <td>Persistent, 1 week</td>
                <td>Used to Join sessions crossed devices</td>
            </tr>
            <tr>
                <td rowspan="4">Google Analytics</td>
                <td>__zlcmid</td>
                <td>Third Party</td>
                <td>Persistent, 1 year</td>
                <td>These Cookies are used by Google Analytics to obtain information of how users use the Site. We
                    use this information to generate reports and to improve user experience on the Site - Analytical
                    Cookies
                </td>
            </tr>
            <tr>
                <td>NID</td>
                <td>Third Party</td>
                <td></td>
                <td>Could cover many things but they definitely place cookies if you have embedded Google Maps into
                    your pages
                </td>
            </tr>
            <tr>
                <td>_gali</td>
                <td>Third Party</td>
                <td></td>
                <td>This Cookie is typically written to the browser upon the first visit. If the Cookie has been
                    deleted by the browser operator, and the browser subsequently visits the Site, a new _gali
                    cookie is written with a different unique ID. In most cases, this cookie is used to determine
                    unique visitors to siniat.co.uk and it is updated with each page view. Additionally, this cookie
                    is provided with a unique ID that Google Analytics uses to ensure both the validity and
                    accessibility of the cookie as an extra security measure.
                </td>
            </tr>
            <tr>
                <td>idb</td>
                <td>Third Party</td>
                <td>Persistent, 1 year</td>
                <td>These cookies are set by google analytics<br>More about google analytics is available here.</td>
            </tr>
            <tr>
                <td rowspan="2">Google Website Optimiser</td>
                <td>__utmx<br>__utmxx</td>
                <td>Third Party</td>
                <td>Persistent, 1 year</td>
                <td>These cookies are used by Google Analytics to obtain information of how users use the Site. We
                    use this information to generate reports and to improve user experience on the Site - Analytical
                    Cookies
                </td>
            </tr>
            <tr>
                <td>idb</td>
                <td>Third Party</td>
                <td>Persistent, 1 year</td>
                <td>These cookies are set by google analytics</td>
            </tr>
            </tbody></table>


        <h2>ADVERTISING/TARGETING COOKIES</h2>
        <table class="table-bordered">
            <tbody><tr>
                <th>Cookie Provider</th>
                <th>Cookie Name</th>
                <th>First or Third Party Cookie</th>
                <th>Persistent or Session Cookie</th>
                <th>Purpose of Cookie</th>
            </tr>
            <tr>
                <td>.adaptv.advertising.com</td>
                <td>adaptv_unique_user_cookie<br>ctsSegments<br>uuid</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.adform.net</td>
                <td>uid</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>These cookies are used to collect data about the websites and individual pages that users visit
                    both on and off our websites (which might indicate, for example, your interests and other
                    attributes)
                </td>
            </tr>
            <tr>
                <td>.1rx.io</td>
                <td>_rxuuid<!--
                <td-->
                </td><td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.adnxs.com</td>
                <td>anj<br>sess<br>uuid2</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Adnxs.com is run by AppNexus, a company that provides technology, data and analytics to help
                    companies buy and sell online display advertising
                </td>
            </tr>
            <tr>
                <td>.advertising.com</td>
                <td>IDSYNC<br>USID<br>APID</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.bidr.io</td>
                <td>bito</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.bidswitch.net</td>
                <td>c<br>tuuid<br>tuuid_lu</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>This cookie regulates synchronisation of user identification and exchange of user data between
                    various ad services.
                </td>
            </tr>
            <tr>
                <td>.casalemedia.com</td>
                <td>CMID<br>CMPS<br>CMPRO<br>CMST<br>CMDD<br>CMRUM3</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.casalemedia.com</td>
                <td>CMSC</td>
                <td>Third Party</td>
                <td>Session</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.ccgateway.net</td>
                <td>ccuid</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness<br><a href="https://carbondmp.com/ads/privacy/html">https://carbondmp.com/ads/privacy/html</a>.
                </td>
            </tr>
            <tr>
                <td>.ccgateway.net</td>
                <td>cccot<br>ccsid</td>
                <td>Third Party</td>
                <td>Session</td>
                <td>Advertising and marketing effectiveness<br><a href="https://carbondmp.com/ad/privacy/html">https://carbondmp.com/ad/privacy/html</a>
                </td>
            </tr>
            <tr>
                <td>.digitru.st</td>
                <td>DigiTrust.v1.Identity</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.doubleclick.net</td>
                <td>IDE<br>DSID</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>This cookie is used for re-targeting, optimisation, reporting and attribution of online
                    adverts.
                </td>
            </tr>
            <tr>
                <td>.everesttech.net</td>
                <td>everest_g_v2</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.gwallet.com</td>
                <td>ra1_pd_1362680559<br>ra1_uid<br>ra1_pd_1131161094</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.impdesk.com</td>
                <td>idb</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.mathtag.com</td>
                <td>uuid<br>mt_mop<br>uuidc</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.mxptint.net</td>
                <td>mxpim</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.netseer.com</td>
                <td>netseer_v3_vi</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>openx.net</td>
                <td>i</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>Google</td>
                <td></td>
                <td>Third Party</td>
                <td></td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.pixel.rubiconproject.com</td>
                <td>rpx</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.pubmatic.com</td>
                <td>KRTBCOOKIE_80<br>PugT<br>PUBMDCID</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.quantserve.com</td>
                <td>d<br>mc&lt;</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.rfihub.com</td>
                <td>rud<br>eud</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.rfihub.com</td>
                <td>ruds<br>euds</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.rubiconproject.com</td>
                <td>khaos<br>rpb<br>put_2249</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.simpli.fi</td>
                <td>uid</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.sitescout.com</td>
                <td>ssi<br>_ssum</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>taboola.com</td>
                <td>t_gid<br>taboola_mk</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>Yandex Metrica</td>
                <td></td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.turn.com</td>
                <td>uid</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.w55c.net</td>
                <td>wfivefivec<br>matchgoogle</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            <tr>
                <td>.yahoo.com</td>
                <td></td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Yahoo’s cookies can also track and remember searches that you have made. Yahoo has paid
                    advertisements included in their search results.
                </td>
            </tr>
            <tr>
                <td>pixel.rubiconproject.com</td>
                <td>c</td>
                <td>Third Party</td>
                <td>Persistent</td>
                <td>Advertising and marketing effectiveness</td>
            </tr>
            </tbody></table>

        <h2>CHANGES</h2>

        <p>Information about the cookies used by us may be updated from time to time, so please check back on a
            regular basis for any changes. The last modification date of this document is shown at the top of this
            page.</p>

        <p>Please note that third parties (including, for example, advertising networks and providers of external
            services like web traffic analysis services) may also use cookies, over which we have no control. These
            cookies are likely to be analytical/performance cookies or targeting cookies</p>
        <p>You block cookies by activating the setting on your browser that allows you to refuse the setting of all
            or some cookies. However, if you use your browser settings to block all cookies (including essential
            cookies) you may not be able to access all or parts of our site.</p>
        <p>How long a cookie remains on your browsing device depends on whether is it a “persistent” or “session”
            cookie. Session cookies will remain on your device while you are browsing. Persistent cookies stay on
            your device while you are browsing and until they expire or are deleted.</p>
    </main>
</div>
