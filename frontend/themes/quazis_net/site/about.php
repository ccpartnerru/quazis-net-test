<?php

/* @var $this \frontend\components\View */

$siteName = Yii::$app->params['siteName'];
$pageName = Yii::t('app', 'About Us');

$this->title = $pageName . ' - ' . $siteName;
$this->params['breadcrumbs'][] = $pageName;

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Find the lowest prices and the best deals online with {siteName} - more choice, more savings.', [
        'siteName' => $siteName
    ])
]);
$this->addBodyClasses(['page-about']);

?>
<div class="wrapper">
    <main class="info-page text-content">
        <h1>About Us</h1>
        <p>We, at <?= \yii\helpers\Html::a($siteName, ['site/index']) ?> look to provide a huge range of products and services through our simple and easy to use platform. Our website is designed to provide our valued users will all the information they need to find the best deals possible. Then, we provide a wide variety of offers to help you compare each one side-by-side and get more bang for your buck.</p>
        <h2>Our Mission</h2>
        <p>The big one is to blow everyone out the water with our customer service and extensive line of products and services. However, we know we are a modest community and it will take time to build our brand. The first step in our journey is to build a great customer base founded on a strong relationship so we can grow slowly and steadily into one of the big boys.</p>
        <h2>Team Values</h2>
        <p>We strongly believe a smart customer is a happy customer. If you have any the information you need in one place, you can make the best decision for yourself and save as much as you can. This, in turn, builds trust and, hopefully, you will use us again for future purchases. We are constantly striving for improvement and are always looking for new and innovative ways to bring a better service to your screens.</p>
    </main>
</div>
