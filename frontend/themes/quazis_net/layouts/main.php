<?php

/* @var $this \frontend\components\View */
/* @var $content string */

use frontend\assets\QuazisNetAsset;
use \yii\helpers\Html;

$assetBundle = QuazisNetAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="keywords" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#7445c0">
    <meta name="msapplication-TileColor" content="#7445c0">
    <meta name="theme-color" content="#7445c0">
    <?php $this->head() ?>
</head>
    <body class="<?= implode(' ', $this->bodyClasses) ?>">
    <?php $this->beginBody() ?>
    <?= $this->render('/partials/header') ?>
    <?= $content ?>
    <?= $this->render('/partials/footer') ?>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
