<?php

/* @var $this \frontend\components\View */
/** @var string $categoryName */

$siteName = Yii::$app->params['siteName'];
$this->title = $categoryName;

$this->registerMetaTag([
    'name' => 'description',
    'content' =>  'Compare ' . $categoryName . ' offers and get lowers prices on ' . $categoryName . ' on ' . $siteName . '.'
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Compare prices on ' . $categoryName.' products, latest ' . $categoryName . ' deals, discount ' . $categoryName . ' offers, cheap ' . $categoryName . ' deals, ' . $categoryName . ' prices'
]);

$this->addBodyClasses(['page-category']);

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist.css');

$defaultCategoryName = 'Electronics';

$categories = [
    [
        'name' => 'Motors',
        'items' => [
        ]
    ],
    [
        'name' => 'Fashion',
        'items' => [
        ]
    ],
    [
        'name' => 'Electronics',
        'items' => [
            [
                'name' => 'Camera & Photo',
                'imageUrl' => $assetFolderUrl . '/images/category-cameras.jpg',
                'items' => [
                    ['name' => 'Binoculars & Telescopes'],
                    ['name' => 'Camcorders'],
                    ['name' => 'Camera & Photo Accessories'],
                    ['name' => 'Camera Drones'],
                    ['name' => 'Camera Lenses Filters'],
                    ['name' => 'Digital Cameras'],
                    ['name' => 'Digital Photo Frames'],
                    ['name' => 'Film Photography'],
                    ['name' => 'Flashes & Flash Accessories'],
                    ['name' => 'Manuals & Guides'],
                    ['name' => 'Other Cameras & Photo'],
                    ['name' => 'Photo Lighting and Studio Equipments'],
                    ['name' => 'Replacement Parts & Tools'],
                    ['name' => 'Tripods & Supports'],
                    ['name' => 'Video Production & Editing'],
                ]
            ],
            [
                'name' => 'Car Electronics',
                'imageUrl' => $assetFolderUrl . '/images/category-car-electronics.jpg',
                'items' => [
                    ['name' => '12-Volt Portable Appliances'],
                    ['name' => 'Car Alarms & Security'],
                    ['name' => 'Car Audio'],
                    ['name' => 'Car Audio & Video Installation'],
                    ['name' => 'Car Electronics Accessories'],
                    ['name' => 'Car Video'],
                    ['name' => 'GPS Accessories & Tracking'],
                    ['name' => 'GPS Units'],
                    ['name' => 'Marine Audio'],
                    ['name' => 'Other Vehicle Electronics'],
                    ['name' => 'Radar & Laser Detectors'],
                ]
            ],
            [
                'name' => 'Cell Phones & Accessories',
                'imageUrl' => $assetFolderUrl . '/images/category-cellphones.jpg',
                'items' => [
                    ['name' => 'Batteries'],
                    ['name' => 'Cables & Adapters'],
                    ['name' => 'Cases, Covers, & Skins'],
                    ['name' => 'Cell Phone Accessories'],
                    ['name' => 'Cell Phone Parts'],
                    ['name' => 'Cell Phones & Smartphones'],
                    ['name' => 'Chargers & Cradles'],
                    ['name' => 'Headsets'],
                    ['name' => 'Screen protectors'],
                    ['name' => 'Smart Watches'],
                ]
            ],
            [
                'name' => 'Computer & Tablets',
                'imageUrl' => $assetFolderUrl . '/images/category-computers.jpg',
                'items' => [
                    ['name' => 'Cables & Connectors'],
                    ['name' => 'Computer Components & Parts'],
                    ['name' => 'Desktops & All-In-Ones'],
                    ['name' => 'Drives, Storage & Blank Media'],
                    ['name' => 'Enterprise Networking & Servers'],
                    ['name' => 'Home Networking & Connectivity'],
                    ['name' => 'Keyboards, Mice, & Pointing'],
                    ['name' => 'Laptop & Desktop Accessories'],
                    ['name' => 'Laptops & Netbooks'],
                    ['name' => 'Manuals & Resources'],
                    ['name' => 'Monitors, Projectors & Accessories'],
                    ['name' => 'Other Computing & Networking'],
                    ['name' => 'Power Protection, & Distribution'],
                    ['name' => 'Printers, Scanners, & Supplies'],
                    ['name' => 'Software'],
                    ['name' => 'Tablet & eBook Readers'],
                    ['name' => 'Tablet & eBook Readers Accessories'],
                    ['name' => 'Tablet & eBook Readers Parts'],
                    ['name' => 'Vintage Computing'],
                    ['name' => 'Wholesale Lots'],
                ]
            ],
            [
                'name' => 'TV, Video, & Audio',
                'imageUrl' => $assetFolderUrl . '/images/category-tv.jpg',
                'items' => [
                    ['name' => 'Calculators'],
                    ['name' => 'DVD & Blu-ray players'],
                    ['name' => 'Gadgets'],
                    ['name' => 'Headphones'],
                    ['name' => 'Home Audio'],
                    ['name' => 'Home Automation'],
                    ['name' => 'Home Speakers & Subwoofers'],
                    ['name' => 'Home Surveillance'],
                    ['name' => 'Media Streamers'],
                    ['name' => 'Portable Audio & Accessories'],
                    ['name' => 'Televisions'],
                    ['name' => 'Vintage Electronics'],
                    ['name' => 'Virtual Reality'],
                    ['name' => ''],
                    ['name' => ''],
                    ['name' => ''],
                ]
            ],
            [
                'name' => 'Video Games & Consoles',
                'imageUrl' => $assetFolderUrl . '/images/category-games.jpg',
                'items' => [
                    ['name' => 'Other Video Games & Consoles'],
                    ['name' => 'Prepaid Gaming Cards'],
                    ['name' => 'Replacement Parts & Tools'],
                    ['name' => 'Strategy Guides & Cheats'],
                    ['name' => 'Video Game Accessories'],
                    ['name' => 'Video Game Memorabilia'],
                    ['name' => 'Video Game Merchandise'],
                    ['name' => 'Video Game Wholesale Lots'],
                    ['name' => 'Video Games'],
                ]
            ],
        ]
    ],
    [
        'name' => 'Collectibles & Art',
        'items' => [
        ]
    ],
    [
        'name' => 'Home & Garden',
        'items' => [

        ]
    ],
    [
        'name' => 'Sporting Goods',
        'items' => [
        ]
    ],
    [
        'name' => 'Toys & Hobbies',
        'items' => [
        ]
    ],
    [
        'name' => 'Business & Industrial',
        'items' => [
        ]
    ],
    [
        'name' => 'Music',
        'items' => [
        ]
    ],
];

function getCategoryByName($categories, $name) {
    if ($categories) {
        foreach ($categories as $category) {
            if (!isset($category['name'])) continue;

            if (mb_strtolower($category['name']) === mb_strtolower($name)) {
                return $category;
            }
        }
    }

    return null;
}

$activeCategory = null;
$activeCategoryName = $categoryName;
if (!is_null($activeCategoryName)) {
    $activeCategory = getCategoryByName($categories, $activeCategoryName);
}

if (is_null($activeCategory)) {
    $activeCategoryName = $defaultCategoryName;
    $activeCategory = getCategoryByName($categories, $activeCategoryName);
}

if (is_null($activeCategory) && count($categories) > 0) {
    $activeCategory = $categories[0];
    $activeCategoryName = $activeCategory['name'];
}

?>

<div class="top-bar">
    <div class="wrapper">
        <h1 class="top-bar__heading" >Electronics</h1>
    </div>
</div>

<?php //echo $this->render('/partials/yahoo/yahoo_top') ?>

<div class="wrapper">

    <section class="categories">
        <?php if (count($categories)) { ?>
        <nav class="priority-nav categories__nav">
            <ul class="priority-nav__wrap">
                <?php foreach ($categories as $category) { ?>
                    <?php $_categoryName = $category['name'] ?>
                    <?php $_isActiveCategory = mb_strtolower($activeCategoryName) === mb_strtolower($_categoryName); ?>
                    <?php $_categoryUrl = \common\helpers\Url::generateShoppingCategoryUrl($_categoryName) ?>
                    <li><a class="<?= $_isActiveCategory ? 'is-active' : '' ?>" href="<?= $_categoryUrl ?>"><?= $_categoryName ?></a></li>
                <?php } ?>
            </ul>
        </nav>
        <?php } ?>

        <?php if ($activeCategory && isset($activeCategory['items']) && count($activeCategory['items'])) { ?>
        <div class="categories__wrap">
            <?php $activeCategoryItems = $activeCategory['items']; ?>
            <?php foreach ($activeCategoryItems as $categoryItem) { ?>
                <?php $_categoryName = $categoryItem['name'] ?>
                <?php $_categoryImageUrl = $categoryItem['imageUrl'] ?>
                <?php $_categoryUrl = \common\helpers\Url::generateShoppingSearchUrl($_categoryName) ?>
                <?php $_categorySubItems = isset($categoryItem['items']) && count($categoryItem['items']) ? $categoryItem['items'] : [] ?>
                <article class="category">
                    <header class="category__header">
                        <a class="category__img" href="<?= $_categoryUrl ?>">
                            <img src="<?= $_categoryImageUrl ?>" alt="<?= $_categoryName ?>">
                        </a>
                        <h2 class="category__name"> <a href="<?= $_categoryUrl ?>"><?= $_categoryName ?></a> </h2>
                    </header>
                    <div class="category__list">
                        <?php foreach ($_categorySubItems as $_num => $_categorySubItem) { ?>
                            <?php if ($_num > 4) break; ?>
                            <?php $_categorySubItemName = $_categorySubItem['name'] ?>
                            <?php $_categorySubItemUrl = \common\helpers\Url::generateShoppingSearchUrl($_categorySubItemName) ?>
                            <a href="<?= $_categorySubItemUrl ?>"><?= $_categorySubItemName ?></a>
                        <?php } ?>
                        <?php if ($_categorySubItems > 5) { ?>
                            <div class="category__hidden">
                            <?php foreach ($_categorySubItems as $_num => $_categorySubItem) { ?>
                                <?php if ($_num <= 4) continue; ?>
                                <?php $_categorySubItemName = $_categorySubItem['name'] ?>
                                <?php $_categorySubItemUrl = \common\helpers\Url::generateShoppingSearchUrl($_categorySubItemName) ?>
                                <a href="<?= $_categorySubItemUrl ?>"><?= $_categorySubItemName ?></a>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($_categorySubItems > 5) { ?>
                        <div class="category__more icon icon-chevron-down"></div>
                    <?php } ?>
                </article>
            <?php } ?>
        </div>
        <?php } ?>

    </section>

</div><!-- end wrapper -->

<?php //echo $this->render('/partials/yahoo/yahoo_bottom') ?>