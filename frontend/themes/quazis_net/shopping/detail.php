<?php

/* @var $this \frontend\components\View */
/** @var \common\libraries\feedContentManager\entities\Item|null $item */
/** @var \common\libraries\feedContentManager\entities\Item[] $relatedItems */
/** @var \common\libraries\feedContentManager\entities\Item[] $similarItems */

use common\libraries\feedContentManager\entities\Item;

$siteName = Yii::$app->params['siteName'];

if ($item && in_array($item->getItemType(), [Item::ITEM_TYPE_OFFER, Item::ITEM_TYPE_PRODUCT])) {
    /** @var \common\libraries\feedContentManager\entities\Offer|\common\libraries\feedContentManager\entities\Product  $item */
    $this->title = Yii::t('app', '{itemTitle} Offers', [
        'itemTitle' => $item->getTitle()
    ]);

    $this->registerMetaTag([
        'name' => 'description',
        'content' => Yii::t('app', 'Compare lowest {itemTitle} prices, see {itemTitle} price history or set a price alert for {itemTitle}. Find the best {itemTitle} deals on {siteName}.', [
            'itemTitle' => $item->getTitle(),
            'siteName' => $siteName
        ])
    ]);

    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => Yii::t('app', 'Compare prices on {itemTitle}, latest deals {itemTitle}, discount {itemTitle}, cheap {itemTitle}, {itemTitle} offers, {itemTitle} prices, buy a {itemTitle}', [
            'itemTitle' => $item->getTitle()
        ])
    ]);
} else {
    $this->title = Yii::t('app', 'Offers');
}
$this->addBodyClasses(['page-search']);

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist-bottom.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/list-middle.css');

?>

<?php //echo $this->render('/partials/yahoo/yahoo_top') ?>

<div class="wrapper">

    <?= $this->render('/partials/shopping/detail/item_detail_section', ['item' => $item, 'similarItems' => $similarItems]); ?>

    <?= $this->render('/partials/shopping/detail/related_items_carousel_section', ['relatedItems' => $relatedItems]); ?>

</div>

<?php //echo $this->render('/partials/yahoo/yahoo_top') ?>