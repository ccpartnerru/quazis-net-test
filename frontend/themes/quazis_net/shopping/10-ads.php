<?php

/* @var $this \frontend\components\View */
/* @var $keyword string */

$siteName = Yii::$app->params['siteName'];
$this->title = Yii::t('app', 'Best Offers on {keyword}', ['keyword' => $keyword]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Compare {keyword} offers and get lowers prices on {keyword} on {siteName}.', [
        'keyword' => $keyword,
        'siteName' => $siteName
    ])
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Yii::t('app', 'compare prices on {keyword}, latest deals {keyword}, discount {keyword}, cheap {keyword}, {keyword} offers, {keyword} prices', [
        'keyword' => $keyword
    ])
]);

$this->addBodyClasses(['page-10-ads']);

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist-bottom.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/list-middle.css');
?>

<?php echo $this->render('/partials/yahoo/yahoo_10_ads') ?>
