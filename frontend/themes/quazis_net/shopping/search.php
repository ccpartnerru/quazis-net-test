<?php

use \yii\helpers\Html;
use common\libraries\feedContentManager\feed\Sort;

/* @var $this \frontend\components\View */
/* @var $resultItems \common\libraries\feedContentManager\entities\Item[] */
/* @var $keyword string */
/* @var \common\entities\RelatedSearch[] $relatedSearches */
/** @var \common\entities\Pagination $pagination */
/* @var \common\entities\PopularSearch[] $popularSearches */

$siteName = Yii::$app->params['siteName'];
$this->title = Yii::t('app', 'Best Offers on {keyword}', [
    'keyword' => $keyword
]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::t('app', 'Compare {keyword} offers and get lowers prices on {keyword} on {siteName}.', [
        'keyword' => $keyword,
        'siteName' => $siteName
    ])
]);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Yii::t('app', 'compare prices on {keyword}, latest deals {keyword}, discount {keyword}, cheap {keyword}, {keyword} offers, {keyword} prices', [
        'keyword' => $keyword
    ])
]);

$this->addBodyClasses(['page-search']);

$assetFolderUrl = $this->theme->baseUrl . '/assets';
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/hclist-bottom.css');
$this->registerCssFile($assetFolderUrl . '/css/theme-default/ypa/list-middle.css');

/** @var \common\libraries\feedContentManager\entities\Item[] $resultItemsFirst */
$resultItemsFirst = [];
/** @var \common\libraries\feedContentManager\entities\Item[] $resultItemsSecond */
$resultItemsSecond = [];

if (count($resultItems) < 6) {
    $resultItemsFirst = $resultItems;
} else {
    $middleNum = ceil(count($resultItems) / 2);
    $resultItemsFirst = array_slice($resultItems, 0, $middleNum);
    $resultItemsSecond = array_slice($resultItems, $middleNum);
}

$minPriceFilter = 0;
$maxPriceFilter = 2000;

$minPriceParam = \common\helpers\Url::getMinPriceParam();
$minPriceValue = \common\helpers\Url::getMinPrice();
$maxPriceParam = \common\helpers\Url::getMaxPriceParam();
$maxPriceValue = \common\helpers\Url::getMaxPrice();

?>
<?php //echo $this->render('/partials/yahoo/yahoo_top') ?>

<div class="wrapper">

    <?php if ($relatedSearches && count($relatedSearches)) { ?>
    <section class="hotspots hotspots--top">
        <div class="hotspots__container hotspots__line">
            <?php foreach ($relatedSearches as $relatedSearch) { ?>
                <?php $url = \common\helpers\Url::generateShoppingSearchUrl($relatedSearch->name) ?>
                <a class="hotspot--btn" href="<?= $url ?>"><?= $relatedSearch->name ?></a>
            <?php } ?>
        </div>
    </section>
    <?php } ?>

    <div class="search-layout">
        <div class="search-layout__main">

            <section class="search-results">

                <header class="search-results__header">
                    <h1 class="search-results__heading"><?= $keyword ?></h1>
                    <form class="sort-form" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
                        <input type="hidden" name="<?= \common\helpers\Url::getKeywordParam()?>" value="<?= \common\helpers\Url::getKeyword() ?>">

                        <label class="sort-form__label" for="sort-form__select"> Sort by </label>
                        <select class="sort-form__select" id="sort-form__select" name="<?= \common\helpers\Url::getSortParam() ?>">
                            <?php
                                $sortList = [
                                    [
                                        'label' => Yii::t('app', 'Popularity'),
                                        'value' => Sort::SORT_VALUE_POPULARITY
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Relevance'),
                                        'value' => Sort::SORT_VALUE_RELEVANCE
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Rating'),
                                        'value' => Sort::SORT_VALUE_RATING
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Price'),
                                        'value' => Sort::SORT_VALUE_PRICE
                                    ],
                                ];
                            ?>
                            <?php foreach ($sortList as $sortItem) { ?>
                                <option value="<?= $sortItem['value'] ?>" <?= \common\helpers\Url::getSort() == $sortItem['value'] ? 'selected' : '' ?>><?= $sortItem['label'] ?></option>
                            <?php } ?>
                        </select>
                    </form>
                    <div class="search-results__filters sm-show"><i class="icon icon-filter"></i></div>
                </header>

                <form class="filter-form filter-form--mobile" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">

                    <div class="filter-form__field">
                        <h4 class="filter-form__heading  sidebar__heading"> <?= Yii::t('app', 'Filter By Price') ?> </h4>

                        <div class="filter-form__range">
                            <div class="price-slider">
                                <div class="price-slider__result">
                                    <span><?= Yii::t('app', 'Price')?>:</span> $<?= $minPriceValue ? $minPriceValue : $minPriceFilter ?> - $<?= $maxPriceValue ? $maxPriceValue : $maxPriceFilter ?>
                                </div>
                                <div class="price-slider__slider slider" se-step="10" se-min="<?= $minPriceFilter ?>" se-min-value="<?= $minPriceValue ?>" se-max-value="<?= $maxPriceValue ?>" se-max="<?= $maxPriceFilter ?>">
                                    <div class="slider-touch-left"> <span></span> </div>
                                    <div class="slider-touch-right"> <span></span> </div>
                                    <div class="slider-line"> <span></span> </div>
                                    <input class="price-slider__min" name="<?= $minPriceParam ?>" type="hidden" value="<?= $minPriceValue ?>">
                                    <input class="price-slider__max" name="<?= $maxPriceParam ?>" type="hidden" value="<?= $maxPriceValue ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn--out btn--c3 btn--rnd filter-form__btn" type="submit"  name="" id=""/><?= Yii::t('app', 'Apply Filters')?></button>
                </form>

                <script>
                    document.addEventListener("DOMContentLoaded", function(e) {
                        var filtersBtn = document.querySelector('.search-results__filters');
                        var mobileFilters = document.querySelector('.filter-form--mobile');
                        mobileFilters.style.maxHeight = mobileFilters.offsetHeight + 'px';
                        mobileFilters.classList.add('is-invisible');
                        filtersBtn.addEventListener('click', function(){
                            mobileFilters.classList.toggle('is-invisible');
                        })
                    });
                </script>

                <div class="search-results__list">
                    <?php
                    foreach ($resultItemsFirst as $itemNum => $item) {
                        echo $this->render('/partials/shopping/search/item/item', ['item' => $item]);
                    }
                    ?>

                    <?php
                    if (count($resultItemsSecond)) {
                        //echo $this->render('/partials/yahoo/yahoo_middle');

                        foreach ($resultItemsSecond as $itemNum => $item) {
                            echo $this->render('/partials/shopping/search/item/item', ['item' => $item]);
                        }
                    }
                    ?>
                </div><!-- end prod-list -->

                <?php //echo $this->render('/partials/yahoo/yahoo_bottom') ?>

                <?php if ($pagination) { ?>
                <div class="search-results__pagination pagination">
                    <?php if ($pagination->isAvailablePrevPage()) { ?>
                    <a class="pagination__prev" href="<?= $pagination->getLinkPrevPage() ?>">
                        <i class="icon icon-arrow-left"></i> <?= Yii::t('app', 'Previous page')?>
                    </a>
                    <?php } ?>
                    <?php if ($pagination->isAvailableNextPage()) { ?>
                    <a class="pagination__next" href="<?= $pagination->getLinkNextPage() ?>">
                        <?= Yii::t('app', 'Next page')?> <i class="icon icon-arrow-right"></i>
                    </a>
                    <?php } ?>
                </div>
                <?php } ?>

            </section><!-- end search-results -->

            <?php if ($relatedSearches && count($relatedSearches)) { ?>
            <section class="hotspots hotspots--bottom">
                <h3 class="hotspots__heading"><?= Yii::t('app', 'Related searches')?></h3>
                <div class="hotspots__container hotspots__columns">
                    <?php foreach ($relatedSearches as $relatedSearch) { ?>
                        <?php $url = \common\helpers\Url::generateShoppingSearchUrl($relatedSearch->name) ?>
                        <a class="hotspot" href="<?= $url ?>"><?= $relatedSearch->name ?></a>
                    <?php } ?>
                </div>
            </section>
            <?php } ?>
        </div> <!--END search-layout__main-->

        <div class="search-layout__sidebar sidebar">

            <section class=" sidebar-search sidebar__section">
                <h4 class="sidebar__heading"> <?= Yii::t('app', 'Refine Search')?> </h4>
                <form class="sidebar__search-bar search-bar" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
                    <input class="sidebar-search__input  search-bar__input" name="<?= \common\helpers\Url::getKeywordParam() ?>" placeholder="<?= Yii::t('app', 'Search')?>..." type="search" required value="<?= \common\helpers\Url::getKeyword() ?>">
                    <button class="sidebar-search__btn search-bar__btn" value="" type="submit"></button>
                </form>
            </section>

            <section class="sidebar__section">

                <form class="filter-form filter-form--sidebar" action="<?= \yii\helpers\Url::toRoute(['shopping/search'], true)?>">
                    <input type="hidden" name="<?= \common\helpers\Url::getKeywordParam()?>" value="<?= \common\helpers\Url::getKeyword() ?>">

                    <div class="filter-form__field">
                        <h4 class="filter-form__heading  sidebar__heading"> <?= Yii::t('app', 'Filter By Price') ?> </h4>

                        <div class="filter-form__range">
                            <div class="price-slider">
                                <div class="price-slider__result">
                                    <span><?= Yii::t('app', 'Price')?>:</span> $<?= $minPriceValue ? $minPriceValue : $minPriceFilter ?> - $<?= $maxPriceValue ? $maxPriceValue : $maxPriceFilter ?>
                                </div>
                                <div class="price-slider__slider slider" se-step="10" se-min="<?= $minPriceFilter ?>" se-min-value="<?= $minPriceValue ?>" se-max="<?= $maxPriceFilter ?>" se-max-value="<?= $maxPriceValue ?>">
                                    <div class="slider-touch-left"> <span></span> </div>
                                    <div class="slider-touch-right"> <span></span> </div>
                                    <div class="slider-line"> <span></span> </div>
                                    <input class="price-slider__min" name="<?= $minPriceParam ?>" type="hidden" value="<?= $minPriceValue ?>">
                                    <input class="price-slider__max" name="<?= $maxPriceParam ?>" type="hidden" value="<?= $maxPriceValue ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn--out btn--c3 btn--rnd filter-form__btn" type="submit"  name="" id=""/><?= Yii::t('app', 'Apply Filters')?></button>
                </form>
            </section>

            <?php if ($relatedSearches && count($relatedSearches)) { ?>
            <section class="sidebar__section">
                <h4 class="sidebar__heading"> <?= Yii::t('app', 'Related Searches')?> </h4>
                <div class="hotspots hotspots--sidebar">
                    <div class="hotspots__container hotspots__list">
                        <?php foreach ($relatedSearches as $relatedSearch) { ?>
                            <?php $url = \common\helpers\Url::generateShoppingSearchUrl($relatedSearch->name) ?>
                            <a class="hotspot" href="<?= $url ?>"><?= $relatedSearch->name ?></a>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <?php } elseif ($popularSearches && count($popularSearches)) { ?>
            <section class="sidebar__section">
                <h4 class="sidebar__heading"> <?= Yii::t('app', 'Popular Searches')?> </h4>
                <div class="hotspots hotspots--sidebar">
                    <div class="hotspots__container hotspots__list">
                        <?php foreach ($popularSearches as $popularSearch) { ?>
                            <?php $url = \common\helpers\Url::generateShoppingSearchUrl($popularSearch->name) ?>
                            <a class="hotspot" href="<?= $url ?>"><?= $popularSearch->name ?></a>
                        <?php } ?>
                    </div>
                </div>
            </section>
            <?php } ?>


        </div> <!--END search-layout__sidebar-->
    </div><!--END search-layout -->
</div><!-- wrapper -->


<script>
    document.addEventListener('DOMContentLoaded', function(){
        document.querySelector('#sort-form__select').addEventListener('change', function(){
            if (this.form) {
                this.form.submit()
            }
        });
    });
</script>