<?php
namespace frontend\controllers;

use common\entities\PopularSearch;
use common\entities\Pagination;
use common\libraries\feedContentManager\entities\Item;
use common\models\Page;
use common\entities\RelatedSearch;
use common\libraries\feedContentManager\feed\FeedRequest;
use common\libraries\feedContentManager\feed\Filter;
use common\libraries\feedContentManager\feed\Sort;
use common\libraries\feedContentManager\FeedContentManager;
use common\libraries\feedContentManager\services\feed\ServiceAction;
use common\helpers\Url;
use common\models\Feed;

class ShoppingController extends DefaultController
{
    /**
     * Display search results for shopping feeds
     * @return string
     */
    public function actionSearch()
    {
        $page = Page::getPageByPageSectionNameAndPageName('shopping', 'search');

        $keyword = Url::getKeyword(true);

        $feedContentManager = new FeedContentManager();

        $filterMinPrice = null;
        if (!is_null(Url::getMinPrice())) {
            $filterMinPrice = new Filter();
            $filterMinPrice->setName(Filter::FILTER_NAME_MIN_PRICE);
            $filterMinPrice->setValue(Url::getMinPrice());
        }

        $filterMaxPrice = null;
        if (!is_null(Url::getMaxPrice())) {
            $filterMaxPrice = new Filter();
            $filterMaxPrice->setName(Filter::FILTER_NAME_MAX_PRICE);
            $filterMaxPrice->setValue(Url::getMaxPrice());
        }

        $sort = null;
        if (Url::getSort()) {
            $sort = new Sort();
            $sort->setValue(Url::getSort());
            $sort->setType(Sort::SORT_TYPE_ASC);
        }

        $currentPage = Url::getPageNumber();

        $pageZone = $page->getPageZoneActiveByName('main_feed_content_block');
        $pageZoneItemsLimit = $pageZone->items_count;
        $requests = [];
        foreach ($pageZone->page_zone_feed_settings as $feedSettingsItemKey => $feedSettingsItem) {
            $feed = $feedSettingsItem->feed;
            $keywordFeed = $feedSettingsItem->keyword_replacement ? $feedSettingsItem->keyword_replacement : $keyword;
            $feedItemsLimit = $feedSettingsItem->items_count;

            $request = new FeedRequest();
            $request->setFeed($feed);
            $request->setAction(ServiceAction::ACTION_SEARCH);
            $request->setKeyword($keywordFeed);
            $request->setPerPage($feedItemsLimit);
            $request->setPage($currentPage);
            if ($filterMinPrice) $request->addFilter($filterMinPrice);
            if ($filterMaxPrice) $request->addFilter($filterMaxPrice);
            if ($sort) $request->setSort($sort);
            $requests[$feedSettingsItemKey] = $request;
            $feedContentManager->addRequest($requests[$feedSettingsItemKey]);
        }

        $feedContentManager->exec();

        $maxResponsePagesCount = 0;
        $resultsItems = [];
        foreach ($requests as $request) {
            $response = $feedContentManager->getResponseByRequest($request);
            if ($response && $response->getResult()) {
                $feedResultItems = $response->getResult()->getItems();
                $resultsItems[] = $feedResultItems;

                if ($response->getResult()->getTotalPagesCount() > $maxResponsePagesCount) {
                    $maxResponsePagesCount = $response->getResult()->getTotalPagesCount();
                }
            }
        }

        $resultItems = $pageZone->page_zone_feed_sort->mixItems($resultsItems);
        $resultItems = array_slice($resultItems, 0, $pageZoneItemsLimit);

        $relatedSearches = RelatedSearch::getByKeyword($keyword);
        $popularSearches = PopularSearch::getPopularSearches(10);

        $pagination = new Pagination();
        $pagination->setTotalPagesCount($maxResponsePagesCount);

        return $this->render('search', [
            'resultItems' => $resultItems,
            'keyword' => $keyword,
            'relatedSearches' => $relatedSearches,
            'pagination' => $pagination,
            'popularSearches' => $popularSearches
        ]);
    }

    /**
     * Mix two one or more resultItems arrays
     * @param Item[] $resultItems1
     * @param Item[] $resultItems2
     * @param Item[] $resultItems3 [optional]
     * @param Item[] $_ [optional]
     * @return array the resulting array.
     */
    private function mixFeedsResultItems(array $resultItems1, array $resultItems2, array $resultItems3 = null, array $_ = null)
    {
        /** @var Item[] $mixResultItems */
        $mixResultItems = [];
        $resultItemsArrays = func_get_args();

        $maxResultItemsCount = 0;
        foreach ($resultItemsArrays as $resultItems) {
            if (count($resultItems) > $maxResultItemsCount) {
                $maxResultItemsCount = count($resultItems);
            }
        }

        if ($maxResultItemsCount > 0) {
            for ($i = 0; $i < $maxResultItemsCount; $i++) {
                foreach ($resultItemsArrays as &$resultItems) {
                    if (count($resultItems) == 0) continue;

                    $resultItem = array_shift($resultItems);
                    $mixResultItems[] = $resultItem;
                }
            }
        }

        return $mixResultItems;
    }

    /**
     * Display offer or product detail page
     * @param $feedPartnerShortName
     * @param $itemId
     * @return string
     */
    public function actionDetail($feedPartnerShortName, $itemId)
    {
        \Yii::$app->language = \common\models\Language::LANGUAGE_CODE_RU_RU;
        $user = $this->user;
        $keyword = Url::getKeyword(true);
        $sourcePartnerId = $user->getSourcePartnerId();

        $feedContentManager = new FeedContentManager();

        $feedDetail = Feed::getByShortName($feedPartnerShortName);
        $requestFeedDetail = new FeedRequest();
        $requestFeedDetail->setFeed($feedDetail);
        $requestFeedDetail->setAction(ServiceAction::ACTION_DETAIL);
        $requestFeedDetail->setItemId($itemId);
        $feedContentManager->addRequest($requestFeedDetail);

        $feed = Feed::getById(Feed::PARTNER_ID_EBAY);
        $keywordFeed = $keyword;
        $requestEbaySearch = new FeedRequest();
        $requestEbaySearch->setFeed($feed);
        $requestEbaySearch->setAction(ServiceAction::ACTION_SEARCH);
        $requestEbaySearch->setKeyword($keywordFeed);
        $requestEbaySearch->setPerPage(10);
        $requestEbaySearch->setPage(1);
        $feedContentManager->addRequest($requestEbaySearch);

        $feed = Feed::getById(Feed::PARTNER_ID_WALMART);
        $keywordFeed = $keyword;
        $requestWalmartSearch = new FeedRequest();
        $requestWalmartSearch->setFeed($feed);
        $requestWalmartSearch->setAction(ServiceAction::ACTION_SEARCH);
        $requestWalmartSearch->setKeyword($keywordFeed);
        $requestWalmartSearch->setPerPage(10);
        $requestWalmartSearch->setPage(1);
        $feedContentManager->addRequest($requestWalmartSearch);

        $feed = Feed::getById(Feed::PARTNER_ID_ALIEXPRESS);
        $keywordFeed = $keyword;
        $requestAliexpressSearch = new FeedRequest();
        $requestAliexpressSearch->setFeed($feed);
        $requestAliexpressSearch->setAction(ServiceAction::ACTION_SEARCH);
        $requestAliexpressSearch->setKeyword($keywordFeed);
        $requestAliexpressSearch->setPerPage(10);
        $requestAliexpressSearch->setPage(1);
        $feedContentManager->addRequest($requestAliexpressSearch);

        $feedContentManager->exec();

        $responseFeedDetail = $feedContentManager->getResponseByRequest($requestFeedDetail);

        $resultItems = $responseFeedDetail && $responseFeedDetail->getResult() ? $responseFeedDetail->getResult()->getItems() : [];
        $item = isset($resultItems[0]) ? $resultItems[0] : null;

        $response = $feedContentManager->getResponseByRequest($requestEbaySearch);
        $resultItemsEbay = [];
        if ($response && $response->getResult()) {
            $resultItemsEbay = $response->getResult()->getItems();
        }

        $response = $feedContentManager->getResponseByRequest($requestWalmartSearch);
        $resultItemsWalmart = [];
        if ($response && $response->getResult()) {
            $resultItemsWalmart = $response->getResult()->getItems();
        }

        $response = $feedContentManager->getResponseByRequest($requestAliexpressSearch);
        $resultItemsAliexpress = [];
        if ($response && $response->getResult()) {
            $resultItemsAliexpress = $response->getResult()->getItems();
        }

        $relatedItems = $this->mixFeedsResultItems($resultItemsWalmart, $resultItemsEbay, $resultItemsAliexpress);
        $relatedItems = array_slice($relatedItems, 0, 30);

        $similarItems = [];
        if ($item) {
            $itemTitle = $item->getTitle();
            $similarItems = $this->getSimilarItems($itemTitle);
        }

        return $this->render('detail', [
            'item' => $item,
            'relatedItems' => $relatedItems,
            'similarItems' => $similarItems
        ]);
    }

    private function getSimilarItems($itemTitle)
    {
        // get first 5 words from item title
        $keyword = implode(' ', array_slice(explode(' ', $itemTitle), 0, 5));

        $feedContentManager = new FeedContentManager();

        $feed = Feed::getById(Feed::PARTNER_ID_EBAY);
        $keywordFeed = $keyword;
        $requestEbaySearch = new FeedRequest();
        $requestEbaySearch->setFeed($feed);
        $requestEbaySearch->setAction(ServiceAction::ACTION_SEARCH);
        $requestEbaySearch->setKeyword($keywordFeed);
        $requestEbaySearch->setPerPage(10);
        $requestEbaySearch->setPage(1);
        $feedContentManager->addRequest($requestEbaySearch);

        $feed = Feed::getById(Feed::PARTNER_ID_WALMART);
        $keywordFeed = $keyword;
        $requestWalmartSearch = new FeedRequest();
        $requestWalmartSearch->setFeed($feed);
        $requestWalmartSearch->setAction(ServiceAction::ACTION_SEARCH);
        $requestWalmartSearch->setKeyword($keywordFeed);
        $requestWalmartSearch->setPerPage(10);
        $requestWalmartSearch->setPage(1);
        $feedContentManager->addRequest($requestWalmartSearch);

        $feedContentManager->exec();

        $response = $feedContentManager->getResponseByRequest($requestEbaySearch);
        $resultItemsEbay = [];
        if ($response && $response->getResult()) {
            $resultItemsEbay = $response->getResult()->getItems();
        }

        $response = $feedContentManager->getResponseByRequest($requestWalmartSearch);
        $resultItemsWalmart = [];
        if ($response && $response->getResult()) {
            $resultItemsWalmart = $response->getResult()->getItems();
        }

        $similarItems = $this->mixFeedsResultItems($resultItemsWalmart, $resultItemsEbay);
        $similarItems = array_slice($similarItems, 0, 30);

        return $similarItems;
    }

    public function actionCategory()
    {
        $categoryName = Url::getCategoryName();

        return $this->render('category', [
            'categoryName' => $categoryName
        ]);
    }

    /**
     * Display 10 yahoo ads
     */
    public function action10Ads() {
        $keyword = Url::getKeyword();

        return $this->render('10-ads', [
            'keyword' => $keyword
        ]);
    }
}

