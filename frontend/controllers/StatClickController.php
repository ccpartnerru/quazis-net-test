<?php
namespace frontend\controllers;

use common\components\Session;
use common\components\User;
use common\helpers\Url;
use common\libraries\botDetector\BotDetector;
use common\libraries\safeDetector\SafeDetector;
use common\models\Device;
use common\models\Link;
use common\models\StatClick;
use Yii;
use common\entities\statClick\Response;

class StatClickController extends DefaultController
{
    public function actionInbound()
    {
        $response = new Response();
        $response->setStatus(Response::STATUS_SUCCESS);

        try {
            $requestUrl = Url::getRequestUrl();
            self::registerInboundClick($requestUrl);
        } catch (\Exception $e) {
            $response = new Response();
            $response->setStatus(Response::STATUS_ERROR);
            $response->setErrorCode(Response::ERROR_CODE_DEFAULT);
            $response->setMessage($e->getMessage());
        }

        return $this->asJson($response->toArray());
    }

    public function actionOutbound()
    {
        $response = new Response();
        $response->setStatus(Response::STATUS_SUCCESS);

        try {
            $requestUrl = Url::getRequestUrl();
            $requestRefer = Url::getRequestRefer();
            $requestFeedPartnerId = Url::getRequestFeedPartnerId();

            $keyword = Url::getKeywordFromUrl($requestUrl);
            $user = $this->user;

            $linkKey = $user->getLinkKey();

            $outboundStatClick = new StatClick();
            $outboundStatClick->type                     = StatClick::STAT_CLICK_TYPE_OUTBOUND;
            $outboundStatClick->partner_id               = $requestFeedPartnerId;
            $outboundStatClick->market_id                = $user->getMarket() ? $user->getMarket()->id : null;
            $outboundStatClick->device_id                = $user->getDevice() ? $user->getDevice()->id : Device::DEVICE_ID_UNKNOWN;
            $outboundStatClick->request_uri              = $requestUrl;
            $outboundStatClick->request_refer            = $requestRefer;
            $outboundStatClick->keyword                  = $keyword;
            $outboundStatClick->user_ip                  = $user->getIp();
            $outboundStatClick->user_agent               = $user->getUserAgent();
            $outboundStatClick->link_key                 = $linkKey;
            $outboundStatClick->session_id               = $user->getSessionId();
            $outboundStatClick->is_bot                   = $user->isBot() ? 1 : 0;
            $outboundStatClick->bot_reason               = $user->getBotReason();
            $outboundStatClick->inbound_stat_click_id    = $user->getInboundStatClickId();
            $outboundStatClick->is_safe                  = $user->isSafe();
            $outboundStatClick->not_safe_reason          = $user->getNotSafeReason();

            if (!$outboundStatClick->save()) {
                throw new \Exception('Can not save outbound click model');
            }

        } catch (\Exception $e) {
            $response = new Response();
            $response->setStatus(Response::STATUS_ERROR);
            $response->setErrorCode(Response::ERROR_CODE_DEFAULT);
            $response->setMessage($e->getMessage());
        }

        return $this->asJson($response->toArray());
    }

    public static function registerInboundClick($requestUrl)
    {
        /** @var User $user */
        $user = Yii::$app->user;
        $inboundStatClickId = $user->getInboundStatClickId();
        $userLinkKey = $user->getLinkKey();
        $currentLinkKey = Url::getLinkKey();
        $userSourceClickId = $user->getSourceClickId();
        $sourceClickId = Url::getSourceClickIdFromUrl($requestUrl);

        if (!$inboundStatClickId || (!is_null($currentLinkKey) && $userLinkKey != $currentLinkKey) || (!is_null($sourceClickId) && $userSourceClickId != $sourceClickId)) {
            try {
                $linkKey = Url::getLinkKeyFromUrl($requestUrl);
                $link = Link::findByKey($linkKey);
                $sourcePartnerId = $link ? $link->source_partner_id : 0;

                $keyword = Url::getKeywordFromUrl($requestUrl);
                $user->setLinkKey($linkKey);
                $user->setSourcePartnerId($sourcePartnerId);

                $botDetector = new BotDetector();
                $botDetector->checkUser($user);
                $user->setBotReason($botDetector->botReason);

                $safeDetector = new SafeDetector();
                $safeDetector->checkUser($user);
                $user->setNotSafeReason($safeDetector->notSafeReason);

                $user->setSourceClickId($sourceClickId);

                $inboundStatClick = new StatClick();
                $inboundStatClick->type                     = StatClick::STAT_CLICK_TYPE_INBOUND;
                $inboundStatClick->partner_id               = $sourcePartnerId;
                $inboundStatClick->market_id                = $user->getMarket() ? $user->getMarket()->id : null;
                $inboundStatClick->device_id                = $user->getDevice() ? $user->getDevice()->id : Device::DEVICE_ID_UNKNOWN;
                $inboundStatClick->request_uri              = $requestUrl;
                $inboundStatClick->request_refer            = null;
                $inboundStatClick->keyword                  = $keyword;
                $inboundStatClick->user_ip                  = $user->getIp();
                $inboundStatClick->user_agent               = $user->getUserAgent();
                $inboundStatClick->link_key                 = $linkKey;
                $inboundStatClick->session_id               = $user->getSessionId();
                $inboundStatClick->is_bot                   = $user->isBot() ? 1 : 0;
                $inboundStatClick->bot_reason               = $user->getBotReason();
                $inboundStatClick->inbound_stat_click_id    = null;
                $inboundStatClick->is_safe                  = $user->isSafe();
                $inboundStatClick->not_safe_reason          = $user->getNotSafeReason();

                if (!$inboundStatClick->save()) {
                    throw new \Exception('Can not save inbound click model');
                }

                $inboundStatClickId = $inboundStatClick->getPrimaryKey();
                $user->setInboundStatClickId($inboundStatClickId);

            } catch (\Exception $e) {
                return false;
            }
        }

        return true;
    }
}
