<?php
namespace frontend\controllers;

use common\libraries\feedContentManager\entities\Item;
use common\libraries\feedContentManager\entities\Offer;
use common\libraries\feedContentManager\entities\Product;
use common\libraries\feedContentManager\FeedContentManager;
use common\libraries\feedContentManager\services\feed\ServiceAction;
use common\helpers\Url;
use common\models\Page;
use Yii;
use yii\base\InvalidParamException;
use yii\base\UserException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\libraries\feedContentManager\feed\FeedRequest;
use common\libraries\feedContentManager\feed\Filter;
use common\libraries\feedContentManager\feed\Sort;
use common\models\Feed;
use common\models\KeywordReplacement;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends DefaultController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionError()
    {
        $exception = (Yii::$app->errorHandler && Yii::$app->errorHandler->exception) ? Yii::$app->errorHandler->exception : null;

        if ($exception === null) {
            $exception = new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof \Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        $name .= " (#$code)";

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        return $this->render('error', [
            'name' => $name,
            'message' => $message,
            'exception' => $exception
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $feedContentManager = new FeedContentManager();

        $keywords = [
            'laptop',
            'digital camera',
            'smartphone'
        ];

        $keywordRequests = [];
        foreach ($keywords as $keywordKey => $keyword) {
            $keywordFeed = $keyword;
            $feedItemsLimit = 10;
            $feed = Feed::getById(Feed::PARTNER_ID_WALMART);
            $requestWalmartSearch = new FeedRequest();
            $requestWalmartSearch->setFeed($feed);
            $requestWalmartSearch->setAction(ServiceAction::ACTION_SEARCH);
            $requestWalmartSearch->setKeyword($keywordFeed);
            $requestWalmartSearch->setPerPage($feedItemsLimit);
            $feedContentManager->addRequest($requestWalmartSearch);
            $keywordRequests[$keywordKey]['walmartSearch'] = $requestWalmartSearch;

            $feed = Feed::getById(Feed::PARTNER_ID_EBAY);
            $requestEbaySearch = new FeedRequest();
            $requestEbaySearch->setFeed($feed);
            $requestEbaySearch->setAction(ServiceAction::ACTION_SEARCH);
            $requestEbaySearch->setKeyword($keywordFeed);
            $requestEbaySearch->setPerPage($feedItemsLimit);
            $feedContentManager->addRequest($requestEbaySearch);
            $keywordRequests[$keywordKey]['ebaySearch'] = $requestEbaySearch;

            $feed = Feed::getById(Feed::PARTNER_ID_ALIEXPRESS);
            $requestAliexpressSearch = new FeedRequest();
            $requestAliexpressSearch->setFeed($feed);
            $requestAliexpressSearch->setAction(ServiceAction::ACTION_SEARCH);
            $requestAliexpressSearch->setKeyword($keywordFeed);
            $requestAliexpressSearch->setPerPage($feedItemsLimit);
            $feedContentManager->addRequest($requestAliexpressSearch);
            $keywordRequests[$keywordKey]['aliexpressSearch'] = $requestAliexpressSearch;
        }

        $feedContentManager->exec();

        $totalResultItems = [];
        $keywordResultItems = [];
        if ($keywordRequests && count($keywordRequests)) {
            foreach ($keywordRequests as $keywordKey => $keywordRequest) {
                $requestEbaySearch = $keywordRequests[$keywordKey]['ebaySearch'];
                $response = $feedContentManager->getResponseByRequest($requestEbaySearch);
                $resultItemsEbay = [];
                if ($response && $response->getResult()) {
                    $resultItemsEbay = $response->getResult()->getItems();
                }

                $requestWalmartSearch = $keywordRequests[$keywordKey]['walmartSearch'];
                $response = $feedContentManager->getResponseByRequest($requestWalmartSearch);
                $resultItemsWalmart = [];
                if ($response && $response->getResult()) {
                    $resultItemsWalmart = $response->getResult()->getItems();
                }

                $requestAliexpressSearch = $keywordRequests[$keywordKey]['aliexpressSearch'];
                $response = $feedContentManager->getResponseByRequest($requestAliexpressSearch);
                $resultItemsAliexpress = [];
                if ($response && $response->getResult()) {
                    $resultItemsAliexpress = $response->getResult()->getItems();
                }

                $keywordResultItems[$keywordKey] = array_merge($resultItemsWalmart, $resultItemsEbay, $resultItemsAliexpress);
                $totalResultItems = array_merge($totalResultItems, $keywordResultItems[$keywordKey]);
            }
        }

        $discountsItems = self::getItemsWithDiscount($totalResultItems);
        shuffle($discountsItems);

        $latestProductsItems        = array_intersect_key($totalResultItems, array_flip(array_rand($totalResultItems, 8)));
        $featuredProductsItems      = array_intersect_key($totalResultItems, array_flip(array_rand($totalResultItems, 8)));
        $bestSellingProductsItems   = array_intersect_key($totalResultItems, array_flip(array_rand($totalResultItems, 8)));

        return $this->render('index', [
            'discountsItems' => $discountsItems,
            'latestProductsItems' => $latestProductsItems,
            'featuredProductsItems' => $featuredProductsItems,
            'bestSellingProductsItems' => $bestSellingProductsItems,
        ]);
    }

    /**
     * @param Item[] $resultItems
     * @return Item[]
     */
    private static function getItemsWithDiscount($resultItems)
    {
        $discountsItems = [];
        foreach ($resultItems as $resultItem) {
            if (in_array($resultItem->getItemType(), [$resultItem::ITEM_TYPE_OFFER, $resultItem::ITEM_TYPE_PRODUCT])) {
                /** @var Offer|Product $resultItem */
                if ($resultItem->getDiscount() && $resultItem->getDiscount()->getValue()) {
                    $discountsItems[] = $resultItem;
                }
            }
        }

        return $discountsItems;
    }

    /**
     * Displays About page.
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays Contact page.
     */
    public function actionContact()
    {
        return $this->render('contact');
    }

    /**
     * Display Terms and Conditions page
     */
    public function actionTerms()
    {
        return $this->render('terms');
    }

    /**
     * Display Privacy Policy page
     */
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }

    /**
     * Display Cookies page
     */
    public function actionCookie()
    {
        return $this->render('cookie');
    }
}
