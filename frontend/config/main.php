<?php

use common\models\Language;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$frontendUrlManagerComponentSettings = require __DIR__ . '/url-manager.php';
$backendUrlManagerComponentSettings  = require __DIR__ . '/../../backend/config/url-manager.php';

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => $frontendUrlManagerComponentSettings,
        'urlManagerFrontend' => $frontendUrlManagerComponentSettings,
        'urlManagerBackend' => $backendUrlManagerComponentSettings,
        'view' => [
            'class' => 'frontend\components\View',
            'theme' => [
                'basePath' => '@app/themes/quazis_net',
                'baseUrl' => '@web/themes/quazis_net',
                'pathMap' => [
                    '@app/views' => '@app/themes/quazis_net',
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => Language::LANGUAGE_CODE_EN_US
                ],
            ],
        ],
    ],
    'params' => $params,
];
