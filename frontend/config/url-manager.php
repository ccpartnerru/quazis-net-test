<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'baseUrl' => '/',
    'rules' => [
        ''          => 'site/index',
        '/about'    => 'site/about',
        '/contact'  => 'site/contact',
        '/terms'    => 'site/terms',
        '/privacy'  => 'site/privacy',
        '/cookie'   => 'site/cookie',
        'shopping/search' => 'shopping/search',
        'shopping/detail/<feedPartnerShortName:\w+>/<itemId:\w+>' => 'shopping/detail',
        'shopping/category' => 'shopping/category',
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    ],
];
