<?php
return [
    'sections' => [
        [
            'name' => 'shopping',
            'description' => 'Price comparison part of our site',
            'pages' => [
                [
                    'name' => 'search',
                    'description' => 'Search page in shopping part of site',
                    'zones' => [
                        [
                            'name' => 'top_ads_block',
                            'items_count' => 4,
                            'is_feed_settings_editable' => false,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_SEQUENCE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_YAHOO
                                ]
                            ],
                        ],
                        [
                            'name' => 'middle_ads_block',
                            'items_count' => 2,
                            'is_feed_settings_editable' => false,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_ONE_BY_ONE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_YAHOO
                                ]
                            ],
                        ],
                        [
                            'name' => 'bottom_ads_block',
                            'items_count' => 4,
                            'is_feed_settings_editable' => false,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_ONE_BY_ONE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_YAHOO
                                ]
                            ],
                        ],
                        [
                            'name' => 'main_feed_content_block',
                            'items_count' => 10,
                            'is_feed_settings_editable' => true,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_ONE_BY_ONE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_WALMART,
                                ],
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_EBAY,
                                ],
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_ALIEXPRESS,
                                ],
                            ]
                        ],
                    ]
                ],
                [
                    'name' => 'detail',
                    'description' => 'Feed detail page in shopping part of site',
                    'zones' => [
                        [
                            'name' => 'top_ads_block',
                            'items_count' => 4,
                            'is_feed_settings_editable' => false,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_SEQUENCE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_YAHOO
                                ]
                            ],
                        ],
                        [
                            'name' => 'bottom_ads_block',
                            'items_count' => 4,
                            'is_feed_settings_editable' => false,
                            'feed_sort_id' => \common\models\PageZoneFeedSort::TYPE_ID_ONE_BY_ONE,
                            'feed_settings' => [
                                [
                                    'feed_id' => \common\models\Feed::PARTNER_ID_YAHOO
                                ]
                            ],
                        ],
                    ]
                ],
            ]
        ]
    ]
];
