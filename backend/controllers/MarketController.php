<?php

namespace backend\controllers;

use Yii;
use common\models\Market;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

class MarketController extends Controller
{
    /**
     * Lists all Market models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Market::getAll(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
