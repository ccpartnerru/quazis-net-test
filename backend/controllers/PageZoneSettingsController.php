<?php

namespace backend\controllers;

use backend\models\PageZoneSettingsForm;
use backend\models\PageZoneSettingsSearchIndexForm;
use common\models\Feed;
use common\models\PageSection;
use common\models\PageZone;
use common\models\PageZoneFeedSort;
use common\models\PageZoneRuleGroup;
use common\models\PageZoneRuleWeekdayTime;
use common\models\Source;
use Yii;
use common\models\PageZoneSettings;
use common\models\PageZoneSettingsSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageZoneSettingsController implements the CRUD actions for PageZoneSettings model.
 */
class PageZoneSettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PageZoneSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $pageZoneSettingsSearchIndexForm = new PageZoneSettingsSearchIndexForm();

        $data = Yii::$app->request->get();
        $data = isset($data['PageZoneSettingsSearchIndexForm']) ? $data['PageZoneSettingsSearchIndexForm'] : [];
        if (isset($data['week_day_time_data'])) {
            $data['week_day_time_data'] = json_decode($data['week_day_time_data']);
        }
        $pageZoneSettingsSearchIndexForm->setAttributes($data);

        $pageSectionActive = PageSection::getPageSectionByName($pageZoneSettingsSearchIndexForm->page_section_name);
        $pageActive = $pageZoneSettingsSearchIndexForm->page_name ? $pageSectionActive->getPageByName($pageZoneSettingsSearchIndexForm->page_name) : [];


        $query = PageZoneRuleGroup::find()->alias('pzrg')->leftJoin(['pzrwt' => PageZoneRuleWeekdayTime::tableName()], 'pzrwt.page_zone_rule_group_id = pzrg.id')->groupBy('pzrg.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'forcePageParam' => false,
                'pageSizeParam' => false,
                'pageSize' => 20
            ]
        ]);


        if (!empty($pageZoneSettingsSearchIndexForm->rule_group_id)) {
            $query->andFilterWhere([
                'pzrg.id' => $pageZoneSettingsSearchIndexForm->rule_group_id
            ]);
        } else {
            if (!empty($pageZoneSettingsSearchIndexForm->page_section_name)) {
                $query->andFilterWhere([
                    'pzrg.page_section_name' => $pageZoneSettingsSearchIndexForm->page_section_name
                ]);
            }

            if (!empty($pageZoneSettingsSearchIndexForm->page_name)) {
                $query->andFilterWhere([
                    'pzrg.page_name' => $pageZoneSettingsSearchIndexForm->page_name
                ]);
            }

            if (!empty($pageZoneSettingsSearchIndexForm->page_zone_name)) {
                $query->andFilterWhere([
                    'pzrg.page_zone_name' => $pageZoneSettingsSearchIndexForm->page_zone_name
                ]);
            }

            if ($pageZoneSettingsSearchIndexForm->source_partner_id != '') {
                $query->andFilterWhere([
                    'pzrg.source_partner_id' => $pageZoneSettingsSearchIndexForm->source_partner_id
                ]);
            }

            if (!empty($pageZoneSettingsSearchIndexForm->inbound_keyword)) {
                $query->andFilterWhere([
                    'like',
                    'pzrg.inbound_keyword',
                    $pageZoneSettingsSearchIndexForm->inbound_keyword
                ]);
            }

            $activeHoursWeekDay = PageZoneRuleWeekdayTime::getActiveHoursWeekDay($pageZoneSettingsSearchIndexForm->week_day_time_data);
            if (!count($activeHoursWeekDay)) {
                $query->andFilterWhere(['pzrg.id' => 0]);
            } else {
                $weekDayHoursConflictCondition = [];
                $weekDayHoursConflictCondition[] = 'OR';
                foreach ($activeHoursWeekDay as $day => $hours) {
                    $weekDayHoursConflictCondition[] = [
                        'AND',
                        ['=', 'pzrwt.week_day', $day],
                        ['IN', 'pzrwt.hour', $hours]
                    ];
                }

                $query->andFilterWhere($weekDayHoursConflictCondition);
            }
        }

        return $this->render('index', [
            'searchFormModel' => $pageZoneSettingsSearchIndexForm,
            'pageSections' => PageSection::getAll(),
            'pages' => $pageSectionActive ? $pageSectionActive->pages : [],
            'pageZones' => $pageActive ? $pageActive->zones : [],
            'sources' => Source::getAll(),
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new PageZoneSettings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $pageZoneSettingsForm = new PageZoneSettingsForm();

        return $this->render('create-new', [
            'model' => $pageZoneSettingsForm,
            'pageSections' => PageSection::getAll(),
            'itemsCountLimit' => 12,
            'pageZoneFeedSorts' => PageZoneFeedSort::getAll(),
            'feeds' => Feed::getAll(),
            'sources' => Source::getAll(),
        ]);
    }

    /**
     * Updates an existing PageZoneSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isAjax) {
            $pageZoneSettingsForm = new PageZoneSettingsForm();

            $data = Yii::$app->request->post();

            $pageZoneSettingsForm->setAttributes($data);
            try {
                $pageZoneSettingsForm->save();
            } catch (\Exception $e) {
                $pageZoneSettingsForm->addError('rule_group_id', $e->getMessage());
            }

            if (empty($pageZoneSettingsForm->getErrors())) {
                return $this->asJson([
                    'status' => 'success'
                ]);
            } else {
                $message = '';
                foreach ($pageZoneSettingsForm->getErrors() as $attributeName => $error) {
                    $message .= implode(', ', $error) . "\n";
                }

                return $this->asJson([
                    'status' => 'error',
                    'message' => $message
                ]);
            }
        }

        $pageZoneSettingsForm = PageZoneSettingsForm::findByRuleGroupId($id);

        $pageSectionActive = PageSection::getPageSectionByName($pageZoneSettingsForm->page_section_name);
        $pageActive = $pageSectionActive->getPageByName($pageZoneSettingsForm->page_name);

        return $this->render('update-new', [
            'model' => $pageZoneSettingsForm,
            'pageSections' => PageSection::getAll(),
            'pages' => $pageSectionActive->pages,
            'pageZones' => $pageActive->zones,
            'sources' => Source::getAll(),
            'itemsCountLimit' => 12,
            'pageZoneFeedSorts' => PageZoneFeedSort::getAll(),
            'feeds' => Feed::getAll()
        ]);
    }

    /**
     * Deletes an existing PageZoneSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PageZoneSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageZoneRuleGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageZoneRuleGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param string $pageSectionName
     * @return \yii\web\Response
     */
    public function actionPageList($pageSectionName)
    {
        $pageList = [];
        if (!empty($pageSectionName)) {
            $pageSection = PageSection::getPageSectionByName($pageSectionName);
            $pages = $pageSection->pages;
            foreach ($pages as $page) {
                $pageList[] = [
                    'name' => $page->name
                ];
            }
        }

        return $this->asJson([
            'pageList' => $pageList
        ]);
    }

    /**
     * @param string $pageSectionName
     * @param string $pageName
     * @return \yii\web\Response
     */
    public function actionPageZoneList($pageSectionName, $pageName)
    {
        $pageZoneList = [];

        if (!empty($pageSectionName) && !empty($pageName)) {
            $pageSection = PageSection::getPageSectionByName($pageSectionName);
            $page = $pageSection->getPageByName($pageName);

            $zones = $page->zones;
            foreach ($zones as $zone) {
                $pageZoneList[] = [
                    'name' => $zone->name
                ];
            }
        }

        return $this->asJson([
            'pageZoneList' => $pageZoneList
        ]);
    }

    public function actionGetPageZoneSettingsDefaultConfig()
    {
        $data = Yii::$app->request->get();

        $pageSectionName    = $data['page_section_name'];
        $pageName           = $data['page_name'];
        $pageZoneName       = $data['page_zone_name'];

        $pageZone = PageZone::getByPageSectionNameAndPageNameAndPageZoneName($pageSectionName, $pageName, $pageZoneName);

        if ($pageZone) {
            $pageZoneFeedSort = $pageZone->page_zone_feed_sort;
            if (!$pageZoneFeedSort) {
                $pageZoneFeedSort = PageZoneFeedSort::getDefaultSort();
            }

            $result = [
                'items_count' => $pageZone->items_count,
                'page_zone_feed_sort_id' => $pageZoneFeedSort->id,
                'is_feed_settings_editable' => $pageZone->is_feed_settings_editable ? $pageZone->is_feed_settings_editable : false
            ];

            $result['page_zone_feed_settings'] = [];
            if ($pageZone->page_zone_feed_settings && count($pageZone->page_zone_feed_settings)) {
                foreach ($pageZone->page_zone_feed_settings as $pageZoneFeedSettingsRow) {
                    $result['page_zone_feed_settings'][] = [
                        'feed_id' => $pageZoneFeedSettingsRow->feed->id,
                        'feed_name' => $pageZoneFeedSettingsRow->feed->name,
                        'items_count' => $pageZoneFeedSettingsRow->items_count ? $pageZoneFeedSettingsRow->items_count : $pageZone->items_count
                    ];
                }
            }

            return $this->asJson([
                'status' => 'success',
                'data' => $result
            ]);
        }

        return $this->asJson([
            'status' => 'success',
            'message' => 'Can not find page zone with this filters: ' . json_encode($data)
        ]);
    }
}
