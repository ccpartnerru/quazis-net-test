<?php

namespace backend\controllers;

use common\models\Page;
use common\models\PageSection;
use Yii;
use common\models\Device;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

class PageController extends Controller
{
    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $pageSections = PageSection::getAll();

        return $this->render('index', [
            'pageSections' => $pageSections,
        ]);
    }

}
