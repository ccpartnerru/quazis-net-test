<?php

namespace backend\controllers;

use Yii;
use common\models\Device;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

class DeviceController extends Controller
{
    /**
     * Lists all Device models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Device::getAll(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}
