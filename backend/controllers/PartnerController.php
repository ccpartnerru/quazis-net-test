<?php

namespace backend\controllers;

use common\models\Feed;
use common\models\Source;
use Yii;
use common\models\Partner;
use yii\data\ArrayDataProvider;

class PartnerController extends DefaultController
{
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Partner::getAll(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFeed()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Feed::getAll(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSource()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels' => Source::getAll(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
