<?php


namespace backend\models;

use common\models\Feed;
use common\models\PageZoneFeedCustomSettings;
use common\models\PageZoneFeedSort;
use common\models\PageZoneRuleGroup;
use common\models\PageZoneRuleWeekdayTime;
use common\models\Source;
use yii\base\Model;
use yii\db\Expression;
use yii\db\ExpressionBuilder;
use yii\db\Query;
use yii\db\QueryBuilder;

class PageZoneSettingsSearchIndexForm extends Model
{
    /** @var  int|null */
    public $rule_group_id;

    /** @var  string */
    public $page_section_name;

    /** @var  string */
    public $page_name;

    /** @var  string */
    public $page_zone_name;

    /** @var  int */
    public $source_partner_id;

    /** @var  string */
    public $inbound_keyword;

    /** @var [][] */
    public $week_day_time_data;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $decompressedWeekDayTimeData = [];
        for ($day = 0; $day < 7; $day++) {
            for ($hour = 0; $hour < 24; $hour ++) {
                $decompressedWeekDayTimeData[$day][$hour] = 1;
            }
        }

        $this->week_day_time_data = $decompressedWeekDayTimeData;
    }

    public function rules()
    {
        return [
            [['rule_group_id', 'source_partner_id'], 'integer'],
            [['page_section_name', 'page_name', 'page_zone_name', 'inbound_keyword'], 'string'],
            [['week_day_time_data'], 'safe'],
        ];
    }
}