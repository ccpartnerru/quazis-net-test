<?php


namespace backend\models;

use common\models\Feed;
use common\models\PageZone;
use common\models\PageZoneFeedCustomSettings;
use common\models\PageZoneFeedSort;
use common\models\PageZoneRuleGroup;
use common\models\PageZoneRuleWeekdayTime;
use common\models\Source;
use yii\base\Model;
use yii\db\Expression;
use yii\db\ExpressionBuilder;
use yii\db\Query;
use yii\db\QueryBuilder;

class PageZoneSettingsForm extends Model
{
    /** @var  int|null */
    public $rule_group_id;

    /** @var  boolean */
    public $is_feed_settings_editable;

    /** @var  string */
    public $page_section_name;

    /** @var  string */
    public $page_name;

    /** @var  string */
    public $page_zone_name;

    /** @var  int */
    public $source_partner_id;

    /** @var  string */
    public $inbound_keyword;

    /** @var [][] */
    public $week_day_time_data;

    /** @var  int */
    public $items_count;

    /** @var  string */
    public $keyword_replacement;

    /** @var int */
    public $feed_sort_id;

    /** @var  [] */
    public $feed_settings = [];

    /** @var array  */
    private $conflict_rule_ids = [];

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $decompressedWeekDayTimeData = [];
        for ($day = 0; $day < 7; $day++) {
            for ($hour = 0; $hour < 24; $hour ++) {
                $decompressedWeekDayTimeData[$day][$hour] = 1;
            }
        }

        $this->week_day_time_data = $decompressedWeekDayTimeData;
    }

    public function rules()
    {
        return [
            [['source_partner_id', 'items_count', 'feed_sort_id', 'page_section_name', 'page_name', 'page_zone_name', 'week_day_time_data', 'feed_settings', 'inbound_keyword'], 'required'],
            [['rule_group_id', 'source_partner_id', 'items_count', 'feed_sort_id'], 'integer'],
            [['page_section_name', 'page_name', 'page_zone_name', 'inbound_keyword', 'keyword_replacement'], 'string'],
            [['week_day_time_data', 'feed_settings'], 'safe'],
        ];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->isConflictRule()) {
            $message = 'This is rule have conflict with another rule(s), id(s): ' . implode(', ', $this->conflict_rule_ids);
            $this->addError('rule_group_id', $message);
            return false;
        }

        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();
        try {
            $isNew = false;
            if ($this->rule_group_id) {
                $pageZoneRuleGroup = PageZoneRuleGroup::findOne($this->rule_group_id);
            }

            if (empty($pageZoneRuleGroup) ){
                $pageZoneRuleGroup = new PageZoneRuleGroup();
                $isNew = true;
            }
            $pageZoneRuleGroup->page_section_name = $this->page_section_name;
            $pageZoneRuleGroup->page_name = $this->page_name;
            $pageZoneRuleGroup->page_zone_name = $this->page_zone_name;
            $pageZoneRuleGroup->source_partner_id = $this->source_partner_id;
            $pageZoneRuleGroup->inbound_keyword = $this->inbound_keyword;
            $pageZoneRuleGroup->feed_sort_id = $this->feed_sort_id;
            $pageZoneRuleGroup->items_count = $this->items_count;
            $pageZoneRuleGroup->keyword_replacement = $this->keyword_replacement;

            if (!$pageZoneRuleGroup->save()) {
                throw new \Exception('Can not save $pageZoneRuleGroup');
            }

            $pageZoneRuleGroupId = $pageZoneRuleGroup->getPrimaryKey();

            if (!$isNew) {
                PageZoneRuleWeekdayTime::deleteAll(['page_zone_rule_group_id' => $pageZoneRuleGroupId]);
                PageZoneFeedCustomSettings::deleteAll(['page_zone_rule_group_id' => $pageZoneRuleGroupId]);
            }

            $activeHoursWeekDay = self::getActiveHoursWeekDay($this->week_day_time_data);
            foreach ($activeHoursWeekDay as $day => $hours) {
                foreach ($hours as $hour) {
                    $pageZoneRuleWeekdayTime = new PageZoneRuleWeekdayTime();
                    $pageZoneRuleWeekdayTime->page_zone_rule_group_id = $pageZoneRuleGroupId;
                    $pageZoneRuleWeekdayTime->week_day = $day;
                    $pageZoneRuleWeekdayTime->hour = $hour;

                    if (!$pageZoneRuleWeekdayTime->save()) {
                        throw new \Exception('Can not save $pageZoneRuleWeekdayTime');
                    }
                }
            }

            foreach ($this->feed_settings as $num => $feedSettingsItem) {
                $feedId = $feedSettingsItem['feed_id'];
                $orderNum = $num;
                $itemsCount = isset($feedSettingsItem['items_count']) ? $feedSettingsItem['items_count'] : null;
                $keywordReplacement = isset($feedSettingsItem['keyword_replacement']) ? $feedSettingsItem['keyword_replacement'] : null;

                $pageZoneFeedCustomSettings = new PageZoneFeedCustomSettings();
                $pageZoneFeedCustomSettings->page_zone_rule_group_id = $pageZoneRuleGroupId;
                $pageZoneFeedCustomSettings->order_number = $orderNum;
                $pageZoneFeedCustomSettings->feed_partner_id = $feedId;
                if (!is_null($itemsCount)) {
                    $pageZoneFeedCustomSettings->items_count = $itemsCount;
                }
                if (!is_null($keywordReplacement) && $keywordReplacement != 'null') {
                    $pageZoneFeedCustomSettings->keyword_replacement = $keywordReplacement;
                }

                if (!$pageZoneFeedCustomSettings->save()) {
                    throw new \Exception('Can not save $pageZoneFeedCustomSettings');
                }
            }

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @param [][] $weekDayTimeData
     * @return array
     */
    private static function getActiveHoursWeekDay($weekDayTimeData)
    {
        return PageZoneRuleWeekdayTime::getActiveHoursWeekDay($weekDayTimeData);
    }

    /**
     * @return bool
     */
    private function isConflictRule()
    {
        $this->conflict_rule_ids = [];

        $weekDayHoursConflictCondition = [];
        $weekDayHoursConflictCondition[] = 'OR';
        $activeHoursWeekDay = self::getActiveHoursWeekDay($this->week_day_time_data);
        foreach ($activeHoursWeekDay as $day => $hours) {
            $weekDayHoursConflictCondition[] = [
                'AND',
                ['=', 'pzrwt.week_day', $day],
                ['IN', 'pzrwt.hour', $hours]
            ];
        }

        $query = (new \yii\db\Query())
            ->select(['pzrg.id'])
            ->from(['pzrg' => PageZoneRuleGroup::tableName()])
            ->leftJoin(['pzrwt' => PageZoneRuleWeekdayTime::tableName()], 'pzrwt.page_zone_rule_group_id = pzrg.id')
            ->where(['=', 'pzrg.page_section_name', $this->page_section_name])
            ->andWhere(['=', 'pzrg.page_name', $this->page_name])
            ->andWhere(['=', 'pzrg.page_zone_name', $this->page_zone_name])
            ->andWhere(['=', 'pzrg.source_partner_id', $this->source_partner_id])
            ->andWhere(['=', 'pzrg.inbound_keyword', $this->inbound_keyword])
            ->andWhere($weekDayHoursConflictCondition)
            ->limit(20)
            ->groupBy('pzrg.id');

        if (!empty($this->rule_group_id)) {
            $query->andWhere(['<>', 'pzrg.id', $this->rule_group_id]);
        }

        $results = $query->all();
        if (count($results)) {
            foreach ($results as $resultRow) {
                if (!empty($resultRow['id'])) {
                    $this->conflict_rule_ids[] = $resultRow['id'];
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $ruleGroupId
     * @return PageZoneSettingsForm
     * @throws \Exception
     */
    public static function findByRuleGroupId($ruleGroupId)
    {
        $pageZoneRuleGroup = PageZoneRuleGroup::findOne($ruleGroupId);
        if (!$pageZoneRuleGroup) {
            throw new \Exception('Could not find group with this id: ' . $ruleGroupId);
        }

        $formModel = new self();
        $formModel->rule_group_id = $ruleGroupId;
        $formModel->page_section_name = $pageZoneRuleGroup->page_section_name;
        $formModel->page_name = $pageZoneRuleGroup->page_name;
        $formModel->page_zone_name = $pageZoneRuleGroup->page_zone_name;
        $formModel->source_partner_id = $pageZoneRuleGroup->source_partner_id;
        $formModel->inbound_keyword = $pageZoneRuleGroup->inbound_keyword;
        $formModel->items_count = $pageZoneRuleGroup->items_count;
        $formModel->keyword_replacement = $pageZoneRuleGroup->keyword_replacement;
        $formModel->feed_sort_id = $pageZoneRuleGroup->feed_sort_id;

        $pageZone = PageZone::getByPageSectionNameAndPageNameAndPageZoneName($formModel->page_section_name, $formModel->page_name, $formModel->page_zone_name);
        $formModel->is_feed_settings_editable = $pageZone->is_feed_settings_editable;


        $decompressedWeekDayTimeData = [];
        for ($day = 0; $day < 7; $day++) {
            for ($hour = 0; $hour < 24; $hour ++) {
                $decompressedWeekDayTimeData[$day][$hour] = 0;
            }
        }
        $formModel->week_day_time_data = $decompressedWeekDayTimeData;
        $pageZoneRuleWeekdayTimeRecords = PageZoneRuleWeekdayTime::findAll(['page_zone_rule_group_id' => $ruleGroupId]);
        foreach ($pageZoneRuleWeekdayTimeRecords as $pageZoneRuleWeekdayTimeRecord) {
            $formModel->week_day_time_data[$pageZoneRuleWeekdayTimeRecord->week_day][$pageZoneRuleWeekdayTimeRecord->hour] = 1;
        }

        $feedSettings = [];
        $pageZoneFeedCustomSettingsRecords = PageZoneFeedCustomSettings::findAll(['page_zone_rule_group_id' => $ruleGroupId]);
        foreach ($pageZoneFeedCustomSettingsRecords as $pageZoneFeedCustomSettingsRecord) {
            $feed = Feed::findOne($pageZoneFeedCustomSettingsRecord->feed_partner_id);

            $feedSettingsItem = [
                'feed_id' => $feed->id,
                'feed_name' => $feed->name,
                'items_count' => $pageZoneFeedCustomSettingsRecord->items_count,
                'keyword_replacement' => $pageZoneFeedCustomSettingsRecord->keyword_replacement
            ];

            $feedSettings[] = $feedSettingsItem;
        }
        $formModel->feed_settings = $feedSettings;

        return $formModel;
    }
}