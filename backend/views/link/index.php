<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Links';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Link', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            [
                'label' => 'Source Partner',
                'value' => function (\common\models\Link $model) {
                    return $model->source->name;
                }
            ],
            'campaign',
            'adgroup',
            'category',
            'keyword',
            'key',
            'url:url',
            [
                'label' => 'Final url',
                'value' => function (\common\models\Link $model) {
                    return $model->getFinalUrl();
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
