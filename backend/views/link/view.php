<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Link */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Links', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="link-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'User name',
                'value' => $model->user->username,
            ],
            [
                'label' => 'Partner name',
                'value' => $model->source->name,
            ],
            'campaign',
            'adgroup',
            'category',
            'keyword',
            'key',
            'url',
            [
                'label' => 'Created',
                'value' => function (\common\models\Link $model) {
                    return $model->created_at;
                },
            ],
            [
                'label' => 'Updated',
                'value' => function (\common\models\Link $model) {
                    return $model->updated_at;
                },
            ],
            [
                'label' => 'Final Link',
                'value' => $model->getFinalUrl(),
            ]
        ],
    ]) ?>

</div>
