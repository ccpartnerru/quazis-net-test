<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Link */
/* @var $form yii\widgets\ActiveForm */
/* @var $sources common\models\Source[] */
?>

<div class="link-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php
        $sourcesList = [];
        foreach ($sources as $source) {
            $sourcesList[$source->id] = $source->name;
        }
    ?>
    <?= $form->field($model, 'source_partner_id')->dropDownList($sourcesList, ['prompt' => 'Select source partner']) ?>

    <?= $form->field($model, 'campaign')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adgroup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create ' : 'Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
