<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var \common\models\PageSection[] $pageSections */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="device-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sections</th>
                            <th>Pages</th>
                            <th>Page zones</th>
                            <th>Page feeds default</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($pageSections as $pageSection) { ?>
                            <?php foreach ($pageSection->pages as $pageNum => $page) { ?>
                                <?php foreach ($page->zones as $zoneNum => $zone) { ?>
                                    <tr>
                                        <td>
                                            <?php if ($zoneNum == 0) { ?>
                                                <div>Name: <?= $pageSection->name ?></div>
                                                <div>Description: <?= $pageSection->description ?></div>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if ($zoneNum == 0) { ?>
                                                <div>Name: <?= $page->name ?></div>
                                                <div>Description: <?= $page->description ?></div>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <div>Name: <?= $zone->name ?></div>
                                            <div>Description: <?= $zone->description ?></div>
                                            <div>Default items number: <?= $zone->items_count ?></div>
                                            <div>Editable: <?= $zone->is_feed_settings_editable ? 'yes' : 'no' ?></div>
                                            <div>feed sort: <?= $zone->page_zone_feed_sort ? $zone->page_zone_feed_sort->name : \common\models\PageZoneFeedSort::getDefaultSort()->name ?></div>
                                        </td>
                                        <td>
                                            <?php
                                            $keys = array_keys($zone->page_zone_feed_settings);
                                            $lastKey = end($keys);
                                            ?>
                                            <?php foreach ($zone->page_zone_feed_settings as $feedSettingsNum => $feedSettings) { ?>
                                                <div>Feed: <?= $feedSettings->feed->name ?></div>
                                                <div>Items count: <?= $feedSettings->items_count ?></div>
                                                <?php if ($feedSettingsNum != $lastKey) { ?><br><?php } ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
