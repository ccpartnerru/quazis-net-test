<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Partners',
                        'icon' => 'circle-thin',
                        'url' => ['#'],
                        'items' => [
                            [
                                'label' => 'All Partners',
                                'url' => ['/partner']
                            ],
                            [
                                'label' => 'Feed Partners',
                                'url' => ['/partner/feed']
                            ],
                            [
                                'label' => 'Source Partners',
                                'url' => ['/partner/source']
                            ],
                        ]
                    ],
                    ['label' => 'Markets', 'icon' => 'circle-thin', 'url' => ['/market']],
                    ['label' => 'Devices', 'icon' => 'circle-thin', 'url' => ['/device']],
                    ['label' => 'Pages', 'icon' => 'circle-thin', 'url' => ['/page']],
                    ['label' => 'Page settings', 'icon' => 'circle-thin', 'url' => ['/page-zone-settings']],
                    [
                        'label' => 'Links',
                        'icon' => 'link',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Simple link generator',
                                'url' => ['/link/create']
                            ],
                            [
                                'label' => 'Link list',
                                'url' => ['/link']
                            ]
                        ]
                    ],
                    ['label' => 'Dev tools', 'options' => ['class' => 'header']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    [
                        'label' => 'Gii',
                        'icon' => 'file-code-o',
                        'url' => ['/gii'],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
