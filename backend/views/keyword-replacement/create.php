<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KeywordReplacement */

$this->title = 'Create Keyword Replacement';
$this->params['breadcrumbs'][] = ['label' => 'Keyword Replacements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-replacement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
