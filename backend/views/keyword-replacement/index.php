<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\KeywordReplacementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Keyword Replacements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keyword-replacement-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Keyword Replacement', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    $sourceFilterList = [];
    $models = \common\models\Source::getAllSortByName();
    $sourceFilterList = [];
    $sourceFilterList[\common\models\KeywordReplacement::SOURCE_PARTNER_ID_ALL] = 'All sources only';
    foreach ($models as $model) {
        $sourceFilterList[$model->id] = $model->name;
    }

    ?>

    <?php

    $feedFilterList = [];
    $models = \common\models\Feed::getAllSortByName();
    $feedFilterList = [];
    $feedFilterList[\common\models\KeywordReplacement::FEED_PARTNER_ID_ALL] = 'All feeds only';
    foreach ($models as $model) {
        $feedFilterList[$model->id] = $model->name;
    }

    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'source_partner_id',
                'label' => 'Source partner',
                'value' => function (\common\models\KeywordReplacement $model) {
                    return $model->source_partner_id == $model::SOURCE_PARTNER_ID_ALL ? 'All sources' : $model->source->name;
                },
                'filter' => $sourceFilterList
            ],
            [
                'attribute' => 'feed_partner_id',
                'label' => 'Feed partner',
                'value' => function (\common\models\KeywordReplacement $model) {
                    return $model->feed_partner_id == $model::FEED_PARTNER_ID_ALL ? 'All feeds' : $model->feed->name;
                },
                'filter' => $feedFilterList,
            ],
            'keyword',
            'replacement'
        ],
    ]); ?>
</div>
