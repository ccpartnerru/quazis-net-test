<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KeywordReplacement */

$this->title = 'Update Keyword Replacement: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Keyword Replacements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="keyword-replacement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
