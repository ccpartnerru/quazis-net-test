<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\KeywordReplacement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keyword-replacement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

    $sources = \common\models\Source::getAll();

    $sourceList = [];
    $sourceList[\common\models\KeywordReplacement::SOURCE_PARTNER_ID_ALL] = 'All sources';
    foreach ($sources as $source) {
        $sourceList[$source->id] = $source->name;
    }

    ?>
    <?= $form->field($model, 'source_partner_id')->dropDownList($sourceList, ['prompt' => 'Select source partner']) ?>

    <?php

    $feeds = \common\models\Feed::getAll();

    $feedList = [];
    $feedList[\common\models\KeywordReplacement::FEED_PARTNER_ID_ALL] = 'All feeds';
    foreach ($feeds as $feed) {
        $feedList[$feed->id] = $feed->name;
    }

    ?>
    <?= $form->field($model, 'feed_partner_id')->dropDownList($feedList, ['prompt' => 'Select feed partner']) ?>

    <?= $form->field($model, 'keyword')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'replacement')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
