<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PageZoneSettings */
/** @var \common\models\PageSection[] $pageSections */
/** @var \common\models\Source[] $sources */

$this->title = 'Create Page Zone Settings';
$this->params['breadcrumbs'][] = ['label' => 'Page Zone Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-zone-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pageSections' => $pageSections,
        'sources' => $sources
    ]) ?>

</div>
