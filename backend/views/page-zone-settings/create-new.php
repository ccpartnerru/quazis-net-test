<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/** @var \backend\models\PageZoneSettingsForm $model */
/** @var \common\models\PageSection[] $pageSections */
/** @var int $itemsCountLimit */
/** @var \common\models\PageZoneFeedSort[] $pageZoneFeedSorts */
/** @var \common\models\Feed[] $feeds */
/** @var \common\models\Source[] $sources */

$this->title = 'Create Page Zone Settings';

?>
<div class="page-zone-settings-create">
    <?= $this->render('_form', [
        'model' => $model,
        'pageSections' => $pageSections,
        'pages' => [],
        'pageZones' => [],
        'itemsCountLimit' => $itemsCountLimit,
        'pageZoneFeedSorts' => $pageZoneFeedSorts,
        'feeds' => $feeds,
        'sources' => $sources
    ]) ?>
</div>
