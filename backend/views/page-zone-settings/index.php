<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this \frontend\components\View */
/** @var \backend\models\PageZoneSettingsSearchIndexForm $searchFormModel */
/** @var \common\models\PageSection[] $pageSections */
/** @var \common\models\Page[] $pages */
/** @var \common\models\PageZone[] $pageZones */
/** @var \common\models\Source[] $sources */

$this->title = 'Page Zone Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-zone-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Page settings rule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_search', [
        'formModel' => $searchFormModel,
        'pageSections' => $pageSections,
        'pages' => $pages,
        'pageZones' => $pageZones,
        'sources' => $sources
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'page_section_name',
            'page_name',
            'page_zone_name',
            [
                'label' => 'Source Partner',
                'value' => function (\common\models\PageZoneRuleGroup $model) {
                    if ($model->source) {
                        return $model->source->name;
                    }

                    return 'All partners';
                }
            ],
            'inbound_keyword',
            /*'campaign',
            'adgroup',
            'category',
            'keyword',
            'key',
            'url:url',
            [
                'label' => 'Final url',
                'value' => function (\common\models\Link $model) {
                    return $model->getFinalUrl();
                }
            ],*/

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
