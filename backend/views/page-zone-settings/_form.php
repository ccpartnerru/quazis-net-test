<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PageZoneSettingsForm */
/* @var $form yii\widgets\ActiveForm */
/** @var \common\models\PageSection[] $pageSections */
/** @var \common\models\Page[] $pages */
/** @var \common\models\PageZone[] $pageZones */
/** @var \common\models\Source[] $sources */
/** @var \common\models\Source[] $sources */
/** @var int $itemsCountLimit */
/** @var \common\models\PageZoneFeedSort[] $pageZoneFeedSorts */

?>
<div class="page-zone-settings-form">
    <form id="w0" action="#" method="post">
        <input type="hidden" id="page_zone_rule_group_id" name="page_zone_rule_group_id" value="<?= isset($model->rule_group_id) ? $model->rule_group_id : '' ?>">
        <input type="hidden" id="is_feed_settings_editable" name="is_feed_settings_editable" value="<?= $model->is_feed_settings_editable ? 1 : 0 ?>">

        <div id="choose_page_zone" class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Choose page zone</h3>
            </div>
            <div class="box-body">
                <div class="form-group field-pagezonesettings-page_section_name required">
                    <label class="control-label" for="pagezonesettings-page_section_name">Page Section Name</label>
                    <?php

                    ?>
                    <select id="pagezonesettings-page_section_name" class="form-control" name="PageZoneSettings[page_section_name]" aria-required="true" aria-invalid="false" <?= !empty($model->rule_group_id) ? 'disabled' : '' ?>>
                        <option value="">-- Please select page section --</option>
                        <?php if (count($pageSections)) { ?>
                            <?php foreach ($pageSections as $pageSection) { ?>
                                <option value="<?= $pageSection->name ?>" <?= $model->page_section_name == $pageSection->name ? 'selected' : '' ?>><?= $pageSection->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pagezonesettings-page_name required">
                    <label class="control-label" for="pagezonesettings-page_name">Page Name</label>
                    <select id="pagezonesettings-page_name" class="form-control" name="PageZoneSettings[page_name]" aria-required="true" aria-invalid="false" <?= !empty($model->rule_group_id) ? 'disabled' : '' ?>>
                        <?php if (count($pages)) { ?>
                            <?php foreach ($pages as $page) { ?>
                                <option value="<?= $page->name ?>" <?= $model->page_name == $page->name ? 'selected' : '' ?>><?= $page->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pagezonesettings-page_zone_name required">
                    <label class="control-label" for="pagezonesettings-page_zone_name">Page Zone Name</label>
                    <select id="pagezonesettings-page_zone_name" class="form-control" name="PageZoneSettings[page_zone_name]" aria-required="true" aria-invalid="false" <?= !empty($model->rule_group_id) ? 'disabled' : '' ?>>
                        <?php if (count($pageZones)) { ?>
                            <?php foreach ($pageZones as $pageZone) { ?>
                                <option value="<?= $pageZone->name ?>" <?= $model->page_zone_name == $pageZone->name ? 'selected' : '' ?>><?= $pageZone->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
            </div>
        </div>

        <div id="rule_filters" class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Filters</h3>
            </div>
            <div class="box-body">
                <div class="form-group field-pagezonesettings-source_partner_id">
                    <label class="control-label" for="pagezonesettings-source_partner_id">Source Partner ID</label>
                    <select id="pagezonesettings-source_partner_id" class="form-control" name="PageZoneSettings[source_partner_id]">
                        <option value="">-- Select source partner --</option>
                        <?php
                        $sourcesList = [];
                        $sourcesList[0] = 'All sources';
                        $sourcesList = $sourcesList + \yii\helpers\ArrayHelper::map($sources, 'id', 'name');
                        ?>
                        <?php if (count($sourcesList)) { ?>
                            <?php foreach ($sourcesList as $id => $sourceName) { ?>
                                <option value="<?= $id ?>" <?= $id == $model->source_partner_id ? 'selected' : '' ?>><?= $sourceName ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
                <div class="form-group field-pagezonesettings-inbound_keyword required">
                    <label class="control-label" for="pagezonesettings-inbound_keyword">Inbound keyword</label>
                    <input type="text" id="pagezonesettings-inbound_keyword" class="form-control" name="PageZoneSettings[inbound_keyword]" aria-required="true" value="<?= $model->inbound_keyword ?>">

                    <div class="help-block">Leave the field blank, if you want that this rule to apply to all keywords</div>
                </div>
                <div class="form-group field-pagezonesettings-items_number required" style="overflow: auto;">
                    <label class="control-label" for="pagezonesettings-items_number">Week Day Time</label>

                    <table class="J_timedSheet_table">
                        <thead></thead>
                        <tbody id="J_timedSheet">

                        </tbody>
                    </table>

                    <div class="help-block"></div>
                </div>
            </div>
        </div>

        <div id="zone_settings" class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Zone settings</h3>
            </div>
            <div class="box-body">
                <div class="form-group field-pagezonesettings-items_count required">
                    <label class="control-label" for="pagezonesettings-items_count">Items Count</label>
                    <select id="pagezonesettings-items_count" class="form-control" name="PageZoneSettings[items_count]" aria-required="true" aria-invalid="false">
                        <?php for ($i = 1; $i <= $itemsCountLimit; $i++) { ?>
                            <option value="<?= $i ?>" <?= $model->items_count == $i ? 'selected' : '' ?>><?= $i ?></option>
                        <?php } ?>
                    </select>

                    <div class="help-block">Max count of items in this zone</div>
                </div>

                <div class="form-group field-pagezonesettings-keyword_replacement required">
                    <label class="control-label" for="pagezonesettings-keyword_replacement">Default Keyword Replacement</label>
                    <input type="text" id="pagezonesettings-keyword_replacement" class="form-control" name="PageZoneSettings[keyword_replacement]" aria-required="true" value="<?= $model->keyword_replacement ?>">

                    <div class="help-block">If the feed does not have a keyword replacement, keyword will be replaced with this word</div>
                </div>
            </div>
        </div>

        <div id="feed_zone_settings" class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Feed Zone Settings</h3>
            </div>
            <div class="box-body">
                <div class="form-group field-pagezonesettings-page_zone_feed_sort required">
                    <label class="control-label" for="pagezonesettings-page_zone_feed_sort">Feed zone settings sort type</label>
                    <select id="pagezonesettings-page_zone_feed_sort" class="form-control" name="PageZoneSettings[page_zone_feed_sort]" aria-required="true" aria-invalid="false">
                        <option value="">-- Please select sorting type --</option>
                        <?php if (count($pageZoneFeedSorts)) { ?>
                            <?php foreach ($pageZoneFeedSorts as $pageZoneFeedSort) { ?>
                                <option value="<?= $pageZoneFeedSort->id ?>" title="<?= $pageZoneFeedSort->description ?>" <?= $model->feed_sort_id == $pageZoneFeedSort->id ? 'selected' : ''?>><?= $pageZoneFeedSort->name ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                    <div class="help-block"></div>
                </div>
                <div>
                    <ul class="sortable feeds-list">
                    </ul>
                </div>
                <div>
                    <a class="btn btn-success add-feed-btn">Add feed</a>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success" id="save-page-zone-settings">Save</button>
        </div>
    </form>
</div>

<div class="modal fade in" id="feed-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Feed form</h4>
            </div>
            <div class="modal-body">
                <div class="form-group field-feed-modal-feed_id required">
                    <label class="control-label" for="feed-modal-feed_id">Select Feed</label>
                    <select id="feed-modal-feed_id" class="form-control" name="PageZoneSettings[feed_id]" aria-required="true" aria-invalid="false">
                        <?php
                        $feedsList = \yii\helpers\ArrayHelper::map($feeds, 'id', 'name');
                        ?>
                        <option value="">-- Please select feed --</option>
                        <?php foreach ($feedsList as $feedId => $feedsListItem) { ?>
                            <option value="<?= $feedId ?>"><?= $feedsListItem ?></option>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>

                <div class="form-group field-feed-modal-items_count required">
                    <label class="control-label" for="feed-modal-items_count">Items Count</label>
                    <select id="feed-modal-items_count" class="form-control" name="PageZoneSettings[items_count]" aria-required="true" aria-invalid="false">
                        <?php for ($i = 1; $i <= $itemsCountLimit; $i++) { ?>
                            <option value="<?= $i ?>"><?= $i ?></option>
                        <?php } ?>
                    </select>

                    <div class="help-block">Items number for feed request</div>
                </div>

                <div class="form-group field-feed-modal-kwr required">
                    <label class="control-label" for="feed-modal-kwr">Keyword Replacement</label>
                    <input type="text" id="feed-modal-kwr" class="form-control" name="PageZoneSettings[kwr]" aria-required="true">

                    <div class="help-block"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary save-changes">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //main logic
    //(function() {
    var BlockChoosePageZone = function () {
        this.container = document.querySelector('#choose_page_zone');
        this.pageSectionNameSelect = this.container.querySelector('#pagezonesettings-page_section_name');
        this.pageNameSelect = this.container.querySelector('#pagezonesettings-page_name');
        this.pageZoneNameSelect = this.container.querySelector('#pagezonesettings-page_zone_name');
    };

    BlockChoosePageZone.prototype = {
        init: function () {
            if (!this.pageSectionNameSelect.value) {
                clearSelect(this.pageNameSelect, '-- Please select page section first! --');
            }

            if (!this.pageNameSelect.value) {
                clearSelect(this.pageZoneNameSelect, '-- Please select page first! --');
            }

            this.addPageNameSelectListenerChange();
            this.addPageSectionNameSelectListenerChange();
        },

        addPageSectionNameSelectListenerChange: function() {
            var self = this;

            this.pageSectionNameSelect.addEventListener('change', function(){
                clearSelect(self.pageNameSelect, '-- Please select page section first! --');
                clearSelect(self.pageZoneNameSelect, '-- Please select page first! --');

                var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/page-list'], true) ?>';
                url += '?pageSectionName=' + self.pageSectionNameSelect.options[self.pageSectionNameSelect.selectedIndex].value;

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", url, true);
                xmlhttp.send();

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState !== 4) return;

                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.pageList && response.pageList.length > 0) {
                        var options = [];
                        response.pageList.forEach(function (item, key) {
                            var option = {
                                'label': item.name,
                                'value': item.name
                            };
                            options.push(option)
                        });

                        fillSelect(self.pageNameSelect, options, '-- Select page --');
                    }
                }
            });
        },

        addPageNameSelectListenerChange: function() {
            var self = this;

            this.pageNameSelect.addEventListener('change', function(){
                clearSelect(self.pageZoneNameSelect, '-- Please select page first! --');

                var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/page-zone-list'], true) ?>';
                url += '?pageSectionName=' + self.pageSectionNameSelect.options[self.pageSectionNameSelect.selectedIndex].value;
                url += '&pageName=' + self.pageNameSelect.options[self.pageNameSelect.selectedIndex].value;

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", url, true);
                xmlhttp.send();

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState !== 4) return;

                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.pageZoneList && response.pageZoneList.length > 0) {
                        var options = [];
                        response.pageZoneList.forEach(function (item, key) {
                            var option = {
                                'label': item.name,
                                'value': item.name
                            };
                            options.push(option)
                        });

                        fillSelect(self.pageZoneNameSelect, options, '-- Select page zone --');
                    }
                }
            });
        }
    };

    var BlockRuleFilters = function () {
        this.container = document.querySelector('#rule_filters');
        this.sourcePartnerSelect = this.container.querySelector('#pagezonesettings-source_partner_id');
        this.inboundKeywordInput = this.container.querySelector('#pagezonesettings-inbound_keyword');
        this.weekDayTimeTable = null;
    };

    BlockRuleFilters.prototype = {
        init: function () {
            this.createWeekDayTimeTable();
        },

        createWeekDayTimeTable: function () {
            var self = this;

            var dayList = [
                {name: "Monday"},
                {name: "Tuesday"},
                {name: "Wednesday"},
                {name: "Thursday"},
                {name: "Friday"},
                {name: "Saturday"},
                {name: "Sunday"}
            ];

            var hourList = [
                {name:"00",title:"00:00-01:00"},{name:"01",title:"01:00-02:00"},{name:"02",title:"02:00-03:00"},{name:"03",title:"03:00-04:00"},
                {name:"04",title:"04:00-05:00"},{name:"05",title:"05:00-06:00"},{name:"06",title:"06:00-07:00"},{name:"07",title:"07:00-08:00"},
                {name:"08",title:"08:00-09:00"},{name:"09",title:"09:00-10:00"},{name:"10",title:"10:00-11:00"},{name:"11",title:"11:00-12:00"},
                {name:"12",title:"12:00-13:00"},{name:"13",title:"13:00-14:00"},{name:"14",title:"14:00-15:00"},{name:"15",title:"15:00-16:00"},
                {name:"16",title:"16:00-17:00"},{name:"17",title:"17:00-18:00"},{name:"18",title:"18:00-19:00"},{name:"19",title:"19:00-20:00"},
                {name:"20",title:"20:00-21:00"},{name:"21",title:"21:00-22:00"},{name:"22",title:"22:00-23:00"},{name:"23",title:"23:00-24:00"}
            ];

            var dimensions = [dayList.length, hourList.length];

            var sheetData = JSON.parse('<?= json_encode($model->week_day_time_data) ?>');

            var updateRemark = function(sheet){

                var sheetStates = sheet.getSheetStates();
                var rowsCount = dimensions[0];
                var colsCount = dimensions[1];
                var rowRemark = [];
                var rowRemarkLen = 0;
                var remarkHTML = '';

                for(var row= 0, rowStates=[]; row<rowsCount; ++row){
                    rowRemark = [];
                    rowStates = sheetStates[row];
                    for(var col=0; col<colsCount; ++col){
                        if (rowStates[col]===0 && rowStates[col-1]===1) {
                            rowRemark[rowRemarkLen-1] += (col<=10?'0':'')+col+':00';
                        } else if(rowStates[col]===1 && (rowStates[col-1]===0 || rowStates[col-1]===undefined)) {
                            rowRemarkLen = rowRemark.push((col<=10?'0':'')+col+':00-');
                        }

                        if (rowStates[col]===1 && col===colsCount-1) {
                            rowRemark[rowRemarkLen-1] += '24:00';
                        }
                    }
                    remarkHTML = rowRemark.join("，");
                    sheet.setRemark(row,remarkHTML==='' ? sheet.getDefaultRemark() : remarkHTML);
                }
            };

            $(document).ready(function(){
                self.weekDayTimeTable = $("#J_timedSheet").TimeSheet({
                    data: {
                        dimensions : dimensions,
                        colHead : hourList,
                        rowHead : dayList,
                        sheetHead : {name:"Week Day\\Time"},
                        sheetData : sheetData
                    },
                    remarks : {
                        title : "Description",
                        default : "N/A"
                    },
                    end : function(ev,selectedArea){
                        updateRemark(self.weekDayTimeTable);
                    }
                });

                updateRemark(self.weekDayTimeTable);
            });
        }
    };

    var BlockZoneSettings = function () {
        this.container = document.querySelector('#zone_settings');
        this.itemsCountSelect = this.container.querySelector('#pagezonesettings-items_count');
        this.keywordReplacementInput = this.container.querySelector('#pagezonesettings-keyword_replacement');
    };

    var BlockFeedModal = function () {
        this.container = document.querySelector('#feed-modal');
        this.feedIdSelect = this.container.querySelector('#feed-modal-feed_id');
        this.itemsCountSelect = this.container.querySelector('#feed-modal-items_count');
        this.kwrInput = this.container.querySelector('#feed-modal-kwr');
        this.saveChangesBtn = this.container.querySelector('.save-changes');
        this.feedItem = null; //for edit
    };

    BlockFeedModal.prototype = {
        init: function () {
            this.reload();
        },

        validate: function () {
            if (this.feedIdSelect.value === '') {
                alert('Select feed, please!');
                return false;
            }

            if (this.itemsCountSelect.value === '') {
                alert('Select itemd count, please!');
                return false;
            }

            return true;
        },

        reload: function () {
            var self = this;

            this.refresh();

            if (blockController.isFeedSettingsEditable.value == 1) {
                this.feedIdSelect.disabled = false;
            } else {
                this.feedIdSelect.disabled = true;
            }

            var oldChangeBtn = this.saveChangesBtn;
            var newChangeBtn = oldChangeBtn.cloneNode(true);
            oldChangeBtn.parentNode.replaceChild(newChangeBtn, oldChangeBtn);
            this.saveChangesBtn = newChangeBtn;

            this.saveChangesBtn.addEventListener('click', function () {
                if (self.validate()) {
                    var feedId = self.feedIdSelect.value;
                    var feedName = self.feedIdSelect.options[self.feedIdSelect.selectedIndex].text;
                    var kwr = self.kwrInput.value;
                    var itemsCount = self.itemsCountSelect.value;

                    if (!self.feedItem) { // add new feed item
                        blockController.blocks.feedZoneSettings.addFeedItemInList(feedId, feedName, kwr, itemsCount, -1);
                    } else { //edit feed item
                        var feedItemNum = Array.prototype.indexOf.call(self.feedItem.parentElement.children, self.feedItem);
                        self.feedItem.remove();
                        blockController.blocks.feedZoneSettings.addFeedItemInList(feedId, feedName, kwr, itemsCount, feedItemNum);
                    }
                    self.hide();
                }
            });
        },

        show: function () {
            $(this.container).modal('show');
        },

        refresh: function () {
            this.feedIdSelect.selectedIndex = 0;
            this.itemsCountSelect.selectedIndex = 0;
            this.kwrInput.value = '';
        },

        hide: function () {
            $(this.container).modal('hide');
        },

        addFeed: function () {
            this.feedItem = null;
            this.reload();
            this.show();
        }
    };

    BlockFeedModal.prototype.setData = function (feedId, kwr, itemsCount) {
        this.feedIdSelect.value = feedId;
        if (kwr !== '' && kwr !== null && typeof kwr !== 'undefined') {
            this.kwrInput.value = kwr;
        }

        if (itemsCount) {
            this.itemsCountSelect.value = itemsCount;
        }
    };

    BlockFeedModal.prototype.editFeed = function (feedtemElement, feedId, kwr, itemsCount) {
        this.feedItem = feedtemElement;
        this.reload();
        this.setData(feedId, kwr, itemsCount);
        this.show();
    };

    var BlockFeedZoneSettings = function () {
        this.container = document.querySelector('#feed_zone_settings');
        this.pageZoneFeedSortSelect = this.container.querySelector('#pagezonesettings-page_zone_feed_sort');
        this.feedsList = this.container.querySelector('.feeds-list');
        this.addFeedBtn = this.container.querySelector('.add-feed-btn');
        this.blocks = {
            feedModal: new BlockFeedModal()
        };
    };

    BlockFeedZoneSettings.prototype = {
        init: function () {
            var self = this;

            var feedSettings = JSON.parse('<?= json_encode($model->feed_settings) ?>');
            if (feedSettings.length) {
                feedSettings.forEach(function(item) {
                    self.addFeedItemInList(item.feed_id, item.feed_name, item.keyword_replacement, item.items_count, -1);
                });
            }

            //add help text in help-block
            this.pageZoneFeedSortSelect.addEventListener('change', function() {
                if (this.parentElement && this.parentElement.querySelector('.help-block') && this[this.selectedIndex].getAttribute('title')) {
                    this.parentElement.querySelector('.help-block').innerHTML = this[this.selectedIndex].getAttribute('title');
                }
            });

            this.blocks.feedModal.init();

            this.addFeedBtn.addEventListener('click', function() {
                self.blocks.feedModal.addFeed();
            });

            this.reload();
        },

        reload: function () {
            this.sortableFeedsList();

            if (blockController.isFeedSettingsEditable.value == 1) {
                this.addFeedBtn.style.display = 'inline-block';
                this.pageZoneFeedSortSelect.disabled = false;
            } else {
                this.addFeedBtn.style.display = 'none';
                this.pageZoneFeedSortSelect.disabled = true;
            }
        },

        sortableFeedsList: function() {
            $(this.feedsList).sortable('destroy');

            if (blockController.isFeedSettingsEditable.value == 1) {
                $(this.feedsList).sortable({
                    forcePlaceholderSize: true
                });
            }
        }
    };

    BlockFeedZoneSettings.prototype.addFeedItemInList = function(id, feedName, keywordReplacement, itemsCount, listNum) {
        var self = this;

        var feedItem = document.createElement('li');
        feedItem.dataset.feed_id = id;
        if (keywordReplacement !== '' && keywordReplacement !== null) {
            feedItem.dataset.kwr = keywordReplacement;
        }
        if (itemsCount !== '') {
            feedItem.dataset.items_count = itemsCount;
        }

        var html = '';
        html += '<div class="box box-default">';
        html += '<div class="box-header with-border">';
        if (blockController.isFeedSettingsEditable.value == 1) {
            html += '<i class="fa fa-arrows"></i>';
        }
        html += '<h3 class="box-title">Feed: ' + feedName + '</h3>';
        html += '<div class="box-tools pull-right">';
        html += '<button class="btn btn-box-tool edit_feed_from_zone_settings" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></button>';
        if (blockController.isFeedSettingsEditable.value == 1) {
            html += '<button class="btn btn-box-tool remove_feed_from_zone_settings" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>';
        }
        html += '</div>';
        html += '</div>';
        html += '<div class="box-body">';
        if (keywordReplacement !== '' && keywordReplacement !== null) {
            html += '<div><b>KWR:</b> ' + keywordReplacement + '</div>';
        }
        if (itemsCount !== '') {
            html += '<div><b>Items cnt:</b> ' + itemsCount + '</div>';
        }
        html += '</div>';
        html += '</div>';

        feedItem.innerHTML = html;

        feedItem.querySelector('.edit_feed_from_zone_settings').addEventListener('click', function () {
            event.preventDefault();

            var feedId = feedItem.dataset['feed_id'];
            var kwr = feedItem.dataset['kwr'];
            var itemsCount = feedItem.dataset['items_count'];

            self.blocks.feedModal.editFeed(feedItem, feedId, kwr, itemsCount);
        });

        if (blockController.isFeedSettingsEditable.value == 1) {
            feedItem.querySelector('.remove_feed_from_zone_settings').addEventListener('click', function (event) {
                event.preventDefault();

                if (confirm('Do you really want to delete this feed settings?')) {
                    feedItem.remove();
                }
            });
        }

        if (listNum === -1) { //insert in the end
            this.feedsList.appendChild(feedItem);
        } else {
            this.feedsList.insertBefore(feedItem, this.feedsList.children[listNum]);
        }
        this.sortableFeedsList();
    };

    var BlockController = function () {
        this.blocks = {
            choosePageZone:   new BlockChoosePageZone(),
            ruleFilters:      new BlockRuleFilters(),
            zoneSettings:     new BlockZoneSettings(),
            feedZoneSettings: new BlockFeedZoneSettings()
        };

        this.saveBtn = document.querySelector('#save-page-zone-settings');
        this.ruleGroupIdInput = document.querySelector('#page_zone_rule_group_id');
        this.isFeedSettingsEditable = document.querySelector('#is_feed_settings_editable');
    };

    BlockController.prototype = {
        init: function () {
            var self = this;

            this.blocksMapping();
            this.initBlocks();

            this.blocks.choosePageZone.pageSectionNameSelect.addEventListener('change', function () {
                self.blocksMapping();
            });

            this.blocks.choosePageZone.pageNameSelect.addEventListener('change', function () {
                self.blocksMapping();
            });

            this.blocks.choosePageZone.pageZoneNameSelect.addEventListener('change', function () {
                self.blocksMapping();
            });

            this.saveBtn.addEventListener('click', function (event) {
                //stop for send or link redirect
                event.preventDefault();

                if (self.validateForm()) {
                    //collect data from form and send request for create records
                    var rule_group_id = self.ruleGroupIdInput.value;
                    var page_section_name = self.blocks.choosePageZone.pageSectionNameSelect.value;
                    var page_name = self.blocks.choosePageZone.pageNameSelect.value;
                    var page_zone_name = self.blocks.choosePageZone.pageZoneNameSelect.value;
                    var source_partner_id = self.blocks.ruleFilters.sourcePartnerSelect.value;
                    var inbound_keyword = self.blocks.ruleFilters.inboundKeywordInput.value;
                    var weekDayTimeData = self.blocks.ruleFilters.weekDayTimeTable.getSheetStates();
                    var items_count = self.blocks.zoneSettings.itemsCountSelect.value;
                    var keyword_replacement = self.blocks.zoneSettings.keywordReplacementInput.value;
                    var feed_sort_id = self.blocks.feedZoneSettings.pageZoneFeedSortSelect.value;

                    var feedListItems = self.blocks.feedZoneSettings.feedsList.children;
                    var feeds_settings = [];
                    if (feedListItems && feedListItems.length) {
                        Array.from(feedListItems).forEach(function(item) {
                            feeds_settings.push({
                                'feed_id': item.dataset['feed_id'],
                                'keyword_replacement': item.dataset['kwr'],
                                'items_count': item.dataset['items_count']
                            });
                        });
                    }

                    var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/update', 'id' => $model->rule_group_id ? $model->rule_group_id : 0], true) ?>';
                    var data = {
                        rule_group_id: rule_group_id,
                        page_section_name: page_section_name,
                        page_name: page_name,
                        page_zone_name: page_zone_name,
                        source_partner_id: source_partner_id,
                        inbound_keyword: inbound_keyword,
                        week_day_time_data: weekDayTimeData,
                        items_count: items_count,
                        keyword_replacement: keyword_replacement,
                        feed_sort_id: feed_sort_id,
                        feed_settings: feeds_settings
                    };


                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (data) {
                            var status = data.status;
                            if (typeof status !== "undefined") {
                                if (status === 'success') {
                                    window.location = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/index'], true) ?>';
                                } else {
                                    alert("Error: \n" + data.message);
                                }
                            } else {
                                alert('some error! try again later!');
                            }
                        },
                        error: function () {
                            alert('some error! try again later!');
                        }
                    });
                }
            });
        },

        validateForm: function () {
            return true;
        },

        initBlocks: function () {
            for (var blockName in this.blocks) {
                if (this.blocks.hasOwnProperty(blockName)) {
                    var block = this.blocks[blockName];

                    if (typeof block.init === 'function') {
                        block.init();
                    }
                }
            }
        },

        showBlock: function(block) {
            block.container.style.display = 'block';
        },

        hideBlock: function(block) {
            block.container.style.display = 'none';
        },

        hideAllBlocks: function() {
            for (var blockName in this.blocks) {
                if (this.blocks.hasOwnProperty(blockName)) {
                    this.hideBlock(this.blocks[blockName]);
                }
            }
        },

        showAllBlocks: function () {
            for (var blockName in this.blocks) {
                if (this.blocks.hasOwnProperty(blockName)) {
                    this.showBlock(this.blocks[blockName]);
                }
            }
        },

        blocksMapping: function() {
            this.hideAllBlocks();
            this.saveBtn.style.display = 'none';
            //show first block - choose page zone
            this.showBlock(this.blocks.choosePageZone);

            //show other blocks, when first block selected
            if (this.blocks.choosePageZone.pageZoneNameSelect.value) {
                if (this.ruleGroupIdInput.value === '' || this.ruleGroupIdInput.value == 0) {
                    var defaultZoneConfig = this.getDefaultZoneConfig();
                    this.setZoneConfig(defaultZoneConfig);
                }

                this.showBlock(this.blocks.ruleFilters);
                this.showBlock(this.blocks.zoneSettings);
                this.showBlock(this.blocks.feedZoneSettings);
                this.saveBtn.style.display = 'block';

                this.blocks.feedZoneSettings.reload();
            }
        },

        setZoneConfig: function(zoneConfig) {
            var self = this;

            this.resetZoneConfig();

            this.isFeedSettingsEditable.value = zoneConfig.is_feed_settings_editable ? 1 : 0;

            if (zoneConfig && zoneConfig.page_zone_feed_settings && zoneConfig.page_zone_feed_settings.length) {
                zoneConfig.page_zone_feed_settings.forEach(function(value, key) {
                    var feedId = value.feed_id;
                    var feedName = value.feed_name;
                    var kwr = '';
                    var itemsCount = value.items_count;

                    self.blocks.feedZoneSettings.addFeedItemInList(feedId, feedName, kwr, itemsCount, -1);
                });
            }

            this.blocks.feedZoneSettings.pageZoneFeedSortSelect.value = zoneConfig.page_zone_feed_sort_id;
            if (!zoneConfig.is_feed_settings_editable) {
                this.blocks.feedZoneSettings.pageZoneFeedSortSelect.disabled = true;
            } else {
                this.blocks.feedZoneSettings.pageZoneFeedSortSelect.disabled = false;
            }
            this.blocks.zoneSettings.itemsCountSelect.value = zoneConfig.items_count;
        },

        resetZoneConfig: function() {
            this.blocks.feedZoneSettings.feedsList.innerHTML = '';
            this.blocks.feedZoneSettings.pageZoneFeedSortSelect.value = '';
            this.blocks.zoneSettings.itemsCountSelect.value = '';
            this.isFeedSettingsEditable.value = 0;
        },

        getDefaultZoneConfig: function() {
            var defaultZoneConfig = null;

            var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/get-page-zone-settings-default-config'], true) ?>';

            var page_section_name = this.blocks.choosePageZone.pageSectionNameSelect.value;
            var page_name = this.blocks.choosePageZone.pageNameSelect.value;
            var page_zone_name = this.blocks.choosePageZone.pageZoneNameSelect.value;

            var data = {
                page_section_name: page_section_name,
                page_name: page_name,
                page_zone_name: page_zone_name
            };

            $.ajax({
                type: "GET",
                url: url,
                data: data,
                async: false,
                success: function (data) {
                    var status = data.status;
                    if (typeof status !== "undefined") {
                        if (status === 'success') {
                            defaultZoneConfig = data.data;
                        } else {
                            alert("Error: \n" + data.message);
                        }
                    } else {
                        alert('some error! try again later!');
                    }
                },
                error: function () {
                    alert('some error! try again later!');
                }
            });

            return defaultZoneConfig;
        }

    };

    var blockController = null;
    document.addEventListener('DOMContentLoaded', function(){
        blockController = new BlockController();
        blockController.init();
    });
    //})();
</script>