<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PageZoneSettings */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Page Zone Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-zone-settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'page_section_name',
            'page_name',
            'page_zone_name',
            'source_partner_id',
            'items_number',
        ],
    ]) ?>

</div>
