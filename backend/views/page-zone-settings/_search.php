<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PageZoneSettingsSearch */
/* @var $formModel \backend\models\PageZoneSettingsSearchIndexForm*/
/** @var \common\models\PageSection[] $pageSections */
/** @var \common\models\Page[] $pages */
/** @var \common\models\PageZone[] $pageZones */
/** @var \common\models\Source[] $sources */

?>

<div class="page-zone-settings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'filter-form'
    ]); ?>

    <?= $form->field($formModel, 'rule_group_id')->hint('If a value is specified in this field (rule_group_id), then the remaining filters will be ignored.') ?>

    <?php
    $pageSectionsList = \yii\helpers\ArrayHelper::map($pageSections, 'name', 'name');
    ?>
    <?= $form->field($formModel, 'page_section_name')->dropDownList($pageSectionsList, ['prompt' => '-- Select page section name --']) ?>

    <?php
    $pagesList = $pages ? \yii\helpers\ArrayHelper::map($pages, 'name', 'name') : [];
    ?>
    <?= $form->field($formModel, 'page_name')->dropDownList($pagesList, ['prompt' => '-- Select page --']) ?>

    <?php
    $pageZonesList = $pageZones ? \yii\helpers\ArrayHelper::map($pageZones, 'name', 'name') : [];
    ?>
    <?= $form->field($formModel, 'page_zone_name')->dropDownList($pageZonesList, ['prompt' => '-- Select page zone --']) ?>

    <?php
    $sourcesList = [];
    $sourcesList[0] = 'All sources';
    $sourcesList = $sourcesList + \yii\helpers\ArrayHelper::map($sources, 'id', 'name');
    ?>
    <?= $form->field($formModel, 'source_partner_id')->dropDownList($sourcesList, ['prompt' => '-- Select source partner --']) ?>

    <?= $form->field($formModel, 'inbound_keyword') ?>

    <div class="form-group field-pagezonesettingssearchindexform-weekdaytime required" style="overflow: auto;">
        <label class="control-label" for="pagezonesettingssearchindexform-weekdaytime">Week Day Time</label>

        <table class="J_timedSheet_table">
            <thead></thead>
            <tbody id="J_timedSheet">

            </tbody>
        </table>

        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Reset', '', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    //(function() {
    var BlockFilter = function () {
        this.container = document.querySelector('.page-zone-settings-search');
        this.pageSectionNameSelect = this.container.querySelector('#pagezonesettingssearchindexform-page_section_name');
        this.pageNameSelect = this.container.querySelector('#pagezonesettingssearchindexform-page_name');
        this.pageZoneNameSelect = this.container.querySelector('#pagezonesettingssearchindexform-page_zone_name');
        this.weekDayTimeTable = null;
    };

    BlockFilter.prototype = {
        init: function () {
            if (!this.pageSectionNameSelect.value) {
                clearSelect(this.pageNameSelect, '-- Select page section first! --');
            }

            if (!this.pageNameSelect.value) {
                clearSelect(this.pageZoneNameSelect, '-- Select page first! --');
            }

            this.addPageNameSelectListenerChange();
            this.addPageSectionNameSelectListenerChange();
            this.createWeekDayTimeTable();
        },

        addPageSectionNameSelectListenerChange: function() {
            var self = this;

            this.pageSectionNameSelect.addEventListener('change', function(){
                clearSelect(self.pageNameSelect, '-- Select page section first! --');
                clearSelect(self.pageZoneNameSelect, '-- Select page first! --');

                var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/page-list'], true) ?>';
                url += '?pageSectionName=' + self.pageSectionNameSelect.options[self.pageSectionNameSelect.selectedIndex].value;

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", url, true);
                xmlhttp.send();

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState !== 4) return;

                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.pageList && response.pageList.length > 0) {
                        var options = [];
                        response.pageList.forEach(function (item, key) {
                            var option = {
                                'label': item.name,
                                'value': item.name
                            };
                            options.push(option)
                        });

                        fillSelect(self.pageNameSelect, options, '-- Select page --');
                    }
                }
            });
        },

        addPageNameSelectListenerChange: function() {
            var self = this;

            this.pageNameSelect.addEventListener('change', function(){
                clearSelect(self.pageZoneNameSelect, '-- Select page first! --');

                var url = '<?= \yii\helpers\Url::toRoute(['page-zone-settings/page-zone-list'], true) ?>';
                url += '?pageSectionName=' + self.pageSectionNameSelect.options[self.pageSectionNameSelect.selectedIndex].value;
                url += '&pageName=' + self.pageNameSelect.options[self.pageNameSelect.selectedIndex].value;

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", url, true);
                xmlhttp.send();

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState !== 4) return;

                    var response = JSON.parse(xmlhttp.responseText);
                    if (response.pageZoneList && response.pageZoneList.length > 0) {
                        var options = [];
                        response.pageZoneList.forEach(function (item, key) {
                            var option = {
                                'label': item.name,
                                'value': item.name
                            };
                            options.push(option)
                        });

                        fillSelect(self.pageZoneNameSelect, options, '-- Select page zone --');
                    }
                }
            });
        },

        createWeekDayTimeTable: function () {
            var self = this;

            var dayList = [
                {name: "Monday"},
                {name: "Tuesday"},
                {name: "Wednesday"},
                {name: "Thursday"},
                {name: "Friday"},
                {name: "Saturday"},
                {name: "Sunday"}
            ];

            var hourList = [
                {name:"00",title:"00:00-01:00"},{name:"01",title:"01:00-02:00"},{name:"02",title:"02:00-03:00"},{name:"03",title:"03:00-04:00"},
                {name:"04",title:"04:00-05:00"},{name:"05",title:"05:00-06:00"},{name:"06",title:"06:00-07:00"},{name:"07",title:"07:00-08:00"},
                {name:"08",title:"08:00-09:00"},{name:"09",title:"09:00-10:00"},{name:"10",title:"10:00-11:00"},{name:"11",title:"11:00-12:00"},
                {name:"12",title:"12:00-13:00"},{name:"13",title:"13:00-14:00"},{name:"14",title:"14:00-15:00"},{name:"15",title:"15:00-16:00"},
                {name:"16",title:"16:00-17:00"},{name:"17",title:"17:00-18:00"},{name:"18",title:"18:00-19:00"},{name:"19",title:"19:00-20:00"},
                {name:"20",title:"20:00-21:00"},{name:"21",title:"21:00-22:00"},{name:"22",title:"22:00-23:00"},{name:"23",title:"23:00-24:00"}
            ];

            var dimensions = [dayList.length, hourList.length];

            var sheetData = JSON.parse('<?= json_encode($formModel->week_day_time_data) ?>');

            var updateRemark = function(sheet){

                var sheetStates = sheet.getSheetStates();
                var rowsCount = dimensions[0];
                var colsCount = dimensions[1];
                var rowRemark = [];
                var rowRemarkLen = 0;
                var remarkHTML = '';

                for(var row= 0, rowStates=[]; row<rowsCount; ++row){
                    rowRemark = [];
                    rowStates = sheetStates[row];
                    for(var col=0; col<colsCount; ++col){
                        if (rowStates[col]===0 && rowStates[col-1]===1) {
                            rowRemark[rowRemarkLen-1] += (col<=10?'0':'')+col+':00';
                        } else if(rowStates[col]===1 && (rowStates[col-1]===0 || rowStates[col-1]===undefined)) {
                            rowRemarkLen = rowRemark.push((col<=10?'0':'')+col+':00-');
                        }

                        if (rowStates[col]===1 && col===colsCount-1) {
                            rowRemark[rowRemarkLen-1] += '24:00';
                        }
                    }
                    remarkHTML = rowRemark.join("，");
                    sheet.setRemark(row,remarkHTML==='' ? sheet.getDefaultRemark() : remarkHTML);
                }
            };

            $(document).ready(function(){
                self.weekDayTimeTable = $("#J_timedSheet").TimeSheet({
                    data: {
                        dimensions : dimensions,
                        colHead : hourList,
                        rowHead : dayList,
                        sheetHead : {name:"Week Day\\Time"},
                        sheetData : sheetData
                    },
                    remarks : {
                        title : "Description",
                        default : "N/A"
                    },
                    end : function(ev,selectedArea){
                        updateRemark(self.weekDayTimeTable);
                    }
                });

                updateRemark(self.weekDayTimeTable);
            });
        }
    };

    var BlockController = function () {
        this.blocks = {
            filter:   new BlockFilter()
        };
    };

    BlockController.prototype = {
        init: function () {
            this.initBlocks();
        },

        initBlocks: function () {
            for (var blockName in this.blocks) {
                if (this.blocks.hasOwnProperty(blockName)) {
                    var block = this.blocks[blockName];

                    if (typeof block.init === 'function') {
                        block.init();
                    }
                }
            }
        }
    };

    var blockController = null;
    document.addEventListener('DOMContentLoaded', function(){
        blockController = new BlockController();
        blockController.init();

        $('#filter-form').submit(function(e) {
            $(this).find('.week_day_time_data_input').remove();

            var sheetStates = blockController.blocks.filter.weekDayTimeTable.getSheetStates();

            $('<input />').attr('type', 'hidden')
                .attr('name', "PageZoneSettingsSearchIndexForm[week_day_time_data]")
                .attr('class', 'week_day_time_data_input')
                .attr('value', JSON.stringify(sheetStates))
                .appendTo(this);

            return true;
        });
    });


    //})();
</script>
