<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'libraries/time-sheet-table/css/TimeSheet.css',
        'libraries/html5sortable/jquery.sortable.css'
    ];

    public $js = [
        'js/helpers.js',
        'libraries/time-sheet-table/js/TimeSheet.js',
        'libraries/html5sortable/jquery.sortable.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
