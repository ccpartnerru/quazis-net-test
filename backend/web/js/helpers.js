"use strict";

var clearSelect;
var fillSelect;
var addSelectOption;

(function() {
    clearSelect = function (select, prompt) {
        if (select && select.options) {
            select.options.length = 0;
        }

        if (prompt && typeof prompt !== 'undefined') {
            addSelectOption(select, prompt, '');
        }
    };

    fillSelect = function (select, optionsConfig, prompt)
    {
        clearSelect(select);

        if (prompt && typeof prompt !== 'undefined') {
            addSelectOption(select, prompt, '');
        }

        if (optionsConfig && optionsConfig.length > 0) {
            optionsConfig.forEach(function(item, key) {
                addSelectOption(select, item.label, item.value);
            });
        }
    };

    addSelectOption = function (select, label, value)
    {
        var opt = document.createElement('option');
        opt.value = value;
        opt.innerHTML = label;
        select.appendChild(opt);
    };
})();